﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;

namespace RewindAndroid
{
    public class Markerclick:Java.Lang.Object,GoogleMap.IOnInfoWindowClickListener
    {
        #region IOnInfoWindowClickListener implementation

        Activity activ;
        //  int type;

        public  Markerclick(Activity ac)
        {
            this.activ = ac;
            //  type = tip;
        }

        public void OnInfoWindowClick(Marker p0)
        {
            Console.WriteLine(p0.Title);
            //activ.StartActivity (typeof(point2point_activity));

            var point2pointActivityIntent = new Intent(activ, typeof(AppuntamentiAllActivity));

            LatLng posmark = p0.Position;
            string lat = posmark.Latitude.ToString();
            string lon = posmark.Longitude.ToString();
            string title = p0.Title;

            point2pointActivityIntent.SetFlags(ActivityFlags.ReorderToFront);
            point2pointActivityIntent.PutExtra("Lat", lat); 
            point2pointActivityIntent.PutExtra("Lon", lon);
            point2pointActivityIntent.PutExtra("Title", title);
            point2pointActivityIntent.PutExtra("case", false);

            activ.StartActivity(point2pointActivityIntent);  

        }

        #endregion



    }
}