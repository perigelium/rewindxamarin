﻿using System;
using System.Collections.Generic;

using Android.Widget;
using Android.Content;
using Android.Views;
using RewindShared;
using Android.Graphics;
using Android.App;

namespace RewindAndroid
{
    public class ClientiFilterDropDownListAdapter: BaseAdapter
    {
        List<string> listItems;

        Typeface OswaldRegualr;
        Context activity;

        public ClientiFilterDropDownListAdapter(List<string> listItems, Context activity)
        {
            this.listItems = listItems;

            OswaldRegualr = TypeFaces.getTypeface(activity, "Oswald-Regular.ttf");
        }

        #region implemented abstract members of BaseAdapter

        public override Java.Lang.Object GetItem(int position)
        {
            return listItems[position];
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override Android.Views.View GetView(int position, Android.Views.View convertView, Android.Views.ViewGroup parent)
        {
            var view = convertView;

            try
            {
                if (view == null)
                {
                    var inflater = parent.Context.GetSystemService(Context.LayoutInflaterService) as LayoutInflater;
                    view = inflater.Inflate(Resource.Layout.DropDownClientListItemLayout, null);
                }

                TextView txtLine = view.FindViewById<TextView>(Resource.Id.txtDropDownItemLine);

                txtLine.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);

                txtLine.Text = listItems[position];
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }

            return view;
        }

        public override int Count
        {
            get
            {
                return listItems.Count;
            }
        }

        #endregion
    }
}