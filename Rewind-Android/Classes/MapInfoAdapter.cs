﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Gms.Maps.Model;
using Android.Gms.Maps;
using Android.Webkit;
using Android.Graphics;


namespace RewindAndroid
{
    public class MapInfoAdapter: Java.Lang.Object, GoogleMap.IInfoWindowAdapter
    {
        Context contextf;
        int index;
        LayoutInflater inflater;
        RelativeLayout BgLayout;
        WebView webView;

        public MapInfoAdapter(Context context, int index, RelativeLayout bglay)
        {
            this.contextf = context;
            this.index = index;
            this.BgLayout = bglay;
        }

        #region IInfoWindowAdapter implementation

        public View GetInfoContents(Marker p0)
        {
            //ImageView img = new ImageView (context);

            return null;
        }

        public View GetInfoWindow(Marker p0)
        {
            View view = null;
            inflater = (LayoutInflater)contextf.GetSystemService(Context.LayoutInflaterService);
            view = inflater.Inflate(Resource.Layout.mapPin, BgLayout, false);

            TextView txt = view.FindViewById<TextView>(Resource.Id.textView1);
            txt.Text = p0.Snippet;

            return view;
        }

        #endregion
    }
}