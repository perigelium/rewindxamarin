﻿using System;
using Android.Widget;
using Android.Views;
using RewindShared;

namespace RewindAndroid
{
    public class ScrollTouchListner:Java.Lang.Object, View.IOnTouchListener
    {
        ScrollView scrl;
        public ScrollTouchListner(ScrollView _scrl)
        {
            scrl = _scrl;
        }
        #region IOnTouchListener implementation
        public bool OnTouch (View v, MotionEvent e)
        {
            try
            {
                MotionEventActions action=e.Action;

                if (action == MotionEventActions.Down)
                {
                    scrl.RequestDisallowInterceptTouchEvent(true);
                    return false;
                }
                if (action == MotionEventActions.Move)
                {
                    scrl.RequestDisallowInterceptTouchEvent(true);
                    return false;
                }
                if (action == MotionEventActions.Up)
                {
                    scrl.RequestDisallowInterceptTouchEvent(false);
                    return true;
                }
            }
            catch(Exception ex) {
                return false;
                Utils.writeToDeviceLog (ex);
            }
            return false;
        }
        #endregion
    }
}
