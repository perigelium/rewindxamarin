using System;
using System.Collections;

using Android.Graphics;
using Android.Content;
using RewindShared;

namespace RewindAndroid
{
    public class TypeFaces
    {


        private static string TAG = "Typefaces";
        private static Hashtable cache = new Hashtable();



        public static Typeface getTypeface(Context c, String assetPath)
        {
            try
            {
                lock (cache)
                {
                    if (!cache.ContainsKey(assetPath))
                    {
                        try
                        {
                            Typeface t = Typeface.CreateFromAsset(c.Assets,
                                "Fonts/" + assetPath);
                            cache.Add(assetPath, t);
                        }
                        catch (Exception e)
                        {
                            return null;
                        }
                    }
                    return (Typeface)cache[assetPath];
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
                return null;
            }
        }
    }
}

