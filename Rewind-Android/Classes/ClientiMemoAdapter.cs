﻿using System;
using Android.Widget;
using Android.App;
using System.Collections.Generic;
using Android.Views;
using Android.Graphics;
using RewindShared;

namespace RewindAndroid
{
    public class ClientiMemoAdapter: BaseAdapter<ClientiMemoItem>
    {
        List<ClientiMemoItem> items;
        Activity context;
        int selectedIndex;

        public ClientiMemoAdapter(Activity context, List<ClientiMemoItem> items,int selIndex)
            : base()
        {
            this.context = context;
            this.items = items;
            this.selectedIndex = selIndex;
        }

        #region implemented abstract members of BaseAdapter

        public override long GetItemId(int position)
        {
            return position;
        }

        public override ClientiMemoItem this [int position]
        {
            get { return items[position]; }
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View view = null;
            try
            {
                var item = items[position];

                view = convertView;
                if (view == null)
                    view = context.LayoutInflater.Inflate(Resource.Layout.ClientiMemoItemLayout, null);

                TextView txtregSociale = view.FindViewById<TextView>(Resource.Id.txtregSociale);
                LinearLayout layBackground=view.FindViewById<LinearLayout>(Resource.Id.layBackground);
                Typeface OswaldRegualr = TypeFaces.getTypeface(context, "Oswald-Regular.ttf");
                txtregSociale.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);

                layBackground.SetBackgroundColor(Android.Graphics.Color.Rgb(0, 95, 167));
                txtregSociale.Text = item.Title;

                if (selectedIndex != -1)
                {
                    if (selectedIndex == position)
                    {

                        layBackground.SetBackgroundColor(Android.Graphics.Color.Rgb(76, 76, 76));
                    }
                    else
                    {
                        //first time
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
            return view;
        }

        public override int Count
        {
            get { return items.Count; }
        }

        #endregion

	
    }
}

