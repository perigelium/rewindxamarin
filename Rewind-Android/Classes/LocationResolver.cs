﻿using System;
using Java.Util;
using Android.Locations;
using Android.OS;
using Android.Content;
using RewindShared;

namespace RewindAndroid
{
    public class LocationResolver
    {
        private static Timer timer;
        private static LocationManager locationManager;
        private static LocationResult locationResult;
        private static bool gpsEnabled = false;
        private static bool networkEnabled = false;
        //private static Handler locationTimeoutHandler;

        //private Callback locationTimeoutCallback = new Callback();

        public static LocationListenerGps locationListenerGps = new LocationListenerGps();
        public static LocationListenerNetwork locationListenerNetwork = new LocationListenerNetwork();

        public void prepare()
        {
            //locationTimeoutHandler = new Handler(locationTimeoutCallback);
        }

        public bool getLocation(Context context, LocationResult result, int maxMillisToWait)
        {
            object lockObj = new object();

            lock (lockObj)
            {
                locationResult = result;
                if (locationManager == null)
                    locationManager = (LocationManager)context.GetSystemService(Context.LocationService);
//                _locationManager = (LocationManager)GetSystemService(Context.LocationService);

                // exceptions will be thrown if provider is not permitted.
                try
                {
                    gpsEnabled = locationManager.IsProviderEnabled(LocationManager.GpsProvider);
                }
                catch (Exception ex)
                {
                    Utils.writeToDeviceLog(ex);
                }

                try
                {
                    networkEnabled = locationManager.IsProviderEnabled(LocationManager.NetworkProvider);
                }
                catch (Exception ex)
                {
                    Utils.writeToDeviceLog(ex);
                }

                // don't start listeners if no provider is enabled
                if (!gpsEnabled && !networkEnabled)
                    return false;

                if (gpsEnabled)
                    locationManager.RequestSingleUpdate(LocationManager.GpsProvider, locationListenerGps, Looper.MyLooper());
                if (networkEnabled)
                    locationManager.RequestSingleUpdate(LocationManager.NetworkProvider, locationListenerNetwork, Looper.MyLooper());

                timer = new Timer();
                timer.Schedule(new GetLastLocationTask(), maxMillisToWait);

                return true;
            }
        }

        public class Callback: Java.Lang.Object, Handler.ICallback
        {
            #region ICallback implementation

            public bool HandleMessage(Message msg)
            {
                locationTimeoutFunc();
                return true;
            }

            #endregion

            private void locationTimeoutFunc()
            { 
                locationManager.RemoveUpdates(locationListenerGps);
                locationManager.RemoveUpdates(locationListenerNetwork);

                GpsStatus status = locationManager.GetGpsStatus(null);

                Location networkLocation = null, gpsLocation = null;
                Console.WriteLine("aici e null ------------------");
                locationResult.gotLocation(null);           
            }
        }

        private class GetLastLocationTask: TimerTask
        {
            #region implemented abstract members of TimerTask

            public override void Run()
            {
                //locationTimeoutHandler.SendEmptyMessage(0);
            }

            #endregion
        }

        public class LocationListenerGps: Java.Lang.Object, ILocationListener
        {
            #region ILocationListener implementation

            public void OnLocationChanged(Location location)
            {
                timer.Cancel();

                GpsStatus status = locationManager.GetGpsStatus(null);              
                LocationInfo locInfo = new LocationInfo();
                locInfo.Location = location;
                locInfo.GpsStatus = status;
                locInfo.IsFromGps = location.Provider.ToLower().Equals("gps") ? true : false;

                Console.WriteLine("gpsProvider---"+locInfo.GpsStatus.ToString()+"---"+location.Latitude.ToString());

                locationResult.gotLocation(locInfo);

                locationManager.RemoveUpdates(this);
                //              locationManager.RemoveUpdates(locationListenerNetwork);
            }

            public void OnProviderDisabled(string provider)
            {
            }

            public void OnProviderEnabled(string provider)
            {
            }

            public void OnStatusChanged(string provider, Availability status, Android.OS.Bundle extras)
            {
            }

            #endregion
        }

        public class LocationListenerNetwork: Java.Lang.Object, ILocationListener
        {
            #region ILocationListener implementation

            public void OnLocationChanged(Location location)
            {
                timer.Cancel(); 

                GpsStatus status = locationManager.GetGpsStatus(null);
                LocationInfo locInfo = new LocationInfo();
                locInfo.Location = location;
                locInfo.GpsStatus = status;
                locInfo.IsFromGps = location.Provider.ToLower().Equals("gps") ? true : false;

                Console.WriteLine("network provider---"+locInfo.GpsStatus.ToString()+"---"+location.Latitude.ToString());

                locationResult.gotLocation(locInfo);

                locationManager.RemoveUpdates(this);
                locationManager.RemoveUpdates(locationListenerGps);  
               
            }

            public void OnProviderDisabled(string provider)
            {
            }

            public void OnProviderEnabled(string provider)
            {
            }

            public void OnStatusChanged(string provider, Availability status, Android.OS.Bundle extras)
            {
            }

            #endregion
        }

        public abstract class LocationResult
        {
            public abstract void gotLocation(LocationInfo location);
        }
    }
}

