﻿using System;
using Android.App;
using Android.OS;
using RewindShared;
using System.IO;
using Android.Content;
using Android.Widget;
using AndroidHUD;

namespace RewindAndroid
{
	public class BaseActivity:Activity
	{
		AndHUD progresV=new AndHUD();
		protected override void OnCreate (Bundle state)
		{
			try {
				base.OnCreate (state);
			} catch (Exception ex) {
				Utils.writeToDeviceLog (ex);
			}
		}

		protected override void OnResume ()
		{
			try {
				base.OnResume ();
				Utils.UserRemovedEventApp += OnLogoutClick;

			} catch (Exception ex) {
				Utils.writeToDeviceLog (ex);
			}
		}

		protected override void OnPause ()
		{
			try {
				base.OnPause ();
				Utils.UserRemovedEventApp -= OnLogoutClick;
			} catch (Exception ex) {
				Utils.writeToDeviceLog (ex);
			}
		}

		protected override void OnStop ()
		{
			try {
				base.OnStop ();
			} catch (Exception ex) {
				Utils.writeToDeviceLog (ex);
			}
		}

		protected override void OnRestart ()
		{
			try {
				base.OnRestart ();
			} catch (Exception ex) {
				Utils.writeToDeviceLog (ex);
			}
		}

		protected override void OnDestroy ()
		{
			try {
				base.OnDestroy ();
				Finish ();
			} catch (Exception ex) {
				Utils.writeToDeviceLog (ex);
			}
		}

		public void OnLogoutClick()
		{
			try
			{
				progresV.Show(this, "Attendere prego", -1, MaskType.Clear, TimeSpan.FromSeconds(120), null, true, null); 
				System.Threading.ThreadPool.QueueUserWorkItem(state =>
					{ 
						try
						{
							if (Utils.CheckForInternetConn())
							{
								string success=ApiCalls.Logout(Utils.UserToken);

								if(success.Contains("true"))
								{
									RunOnUiThread(()=>{
										if (File.Exists(Utils.PersonalFolder + "/" + "userToken.txt"))
										{
											File.Delete(Utils.PersonalFolder + "/" + "userToken.txt");
										}

										if(File.Exists(RewindShared.Consts.USER_AVATAR_IMAGE_PATH))
										{
											File.Delete(RewindShared.Consts.USER_AVATAR_IMAGE_PATH);
										}

										Intent langIntent = new Intent(this, typeof(HomeActivity));
										langIntent.PutExtra("finish",true);
										//                                langIntent.SetFlags(ActivityFlags.ReorderToFront);
										langIntent.SetFlags(ActivityFlags.NewTask | ActivityFlags.ClearTop);
										this.StartActivity(langIntent);
										Finish();
										RunOnUiThread(()=>{
											Toast.MakeText(this,"Utente non attivo",ToastLength.Short).Show();
										});
									});

								}
								else
								{
									RunOnUiThread(()=>{
										Toast.MakeText(this,"Riprova, per favore",ToastLength.Short).Show();
									});
								}

							}
							else
							{
								//no internet connection
								RunOnUiThread(()=>{
									Toast.MakeText(this,"Nessuna connessione Internet",ToastLength.Short).Show();
								});

							}
						}
						catch (Exception ex)
						{
							Utils.writeToDeviceLog(ex);
						}
						finally
						{
							RunOnUiThread(() =>
								{
									if (progresV != null)
									{
										progresV.Dismiss();
									}
								});
						}
					});
			}
			catch (Exception ex)
			{
				Utils.writeToDeviceLog(ex);
			}
		}
	}
}

