﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Content.Res;
using Android.Graphics;
using System.Globalization;
using RewindShared;
using System.Net;

namespace RewindAndroid
{
    [Activity(Label = "ApputamentiSearchAdapter")]			
    public class ApputamentiSearchAdapter :BaseAdapter<AppuntamentiItem>
    {
        #region implemented abstract members of BaseAdapter

        List<AppuntamentiItem> items;
        Activity context;
        public List<AppuntamentiItem> Items
        {
            get
            {
                return items;
            }
            set
            {
                items = value;
            }
        }

        public delegate void ItemClick(int index);

        public event ItemClick ItemClickIdEvent;

        public delegate void RowClick(int index);

        public event RowClick RowClickEvent;

        List<LinearLayout> lay = new List<LinearLayout>();

        int selectedIndex;

        public int SelectedIndex
        {
            get
            {
                return selectedIndex;
            }
            set
            {
                selectedIndex = value;
            }
        }

        public ApputamentiSearchAdapter(Activity _context, List<AppuntamentiItem> _items, int _selIndex)
            : base()
        {
            this.context = _context;
            this.items = _items;

            this.selectedIndex = _selIndex;
        }

        public override AppuntamentiItem this [int position]
        {
            get { return items[position]; }
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override int Count
        {
            get { return items.Count; }
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View view = null;
            try
            {
                System.Console.WriteLine(position.ToString());

                var item = items[position];

                view = convertView;
                if (view == null)
                    view = context.LayoutInflater.Inflate(Resource.Layout.AppuntamentiSearchItemLayout, null);

                LinearLayout boxName = view.FindViewById<LinearLayout>(Resource.Id.boxName);
                LinearLayout boxTime = view.FindViewById<LinearLayout>(Resource.Id.boxTime);
                LinearLayout layArrow = view.FindViewById<LinearLayout>(Resource.Id.layArrow);
                LinearLayout layappAdapterColor = view.FindViewById<LinearLayout>(Resource.Id.layappAdapterColor);
                ImageView imgarrowRight = view.FindViewById<ImageView>(Resource.Id.imgArrowRight);
                TextView txtHour = view.FindViewById<TextView>(Resource.Id.txtHour);
                TextView txtMin = view.FindViewById<TextView>(Resource.Id.txtMin);
                TextView txtDate = view.FindViewById<TextView>(Resource.Id.txtDate);
                TextView txtRegSociale = view.FindViewById<TextView>(Resource.Id.txtRegSociale);
                TextView txtStatus = view.FindViewById<TextView>(Resource.Id.txtStatus);

                Typeface OswaldRegualr = TypeFaces.getTypeface(context, "Oswald-Regular.ttf");
                Typeface LucidSans = TypeFaces.getTypeface(context, "Lucida Sans Regular.ttf");

                txtHour.SetTypeface(LucidSans, TypefaceStyle.Normal);
                txtMin.SetTypeface(LucidSans, TypefaceStyle.Normal);
                txtDate.SetTypeface(LucidSans, TypefaceStyle.Normal);
                txtRegSociale.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtStatus.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
			
                if(item.Id_appto_memo!="")
                {
                    txtRegSociale.Text = "MEMO";
                }
                else
                {
                    if(item.TipAppuntamenti=="business")
                    {
                        txtRegSociale.Text = item.RegSociale;
                    }
                    else if(item.TipAppuntamenti=="personal")
                    {
                        txtRegSociale.Text = "IMPEGNO PERSONALE";
                    }
                }

//                txtRegSociale.Text = item.RegSociale;
                txtStatus.Text = WebUtility.HtmlDecode(item.Status);
                txtDate.Text = item.AppuntamentiDateHour.Date.ToString("dd-MM-yy", CultureInfo.InvariantCulture);
                txtHour.Text = item.AppuntamentiDateHour.ToString("HH", CultureInfo.InvariantCulture);
                txtMin.Text = item.AppuntamentiDateHour.ToString("mm", CultureInfo.InvariantCulture);

                layArrow.Visibility = ViewStates.Gone;
                layappAdapterColor.Visibility = ViewStates.Gone;
                boxTime.SetBackgroundColor(Android.Graphics.Color.Rgb(0, 95, 167));
		
                if (selectedIndex != -1)
                {
                    boxName.Visibility = ViewStates.Gone;
                    layappAdapterColor.Visibility = ViewStates.Visible;
                    view.Click -= OnRowClick;
                    view.Click += OnRowClick;
                    view.Tag = position;

                    if (selectedIndex == position)
                    {
                        imgarrowRight.Visibility = ViewStates.Visible;
                        boxTime.SetBackgroundColor(Android.Graphics.Color.Rgb(76, 76, 76));
                        layArrow.SetBackgroundColor(Android.Graphics.Color.Transparent);
                        layArrow.Visibility = ViewStates.Visible;
                        //view.Enabled=false;
                    }
                    else
                    {
                        //first time
                        imgarrowRight.Visibility = ViewStates.Invisible;
                        layArrow.SetBackgroundColor(Android.Graphics.Color.Transparent);
                        //	view.Enabled=true;

                    }
                }
                else
                {
                    view.Click -= OnItemClick;
                    view.Click += OnItemClick;
                    view.Tag = position;
                }

                //lay.Add(boxName);
				
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
            return view;
        }


        public void OnItemClick(object sender, EventArgs e)
        {
            try
            {
                View v = (View)sender;
                v.Click -= OnItemClick;
                var position = (int)v.Tag;
                if(ItemClickIdEvent!=null)
                {
                    ItemClickIdEvent(position);
                }
                //	lay[position].Visibility= ViewStates.Gone;
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void OnRowClick(object sender, EventArgs e)
        {
            try
            {
                View v = (View)sender;
                v.Click -= OnRowClick;
                var position = (int)v.Tag;
                if(RowClickEvent!=null)
                {
                    RowClickEvent(position);
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public int selectItemPositon()
        {
            return selectedIndex;
        }

        #endregion


    }
}

