﻿using System;
using Android.Widget;
using Android.App;
using System.Collections.Generic;
using Android.Views;
using Android.Graphics;
using System.Globalization;
using RewindShared;
using System.Linq;

namespace RewindAndroid
{
    public class PraticheListAdapter: BaseAdapter<ClientPraticheItemInfo>
    {
        List<ClientPraticheItemInfo> items;
        Activity context;
        Typeface OswaldRegualr;

        public delegate void DetailsClick(ClientPraticheItemInfo index);

        public event DetailsClick DetailsClickEvent;

        public PraticheListAdapter(Activity context, List<ClientPraticheItemInfo> items)
            : base()
        {
            this.context = context;
            this.items = items;
            OswaldRegualr = TypeFaces.getTypeface(context, "Oswald-Regular.ttf");
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override ClientPraticheItemInfo this [int position]
        {
            get { return items[position]; }
        }

        public override int Count
        {
            get { return items.Count; }
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View view = null;
            try
            {
                var item = items[position];

                view = convertView;
                if (view == null)
                    view = context.LayoutInflater.Inflate(Resource.Layout.PraticheListItemLayout, null);

                TextView txtPraticheDate = view.FindViewById<TextView>(Resource.Id.txtPraticheDate);
                TextView txtGntInserita = view.FindViewById<TextView>(Resource.Id.txtGntInserita);
                TextView txtGntAttiva = view.FindViewById<TextView>(Resource.Id.txtGntAttiva);
                TextView txtTipoProdotto = view.FindViewById<TextView>(Resource.Id.txtTipoProdotto);
                TextView txtProdotto = view.FindViewById<TextView>(Resource.Id.txtProdotto);
                TextView txtDettagliBtn = view.FindViewById<TextView>(Resource.Id.txtDettagliBtn);
                LinearLayout lyPraticheDate = view.FindViewById<LinearLayout>(Resource.Id.lyPraticheDate);
                LinearLayout lyDettagliBtn = view.FindViewById<LinearLayout>(Resource.Id.lyDettagliBtn);

                TextView labelGntInserita = view.FindViewById<TextView>(Resource.Id.labelGntInserita);
                TextView labelGntAttiva = view.FindViewById<TextView>(Resource.Id.labelGntAttiva);
                TextView labelTipoProdotto = view.FindViewById<TextView>(Resource.Id.labelTipoProdotto);
                TextView txtlabelProdotto = view.FindViewById<TextView>(Resource.Id.txtlabelProdotto);

                txtPraticheDate.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtGntInserita.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtGntAttiva.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtTipoProdotto.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtProdotto.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtDettagliBtn.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                labelGntInserita.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                labelGntAttiva.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                labelTipoProdotto.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtlabelProdotto.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);

                txtGntInserita.Text = item.Gnt_inserita;
                txtGntAttiva.Text = item.Gnt_attiva;
                txtTipoProdotto.Text = item.Tipo_prodotto;
                txtProdotto.Text = item.Prodotto;
                txtPraticheDate.Text = item.Data_inserimento.ToString("dd.MM.\nyyyy", CultureInfo.InvariantCulture);

                lyDettagliBtn.Click -= OnDetailsClick;
                lyDettagliBtn.Click += OnDetailsClick;
                lyDettagliBtn.Tag = position;
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }

            return view;
        }

        public void OnDetailsClick(object sender, EventArgs e)
        {
            try
            {
                LinearLayout v = (LinearLayout)sender;
//                v.Click -= OnDetailsClick;
                var position = (int)v.Tag;
                DetailsClickEvent(items[position]);
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void FilterName(string filter, ClientPraticheItemInfo[] searchlist)
        {

            ClientPraticheItemInfo[] result = null;
            try
            {
                var query = from p in searchlist
                        where(
                            (p.Seriale.ToLower()).Contains(filter)
                        )
                    select p;
                try
                {
                    result = query.ToArray();
                }
                catch (Exception ex)
                {
                    Utils.writeToDeviceLog(ex);
                }
                if (result != null)
                {
                    items = result.ToList();
                }
                NotifyDataSetChanged();
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }
    }
}