﻿using System;

namespace RewindAndroid
{
    public class ApputamentiTimeList
    {
        string hour;
        string date;

        public string Hour
        {
            get
            {
                return hour;
            }
            set
            {
                hour = value;
            }
        }

        public string Date
        {
            get
            {
                return date;
            }
            set
            {
                date = value;
            }
        }
    }
}

