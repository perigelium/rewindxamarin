﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.IO;
using RewindAndroid;
using Android.Graphics;
using System.Globalization;
using RewindShared;

namespace RewindAndroid
{
    public class ApputamentiTimeAdapter : BaseAdapter<AppuntamentiItem>
    {
        List<AppuntamentiItem> items;
        Activity context;

        public List<AppuntamentiItem> Items
        {
            get
            {
                return items;
            }
            set
            {
                items = value;
            }
        }

        public delegate void ItemClick(int index);

        public event ItemClick ItemClickIdEvent;

        public delegate void RowClick(int index);

        public event RowClick RowClickEvent;

        List<LinearLayout> lay = new List<LinearLayout>();

        int selectedIndex;

        public int SelectedIndex
        {
            get
            {
                return selectedIndex;
            }
            set
            {
                selectedIndex = value;
            }
        }

        public ApputamentiTimeAdapter(Activity context, List<AppuntamentiItem> items, int selIndex)
            : base()
        {
            this.context = context;
            this.items = items;
            this.selectedIndex = selIndex;
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override AppuntamentiItem this [int position]
        {
            get { return items[position]; }
        }

        public override int Count
        {
            get 
            {
                if (items != null)
                {
                    return items.Count;
                }

                return 0;
            }
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View view = null;
            try
            {
                System.Console.WriteLine(position.ToString());

                var item = items[position];

                view = convertView;
                if (view == null)
                    view = context.LayoutInflater.Inflate(Resource.Layout.ApputamentiTimeItemLayout, null);

                TextView txtCompany = view.FindViewById<TextView>(Resource.Id.txtAppCompanyName);
                TextView txtDate = view.FindViewById<TextView>(Resource.Id.txtDate);
                TextView txtHour = view.FindViewById<TextView>(Resource.Id.txtHour);
                TextView txtMin = view.FindViewById<TextView>(Resource.Id.txtMin);
                LinearLayout layAppuntamentiRegSociale = view.FindViewById<LinearLayout>(Resource.Id.layAppuntamentiRegSociale);
                LinearLayout boxTime = view.FindViewById<LinearLayout>(Resource.Id.boxTime);
                LinearLayout layArrow = view.FindViewById<LinearLayout>(Resource.Id.layArrow);
                LinearLayout layappAdapterColor = view.FindViewById<LinearLayout>(Resource.Id.layappAdapterColor);
                ImageView imgarrowRight = view.FindViewById<ImageView>(Resource.Id.imgArrowRight);

                Typeface OswaldRegualr = TypeFaces.getTypeface(context, "Oswald-Regular.ttf");
                Typeface LucidaSansRegualr = TypeFaces.getTypeface(context, "Lucida Sans Regular.ttf");

                txtCompany.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtDate.SetTypeface(LucidaSansRegualr, TypefaceStyle.Normal);
                txtHour.SetTypeface(LucidaSansRegualr, TypefaceStyle.Normal);
                txtMin.SetTypeface(LucidaSansRegualr, TypefaceStyle.Normal);

                if(item.AppBloccati)
                {
					txtCompany.Text = "STOP APPUNTAMENTI";
                }
                else
                {
                    if(item.Id_appto_memo!="")
                    {
                        txtCompany.Text = "MEMO";
                    }
                    else
                    {
                        if(item.TipAppuntamenti=="business")
                        {
                            txtCompany.Text = item.RegSociale;
                        }
                        else if(item.TipAppuntamenti=="personal")
                        {
                            txtCompany.Text = "IMPEGNO PERSONALE";
                        }
                    }
                }

               

                txtDate.Text = item.AppuntamentiDateHour.Date.ToString("dd-MM-yy", CultureInfo.InvariantCulture);
                txtHour.Text = item.AppuntamentiDateHour.ToString("HH", CultureInfo.InvariantCulture);
                txtMin.Text = item.AppuntamentiDateHour.ToString("mm", CultureInfo.InvariantCulture);
                layAppuntamentiRegSociale.SetBackgroundColor(Android.Graphics.Color.Rgb(0, 95, 167));
                boxTime.SetBackgroundColor(Android.Graphics.Color.Rgb(0, 95, 167));
                layArrow.Visibility = ViewStates.Gone;
                layappAdapterColor.Visibility = ViewStates.Gone;
                
                if (selectedIndex != -1)
                {

                    layAppuntamentiRegSociale.Visibility = ViewStates.Gone;
//                    layappAdapterColor.Visibility = ViewStates.Visible;
                    view.Click -= OnRowClick;
                    view.Click += OnRowClick;
                    view.Tag = position;

                    if (selectedIndex == position)
                    {
                       
                        boxTime.SetBackgroundColor(Android.Graphics.Color.Rgb(76, 76, 76));
                        layArrow.SetBackgroundColor(Android.Graphics.Color.Transparent);
                        layArrow.Visibility = ViewStates.Visible;
                        layappAdapterColor.Visibility = ViewStates.Visible;
                    }
                    else
                    {
                        //first time
                        layArrow.Visibility = ViewStates.Gone;
                        layArrow.SetBackgroundColor(Android.Graphics.Color.Transparent);
                    }
                }
                else
                {
                    view.Click -= OnItemClick;
                    view.Click += OnItemClick;
                    view.Tag = position;
                }

                lay.Add(layAppuntamentiRegSociale);
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
			
            return view;
        }

        public void OnItemClick(object sender, EventArgs e)
        {
            try
            {
                View v = (View)sender;
                v.Click -= OnItemClick;
                var position = (int)v.Tag;
                if(ItemClickIdEvent!=null)
                {
                    ItemClickIdEvent(position);
                }
                // lay[position].Visibility= ViewStates.Gone;
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void OnRowClick(object sender, EventArgs e)
        {
            try
            {
                View v = (View)sender;
                v.Click -= OnRowClick;
                var position = (int)v.Tag;
                if(RowClickEvent!=null)
                {
                    RowClickEvent(position);
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public int selectItemPositon()
        {
            return selectedIndex;
        }
	
    }
}