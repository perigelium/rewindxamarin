﻿using System;
using System.Collections.Generic;
using Android.Widget;
using Android.Views;
using Android.Content;
using RewindShared;
using Android.Graphics;

namespace RewindAndroid
{
    public class TicketTypeDropDownAdapter: BaseAdapter
    {
        List<TicketTypteItem> listItems;
        Typeface OswaldRegualr;

        public TicketTypeDropDownAdapter(List<TicketTypteItem> listItems, Context _context)
        {
            this.listItems = listItems;
            OswaldRegualr = TypeFaces.getTypeface(_context, "Oswald-Regular.ttf");
        }

        #region implemented abstract members of BaseAdapter

        public override Java.Lang.Object GetItem(int position)
        {
            return listItems[position].Type;
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override Android.Views.View GetView(int position, Android.Views.View convertView, Android.Views.ViewGroup parent)
        {
            var view = convertView;

            try
            {
                if (view == null)
                {
                    var inflater = parent.Context.GetSystemService(Context.LayoutInflaterService) as LayoutInflater;
                    view = inflater.Inflate(Resource.Layout.DropDownListItemLayout, null);
                }

                TextView txtLine = view.FindViewById<TextView>(Resource.Id.txtDropDownItemLine);
                txtLine.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);

                txtLine.Text = listItems[position].Type;
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }

            return view;
        }

        public override int Count
        {
            get
            {
                return listItems.Count;
            }
        }

        #endregion
    }
}