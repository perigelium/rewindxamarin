﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
//using Bugsnag;
using Android.Content.PM;
using Android.Views.InputMethods;
using RewindShared;

namespace RewindAndroid
{
    [Application(Theme = "@android:style/Theme.Black.NoTitleBar")]		
    public class ApplicationContextProvider : Application
    {

//        private static BugsnagClient bugsnagClient;

        public ApplicationContextProvider(IntPtr a, JniHandleOwnership b)
            : base(a, b)
        {
        }

        private static Context sContext;

        public override void OnCreate()
        {
            base.OnCreate();

            sContext = ApplicationContext;
		
//            if (bugsnagClient == null)
//            {
//                // create new BugsnagClient which will monitor for errors and send them to the server
//
//                bugsnagClient = new BugsnagClient(this, "7ecd58df84e0e286c8114f039a7d9a0a")
//                {
//                    DeviceId = GetInstalId(),
//                    ProjectNamespaces = new List<string>() { "Rewind." }
//                };
//            }

            Xamarin.Insights.Initialize ("53d237d7927d2d550c26f56a101d65de33676571", sContext);
//            var appInfo = new Dictionary<string,string> {
//                { "IMEI", Utils.DeviceIMEI }
//            };
//
//            Insights.Identify (Utils.DeviceIMEI, appInfo);
        }

        public static Context getContext()
        {
            return sContext;
        }

        public override void OnLowMemory()
        {
            base.OnLowMemory();
            Console.WriteLine("OnLowMemory() method call");
//            Utils.writeToDeviceLog("OnLowMemory() method call");

            GC.Collect();
        }

        private string GetInstalId()
        {
            return Utils.getDeviceIMEI();
        }

//        public static BugsnagClient BugsnagClient
//        {
//            get { return bugsnagClient; }
//        }
    }
}

