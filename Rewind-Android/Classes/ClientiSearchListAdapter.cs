﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.IO;
using RewindAndroid;
using Android.Graphics;
using RewindShared;

namespace RewindAndroid
{
    public class ClientiSearchListAdapter: BaseAdapter<ClientInfoItem>
    {
        List<ClientInfoItem> items;
        Activity context;

        public delegate void ItemClick(int index);

        public event ItemClick ItemClickIdEvent;


        List<LinearLayout> lay = new List<LinearLayout>();
        int selectedIndex;

        public ClientiSearchListAdapter(Activity context, List<ClientInfoItem> items, int selIndex)
            : base()
        {
            this.context = context;
            this.items = items;
            this.selectedIndex = selIndex;
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override ClientInfoItem this [int position]
        {
            get { return items[position]; }
        }

        public override int Count
        {
            get { return items.Count; }
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View view = null;
            try
            {
                var item = items[position];

                view = convertView;
                if (view == null)
                    view = context.LayoutInflater.Inflate(Resource.Layout.ClientiSearchListViewItemLayout, null);

                TextView txtregSociale = view.FindViewById<TextView>(Resource.Id.txtregSociale);
                LinearLayout layBackground = view.FindViewById<LinearLayout>(Resource.Id.layBackground);
                LinearLayout layArrow = view.FindViewById<LinearLayout>(Resource.Id.layArrow);
                LinearLayout layappAdapterColor = view.FindViewById<LinearLayout>(Resource.Id.layClientAdapterColor);
                ImageView imgarrowRight = view.FindViewById<ImageView>(Resource.Id.imgArrowRightClienti);

                Typeface OswaldRegualr = TypeFaces.getTypeface(context, "Oswald-Regular.ttf");
                txtregSociale.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);

                txtregSociale.Text = item.Regione_sociale;
                layBackground.SetBackgroundColor(Android.Graphics.Color.Rgb(0, 95, 167));

                layArrow.Visibility = ViewStates.Gone;
                layappAdapterColor.Visibility = ViewStates.Gone;

                if (selectedIndex != -1)
                {
                    layappAdapterColor.Visibility = ViewStates.Visible;
                    if (selectedIndex == position)
                    {
                        
                        layBackground.SetBackgroundColor(Android.Graphics.Color.Rgb(76, 76, 76));

                        imgarrowRight.Visibility = ViewStates.Visible;
                        layArrow.SetBackgroundColor(Android.Graphics.Color.Transparent);
                        layArrow.Visibility = ViewStates.Visible;
                    }
                    else
                    {
                        //first time
                        imgarrowRight.Visibility = ViewStates.Invisible;
                        layArrow.SetBackgroundColor(Android.Graphics.Color.Transparent);
                        layArrow.Visibility = ViewStates.Invisible;
                    }
                }

                view.Click -= OnItemClick;
                view.Click += OnItemClick;
                view.Tag = position;

                lay.Add(layBackground);
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }

            return view;
        }

        public void OnItemClick(object sender, EventArgs e)
        {
            try
            {
                View v = (View)sender;
                v.Click -= OnItemClick;
                var position = (int)v.Tag;
                ItemClickIdEvent(position);
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }
    }
}