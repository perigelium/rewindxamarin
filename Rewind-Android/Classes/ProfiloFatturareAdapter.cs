﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using AndroidHUD;
using RewindShared;
using System.Globalization;

namespace RewindAndroid
{
    [Activity(Label = "ProfiloFatturareAdapter")]			
    public class ProfiloFatturareAdapter : BaseAdapter<FatturarePeriodItem>
    {

        List<FatturarePeriodItem> items;
        Activity context;
        //        LinearLayout lyDetails;
        //        LinearLayout lyimgDetails;
        //        LinearLayout lyimgRefresh;
        //        Button butDetails;
        //        List<LinearLayout> lay = new List<LinearLayout>();
        //        List<LinearLayout> imgDown = new List<LinearLayout>();
        //        List<LinearLayout> imgUp = new List<LinearLayout>();
        //        TextView txtDate;
        ListView lst;
        AndHUD progresV;
        Typeface OswaldRegualr;

        //        Dictionary<int, LinearLayout> layDict = new Dictionary<int, LinearLayout>();
        //        Dictionary<int, LinearLayout> imgDownDict = new Dictionary<int, LinearLayout>();
        //        Dictionary<int, LinearLayout> imgUpDict = new Dictionary<int, LinearLayout>();

        UserLocalDB db;

        public ProfiloFatturareAdapter(Activity context, List<FatturarePeriodItem> items, ListView lst)
            : base()
        {
            this.context = context;
            this.items = items;
            this.lst = lst;
            progresV = new AndHUD();
            db=new UserLocalDB();
            OswaldRegualr = TypeFaces.getTypeface(context, "Oswald-Regular.ttf");
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override FatturarePeriodItem this [int position]
        {
            get { return items[position]; }
        }

        public override int Count
        {
            get { return items.Count; }
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            FattureViewHolder holder = null;
            View view = null;

            try
            {
                var item = items[position];

                view = convertView;

                if (view != null)
                    holder = view.Tag as FattureViewHolder;

//                if (view == null)
//                    view = context.LayoutInflater.Inflate(Resource.Layout.ProfiloFatturareItemLayout, null);

//                TextView txtDate = view.FindViewById<TextView>(Resource.Id.txtDate);
//                TextView txtTotal = view.FindViewById<TextView>(Resource.Id.txtTotal);
//                TextView txtStorni = view.FindViewById<TextView>(Resource.Id.txtStorni);
//                TextView txtStorniConsumer = view.FindViewById<TextView>(Resource.Id.txtStorniConsumer);
//                TextView txtBonus = view.FindViewById<TextView>(Resource.Id.txtBonus);
//                TextView txtGara = view.FindViewById<TextView>(Resource.Id.txtGara);
//                TextView txtAnticipo = view.FindViewById<TextView>(Resource.Id.txtAnticipo);
//                LinearLayout lyimgDetails = view.FindViewById<LinearLayout>(Resource.Id.lyimgDetails);
//                LinearLayout lyimgRefresh = view.FindViewById<LinearLayout>(Resource.Id.lyimgRefresh);
//                Button butDetails = view.FindViewById<Button>(Resource.Id.butDetails);
//                CheckBox checkBill = view.FindViewById<CheckBox>(Resource.Id.checkBill);
//                LinearLayout lyDetails = view.FindViewById<LinearLayout>(Resource.Id.lyDetails);

                if (holder == null)
                {                    
                    holder = new FattureViewHolder();

                    view = context.LayoutInflater.Inflate(Resource.Layout.ProfiloFatturareItemLayout, null);
                    holder.TxtDate = view.FindViewById<TextView>(Resource.Id.txtDate);
                    holder.TxtTotal = view.FindViewById<TextView>(Resource.Id.txtTotal);
                    holder.TxtStorni = view.FindViewById<TextView>(Resource.Id.txtStorni);
                    holder.TxtStorniConsumer = view.FindViewById<TextView>(Resource.Id.txtStorniConsumer);
                    holder.TxtBonus = view.FindViewById<TextView>(Resource.Id.txtBonus);
                    holder.TxtGara = view.FindViewById<TextView>(Resource.Id.txtGara);
                    holder.TxtAnticipo = view.FindViewById<TextView>(Resource.Id.txtAnticipo);
                    holder.LyimgDetails = view.FindViewById<LinearLayout>(Resource.Id.lyimgDetails);
                    holder.LyimgRefresh = view.FindViewById<LinearLayout>(Resource.Id.lyimgRefresh);
                    holder.ButDetails = view.FindViewById<Button>(Resource.Id.butDetails);
                    holder.CheckBill = view.FindViewById<CheckBox>(Resource.Id.checkBill);
                    holder.LyDetails = view.FindViewById<LinearLayout>(Resource.Id.lyDetails);

//                    Typeface OswaldRegualr = TypeFaces.getTypeface(context, "Oswald-Regular.ttf");
                    holder.TxtDate.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                    holder.TxtTotal.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                    holder.TxtStorni.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                    holder.TxtStorniConsumer.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                    holder.TxtBonus.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                    holder.TxtGara.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                    holder.TxtAnticipo.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                    holder.ButDetails.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);

//                    holder.LyDetails.Visibility = ViewStates.Gone;
//                    holder.ButDetails.Click += OnDetailsClick;
//                    holder.LyimgRefresh.Click += OnDetailsGoneClick;
//                    holder.CheckBill.CheckedChange += OnCheckedBillClick;

                    view.Tag = holder;
                }

//                holder.LyimgRefresh.Visibility = ViewStates.Gone;               
                holder.LyDetails.Visibility = ViewStates.Gone;
                //TODO
                holder.LyimgDetails.Visibility = ViewStates.Visible;
                holder.LyimgRefresh.Visibility = ViewStates.Gone;


//                holder.LyDetails.Visibility = ViewStates.Visible;

                holder.LyimgDetails.Visibility = ViewStates.Gone;
                holder.LyimgRefresh.Visibility = ViewStates.Gone;

//                if (!layDict.ContainsKey(position))
//                {
//                    layDict.Add(position, lyDetails);
//                }
//
//                if (!imgDownDict.ContainsKey(position))
//                {
//                    imgDownDict.Add(position, lyimgDetails);
//                }
//
//                if (!imgUpDict.ContainsKey(position))
//                {
//                    imgUpDict.Add(position, lyimgRefresh);
//                }
//                lay.Add(lyDetails);
//                imgDown.Add(lyimgDetails);
//                imgUp.Add(lyimgRefresh);

//                holder.LyimgDetails.Click -= OnDetailsClick;
//                holder.LyimgDetails.Click += OnDetailsClick;
//                holder.LyimgRefresh.Click -= OnDetailsGoneClick;
//                holder.LyimgRefresh.Click += OnDetailsGoneClick;

                int currPosition = position;
                holder.Position = currPosition;

                holder.LyimgDetails.Tag = holder;
                holder.LyimgRefresh.Tag = holder;
//                holder.LyimgDetails.Tag = position;
//                holder.LyimgRefresh.Tag = position;
                DateTime fattDateName=DateTime.Now;
                if(item.Period!=null && item.Period!="")
                {
                    fattDateName=new DateTime(Int32.Parse(item.Period.Substring(0,4)),Int32.Parse(item.Period.Substring(4,2)),2);
                }

                holder.TxtTotal.Text = item.Total.ToString("0.00")+"€";
//                holder.TxtDate.Visibility = ViewStates.Gone;
                System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("it-IT");
                holder.TxtDate.Text = "FATTURA "+fattDateName.ToString("MMMMM", CultureInfo.CurrentCulture).ToUpper() +" "+ fattDateName.Year;
                holder.TxtStorni.Visibility = ViewStates.Gone;

                holder.TxtStorniConsumer.Visibility = ViewStates.Gone;
                holder.TxtBonus.Visibility = ViewStates.Gone;
                holder.TxtGara.Visibility = ViewStates.Gone;
                holder.TxtAnticipo.Visibility = ViewStates.Gone;

                holder.CheckBill.Tag = holder;

                holder.CheckBill.Checked = bool.Parse(item.Check);

//                if(bool.Parse(item.Check)==false)
//                {
//                    holder.CheckBill.CheckedChange -= OnCheckedBillClick;
//                    holder.CheckBill.CheckedChange += OnCheckedBillClick;
//                }
                holder.CheckBill.Click-=OnCheckedBillClick;
                holder.CheckBill.Click+=OnCheckedBillClick;



//                holder.CheckBill.Tag = position;
                holder.ButDetails.Click -= OnDetailsClick;
                holder.ButDetails.Click += OnDetailsClick;
                holder.ButDetails.Tag = holder;

                //inflate allfactures



                //	butDetails.Tag = position;
                //	txtDate.Click+=OnDetailsClick;
                //	txtDate.Tag = position;
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }

            return view;
        }

        void OnDetailsClickEvent()
        {
            try
            {



            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }

        }

        public void OnDetailsClick(object sender, EventArgs e)
        {
            try
            {
//                View v = (View)sender;
//                var position = (int)v.Tag;

//                lay[position].Visibility = ViewStates.Visible;
//                imgDown[position].Visibility = ViewStates.Gone;
//                imgUp[position].Visibility = ViewStates.Visible;

//                layDict[position].Visibility = ViewStates.Visible;
//                imgDownDict[position].Visibility = ViewStates.Gone;
//                imgUpDict[position].Visibility = ViewStates.Visible;

//                if (position == items.Count - 1)
//                {
//                    lst.SetSelection(position); 
//                }
//                NotifyDataSetChanged();
                View v = (View)sender;
                FattureViewHolder vh = (FattureViewHolder)v.Tag;
                vh.LyimgDetails.Visibility = ViewStates.Gone;
                vh.ButDetails.Visibility=ViewStates.Visible;
                var item = items[vh.Position];

                if(vh.LyDetails.Visibility== ViewStates.Visible)
                {
                    vh.LyDetails.Visibility= ViewStates.Gone;
                    vh.LyDetails.RemoveAllViews();
                }
                else
                {
                    vh.LyDetails.Visibility= ViewStates.Visible;
                    vh.LyDetails.RemoveAllViews();


                    foreach (var itemFat in item.Facturi) 
                    {
                        LayoutInflater infl = (LayoutInflater)context.GetSystemService (Context.LayoutInflaterService);
                        View entryLayout = infl.Inflate (Resource.Layout.FattureInflateGroup, vh.LyDetails, false);
                        TextView txtGroupName = entryLayout.FindViewById<TextView> (Resource.Id.txtFattGroupName);
                        LinearLayout layForFattNames=entryLayout.FindViewById<LinearLayout>(Resource.Id.layForFattNames);
                        txtGroupName.Text=itemFat.Keys.ToList()[0];
                        txtGroupName.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                        vh.LyDetails.AddView (entryLayout);

                        foreach (var itemFatName in itemFat.Values.ToList()[0]) 
                        {
                            //                        LayoutInflater infl = (LayoutInflater)Activity.GetSystemService (Context.LayoutInflaterService);
                            View entryLayoutName = infl.Inflate (Resource.Layout.FattureInflateName, layForFattNames, false);
                            TextView txtFattName = entryLayoutName.FindViewById<TextView> (Resource.Id.txtFattName);
                            TextView txtFattPrice = entryLayoutName.FindViewById<TextView> (Resource.Id.txtFattPrice);
                            txtFattName.Text=itemFatName.Title_invoice;
                            txtFattPrice.Text=itemFatName.Total.ToString("0.00")+"€";
                            txtFattName.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                            txtFattPrice.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                            layForFattNames.AddView(entryLayoutName);
                        }
                    }
                }

                //TODO
//                vh.LyimgRefresh.Visibility = ViewStates.Visible;

                if (vh.Position == items.Count - 1)
                {
                    lst.SetSelection(vh.Position); 
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void OnDetailsGoneClick(object sender, EventArgs e)
        {
            try
            {
//                View v = (View)sender;
//                var position = (int)v.Tag;

//                imgDown[position].Visibility = ViewStates.Visible;
//                imgUp[position].Visibility = ViewStates.Gone;
//                lay[position].Visibility = ViewStates.Gone;

//                imgDownDict[position].Visibility = ViewStates.Visible;
//                imgUpDict[position].Visibility = ViewStates.Gone;
//                layDict[position].Visibility = ViewStates.Gone;

                View v = (View)sender;
                FattureViewHolder vh = (FattureViewHolder)v.Tag;

                vh.LyDetails.Visibility = ViewStates.Gone;
                vh.LyimgDetails.Visibility = ViewStates.Visible;
                vh.LyimgRefresh.Visibility = ViewStates.Gone;
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void OnCheckedBillClick(object sender, EventArgs e)//CompoundButton.CheckedChangeEventArgs
        {
            try
            {                
                CheckBox boxSender = (CheckBox)sender;
//                int position = (int)boxSender.Tag;
                FattureViewHolder vh = (FattureViewHolder)boxSender.Tag;
                if(bool.Parse(items[vh.Position].Check)==false)
                {
                    var builder = new AlertDialog.Builder(context);
                    builder.SetMessage("Sei sicuro di voler accettare questa pre-fattura?");
                    builder.SetPositiveButton("SI", (s2, e2) => { 

                        if (boxSender.Checked != bool.Parse(items[vh.Position].Check))
                        {     
                            //boxSender.Checked=!e.IsChecked;
                            progresV.Show(context, "Attendere prego", -1, MaskType.Clear, TimeSpan.FromSeconds(120), null, true, null);

                            System.Threading.ThreadPool.QueueUserWorkItem(state =>
                                {   
                                    try
                                    {

                                        if (Utils.CheckForInternetConn())
                                        {
                                            bool success = ApiCalls.updateInvoiceChecked(Utils.UserToken, items[vh.Position].Id);

                                            items[vh.Position].Check = (!bool.Parse(items[vh.Position].Check)).ToString();
                                            context.RunOnUiThread(() =>
                                                {                                    
                                                    if (success)
                                                    {
                                                        Toast.MakeText(context, "Fattura è stato aggiornato con successo", ToastLength.Short).Show();
                                                        foreach (var itemFat in items[vh.Position].Facturi) 
                                                        {
                                                            foreach (var itemFatName in itemFat.Values.ToList()[0]) 
                                                            {
                                                                itemFatName.Check= items[vh.Position].Check;
                                                                db.UpdateinvoicesInfo(itemFatName);
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        Toast.MakeText(context, "Impossibile aggiornare Fatture", ToastLength.Short).Show();
                                                    }
                                                });
                                        }
                                        else
                                        {
                                            context.RunOnUiThread(() =>
                                                {
                                                    vh.CheckBill.CheckedChange -= OnCheckedBillClick;
                                                    vh.CheckBill.Checked = !vh.CheckBill.Checked;
                                                    vh.CheckBill.CheckedChange += OnCheckedBillClick;

                                                    Toast.MakeText(context, "Nessuna connessione Internet", ToastLength.Short).Show();
                                                });
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Utils.writeToDeviceLog(ex);
                                    }
                                    finally
                                    {
                                        context.RunOnUiThread(() =>
                                            {
                                                if (progresV != null)
                                                {
                                                    progresV.Dismiss();
                                                }
                                            });
                                    }
                                });
                        }

                    });
                    builder.SetNegativeButton("CANCELLA", (s3, e3) => { vh.CheckBill.Checked = !vh.CheckBill.Checked; });
                    builder.Create().Show();


                }
                else
                {
                    vh.CheckBill.Checked = !vh.CheckBill.Checked;
                }

                    

            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }
    }

    class FattureViewHolder : Java.Lang.Object
    {
        public TextView TxtDate { get; set; }

        public TextView TxtTotal { get; set; }

        public TextView TxtStorni { get; set; }

        public TextView TxtStorniConsumer { get; set; }

        public TextView TxtBonus { get; set; }

        public TextView TxtGara { get; set; }

        public TextView TxtAnticipo { get; set; }

        public Button ButDetails { get; set; }

        public CheckBox CheckBill { get; set; }

        public LinearLayout LyDetails { get; set; }

        public LinearLayout LyimgDetails { get; set; }

        public LinearLayout LyimgRefresh { get; set; }

        public int Position { get; set; }
    }
}
