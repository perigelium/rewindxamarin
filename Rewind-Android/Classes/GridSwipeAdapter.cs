﻿using System;
using Android.App;
using Android.Support.V4.App;
using Android.Support.V4.View;

namespace RewindAndroid
{
    public class GridSwipeAdapter:FragmentStatePagerAdapter
    {
        Activity context;
        ViewPager viewpager;

        public delegate void DateTimeDelegateDuplicate(DateTime date_from_to);

        public event DateTimeDelegateDuplicate GridClickCalendar;

        public GridSwipeAdapter(Android.Support.V4.App.FragmentManager fm, Activity context,ViewPager viewpgr) : base(fm)
        {
            this.context = context;
            this.viewpager = viewpgr;
        }

        public override int Count {
            get {
                return 120;
            }
        }

        public override Android.Support.V4.App.Fragment GetItem (int p0)
        {
            CalendarGridFragment sldImgFrg = CalendarGridFragment.newInstance(p0, context, viewpager);
            sldImgFrg.datePickEvent += GridCalendarTouch;
            return sldImgFrg;
        }

        void GridCalendarTouch(DateTime date_from_to)
        {
            if (GridClickCalendar != null)
            {
                GridClickCalendar(date_from_to);
            }
        }
    }
}

