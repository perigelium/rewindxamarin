﻿using System;
using Android.Locations;

namespace RewindAndroid
{
    public class LocationInfo
    {
        Location location = null;
        GpsStatus gpsStatus = null;
        bool isFromGps = false;

        public Location Location
        {
            get
            {
                return location;
            }
            set
            {
                location = value;
            }
        }

        public GpsStatus GpsStatus
        {
            get
            {
                return gpsStatus;
            }
            set
            {
                gpsStatus = value;
            }
        }

        public bool IsFromGps
        {
            get
            {
                return isFromGps;
            }
            set
            {
                isFromGps = value;
            }
        }
    }
}

