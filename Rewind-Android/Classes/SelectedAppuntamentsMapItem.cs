﻿using System;

namespace RewindAndroid
{
    public class SelectedAppuntamentsMapItem
    {
        public string type;
        public double latitude;
        public double longitude;

        public string Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value;
            }
        }


        public double Latitude
        {
            get
            {
                return latitude;
            }
            set
            {
                latitude = value;
            }
        }

        public double Longitude
        {
            get
            {
                return longitude;
            }
            set
            {
                longitude = value;
            }
        }
    }
}

