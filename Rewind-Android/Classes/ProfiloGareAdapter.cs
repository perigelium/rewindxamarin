﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using RewindShared;

namespace RewindAndroid
{
    [Activity(Label = "ProfiloGareAdapter")]			
    public class ProfiloGareAdapter : BaseAdapter<GareItem>
    {
        List<GareItem> items;
        Activity context;


        public ProfiloGareAdapter(Activity context, List<GareItem> items)
            : base()
        {
            this.context = context;
            this.items = items;

        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override GareItem this [int position]
        {
            get { return items[position]; }
        }

        public override int Count
        {
            get { return items.Count; }
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View view = null;
            try
            {
                var item = items[position];

                view = convertView;
                if (view == null)
                    view = context.LayoutInflater.Inflate(Resource.Layout.ProfiloGareItemLayout, null);


                TextView textPos = view.FindViewById<TextView>(Resource.Id.textPos);
                TextView textAgente = view.FindViewById<TextView>(Resource.Id.textAgente);
                TextView textPun = view.FindViewById<TextView>(Resource.Id.textPun);
                TextView textPA = view.FindViewById<TextView>(Resource.Id.textPA);
                TextView textPS = view.FindViewById<TextView>(Resource.Id.textPS);
                TextView textSS = view.FindViewById<TextView>(Resource.Id.textSS);


                Typeface OswaldRegualr = TypeFaces.getTypeface(context, "Oswald-Regular.ttf");
                textPos.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                textAgente.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                textPun.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                textPA.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                textPS.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                textSS.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);

                textPos.Text = (position + 1).ToString();
                textAgente.Text = item.Agente;
                textPun.Text = item.Punti;
                textPA.Text = item.PA;
                textPS.Text = item.PS;
                textSS.Text = item.SS;

            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }

            return view;
        }


    }
}

