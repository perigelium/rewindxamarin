﻿using System;
using Android.Content;
using Java.Lang;
using Android.Locations;
using Android.OS;
using System.Threading;


namespace RewindAndroid
{
    public class LocationGetter
    {
        private Context context;
        private static LocationInfo theLocation = null;
        //private static MyJavaObject gotLocationLock = new MyJavaObject();

        private static AutoResetEvent autoReset = new AutoResetEvent(false);

        MyLocationResult locationResult = new MyLocationResult();

        public LocationGetter(Context context)
        {
            if (context == null)
                throw new IllegalArgumentException("context == null");

            this.context = context;
        }

        public LocationInfo getLocation(int maxWaitingTime, int updateTimeout)
        {
            Console.WriteLine("------------GET LOCATION FUNCTION----------------");
            object lockObj = new object();

            lock (lockObj)
            {
                try
                {
                    int updateTimeoutPar = updateTimeout;

                    //lock(gotLocationLock)
                    //lock(autoReset)
                    {
                        System.Threading.Thread thread = new System.Threading.Thread(() => threadMethod(updateTimeoutPar));
                        thread.Start();

                        autoReset.WaitOne(maxWaitingTime);

                        //  gotLocationLock.Wait(maxWaitingTime);
                    }
                }
                catch (InterruptedException e1)
                {
                    Console.WriteLine(e1.Message + "  --  " + e1.StackTrace);
                }
                return theLocation; 
            }
        }

        private void threadMethod(int updateTimeoutPar)
        {
            Looper.Prepare();
            LocationResolver locationResolver = new LocationResolver();
            Console.WriteLine("------------------threadMethod-------");
            //locationResolver.prepare();
            locationResolver.getLocation(context, locationResult, updateTimeoutPar);
            Looper.Loop();
        }

        private class MyJavaObject: Java.Lang.Object, Android.Runtime.IJavaObject
        {

        }

        private class MyLocationResult: RewindAndroid.LocationResolver.LocationResult
        {
            #region implemented abstract members of LocationResult

            public override void gotLocation(LocationInfo location)
            {
                object lockObject = new object();
                //lock (gotLocationLock)
                //lock (autoReset)
                Console.WriteLine("--------------getLocation--------------");
                Console.WriteLine("----------------------------");

                lock (lockObject)
                {
                    theLocation = location;
                    //gotLocationLock.NotifyAll ();
                    autoReset.Set();
                    Looper.MyLooper().Quit();
                }
            }

            #endregion
        }
    }
}

