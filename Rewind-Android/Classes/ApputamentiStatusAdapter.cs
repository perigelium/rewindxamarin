﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using RewindShared;

namespace RewindAndroid
{
    public class ListStatusAdapter :  BaseAdapter
    {

        public override Java.Lang.Object GetItem(int position)
        {
            throw new NotImplementedException();
        }


        TextView itemm;
        readonly List<string> items;
        Activity activity;

        public ListStatusAdapter(Activity activity, List<string> items)
        {
            this.items = items;
            this.activity = activity;
        }



        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            try
            {
                var item = items[position];

                View view = convertView;

                if (view == null) // no view to re-use, create new
					view = activity.LayoutInflater.Inflate(Resource.Layout.ItemTextView, null);

                itemm = view.FindViewById<TextView>(Resource.Id.text);

                itemm.Text = item.ToString();

                return view;
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
                return null;
            }

        }

        public override int Count
        {
            get
            {
                return items.Count;
            }
        }



    }
}

