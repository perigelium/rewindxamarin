﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using RewindShared;

namespace RewindAndroid
{
    public class ProfiloBonusAdapter: BaseAdapter<BonusItem>
    {

        List<BonusItem> items;
        Activity context;

		
        public ProfiloBonusAdapter(Activity context, List<BonusItem> items)
            : base()
        {
            this.context = context;
            this.items = items;

        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override BonusItem this [int position]
        {
            get { return items[position]; }
        }

        public override int Count
        {
            get { return items.Count; }
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View view = null;
            try
            {
                var item = items[position];

                view = convertView;
                if (view == null)
                    view = context.LayoutInflater.Inflate(Resource.Layout.ProfiloBonusItemLayout, null);


                TextView txtRegSoc = view.FindViewById<TextView>(Resource.Id.txtRegSoc);
                TextView txtProduct = view.FindViewById<TextView>(Resource.Id.txtProduct);
                TextView txtBonus = view.FindViewById<TextView>(Resource.Id.txtBonus);

                Typeface OswaldRegualr = TypeFaces.getTypeface(context, "Oswald-Regular.ttf");
                txtRegSoc.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtProduct.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtBonus.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);

                txtRegSoc.Text = item.Regionalesociale;
                txtProduct.Text = item.Prodato;
                txtBonus.Text = item.Bonus.ToString("0.00") + " €";
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }

            return view;
        }

		

    }

}

