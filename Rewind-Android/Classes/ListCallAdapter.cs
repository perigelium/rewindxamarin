﻿using System;
using Android.App;
using Android.Widget;
using System.Collections.Generic;
using Android.Views;

namespace RewindAndroid
{
    public class ListCallAdapter: BaseAdapter<string>
    {
        private List<string> _items; 
        private readonly Activity _context;

        public ListCallAdapter(Activity activity, List<string> contacts)
        {
            _items = contacts;
            _context = activity;
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var view = _context.LayoutInflater.Inflate(Resource.Layout.PhoneEmailListItemLayout, null);

            var cts = _items[position];

            var phoneView = view.FindViewById<TextView> (Resource.Id.txtNrCall);

            phoneView.Text = _items[position];

            return view;
        }

        public override int Count
        {
            get { return _items.Count; }
        }

        public override string this[int position]
        {
            get { return _items[position]; }
        }
    }
}