﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using System.Globalization;
using RewindShared;

namespace RewindAndroid
{
    [Activity(Label = "NotificheTimeAdapter")]			
    public class NotificheTimeAdapter :BaseAdapter<NotificheItem>
    {
        List<NotificheItem> items;
        Activity context;
        int selectedIndex;
        int pos;

        public delegate void ItemClick(int index);

        public event ItemClick ItemClickIdEvent;

        public delegate void RowClick(int index);

        public event RowClick RowClickEvent;



        public NotificheTimeAdapter(Activity _context, List<NotificheItem> _items, int _selIndex, int _pos)
            : base()
        {
            this.context = _context;
            this.items = _items;
            this.selectedIndex = _selIndex;
            this.pos = _pos;
        }

        public override NotificheItem this [int position]
        {
            get { return items[position]; }
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override int Count
        {
            get { return items.Count; }
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View view = convertView;
            if (view == null)
                view = context.LayoutInflater.Inflate(Resource.Layout.NotificheTimeItemLayout, null);
            var item = items[position];
            try
            {
                LinearLayout boxTime = view.FindViewById<LinearLayout>(Resource.Id.boxTime);
                LinearLayout lyImag = view.FindViewById<LinearLayout>(Resource.Id.lyImag);

                ImageView imgarrowRight = view.FindViewById<ImageView>(Resource.Id.imgArrowRight);
                TextView txtDate = view.FindViewById<TextView>(Resource.Id.txtDate);
                TextView txtHour = view.FindViewById<TextView>(Resource.Id.txtHour);


                Typeface OswaldRegualr = TypeFaces.getTypeface(context, "Oswald-Regular.ttf");
                txtDate.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtHour.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);


                txtDate.Text = item.Data.Date.ToString("dd.MM yyyy", CultureInfo.CurrentCulture);
                txtHour.Text = item.Data.Hour.ToString() + ":" + item.Data.Minute.ToString();
                txtHour.Visibility=ViewStates.Gone;

                boxTime.SetBackgroundColor(Android.Graphics.Color.Rgb(0, 95, 167));
                lyImag.Visibility = ViewStates.Invisible;

                if (selectedIndex == -1)
                {
                    view.Click += OnItemClick;
                    view.Tag = position;
                }

                view.Click += OnRowClick;
                view.Tag = position;

                if (pos == position)
                {
                    lyImag.Visibility = ViewStates.Visible;
                    boxTime.SetBackgroundColor(Android.Graphics.Color.Rgb(76, 76, 76));
                }
                else
                {

                    lyImag.Visibility = ViewStates.Invisible;

                }

			
			
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
            return view;
        }


        public void OnItemClick(object sender, EventArgs e)
        {
            try
            {
                View v = (View)sender;
                v.Click -= OnItemClick;
                var position = (int)v.Tag;
                ItemClickIdEvent(position);
			
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void OnRowClick(object sender, EventArgs e)
        {
            try
            {
                View v = (View)sender;
                v.Click -= OnRowClick;
                var position = (int)v.Tag;
                if(RowClickEvent!=null)
                {
                    RowClickEvent(position);
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

    }
}

