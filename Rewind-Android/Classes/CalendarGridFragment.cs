﻿using System;
using Android.OS;
using Android.App;
using Android.Support.V4.View;
using Android.Views;
using Java.Util;
using Android.Widget;
using RewindShared;
using Android.Graphics;
using Android.Text.Format;

namespace RewindAndroid
{
    public class CalendarGridFragment: Android.Support.V4.App.Fragment
    {
        private static string IMAGE_DATA_EXTRA = "resId";
        private int mImageNum;

        public delegate void DatetimePickDelegate (DateTime date_from_to);

        public event DatetimePickDelegate datePickEvent;
       
        static ViewPager viewpg;
        static Activity context;

        int count = 1;
        public Calendar month;
        public CalendarAdapter adapter;
        public ArrayList items;
        public Handler handler;
        TextView title ;
        GridView gridview;
        DateTime newDate=DateTime.Now.AddMonths(-59);
        TextView titleYear;

//        static DateFormat dateFormat;// = Android.Text.Format.DateFormat.GetDateFormat(context);
        public static CalendarGridFragment newInstance(int imageNum, Activity _context, ViewPager viewPager)
        {
            CalendarGridFragment f = new CalendarGridFragment();
            viewpg = viewPager;
            context = _context;
//            dateFormat = Android.Text.Format.DateFormat.GetDateFormat(_context);
           
            try
            {
                Bundle args = new Bundle();
                args.PutInt(IMAGE_DATA_EXTRA, imageNum);

                f.Arguments = args;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            return f;
        }

        public CalendarGridFragment()
        {
            month = Calendar.Instance;
        }

        public override void OnCreate(Bundle savedInstanceState)
        {
            try
            {
                base.OnCreate(savedInstanceState);
                mImageNum = Arguments != null ? Arguments.GetInt(IMAGE_DATA_EXTRA) : -1;


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }



        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {

            View v = inflater.Inflate(Resource.Layout.CalendarViewLayout, container, false);

            try
            {
                Typeface OswaldRegualr = TypeFaces.getTypeface(context, "Oswald-Regular.ttf");

                gridview = v.FindViewById<GridView>(Resource.Id.gridview);
//                gridview.SetAdapter(adapter);
                gridview.ItemClick += grid_click;

                title  = v.FindViewById<TextView>(Resource.Id.title);
                title.Text = Android.Text.Format.DateFormat.Format("MMMM",month).ToUpper();//"MMMM yyyy

//                title.Text = Android.Text.Format.DateFormat.GetDateFormat(context).Format(month.Time).ToString(System.Globalization.CultureInfo.CurrentCulture);

                title.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                titleYear=v.FindViewById<TextView>(Resource.Id.titleYear);

                titleYear.Text= Android.Text.Format.DateFormat.Format("yyyy",month);
                titleYear.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                ImageView previous =  v.FindViewById<ImageView>(Resource.Id.previous);
                previous.Click += previous_click; 

                ImageView next =  v.FindViewById<ImageView> (Resource.Id.next);
                next.Click += next_click;

                ImageView previousYear =  v.FindViewById<ImageView>(Resource.Id.imgYearLeft);
                previousYear.Click += previous_Year_click;; 

                ImageView nextYear =  v.FindViewById<ImageView> (Resource.Id.imgYearRight);
                nextYear.Click += next_Year_click;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            return v;
        }





        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            try
            {
                base.OnActivityCreated(savedInstanceState);

                //Static date set
                System.String date = DateTime.Now.Date.ToString("yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);//"2015-07-31";
                char[] splitchar = { '-' };
                System.String[] dateArr = date.Split(splitchar); // date format is yyyy-mm-dd

                //month.set(Integer.parseInt(dateArr[0]), Integer.parseInt(dateArr[1]), Integer.parseInt(dateArr[2]));
                //month.Set(Int16.Parse(dateArr[0]), Int16.Parse(dateArr[1]),Int16.Parse(dateArr[2]));
                newDate=newDate.AddMonths(mImageNum);
                month.Set(newDate.Year,newDate.Month,newDate.Day);

                items = new ArrayList();
                System.Threading.ThreadPool.QueueUserWorkItem (state => 
                    { 
					try
						{
							adapter = new CalendarAdapter(context, month);
							gridview.Adapter = adapter;
							//date_date ();
							refreshCalendar();
						}
						catch (Exception ex)
						{
							Utils.writeToDeviceLog(ex);
						}
                    });
                
               

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }


        public void previous_click(object sender, EventArgs e)
        {
            try
            {
                if(month.Get(Calendar.Month) == month.GetActualMinimum(Calendar.Month)) {               
                    month.Set((month.Get(Calendar.Year)-1),month.GetActualMaximum(Calendar.Month),1);
                } else {
                    month.Set(Calendar.Month,month.Get(Calendar.Month)-1);
                }

                viewpg.CurrentItem=viewpg.CurrentItem-1;
//                refreshCalendar();
                title.Text = Android.Text.Format.DateFormat.Format("MMMM",month).ToUpper();
                titleYear.Text = Android.Text.Format.DateFormat.Format("yyyy",month);
                System.Threading.ThreadPool.QueueUserWorkItem (state => 
                    { 
                        adapter.refreshDays();
                        adapter.NotifyDataSetChanged();
                    });
                
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
           
        }

        public void next_click(object sender, EventArgs e){
            try
            {
                if(month.Get(Calendar.Month)== month.GetActualMaximum(Calendar.Month)) {                
                    month.Set((month.Get(Calendar.Year)+1),month.GetActualMinimum(Calendar.Month),1);
                } else {
                    month.Set(Calendar.Month,month.Get(Calendar.Month)+1);
                }
                viewpg.CurrentItem+=1;
//                refreshCalendar();
                title.Text = Android.Text.Format.DateFormat.Format("MMMM",month).ToUpper();
                titleYear.Text = Android.Text.Format.DateFormat.Format("yyyy",month);
                System.Threading.ThreadPool.QueueUserWorkItem (state => 
                    { 
                        adapter.refreshDays();
                        adapter.NotifyDataSetChanged();
                    });
               
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }


        }

        public void previous_Year_click(object sender, EventArgs e)
        {
//            if(month.Get(Calendar.Month) == month.GetActualMinimum(Calendar.Month)) {               
//                month.Set((month.Get(Calendar.Year)-1),month.GetActualMaximum(Calendar.Month),1);
//            } else {
//                month.Set(Calendar.Month,month.Get(Calendar.Month)-1);
//            }
//            refreshCalendar();
            try
            {
                month.Set(Calendar.Month,month.Get(Calendar.Month)-12);
                viewpg.CurrentItem-=12;
                refreshCalendar();
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }


        }

        public void next_Year_click(object sender, EventArgs e){
//            if(month.Get(Calendar.Month)== month.GetActualMaximum(Calendar.Month)) {                
//                month.Set((month.Get(Calendar.Year)+1),month.GetActualMinimum(Calendar.Month),1);
//            } else {
//                month.Set(Calendar.Month,month.Get(Calendar.Month)+1);
//            }
            try
            {
                viewpg.CurrentItem+=12;
                month.Set(Calendar.Month,month.Get(Calendar.Month)+12);
                refreshCalendar();
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
           
        }

        public void grid_click(object sender, AdapterView.ItemClickEventArgs e){
            try
            {
                TextView date = (TextView)e.View.FindViewById(Resource.Id.date);
                System.String day = date.Text.ToString ();

                if(day.Length == 1) {
                    day = "0"+day;
                }

                //Toast.MakeText (context, "Date --->" + Android.Text.Format.DateFormat.Format("yyyy-MM",month)+"-"+day, ToastLength.Short).Show ();
                if (datePickEvent != null)
                {
                    datePickEvent(DateTime.Parse(Android.Text.Format.DateFormat.Format("yyyy-MM", month) + "-" + day, System.Globalization.CultureInfo.InvariantCulture));
                } 
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void refreshCalendar()
        {
            adapter.refreshDays();
            adapter.NotifyDataSetChanged();
//            date_date();
            title.Text = Android.Text.Format.DateFormat.Format("MMMM",month).ToUpper();
            titleYear.Text = Android.Text.Format.DateFormat.Format("yyyy",month);
        }
        public void date_date(){
            items.Clear ();
            // format random values. You can implement a dedicated class to provide real values
            for(int i=0;i<31;i++) {
                System.Random r = new System.Random();

                int a = r.Next (10);
                if (a > 6) {
                    items.Add (i.ToString ());
                } else {
                    Console.WriteLine ("Ok");
                }

            }

            adapter.setItems(items);
            adapter.NotifyDataSetChanged ();
        }

        public override void OnDestroyView()
        {
            base.OnDestroyView();

            GC.Collect();
        }

        public override void OnDestroy()
        {
            base.OnDestroy();

            try
            {
                
                GC.Collect();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private readonly static DateTime jan1970 = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        private static Int64 TimeInMillis(DateTime dateTime)
        {
            return (Int64)(dateTime - jan1970).TotalMilliseconds;
        }

    }
}