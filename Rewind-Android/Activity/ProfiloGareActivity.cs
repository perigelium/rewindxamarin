﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading;
using Android.Graphics;
using System.Globalization;
using Android.Content.PM;
using Newtonsoft.Json.Linq;
using AndroidHUD;
using RewindShared;

namespace RewindAndroid
{
    [Activity(Label = "ProfiloGareActivity", ScreenOrientation = ScreenOrientation.Portrait, WindowSoftInputMode = SoftInput.StateAlwaysHidden)]			
	public class ProfiloGareActivity : BaseActivity
    {

        List<GareItem> itemsGare;
        ProfiloGareAdapter gareAdapeter;
        LinearLayout screenBackground;
        LinearLayout boxMenuBack;
        TextView txtDateBegin;
        TextView txtDateFinal;
        ListView listGare;
        UserLocalDB db;
        AndHUD progresV=new AndHUD();
        private const string DATE_FORMAT="MM/yyyy";
        PowerManager.WakeLock mWakeLock;

        protected override void OnCreate(Bundle bundle)
        {
            try
            {
                
//                progresV.Show(this, "Please wait", -1, MaskType.Clear, null, null, true, null); 
                base.OnCreate(bundle);
                db = new UserLocalDB();
                SetContentView(Resource.Layout.ProfiloGareLayout);
//                this.Window.SetFlags(WindowManagerFlags.KeepScreenOn, WindowManagerFlags.KeepScreenOn);
                PowerManager pm = (PowerManager)GetSystemService(Context.PowerService);
                mWakeLock = pm.NewWakeLock(WakeLockFlags.ScreenDim | WakeLockFlags.OnAfterRelease, "My Tag");

                screenBackground = FindViewById<LinearLayout>(Resource.Id.screenBackground);
                TextView textPos = FindViewById<TextView>(Resource.Id.textPos);
                TextView textAgente = FindViewById<TextView>(Resource.Id.textAgente);
                TextView textPun = FindViewById<TextView>(Resource.Id.textPun);
                TextView textPA = FindViewById<TextView>(Resource.Id.textPA);
                TextView textPS = FindViewById<TextView>(Resource.Id.textPS);
                TextView textSS = FindViewById<TextView>(Resource.Id.textSS);
                TextView txtTitle = FindViewById<TextView>(Resource.Id.txtTitle);
                TextView textDB = FindViewById<TextView>(Resource.Id.textDB);
                TextView textDF = FindViewById<TextView>(Resource.Id.textDF);


                txtDateBegin = FindViewById<TextView>(Resource.Id.txtDateBegin);
                txtDateFinal = FindViewById<TextView>(Resource.Id.txtDateFinal);
                boxMenuBack = FindViewById<LinearLayout>(Resource.Id.boxMenuBack);
                LinearLayout lyBeginDate = FindViewById<LinearLayout>(Resource.Id.lyBeginDate);
                LinearLayout lyFinalDate = FindViewById<LinearLayout>(Resource.Id.lyFinalDate);
                listGare = FindViewById<ListView>(Resource.Id.listGare);

                Typeface OswaldRegualr = TypeFaces.getTypeface(this, "Oswald-Regular.ttf");
                textPos.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                textAgente.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                textPun.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                textPA.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                textPS.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                textSS.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtTitle.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                textDB.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                textDF.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtDateBegin.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtDateFinal.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);

                Thread.CurrentThread.CurrentCulture = new CultureInfo("it-IT");
                txtDateBegin.Text = DateTime.Now.Date.ToString(DATE_FORMAT, CultureInfo.CurrentCulture);
                txtDateFinal.Text = DateTime.Now.Date.ToString(DATE_FORMAT, CultureInfo.CurrentCulture);


                LoadClasificationData(DateTime.Now.Date, DateTime.Now.Date,false);


                lyBeginDate.Click += (object sender, EventArgs e) =>
                {

                    OnClickPickDate(txtDateBegin);
                };

                lyFinalDate.Click += (object sender, EventArgs e) =>
                {

                    OnClickPickDate(txtDateFinal);
                };
                boxMenuBack.Click += OnBackPressedClick;

            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }


        public void LoadClasificationData(DateTime begin, DateTime end,bool init)
        {
            try
            {
                progresV.Show(this, "Attendere prego", -1, MaskType.Clear, TimeSpan.FromSeconds(120), null, true, null); 
                if(mWakeLock!=null)
                {
                    mWakeLock.Acquire();
                }
                System.Threading.ThreadPool.QueueUserWorkItem(state =>
                    { 
                        try 
                        {
//                            if(init)
//                            {
                                //true when select other period then initial one
                                if (Utils.CheckForInternetConn())
                                {
                                    itemsGare=getClassificationList(begin,end);
                                }
                                else
                                {
                                    //no internet connection
                                    RunOnUiThread(()=>{
                                        Toast.MakeText(this,"Nessuna connessione Internet",ToastLength.Short).Show();
                                    });
                                    itemsGare=db.getLastInsertedClassifications();
                                }
//                            }
//                            else
//                            {
//                                if (!db.CheckIfTableIsEmpty(Consts.PROFILE_CLASSIFICATIOB_TABLE_NAME))
//                                {
//                                    //if table is empty get classifications
//                                    if (Utils.CheckForInternetConn())
//                                    {
//                                        itemsGare=getClassificationList(begin,end);
//                                    }
//                                    else
//                                    {
//                                        //no internet connection
//                                        RunOnUiThread(()=>{
//                                            Toast.MakeText(this,"Nessuna connessione Internet",ToastLength.Short).Show();
//                                        });
//                                    }
//                                }
//                                else
//                                {
//                                    itemsGare=db.getLastInsertedClassifications();
//                                } 
//                            }

                            RunOnUiThread(()=>{
                                try {
                                    if(itemsGare!=null && itemsGare.Count>0)
                                    {
                                        gareAdapeter = new ProfiloGareAdapter(this, itemsGare);
                                        listGare.Adapter = gareAdapeter;
                                    }else
                                    {
                                        gareAdapeter = new ProfiloGareAdapter(this,new List<GareItem>());
                                        listGare.Adapter = gareAdapeter;
                                    }
                                } catch (Exception ex) {
                                    Utils.writeToDeviceLog(ex);
                                }
 
                            });
                        }
                        catch (Exception ex) 
                        {
                            Utils.writeToDeviceLog(ex);
                        }
                        finally
                        {
                            RunOnUiThread(()=>
                                {
                                    RunOnUiThread(()=>{
                                        progresV.Dismiss();
                                    });
                                    if(mWakeLock!=null)
                                    {
                                        mWakeLock.Release();
                                    }
                                });
                        }
                    });
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public List<GareItem> getClassificationList(DateTime begin, DateTime end)
        {
            List<GareItem> listClass = new List<GareItem>();
            string period = string.Empty;
            try
            {
                if (begin <= end)
                {
                    if (begin == end)
                    {
                        period = begin.ToString("yyyyMM", CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        int diffMonths = (end.Month + end.Year * 12) - (begin.Month + begin.Year * 12);
                        for (int i = diffMonths + 1; i > 1; i--)
                        {
                            period += end.ToString("yyyyMM", CultureInfo.InvariantCulture);
                            end = end.AddMonths(-1);
                            period += ",";
                        }
                        period += end.ToString("yyyyMM", CultureInfo.InvariantCulture);
                    }
//                    itemsGare = new List<GareItem>();

                    JToken clasificationJtoken = ApiCalls.getProfilClasificationandBonus(Utils.UserToken, period, Consts.API_PROFILE_CLASIFICATION_URL);
                    if (clasificationJtoken != null && clasificationJtoken.HasValues)
                    {
                        foreach (var item in clasificationJtoken)
                        {
                            GareItem item1;
                            item1 = new GareItem();
                            item1.Id = Int32.Parse(item["id"].ToString());
//                            item1.Data = DateTime.Now.Date.ToString("dd MMMMM yyyy", CultureInfo.InvariantCulture);
                            item1.Agente = item["name_agent"].ToString();
                            item1.Punti = item["punti"].ToString();
                            item1.PA = item["pa"].ToString();
                            item1.PS = item["ps"].ToString();
                            item1.SS = item["ss"].ToString();
                            listClass.Add(item1);
                        }
                    }

                    db.deleteTableName(Consts.PROFILE_CLASSIFICATIOB_TABLE_NAME);
                    foreach (var item in listClass) {
                        db.InsertClasificationToTable(item);
                    }
                }
                else
                {
                    //invalid input- end has to be grater than begin
                    RunOnUiThread(()=>{
                        Toast.MakeText(this,"Data inserita non valida: la data finale deve essere maggiore di quella iniziale",ToastLength.Short).Show();
                    });

                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
                listClass = null;
            }
            return listClass;
        }

        public void OnBackPressedClick(object sender, EventArgs e)
        {
            try
            {
                OnBackPressed();
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        void OnClickPickDate(TextView label)
        {

            try
            {

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                AlertDialog builder = alertDialogBuilder.Create();
                View view = this.LayoutInflater.Inflate(Resource.Layout.DateAndTimePicker, null);
                builder.SetView(view, 0, 0, 0, 0);

                TimePicker timePicker = view.FindViewById<TimePicker>(Resource.Id.orderTimePicker);
                DatePicker datePicker = view.FindViewById<DatePicker>(Resource.Id.orderDatePicker);
                datePicker.Visibility = ViewStates.Visible;
                timePicker.Visibility = ViewStates.Gone;
                if(label.Text!="")
                {
                    DateTime pickerDateHour;
                    DateTime.TryParseExact(label.Text, DATE_FORMAT, CultureInfo.CurrentCulture, DateTimeStyles.None, out pickerDateHour);
                    datePicker.DateTime=pickerDateHour;
//                    datePicker.DateTime=DateTime.Parse(label.Text, CultureInfo.InvariantCulture);
                }
                timePicker.SetIs24HourView(Java.Lang.Boolean.True);
                Button btn_OK = view.FindViewById<Button>(Resource.Id.step1DateTimeBtnOK);
                Button btn_Cancel = view.FindViewById<Button>(Resource.Id.step1DateTimeBtnCancel);

                datePicker.FindViewById(Android.Content.Res.Resources.System.GetIdentifier("day", "id", "android")).Visibility=ViewStates.Gone;

                btn_OK.Touch += (object sender2, View.TouchEventArgs e2) =>
                {
                    try
                    {
                        Button b = (Button)sender2;
                        if (e2.Event.Action == MotionEventActions.Down)
                        {
                            b.Background.SetColorFilter(new Color(100, 100, 100), PorterDuff.Mode.Multiply);
                        }
                        if (e2.Event.Action == MotionEventActions.Cancel)
                        {
                            b.Background.ClearColorFilter();
                        }
                        if (e2.Event.Action == MotionEventActions.Up)
                        {
                            b.Background.ClearColorFilter();
                            Thread.CurrentThread.CurrentCulture = new CultureInfo("it-IT");
                                label.Text = datePicker.DateTime.ToString(DATE_FORMAT, CultureInfo.CurrentCulture);

                            builder.Dismiss();

                                DateTime pickerStartDateHour;
                                DateTime.TryParseExact(txtDateBegin.Text, DATE_FORMAT, CultureInfo.CurrentCulture, DateTimeStyles.None, out pickerStartDateHour);

                                DateTime pickerEndDateHour;
                                DateTime.TryParseExact(txtDateFinal.Text, DATE_FORMAT, CultureInfo.CurrentCulture, DateTimeStyles.None, out pickerEndDateHour);

                                LoadClasificationData(pickerStartDateHour, pickerStartDateHour,true);
                        }
                    }
                    catch (Exception ex)
                    {
                        Utils.writeToDeviceLog(ex);
                    }
                };

                btn_Cancel.Touch += (object sender1, View.TouchEventArgs e1) =>
                {
                    try
                    {
                        Button b = (Button)sender1;
                        if (e1.Event.Action == MotionEventActions.Down)
                        {
                            b.Background.SetColorFilter(new Color(100, 100, 100), PorterDuff.Mode.Multiply);
                        }
                        if (e1.Event.Action == MotionEventActions.Cancel)
                        {
                            b.Background.ClearColorFilter();
                        }
                        if (e1.Event.Action == MotionEventActions.Up)
                        {
                            b.Background.ClearColorFilter();
                            builder.Dismiss();
                        }
                    }
                    catch (Exception ex)
                    {
                        Utils.writeToDeviceLog(ex);
                    }
                };

                builder.Show();
                builder.Window.SetLayout(RelativeLayout.LayoutParams.MatchParent, RelativeLayout.LayoutParams.WrapContent);


            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }


        }

        protected override void OnPause()
        {
            base.OnPause();
            if (progresV != null)
            {
                progresV.Dismiss();
            }
        }

        protected override void OnStop()
        {
            base.OnStop();
            try
            {
                GC.Collect();
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            try
            {
                GC.Collect();
                boxMenuBack.Click -= OnBackPressedClick;

                if (screenBackground != null)
                {
                    unbindDrawables(screenBackground);
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        private void unbindDrawables(View rootView)
        {
            try
            {
                if (rootView.Background != null)
                {
                    rootView.Background.SetCallback(null);
                }
                if (rootView is ViewGroup)
                {
                    for (int i = 0; i < ((ViewGroup)rootView).ChildCount; i++)
                    {
                        unbindDrawables(((ViewGroup)rootView).GetChildAt(i));

                        ((IDisposable)((ViewGroup)rootView).GetChildAt(i)).Dispose();
                    }

                    try
                    {
                        ((ViewGroup)rootView).RemoveAllViews();
                    }
                    catch (Java.Lang.UnsupportedOperationException mayHappen)
                    {
                        // AdapterViews, ListViews and potentially other ViewGroups don抰 support the removeAllViews operation
                        //Utils.writeToDeviceLog (mayHappen);
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

    }
}

