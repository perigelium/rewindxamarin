﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using System.Threading;
using System.Globalization;
using Android.Content.PM;
using Newtonsoft.Json.Linq;
using AndroidHUD;
using RewindShared;
using Java.Lang.Reflect;

namespace RewindAndroid
{
    [Activity(Label = "ProfiloBonusActivity", ScreenOrientation = ScreenOrientation.Portrait, WindowSoftInputMode = SoftInput.StateAlwaysHidden)]			
	public class ProfiloBonusActivity : BaseActivity
    {
        Typeface OswaldRegualr;
        List<BonusItem> itemsBonus;
        ProfiloBonusAdapter bonusAdapter;
        LinearLayout screenBackground;
        LinearLayout boxMenuBack;

        ListView listBonus;
        TextView txtTotal;
        TextView txtDateBegin;
        TextView txtDateFinal;
        UserLocalDB db;
        AndHUD progresV=new AndHUD();
        public const string DATE_FORMAT="MM/yyyy";
        PowerManager.WakeLock mWakeLock;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.ProfiloBonusLayout);

            try
            {
//                this.Window.SetFlags(WindowManagerFlags.KeepScreenOn, WindowManagerFlags.KeepScreenOn);
                PowerManager pm = (PowerManager)GetSystemService(Context.PowerService);
                mWakeLock = pm.NewWakeLock(WakeLockFlags.ScreenDim | WakeLockFlags.OnAfterRelease, "My Tag");

                db=new UserLocalDB();
                screenBackground=FindViewById<LinearLayout>(Resource.Id.screenBackground);
                TextView txtTitle = FindViewById<TextView>(Resource.Id.txtTitle);
                TextView textDB = FindViewById<TextView>(Resource.Id.textDB);
                TextView textDF = FindViewById<TextView>(Resource.Id.textDF);
                TextView textRS = FindViewById<TextView>(Resource.Id.textRS);
                TextView textP = FindViewById<TextView>(Resource.Id.textP);
                TextView textB = FindViewById<TextView>(Resource.Id.textB);
                TextView textBT = FindViewById<TextView>(Resource.Id.textBT);

                txtDateBegin = FindViewById<TextView>(Resource.Id.txtDateBegin);
                txtDateFinal = FindViewById<TextView>(Resource.Id.txtDateFinal);
                txtTotal = FindViewById<TextView>(Resource.Id.txtTotal);
                listBonus = FindViewById<ListView>(Resource.Id.listBonus);
                LinearLayout lyBeginDate = FindViewById<LinearLayout>(Resource.Id.lyBeginDate);
                LinearLayout lyFinalDate = FindViewById<LinearLayout>(Resource.Id.lyFinalDate);
                boxMenuBack = FindViewById<LinearLayout>(Resource.Id.boxMenuBack);

                //set fonds
                OswaldRegualr = TypeFaces.getTypeface(this, "Oswald-Regular.ttf");

                txtTitle.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                textDB.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                textDF.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                textRS.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                textP.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                textB.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                textBT.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtDateBegin.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtDateFinal.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtTotal.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);

                Thread.CurrentThread.CurrentCulture = new CultureInfo("it-IT");


                txtDateBegin.Text = DateTime.Now.Date.ToString(DATE_FORMAT, CultureInfo.CurrentCulture);
                txtDateFinal.Text = DateTime.Now.Date.ToString(DATE_FORMAT, CultureInfo.CurrentCulture);

                LoadBonusData( DateTime.Now.Date, DateTime.Now.Date,false);
                lyBeginDate.Click += (object sender, EventArgs e) =>
                {

                    OnClickPickDate(txtDateBegin);
                };

                lyFinalDate.Click += (object sender, EventArgs e) =>
                {

                    OnClickPickDate(txtDateFinal);

                };
                boxMenuBack.Click += OnBackPressedClick;
				
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void LoadBonusData(DateTime begin, DateTime end, bool init)
        {
            try
            {
                progresV.Show(this, "Attendere prego", -1, MaskType.Clear, TimeSpan.FromSeconds(120), null, true, null); 

                if(mWakeLock!=null)
                {
                    mWakeLock.Acquire();
                }

                System.Threading.ThreadPool.QueueUserWorkItem(state =>
                    { 
                        try 
                        {
//                            if(init)
//                            {
                                //true when select other period then initial one
                                if (Utils.CheckForInternetConn())
                                {
                                    itemsBonus=getBonusesList(begin,end);
                                }
                                else
                                {
                                    //no internet connection
                                    RunOnUiThread(()=>
                                        {
                                            Toast.MakeText(this,"Nessuna connessione Internet",ToastLength.Short).Show();
                                        });
                                    itemsBonus=db.getLastInsertedBonuses();
                                }
//                            }
//                            else
//                            {
//                                if (!db.CheckIfTableIsEmpty(Consts.PROFILE_BONUS_TABLE_NAME))
//                                {
//                                    //if table is empty get classifications
//                                    if (Utils.CheckForInternetConn())
//                                    {
//                                        itemsBonus=getBonusesList(begin,end);
//                                    }
//                                    else
//                                    {
//                                        //no internet connection
//                                        RunOnUiThread(()=>
//                                            {
//                                                Toast.MakeText(this,"Nessuna connessione Internet",ToastLength.Short).Show();
//                                            });
//                                    }
//                                }
//                                else
//                                {
//                                    itemsBonus=db.getLastInsertedBonuses();
//                                } 
//                            }

                            RunOnUiThread(()=>
                                {
                                    try 
                                    {
                                        if(itemsBonus!=null && itemsBonus.Count>0)
                                        {
                                            bonusAdapter = new ProfiloBonusAdapter(this, itemsBonus);
                                            listBonus.Adapter = bonusAdapter;
                                            float totalBonus = 0;
                                            foreach (var bonus in itemsBonus)
                                            {
                                                totalBonus += bonus.Bonus;
                                            }
                                            txtTotal.Text = totalBonus.ToString("0.00") + " €";
                                        }
                                        else
                                        {
                                            bonusAdapter = new ProfiloBonusAdapter(this, new List<BonusItem>());
                                            listBonus.Adapter = bonusAdapter;
                                            txtTotal.Text = "0,00 €";
                                        }    
                                    }
                                    catch (Exception ex) {
                                        Utils.writeToDeviceLog(ex);
                                    }

                                });
                        }
                        catch(Exception ex)
                        {
                            Utils.writeToDeviceLog(ex);
                        }
                        finally
                        {
                            RunOnUiThread(()=> {
                                if(progresV != null)
                                {
                                    progresV.Dismiss();
                                }

                                if(mWakeLock!=null)
                                {
                                    mWakeLock.Release();
                                }
                            });
                        }
                    });
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public List<BonusItem> getBonusesList(DateTime begin, DateTime end)
        {
            List<BonusItem> listBonus = new List<BonusItem>();
            string period = string.Empty;
            try
            {
                if (begin <= end)
                {
                    if(begin==end)
                    {
                        period=begin.ToString("yyyyMM", CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        int diffMonths = (end.Month + end.Year * 12) - (begin.Month + begin.Year * 12);
                        for (int i = diffMonths+1; i > 1; i--) {
                            period+=end.ToString("yyyyMM", CultureInfo.InvariantCulture);
                            end=end.AddMonths(-1);
                            period+=",";
                        }
                        period+=end.ToString("yyyyMM", CultureInfo.InvariantCulture);
                    }
//                    itemsBonus = new List<BonusItem>();

                    JToken bonusJtoken=ApiCalls.getProfilClasificationandBonus(Utils.UserToken,period,Consts.API_PROFILE_BONUS_URL);

                    if(bonusJtoken!=null && bonusJtoken.HasValues)
                    {
                        foreach (var item in bonusJtoken) {
                            BonusItem bonusItem = new BonusItem();
                            bonusItem.Id = item["id"].ToString();
                            bonusItem.Regionalesociale = item["ragione_sociale"].ToString();
                            bonusItem.Prodato = item["produs"].ToString();
                            if(item["bonus"].ToString()!="")
                            {
                                float bonus;
                                float.TryParse(item["bonus"].ToString(), NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingSign, CultureInfo.InvariantCulture, out bonus);

                                bonusItem.Bonus =bonus;
                            }
                           
                            listBonus.Add(bonusItem);
                        }
                    }

                    db.deleteTableName(Consts.PROFILE_BONUS_TABLE_NAME);
                    foreach (var item in listBonus) {
                        db.InsertBonusesToTable(item);
                    }
                }
                else
                {
                    //invalid input- end has to be grater than begin
                    RunOnUiThread(()=>{
                        Toast.MakeText(this,"Data inserita non valida: la data finale deve essere maggiore di quella iniziale",ToastLength.Short).Show();
                    });
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
                listBonus = null;
            }
            return listBonus;
        }

        public void OnBackPressedClick(object sender, EventArgs e)
        {
            try
            {
                OnBackPressed();
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        void OnClickPickDate(TextView label)
        {
            try
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                AlertDialog builder = alertDialogBuilder.Create();
                View view = this.LayoutInflater.Inflate(Resource.Layout.DateAndTimePicker, null);
                builder.SetView(view, 0, 0, 0, 0);

                TimePicker timePicker = view.FindViewById<TimePicker>(Resource.Id.orderTimePicker);
                DatePicker datePicker = view.FindViewById<DatePicker>(Resource.Id.orderDatePicker);
                if(label.Text!="")
                {
//                    char.ToUpper(label.Text[0]) + label.Text.Substring(1);
                    DateTime pickerDateHour;
                    DateTime.TryParseExact(label.Text.Trim(), DATE_FORMAT, CultureInfo.CurrentCulture,DateTimeStyles.None, out pickerDateHour);
                    datePicker.DateTime=pickerDateHour;
//                    datePicker.DateTime=DateTime.Parse(label.Text, CultureInfo.InvariantCulture);
                }

                datePicker.Visibility = ViewStates.Visible;
                timePicker.Visibility = ViewStates.Gone;
                timePicker.SetIs24HourView(Java.Lang.Boolean.True);
                Button btn_OK = view.FindViewById<Button>(Resource.Id.step1DateTimeBtnOK);
                Button btn_Cancel = view.FindViewById<Button>(Resource.Id.step1DateTimeBtnCancel);

                datePicker.FindViewById(Android.Content.Res.Resources.System.GetIdentifier("day", "id", "android")).Visibility=ViewStates.Gone;

//                try 
//                {
//
//                    Field[] f = datePicker.Class.GetDeclaredFields();
//                    foreach (Field field in f) {
//                        if (field.Name==("mDaySpinner")) {
//                            field.Accessible=true;
//                            Object dayPicker = new Object();
//                            dayPicker = field.Get(datePicker);
//                            ((View) dayPicker).Visibility=ViewStates.Gone;
//                        }
//                    }
//                }
//                catch (Exception ex) 
//                {
//                    Utils.writeToDeviceLog(ex);    
//                }

                btn_OK.Touch += (object sender2, View.TouchEventArgs e2) =>
                {
                    try
                    {
                        Button b = (Button)sender2;
                        if (e2.Event.Action == MotionEventActions.Down)
                        {
                            b.Background.SetColorFilter(new Color(100, 100, 100), PorterDuff.Mode.Multiply);
                        }
                        if (e2.Event.Action == MotionEventActions.Cancel)
                        {
                            b.Background.ClearColorFilter();
                        }
                        if (e2.Event.Action == MotionEventActions.Up)
                        {
                            b.Background.ClearColorFilter();
                            Thread.CurrentThread.CurrentCulture = new CultureInfo("it-IT");
                                label.Text = datePicker.DateTime.ToString(DATE_FORMAT, CultureInfo.CurrentCulture);

                            builder.Dismiss();

                                DateTime pickerStartDateHour;
                                DateTime.TryParseExact(txtDateBegin.Text, DATE_FORMAT, CultureInfo.CurrentCulture,DateTimeStyles.None, out pickerStartDateHour);

                                DateTime pickerEndDateHour;
                                DateTime.TryParseExact(txtDateFinal.Text, DATE_FORMAT, CultureInfo.CurrentCulture,DateTimeStyles.None, out pickerEndDateHour);

                                LoadBonusData(pickerStartDateHour, pickerStartDateHour,true);
                        }
                    }
                    catch (Exception ex)
                    {
                        Utils.writeToDeviceLog(ex);
                    }
                };

                btn_Cancel.Touch += (object sender1, View.TouchEventArgs e1) =>
                {
                    try
                    {
                        Button b = (Button)sender1;
                        if (e1.Event.Action == MotionEventActions.Down)
                        {
                            b.Background.SetColorFilter(new Color(100, 100, 100), PorterDuff.Mode.Multiply);
                        }
                        if (e1.Event.Action == MotionEventActions.Cancel)
                        {
                            b.Background.ClearColorFilter();
                        }
                        if (e1.Event.Action == MotionEventActions.Up)
                        {
                            b.Background.ClearColorFilter();
                            builder.Dismiss();
                        }
                    }
                    catch (Exception ex)
                    {
                        Utils.writeToDeviceLog(ex);
                    }
                };

                builder.Show();
                builder.Window.SetLayout(RelativeLayout.LayoutParams.MatchParent, RelativeLayout.LayoutParams.WrapContent);

			
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }

		
        }

        protected override void OnPause()
        {
            base.OnPause();
            if (progresV != null)
            {
                progresV.Dismiss();
            }
        }

        protected override void OnStop()
        {
            base.OnStop();
            try
            {
                GC.Collect();
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            try
            {
                GC.Collect();
                boxMenuBack.Click -= OnBackPressedClick;

                if (screenBackground != null)
                {
                    unbindDrawables(screenBackground);
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        private void unbindDrawables(View rootView)
        {
            try
            {
                if (rootView.Background != null)
                {
                    rootView.Background.SetCallback(null);
                }
                if (rootView is ViewGroup)
                {
                    for (int i = 0; i < ((ViewGroup)rootView).ChildCount; i++)
                    {
                        unbindDrawables(((ViewGroup)rootView).GetChildAt(i));

                        ((IDisposable)((ViewGroup)rootView).GetChildAt(i)).Dispose();
                    }

                    try
                    {
                        ((ViewGroup)rootView).RemoveAllViews();
                    }
                    catch (Java.Lang.UnsupportedOperationException mayHappen)
                    {
                        // AdapterViews, ListViews and potentially other ViewGroups don抰 support the removeAllViews operation
                        //Utils.writeToDeviceLog (mayHappen);
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }


    }
}

