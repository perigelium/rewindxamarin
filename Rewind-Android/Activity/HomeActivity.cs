
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Content.PM;
using Android.Graphics;
using AndroidHUD;
using RewindShared;

namespace RewindAndroid
{
    [Activity(Theme = "@style/Theme.Splash", ScreenOrientation = ScreenOrientation.Portrait, Label = "HomeLayout")]	//Theme = "@style/Theme.BackGround", 		
	public class HomeActivity : BaseActivity
    {
        AndHUD test = new AndHUD();
        LinearLayout homeLayoutBg;
        LinearLayout btnAppuntament;
        LinearLayout btnClienti;
        LinearLayout btnNotifiche;
        LinearLayout btnProfilo;
        PowerManager.WakeLock mWakeLock;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.HomeLayout);
            try
            {

//                this.Window.SetFlags(WindowManagerFlags.KeepScreenOn, WindowManagerFlags.KeepScreenOn);
                PowerManager pm = (PowerManager)GetSystemService(Context.PowerService);
                mWakeLock = pm.NewWakeLock(WakeLockFlags.ScreenDim | WakeLockFlags.OnAfterRelease, "My Tag");

                bool finish = Intent.GetBooleanExtra("finish", false);
                bool fromLogin = Intent.GetBooleanExtra("fromLogin", false);
                if (finish)
                {
                    Intent loginIntent = new Intent(this, typeof(LoginActivity));
                    loginIntent.SetFlags(ActivityFlags.ReorderToFront);
                    this.StartActivity(loginIntent);
                    Finish();
                    return;
                }
                homeLayoutBg = FindViewById<LinearLayout>(Resource.Id.homeLayoutBg);
                TextView textAppuntament = FindViewById<TextView>(Resource.Id.textAppuntament);
                TextView textClienti = FindViewById<TextView>(Resource.Id.textClienti);
                TextView textNotifiche = FindViewById<TextView>(Resource.Id.textNotifiche);
                TextView textProfilo = FindViewById<TextView>(Resource.Id.textProfilo);
                btnAppuntament = FindViewById<LinearLayout>(Resource.Id.btnAppuntament);
                btnClienti = FindViewById<LinearLayout>(Resource.Id.btnClienti);
                btnNotifiche = FindViewById<LinearLayout>(Resource.Id.btnNotifiche);
                btnProfilo = FindViewById<LinearLayout>(Resource.Id.btnProfilo);


                #region design

                Typeface OswaldRegualr = TypeFaces.getTypeface(this, "Oswald-Regular.ttf");

                textAppuntament.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                textClienti.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                textNotifiche.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                textProfilo.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);

                #endregion

                btnAppuntament.Click += OnClickAppuntament;
                btnClienti.Click += OnClickClienti;
                btnNotifiche.Click += OnClickNotifiche;
                btnProfilo.Click += OnClickProfilo;

                #region getStatus and ticket type
                if(!fromLogin)
                {
                    if(mWakeLock!=null)
                    {
                        mWakeLock.Acquire();
                    }
                    System.Threading.ThreadPool.QueueUserWorkItem(state =>
                        {   
                            try
                            {
                                if (Utils.CheckForInternetConn())
                                {
                                    Utils.AllStatuses = ApiCalls.getAllStatuses(Utils.UserToken);
                                    Utils.AllTicketTipes = ApiCalls.getAllTicketTypes(Utils.UserToken);

                                }
                            }
                            catch (Exception ex)
                            {
                                Utils.writeToDeviceLog(ex);
                            }
                            finally
                            {
                                if(mWakeLock!=null)
                                {
                                    mWakeLock.Release();
                                }
                            }
                        });
                }


                #endregion
				
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }

		
        }

        void OnClickAppuntament(object sender, EventArgs e)
        {
            try
            {
                test.Show(this, "Attendere prego", -1, MaskType.Clear, TimeSpan.FromSeconds(120), null, true, null); 
                Intent langIntent = new Intent(this, typeof(AppuntamentiAllActivity));
                langIntent.SetFlags(ActivityFlags.ReorderToFront);
                this.StartActivity(langIntent);
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        void OnClickClienti(object sender, EventArgs e)
        {
            try
            {
                test.Show(this, "Attendere prego", -1, MaskType.Clear, TimeSpan.FromSeconds(120), null, true, null); 
                Intent langIntent = new Intent(this, typeof(ClientiActivity));
                langIntent.SetFlags(ActivityFlags.ReorderToFront);
                this.StartActivity(langIntent);
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        void OnClickNotifiche(object sender, EventArgs e)
        {
            try
            {
                Intent langIntent = new Intent(this, typeof(NotificheActivity));
                langIntent.SetFlags(ActivityFlags.ReorderToFront);
                this.StartActivity(langIntent);
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }


        void OnClickProfilo(object sender, EventArgs e)
        {
            try
            {
                Intent langIntent = new Intent(this, typeof(ProfiloActivity));
                langIntent.SetFlags(ActivityFlags.ReorderToFront);
                this.StartActivity(langIntent);
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        protected override void OnStop()
        {
            base.OnStop();
            test.Dismiss();
            GC.Collect();
        }

        protected override void OnPause()
        {
            base.OnPause();
        }


        protected override void OnDestroy()
        {
            base.OnDestroy();
            try
            {
                GC.Collect();
                if (btnAppuntament != null)
                {
                    btnAppuntament.Click -= OnClickAppuntament;
                }
                if (btnClienti != null)
                {
                    btnClienti.Click -= OnClickClienti;
                }
                if (btnNotifiche != null)
                {
                    btnNotifiche.Click -= OnClickNotifiche;
                }
                if (btnProfilo != null)
                {
                    btnProfilo.Click -= OnClickProfilo;
                }
               
                if (homeLayoutBg != null)
                {
                    unbindDrawables(homeLayoutBg);
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        private void unbindDrawables(View rootView)
        {
            try
            {
                if (rootView.Background != null)
                {
                    rootView.Background.SetCallback(null);
                }
                if (rootView is ViewGroup)
                {
                    for (int i = 0; i < ((ViewGroup)rootView).ChildCount; i++)
                    {
                        unbindDrawables(((ViewGroup)rootView).GetChildAt(i));

                        ((IDisposable)((ViewGroup)rootView).GetChildAt(i)).Dispose();
                    }

                    try
                    {
                        ((ViewGroup)rootView).RemoveAllViews();
                    }
                    catch (Java.Lang.UnsupportedOperationException mayHappen)
                    {
                        // AdapterViews, ListViews and potentially other ViewGroups don抰 support the removeAllViews operation
                        //Utils.writeToDeviceLog (mayHappen);
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }
		
    }
}

