﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Content.PM;
using RewindShared;
using AndroidHUD;
using System.IO;


namespace RewindAndroid
{
    [Activity(Label = "ProfiloActivity", ScreenOrientation = ScreenOrientation.Portrait, WindowSoftInputMode = SoftInput.StateAlwaysHidden)]    	
    public class ProfiloActivity : BaseActivity
    {
        Typeface OswaldRegualr;
        LinearLayout profiloLayoutBg;
        LinearLayout butBonus;
        LinearLayout butInvito;
        LinearLayout butGare;
        LinearLayout boxMenuBack;
        LinearLayout butLogout;

        TextView txtName;
        TextView txtMail;
        TextView txtPhone;
        TextView txtAddress;
        TextView txtVAT;
        ImageButton butStar;
        UserLocalDB db = new UserLocalDB();
        AndHUD progresV = new AndHUD();
        PowerManager.WakeLock mWakeLock;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.ProfiloLayout);
            try
            {
//                this.Window.SetFlags(WindowManagerFlags.KeepScreenOn, WindowManagerFlags.KeepScreenOn);
                PowerManager pm = (PowerManager)GetSystemService(Context.PowerService);
                mWakeLock = pm.NewWakeLock(WakeLockFlags.ScreenDim | WakeLockFlags.OnAfterRelease, "My Tag");

                TextView textMail = FindViewById<TextView>(Resource.Id.textMail);
                TextView textPhone = FindViewById<TextView>(Resource.Id.textPhone);
                TextView textAddress = FindViewById<TextView>(Resource.Id.textAddress);
                TextView textVAT = FindViewById<TextView>(Resource.Id.textVAT);
                TextView txtTitle = FindViewById<TextView>(Resource.Id.txtTitle);
                TextView textGare = FindViewById<TextView>(Resource.Id.textGare);
                TextView textInvito = FindViewById<TextView>(Resource.Id.textInvito);
                TextView textBonus = FindViewById<TextView>(Resource.Id.textBonus);
                TextView textLogout = FindViewById<TextView>(Resource.Id.textLogout);


                txtName = FindViewById<TextView>(Resource.Id.txtName);
                txtMail = FindViewById<TextView>(Resource.Id.txtMail);
                txtPhone = FindViewById<TextView>(Resource.Id.txtPhone);
                txtAddress = FindViewById<TextView>(Resource.Id.txtAddress);
                txtVAT = FindViewById<TextView>(Resource.Id.txtVAT);
                ImageView imgProfile = FindViewById<ImageView>(Resource.Id.imgProfile);
                butStar = FindViewById<ImageButton>(Resource.Id.butStar);
                butBonus = FindViewById<LinearLayout>(Resource.Id.butBonus);
                butInvito = FindViewById<LinearLayout>(Resource.Id.butInvito);
                butGare = FindViewById<LinearLayout>(Resource.Id.butGare);
                boxMenuBack = FindViewById<LinearLayout>(Resource.Id.boxMenuBack);
                butLogout=FindViewById<LinearLayout>(Resource.Id.butLogout);


                profiloLayoutBg = FindViewById<LinearLayout>(Resource.Id.profiloLayoutBg);
                //set fonds
                OswaldRegualr = TypeFaces.getTypeface(this, "Oswald-Regular.ttf");
                textMail.SetTypeface(OswaldRegualr, TypefaceStyle.Bold);
                textPhone.SetTypeface(OswaldRegualr, TypefaceStyle.Bold);
                textAddress.SetTypeface(OswaldRegualr, TypefaceStyle.Bold);
                textVAT.SetTypeface(OswaldRegualr, TypefaceStyle.Bold);
                txtTitle.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                textGare.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                textInvito.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                textBonus.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtName.SetTypeface(OswaldRegualr, TypefaceStyle.Bold);
                txtMail.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtPhone.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtAddress.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtVAT.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                textLogout.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);

//                txtName.Text = "NAME\nFORENAME";
//                txtMail.Text = "e-mail@yahoo.com";
//                txtPhone.Text = "000000000000";
//                txtAddress.Text = "Bularga nr 8 , Iasi, Romania, cladirea Open Mind";
//                txtVAT.Text = "0000";
                loadProfileInfo();

                boxMenuBack.Click += OnBackPressedClick;
                butBonus.Click += OnButBonusPressedClick;
                butInvito.Click += OnButInvitoPressedClick;
                butGare.Click += OnButGarePressedClick;
                butLogout.Click += OnLogoutClick;

                if(File.Exists(RewindShared.Consts.USER_AVATAR_IMAGE_PATH))
                {
                    imgProfile.SetImageBitmap(BitmapFactory.DecodeFile(RewindShared.Consts.USER_AVATAR_IMAGE_PATH));
                }
				else
				{
					imgProfile.SetImageResource(Resource.Drawable.malecostume);
				}
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }


        }

        public void loadProfileInfo()
        {
            try
            {
                if (db.CheckIfExistsProfile())
                {
                    ProfileInfoItem profil = db.getProfileInfo();
                    txtName.Text = profil.Name + " " + profil.Surname;
                    txtMail.Text = profil.Mail;
					if (profil.Mobile!= "")
						txtPhone.Text = profil.Mobile.Substring(1);
					else
						txtPhone.Text = profil.Mobile;
                    txtAddress.Text = profil.Address;
                    txtVAT.Text=profil.Partita_iva;
                    if (profil.Is_in_top == "1")
                    {
                        butStar.Visibility = ViewStates.Visible;
                    }
                    else
                    {
                        butStar.Visibility = ViewStates.Gone;
                    }
                }
                else
                {
                    Toast.MakeText(this,"Siamo spiacenti, nessun profilo salvato",ToastLength.Short).Show();
                }
                    
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void OnButBonusPressedClick(object sender, EventArgs e)
        {
            try
            {
                var intent = new Intent();
                intent.SetClass(this, typeof(ProfiloBonusActivity));
                StartActivityForResult(intent, 14);
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void OnButInvitoPressedClick(object sender, EventArgs e)
        {
            try
            {
                var intent = new Intent();
                intent.SetClass(this, typeof(ProfiloFatturareActivity));
                StartActivityForResult(intent, 14);
				

            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void OnButGarePressedClick(object sender, EventArgs e)
        {
            try
            {
                var intent = new Intent();
                intent.SetClass(this, typeof(ProfiloGareActivity));
                StartActivityForResult(intent, 14);
				
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void OnBackPressedClick(object sender, EventArgs e)
        {
            try
            {
                OnBackPressed();
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void OnLogoutClick(object sender, EventArgs e)
        {
            try
            {
                progresV.Show(this, "Attendere prego", -1, MaskType.Clear, TimeSpan.FromSeconds(120), null, true, null); 


                if(mWakeLock!=null)
                {
                    mWakeLock.Acquire();
                }
                System.Threading.ThreadPool.QueueUserWorkItem(state =>
                    { 
                        try
                        {
                            if (Utils.CheckForInternetConn())
                            {
                                string success=ApiCalls.Logout(Utils.UserToken);

                                if(success.Contains("true"))
                                {
                                    RunOnUiThread(()=>{
                                        if (File.Exists(Utils.PersonalFolder + "/" + "userToken.txt"))
                                        {
                                            File.Delete(Utils.PersonalFolder + "/" + "userToken.txt");
                                        }

                                        if(File.Exists(RewindShared.Consts.USER_AVATAR_IMAGE_PATH))
                                        {
                                            File.Delete(RewindShared.Consts.USER_AVATAR_IMAGE_PATH);
                                        }

                                        Intent langIntent = new Intent(this, typeof(HomeActivity));
                                        langIntent.PutExtra("finish",true);
                                        //                                langIntent.SetFlags(ActivityFlags.ReorderToFront);
                                        langIntent.SetFlags(ActivityFlags.NewTask | ActivityFlags.ClearTop);
                                        this.StartActivity(langIntent);
                                        Finish();
                                    });

                                }
                                else
                                {
                                    RunOnUiThread(()=>{
                                        Toast.MakeText(this,"Riprova, per favore",ToastLength.Short).Show();
                                    });
                                }

                            }
                            else
                            {
                                //no internet connection
                                RunOnUiThread(()=>{
                                    Toast.MakeText(this,"Nessuna connessione Internet",ToastLength.Short).Show();
                                });
                               
                            }
                        }
                        catch (Exception ex)
                        {
                            Utils.writeToDeviceLog(ex);
                        }
                        finally
                        {
                            RunOnUiThread(() =>
                                {
                                    if (progresV != null)
                                    {
                                        progresV.Dismiss();
                                    }
                                    if(mWakeLock!=null)
                                    {
                                        mWakeLock.Release();
                                    }
                                });
                        }
                    });
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        protected override void OnPause()
        {
            base.OnPause();
            try
            {
                if (progresV != null)
                {
                    progresV.Dismiss();
                } 
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }

        }

        protected override void OnStop()
        {
            base.OnStop();
            try
            {
                GC.Collect();
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            try
            {
                GC.Collect();
                boxMenuBack.Click -= OnBackPressedClick;
                butBonus.Click -= OnButBonusPressedClick;
                butInvito.Click -= OnButInvitoPressedClick;
                butGare.Click -= OnButGarePressedClick;

                if (profiloLayoutBg != null)
                {
                    unbindDrawables(profiloLayoutBg);
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        private void unbindDrawables(View rootView)
        {
            try
            {
                if (rootView.Background != null)
                {
                    rootView.Background.SetCallback(null);
                }
                if (rootView is ViewGroup)
                {
                    for (int i = 0; i < ((ViewGroup)rootView).ChildCount; i++)
                    {
                        unbindDrawables(((ViewGroup)rootView).GetChildAt(i));

                        ((IDisposable)((ViewGroup)rootView).GetChildAt(i)).Dispose();
                    }

                    try
                    {
                        ((ViewGroup)rootView).RemoveAllViews();
                    }
                    catch (Java.Lang.UnsupportedOperationException mayHappen)
                    {
                        // AdapterViews, ListViews and potentially other ViewGroups don抰 support the removeAllViews operation
                        //Utils.writeToDeviceLog (mayHappen);
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }
    }
}

