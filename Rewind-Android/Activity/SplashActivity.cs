﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Content.PM;
using Android.Views.Animations;
using System.Threading;
using Mono.Data.Sqlite;
using System.IO;
using RewindShared;
using Xamarin;

namespace RewindAndroid
{
    [Activity(NoHistory = true, MainLauncher = true, ScreenOrientation = ScreenOrientation.Portrait)]	// 	, Theme = "@style/Theme.Splash"
	public class SplashScreen : BaseActivity
    {
        ImageView rotateImageBig;
        ImageView rotateImageSmall;
        RelativeLayout BoxInternet;
        LinearLayout BoxNoInternet;
        TextView textNoInternet;
        ImageView refreshImage;
        Animation rotateAboutCornerAnimation;
        Animation rotateAboutCornerAnimationS;
        LinearLayout splashLayoutBg;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.SpashLayout);
            try
            {
                

                splashLayoutBg=FindViewById<LinearLayout>(Resource.Id.splashLayoutBg);
                rotateImageBig = FindViewById<ImageView>(Resource.Id.rotateImageBig);
                rotateImageSmall = FindViewById<ImageView>(Resource.Id.rotateImageSmall);
                BoxInternet = FindViewById<RelativeLayout>(Resource.Id.BoxInternet);
                BoxNoInternet = FindViewById<LinearLayout>(Resource.Id.BoxNoInternet);
                textNoInternet = FindViewById<TextView>(Resource.Id.textNoInternet);
//                textNoInternet.Text = "Per utilizzare l'applicazione attivare la connessione ad Internet.";
                refreshImage = FindViewById<ImageView>(Resource.Id.refreshImage);
                BoxNoInternet.Touch += ClickRefresh;
                AnimationRotation();
                Utils.createDirectory();
//                initDatabase();
                if (File.Exists(Utils.PersonalFolder + "/" + "userToken.txt"))
                {
                    System.Threading.ThreadPool.QueueUserWorkItem(state =>
                        {   
                            Thread.Sleep(500);
                            try 
                            {
//                                if(Utils.CheckForInternetConn())
//                                {
//                                    Utils.allStatuses=ApiCalls.getAllStatuses(Utils.UserToken);
//                                    Utils.allTicketTipes=ApiCalls.getAllTicketTypes(Utils.UserToken);
//                                    if(Utils.allStatuses!=null && Utils.allStatuses.ToString()!="")
//                                    {
//                                        Utils.saveAppuntamentiStatus(Utils.allStatuses.ToString());
//                                    }
//                                    if(Utils.allTicketTipes!=null && Utils.allTicketTipes.ToString()!="")
//                                    {
//                                        Utils.saveTicketTypes(Utils.allTicketTipes.ToString());
//                                    }
//
//                                }
//                                else
//                                {
//                                    //get statuses from db
//                                    Utils.allStatuses=Utils.LoadAppuntamentiStatus();
//                                    Utils.allTicketTipes=Utils.LoadTicketTypes();
//                                }
                                RunOnUiThread(()=>{
                                    Intent homeIntent = new Intent(this, typeof(HomeActivity));
                                    homeIntent.SetFlags(ActivityFlags.ReorderToFront);
                                    this.StartActivity(homeIntent); 
                                });
                            } catch (Exception ex) 
                            {
                                Utils.writeToDeviceLog(ex);
                            }
                        });
                }
                else
                {
                    Intent langIntent = new Intent(this, typeof(LoginActivity));
                    langIntent.SetFlags(ActivityFlags.ReorderToFront);
                    this.StartActivity(langIntent);
                }



//                timerRefresh.Enabled = true;


            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
            finally
            {
                initDatabase();
            }
        }

        void ClickRefresh(object sender, View.TouchEventArgs e)
        {
            ImageView b = refreshImage;

            if (e.Event.Action == MotionEventActions.Down)
            {
                var rotateAboutCornerAnimation = AnimationUtils.LoadAnimation(this, Resource.Animation.refreshAnimation);
                b.StartAnimation(rotateAboutCornerAnimation);

            }
            if (e.Event.Action == MotionEventActions.Cancel)
            {
                b.ClearAnimation();
            }
            if (e.Event.Action == MotionEventActions.Up)
            {
                b.ClearAnimation();
                try
                {
                    if (Utils.CheckForInternetConn())
                    {
                        AnimationRotation();
                    }
                }
                catch (Exception ex)
                {
                    Utils.writeToDeviceLog(ex);
                }
            }
        }

        void AnimationRotation()
        {
            try
            {
                BoxNoInternet.Visibility = ViewStates.Gone;
                BoxInternet.Visibility = ViewStates.Visible;
                rotateAboutCornerAnimation = AnimationUtils.LoadAnimation(this, Resource.Animation.rotateAnimation);
                rotateImageBig.StartAnimation(rotateAboutCornerAnimation);
                rotateAboutCornerAnimationS = AnimationUtils.LoadAnimation(this, Resource.Animation.rotateAnimationSmall);
                rotateImageSmall.StartAnimation(rotateAboutCornerAnimationS);

            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        void AnimationRefresh()
        {
            BoxNoInternet.Visibility = ViewStates.Visible;
            BoxInternet.Visibility = ViewStates.Gone;
        }

        public void initDatabase()
        {
            try
            {
                //SqliteConnection.SetConfig (SQLiteConfig.Serialized);
                Utils.checkApplicationDocuments();
                UserLocalDB db = new UserLocalDB();
                ConnSingleton cs = ConnSingleton.getDbInstance();
                cs.GetDBConnection();  
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        protected override void OnStop()
        {
            base.OnStop();
            rotateAboutCornerAnimation.Dispose();
            rotateAboutCornerAnimationS.Dispose();

            try
            {
                GC.Collect();
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        protected override void OnPause()
        {
            base.OnPause();
        }


        protected override void OnDestroy()
        {
            base.OnDestroy();
            try
            {
                GC.Collect();
                BoxNoInternet.Touch -= ClickRefresh;
                if (splashLayoutBg != null) {
                    unbindDrawables (splashLayoutBg);
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        private void unbindDrawables (View rootView)
        {
            try {
                if (rootView.Background != null) {
                    rootView.Background.SetCallback (null);
                }
                if (rootView is ViewGroup) {
                    for (int i = 0; i < ((ViewGroup)rootView).ChildCount; i++) {
                        unbindDrawables (((ViewGroup)rootView).GetChildAt (i));

                        ((IDisposable)((ViewGroup)rootView).GetChildAt (i)).Dispose ();
                    }

                    try {
                        ((ViewGroup)rootView).RemoveAllViews ();
                    } catch (Java.Lang.UnsupportedOperationException mayHappen) {
                        // AdapterViews, ListViews and potentially other ViewGroups don抰 support the removeAllViews operation
                        //Utils.writeToDeviceLog (mayHappen);
                    }
                }
            } catch (Exception ex) {
                Utils.writeToDeviceLog (ex);
            }
        }
    }
}

