﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Content.PM;
using Android.Graphics;
using Newtonsoft.Json.Linq;
using AndroidHUD;
using System.Threading;
using RewindShared;
using System.IO;


namespace RewindAndroid
{
    [Activity(Label = "LoginActivity", ScreenOrientation = ScreenOrientation.Portrait, NoHistory=true)]		//Theme = "@style/Theme.Splash",	
	public class LoginActivity : BaseActivity
    {
        LinearLayout loginLayoutBg;
        AutoCompleteTextView txtUsername;
        EditText txtpassword;
        UserLocalDB db=new UserLocalDB();
        AndHUD progresV = new AndHUD();
        Button butLogIn;
        PowerManager.WakeLock mWakeLock;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.LoginLayout);
            try
            {
//                this.Window.SetFlags(WindowManagerFlags.KeepScreenOn, WindowManagerFlags.KeepScreenOn);
                PowerManager pm = (PowerManager)GetSystemService(Context.PowerService);
                mWakeLock = pm.NewWakeLock(WakeLockFlags.ScreenDim | WakeLockFlags.OnAfterRelease, "My Tag");

                txtUsername = FindViewById<AutoCompleteTextView>(Resource.Id.txtUsername);
                txtpassword = FindViewById<EditText>(Resource.Id.txtpassword);
                butLogIn = FindViewById<Button>(Resource.Id.butLogIn);

                loginLayoutBg=FindViewById<LinearLayout>(Resource.Id.loginLayoutBg);


                Typeface OswaldLight = TypeFaces.getTypeface(this, "Oswald-Light.ttf");
                Typeface OswaldRegualr = TypeFaces.getTypeface(this, "Oswald-Regular.ttf");
				
                txtUsername.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtpassword.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                butLogIn.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);


                butLogIn.Click += OnClickButLogIn;
//                txtUsername.Text="sebastian-macheda";
//                txtpassword.Text="lucaLodi";
//
//                txtUsername.Text="mirabela-amatiesei";
//                txtpassword.Text="dt2o1udA";
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        void OnClickButLogIn(object sender, EventArgs e)
        {
            try
            {
				string username = txtUsername.Text.ToLower().Trim();
				string password = txtpassword.Text.Trim();

                if (string.IsNullOrEmpty(username) && string.IsNullOrEmpty(password))
                {
					Toast.MakeText(this,"Inserire utente e password",ToastLength.Long).Show();
                }
                else if (string.IsNullOrEmpty(username))
                {
					Toast.MakeText(this,"Inserisci Utente",ToastLength.Long).Show();
                }
                else if (string.IsNullOrEmpty(password))
                {
					Toast.MakeText(this,"Inserire la password",ToastLength.Long).Show();
                }

                if (username != "" && password != "")
                {
                    string android_id = Android.Provider.Settings.Secure.GetString(this.ContentResolver,Android.Provider.Settings.Secure.AndroidId);
                    progresV.Show(this, "Attendere prego", -1, MaskType.Clear, TimeSpan.FromSeconds(120), null, true, null); 
                    if(mWakeLock!=null)
                    {
                        mWakeLock.Acquire();
                    }

                    ThreadPool.QueueUserWorkItem(state => {
                        try 
                        {
                            if(Utils.CheckForInternetConn())
                            {  
                                JObject obj=ApiCalls.HttpLogin(username,password,android_id);

                                if(obj!=null)
                                {
									if(!obj.First.ToString().Contains("error"))
                                    {
                                        Utils.UserToken=obj["token"].ToString();

                                        Utils.AllStatuses=ApiCalls.getAllStatuses(Utils.UserToken);
                                        Utils.AllTicketTipes=ApiCalls.getAllTicketTypes(Utils.UserToken);


                                        ProfileInfoItem profil=new ProfileInfoItem();
                                        profil.Address=obj["address"].ToString();
                                        profil.Id_agency=obj["id_agency"].ToString();
                                        profil.Id_agent=obj["id_agent"].ToString();
                                        profil.Id_user=obj["id_user"].ToString();
                                        profil.Is_in_top=obj["is_in_top"].ToString();
                                        profil.Mail=obj["mail"].ToString();
                                        profil.Mobile=obj["mobile"].ToString();
                                        profil.Name=obj["name"].ToString();
                                        profil.Surname=obj["surname"].ToString();
                                        profil.Number=obj["number"].ToString();
                                        profil.Partita_iva=obj["vat"].ToString();
                                        profil.Avatar = obj["avatar"].ToString();

//                                        if(db.CheckIfExistsProfile())
//                                        {
//                                            db.UpdateProfileInfo(profil);
//                                        }
//                                        else
//                                        {
//                                            db.InsertProfileInfoToTable(profil);
//                                        }



                                        Utils.saveUserAvatar(RewindShared.Consts.USER_AVATAR_URL + profil.Id_agent + "/" + profil.Avatar);

                                        if(Utils.IdAgent!=null)
                                        {
                                            if(obj["id_agent"].ToString()==Utils.IdAgent)
                                            {
                                                //same agent
                                                Utils.IdAgent=obj["id_agent"].ToString();
                                            }
                                            else
                                            {
                                                //new agent
                                                Utils.IdAgent=obj["id_agent"].ToString();
                                                Utils.deleteDB();
                                                Utils.checkApplicationDocuments();
                                                db = new UserLocalDB();
                                            }
                                        }
                                        else
                                        {
                                            //first time install
                                            Utils.IdAgent=obj["id_agent"].ToString();
                                        }
                                            
//                                        if (Directory.Exists(RewindShared.Consts.USER_DB_FOLDER_PATH))
//                                        {
//                                            Console.WriteLine("exisa");
//                                            if(File.Exists(RewindShared.Consts.USER_DB_FOLDER_PATH + "/" + RewindShared.Consts.USER_DB_NAME))
//                                            {
//                                                Console.WriteLine("exisa");
//                                            }
//                                        }
                                        if(db.CheckIfExistsProfile())
                                        {
                                            db.UpdateProfileInfo(profil);
                                        }
                                        else
                                        {
                                            db.InsertProfileInfoToTable(profil);
                                        }


                                        RunOnUiThread(()=>{
                                            if(Utils.UserValid)
                                            {
                                                Intent langIntent = new Intent(this, typeof(HomeActivity));
                                                langIntent.PutExtra("fromLogin",true);
                                                langIntent.SetFlags(ActivityFlags.ReorderToFront);
                                                this.StartActivity(langIntent);
                                            }
                                        });

                                    }
                                    else
                                    {
                                        RunOnUiThread(()=>{
											Toast.MakeText(this,obj["error"].ToString(),ToastLength.Long).Show();
                                        });

                                    }
                                }
                                else
                                {
                                    RunOnUiThread(()=>{
										Toast.MakeText(this,"Nome utente o password sbagliata",ToastLength.Long).Show();
                                    });
                                }
                            }
                            else
                            {
                                RunOnUiThread(()=>{
									Toast.MakeText(this,"Nessuna connessione Internet",ToastLength.Long).Show();
                                });
                            }
                        } 
                        catch (Exception ex) 
                        {
                            Utils.writeToDeviceLog(ex);
                        }
                        finally
                        {
                            RunOnUiThread(()=>
                                {
                                    if(progresV != null)
                                    {
                                        progresV.Dismiss();
                                    }
                                    if(mWakeLock!=null)
                                    {
                                        mWakeLock.Release();
                                    }
                                });
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }	
        }

        protected override void OnPause()
        {
            try
            {
                base.OnPause();

                if(progresV != null)
                {
                    progresV.Dismiss();
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        protected override void OnStop()
        {
            base.OnStop();
            try
            {
                GC.Collect();
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

//        public override void OnBackPressed()
//        {
//            base.OnBackPressed();
//            try
//            {
//               // MoveTaskToBack(true);
//                this.Finish();
//            }
//            catch (Exception ex)
//            {
//                Utils.writeToDeviceLog(ex);
//            }
//        }


        protected override void OnDestroy()
        {
            base.OnDestroy();
            try
            {
                GC.Collect();
                if(butLogIn!=null)
                {
                    butLogIn.Click -= OnClickButLogIn;
                }

                if (loginLayoutBg != null) {
                    unbindDrawables (loginLayoutBg);
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        private void unbindDrawables (View rootView)
        {
            try {
                if (rootView.Background != null) {
                    rootView.Background.SetCallback (null);
                }
                if (rootView is ViewGroup) {
                    for (int i = 0; i < ((ViewGroup)rootView).ChildCount; i++) {
                        unbindDrawables (((ViewGroup)rootView).GetChildAt (i));

                        ((IDisposable)((ViewGroup)rootView).GetChildAt (i)).Dispose ();
                    }

                    try {
                        ((ViewGroup)rootView).RemoveAllViews ();
                    } catch (Java.Lang.UnsupportedOperationException mayHappen) {
                        // AdapterViews, ListViews and potentially other ViewGroups don抰 support the removeAllViews operation
                        //Utils.writeToDeviceLog (mayHappen);
                    }
                }
            } catch (Exception ex) {
                Utils.writeToDeviceLog (ex);
            }
        }

    }


}

