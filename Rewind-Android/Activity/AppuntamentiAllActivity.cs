﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Views.Animations;
using Android.Content.PM;
using Android.Graphics;
using Android.Locations;
using Android.Gms.Maps;
using AndroidHUD;
using Android.Gms.Maps.Model;
using Android.Text;
using Android.Graphics.Drawables;
using System.Threading;
using System.Globalization;
using System.Xml;
using System.Net;
using System.IO;
using Android.Views.InputMethods;
using Newtonsoft.Json.Linq;
using Android.Support.V4.View;
using Java.Util;
using Android.Gms.Location;
using Android.Gms.Common;
using Android.Gms.Common.Apis;
using Android.Util;
using RewindShared;
using System.Threading.Tasks;

namespace RewindAndroid
{
    [Activity(Label = "AppuntamentiAllActivity", ScreenOrientation = ScreenOrientation.Portrait, WindowSoftInputMode = SoftInput.AdjustPan)]			
	public class AppuntamentiAllActivity : BaseFragmentActivity,Android.Gms.Location.ILocationListener,GoogleApiClient.IConnectionCallbacks,GoogleApiClient.IOnConnectionFailedListener
    {
        int kilometers=20;
        LinearLayout Events;
        LinearLayout AddEvent;
        RelativeLayout Map;
        LinearLayout Search;
        ViewAnimator Calendar;

        Typeface OswaldRegualr;
        Typeface OswaldLight;
        Typeface LucidaSansRegular;
        List<AppuntamentiItem> appuntamentiList;
        PopupWindow window;
        AlertDialog alertDialog;
        Location currLocation;
        LatLng location;

        //top Menu
        LinearLayout boxMenuBack;
        LinearLayout boxMenuAppointments;
        ImageView imgMenuAppointments;
        LinearLayout boxMenuCalendar;
        ImageView imgMenuCalendar;
        LinearLayout boxMenuMap;
        ImageView imgMenuMap;
        LinearLayout boxMenuSearch;
        ImageView imgMenuSearch;
        EditText editNote;
        LinearLayout screenBackground;
        ViewPager calendarPager;
        UserLocalDB db = new UserLocalDB();

        AndHUD progresV = new AndHUD();
        bool _isGooglePlayServicesInstalled;
        GoogleApiClient apiClient;
        LocationRequest locRequest;
        int androidPhoneVersion;
        FrameLayout map;
        PowerManager.WakeLock mWakeLock;
        string aagInBlocco = "";
        bool routeShow = false;
        ImageView imgChangeAppHour;
        bool statusChanged=false;
        bool statusChangedSearched=false;
		double zoomLevel=1;
		TextView txtAppImpPersEvento_Pers;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.AppuntamentiAllLayout);
            try
            {
//                this.Window.SetFlags(WindowManagerFlags.KeepScreenOn, WindowManagerFlags.KeepScreenOn);
                PowerManager pm = (PowerManager)GetSystemService(Context.PowerService);
                mWakeLock = pm.NewWakeLock(WakeLockFlags.ScreenDim | WakeLockFlags.OnAfterRelease, "My Tag");

                progresV.Show(this, "Attendere prego", -1, MaskType.Clear, TimeSpan.FromSeconds(120), null, true, null); 
                OswaldRegualr = TypeFaces.getTypeface(this, "Oswald-Regular.ttf");
                LucidaSansRegular = TypeFaces.getTypeface(this, "Lucida Sans Regular.ttf");
                OswaldLight = TypeFaces.getTypeface(this, "Oswald-Light.ttf");
			
                Events = FindViewById<LinearLayout>(Resource.Id.Events);
                AddEvent = FindViewById<LinearLayout>(Resource.Id.AddEvent);
                Map = FindViewById<RelativeLayout>(Resource.Id.Map);
                Search = FindViewById<LinearLayout>(Resource.Id.Search);
                Calendar = FindViewById<ViewAnimator>(Resource.Id.Calendar);
                screenBackground = FindViewById<LinearLayout>(Resource.Id.screenBackground);

                //topmenu
                boxMenuBack = FindViewById<LinearLayout>(Resource.Id.boxMenuBack);
                boxMenuAppointments = FindViewById<LinearLayout>(Resource.Id.boxMenuAppointments);
                imgMenuAppointments = FindViewById<ImageView>(Resource.Id.imgMenuAppointments);
                boxMenuCalendar = FindViewById<LinearLayout>(Resource.Id.boxMenuCalendar);
                imgMenuCalendar = FindViewById<ImageView>(Resource.Id.imgMenuCalendar);
                boxMenuMap = FindViewById<LinearLayout>(Resource.Id.boxMenuMap);
                imgMenuMap = FindViewById<ImageView>(Resource.Id.imgMenuMap);
                boxMenuSearch = FindViewById<LinearLayout>(Resource.Id.boxMenuSearch);
                imgMenuSearch = FindViewById<ImageView>(Resource.Id.imgMenuSearch);
                addMenuEventsForAll();
				zoomLevel = Utils.calculateZoomLevel((int)Utils.getScreenWidth());

                Events.Visibility = ViewStates.Visible;
                AddEvent.Visibility = ViewStates.Gone;
                Map.Visibility = ViewStates.Gone;
                Search.Visibility = ViewStates.Gone;
                Calendar.Visibility = ViewStates.Gone;
                imgSearchChangeAppHour=FindViewById<ImageView>(Resource.Id.imgSearchChangeAppHour);
                EventsLayout();
                CalendarLayout();
                AddEventLayout();
                MapLayout();
                SearchLayout();

                _isGooglePlayServicesInstalled = IsGooglePlayServicesInstalled();
                if (_isGooglePlayServicesInstalled)
                {
                    apiClient = new GoogleApiClient.Builder(this, this, this).AddApi(LocationServices.Api).Build();
                    locRequest = new LocationRequest();

                }
                else
                {
                    if (progresV != null)
                    {
                        progresV.Dismiss();
                    }
                    
                    Log.Error("OnCreate", "Google Play Services is not installed");
                    Toast.MakeText(this, "Google Play Services non è installato", ToastLength.Long).Show();
                }

                String androidOS = Build.VERSION.Sdk;
                androidPhoneVersion = Int32.Parse(androidOS);
                Console.WriteLine(androidOS + "---------versione name-------");

                markersList = new List<Marker>();
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }


        protected override void OnResume()
        {
            base.OnResume();
            try
            {
                if (apiClient != null)
                {
                    apiClient.Connect();
                }

                {
                    _mapSearchAppuntamenti = _mapFragmentSupport.Map;
                }
                {
                    _map = _mapFragmentASupport.Map;
                }
//                _map = _mapFragment.Map;
                //_mapA = _mapFragmentA.Map;
                //currLocation = Utils.requestNewLocation(this).Location;
//                System.Threading.ThreadPool.QueueUserWorkItem(state =>
//                    {
//                        Object obj = new Object();
//                        try
//                        {
//                            lock (obj)
//                            {
////                                currLocation = Utils.requestNewLocation(Context).Location;
//                            }
//                        }
//                        catch (Exception ex)
//                        {
//                            Utils.writeToDeviceLog(ex);
//                        }
//                        RunOnUiThread(() =>
//                            {
//                                SetupMapIfNeeded();
//                                SetupMapIfNeededEvents();
//                            });
//                    });
                

              
//                _map.MyLocationEnabled = true;
               
                // Setup a handler for when the user clicks on a marker.
                //_map.MarkerClick += MapOnMarkerClick;

                // _mapA.MyLocationEnabled = true;

                // Setup a handler for when the user clicks on a marker.
//                _map.MarkerClick += MapOnMarkerClickEvents;
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }

        }

        protected override async void OnPause()
        {
            try
            {
                base.OnPause();

                if (progresV != null)
                {
                    progresV.Dismiss();
                }

                if (apiClient.IsConnected)
                {
                    // stop location updates, passing in the LocationListener
                    await LocationServices.FusedLocationApi.RemoveLocationUpdates(apiClient, this);

                    apiClient.Disconnect();
                }
//                _map.MyLocationEnabled = false;
//
//                _map.MarkerClick -= MapOnMarkerClick;
//
//                _mapA.MyLocationEnabled = false;
//                _mapA.MarkerClick -= MapOnMarkerClickEvents;
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public override void OnBackPressed()
        {
            //base.OnBackPressed();
            try
            {
                if (AddEvent.Visibility == ViewStates.Visible || Map.Visibility == ViewStates.Visible || Search.Visibility == ViewStates.Visible || Calendar.Visibility == ViewStates.Visible)
                {
                    if (Calendar.Visibility == ViewStates.Visible)
                    {
                        expandOrCollapse(Calendar, "as");

                    }

                    if (map.Parent == null)
                    {
                        boxMaps.AddView(map);

                    }
                    if (imgAppdiOgiNavigatore.Parent == null)
                    {
                        boxMaps.AddView(imgAppdiOgiNavigatore);
                    }

                    boxMapSearch.RemoveView(mapBox);

                    Map.Visibility = ViewStates.Gone;
                    Search.Visibility = ViewStates.Gone;
                    AddEvent.Visibility = ViewStates.Gone;
                    Events.Visibility = ViewStates.Visible;
                    ItemMenuSelected("events");

                }
                else
                {
                    this.Finish();
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }


        }

        #region Events

        RelativeLayout linearNote;
        TextView txtEsito;
        TextView lblAppOggiEsito;
        LinearLayout detailsApuntamenti;
        RelativeLayout rlForListView;
        ApputamentiTimeAdapter timeListAdapter;
        ListView listApputamenti;
        TextView txtLocation;
        TextView txtName;
        TextView txtPhone;
        TextView txtMail;
        RelativeLayout boxMaps;
        LinearLayout lyBackground;

        private GoogleMap _mapSearchAppuntamenti;
        private MapFragment _mapFragmentA;
        private SupportMapFragment _mapFragmentASupport;
        private Location _currentLocationA;
        bool checkForGpsA = true;
        Polyline routeA;
        bool foundA = false;

        //custom pin inflate
        ViewGroup containerEvents;
        ImageView iconViewEvents;
        TextView textViewEvents;
        //dropdown status
        ImageView imgStatusArrow;
        LinearLayout boxEsito;
        LinearLayout btnSalva;
        LinearLayout btnEmail;
        LinearLayout btnPhone;
        ImageView imgAppdiOgiNavigatore;

        //new app di ogi details
        LinearLayout boxDetails;
        LinearLayout BoxAppuntamentiMoreDetails;
        TextView txtAppCodChiamataLabel;
        TextView txtAppCodChiamataValue;
        TextView txtAppDataChiamataLabel;
        TextView txtAppDataChiamataValue;
        TextView txtAppDataAppuntamentoLabel;
        TextView txtAppDataAppuntamentoValue;
        TextView txtAppOraAppuntamentoLabel;
        TextView txtAppOraAppuntamentoValue;
        TextView txtAppNoteAgenteLabel;
        TextView txtAppNoteAgenteValue;
        TextView txtAppNoteChiamataLabel;
        TextView txtAppNoteChiamataValue;
        TextView txtAppNoteSegretariaLabel;
        TextView txtAppNoteSegretariaValue;
        TextView txtAppMotiveLabel;
        TextView txtAppMotiveValue;
        TextView txtAppDatiClienteLabel;
        TextView txtAppRagioneSocialeLabel;
        TextView txtAppRagioneSocialeValue;
        TextView txtAppCategoriaLabel;
        TextView txtAppCategoriaValue;
        TextView txtAppIndirizzoLabel;
        TextView txtAppIndirizzoValue;
        TextView txtAppContattiLabel;
        TextView txtAppContattiValue;
        TextView txtAppDescription;

        TextView txtAppIndirizzoLine2;
        LinearLayout lyContattiLine2;
        LinearLayout lyContattiLine3;
        LinearLayout lyContattiLine4;

        TextView txtAppContattiLine2;
        TextView txtAppContattiLine3;
        TextView txtAppContattiLine4;

        LinearLayout LaydetailsImpPersonale;
        TextView lblDetailsAggImpDataDal;
        TextView lblDetailsAggImpDataDalValue;
        TextView lblDetailsAggImpDataAl;
        TextView lblDetailsAggImpDataAlValue;
        TextView lblDetailsAggImpOra;
        TextView lblDetailsAggImpOraValue;
        TextView lblDetailsAggImpLuogo;
        TextView lblDetailsAggImpLuogoValue;

        TextView txtAppImpPersDescription;
        LinearLayout boxButtons;
        ScrollView scrlAppOggDetails;

        LinearLayout LaydetailsAppMemo;
        TextView lblDetailsAggImpTitle;
        TextView lblDetailsAppMemoPlace;
        TextView lblDetailsAppMemoPlaceValue;
        TextView lblDetailsAppMemoTitle;
        TextView lblDetailsAppMemoTitleValue;
        TextView txtAppMemoDescription;
        TextView txtMemovaiAiClient;
        LinearLayout boxMemoGottoClientDetail;
		TextView lblDetailsAggMemoTitle;

        void EventsLayout()
        {
            try
            {
                listApputamenti = FindViewById<ListView>(Resource.Id.listApputamenti);

                boxEsito = FindViewById<LinearLayout>(Resource.Id.boxEsito);
                lyBackground = FindViewById<LinearLayout>(Resource.Id.lyBackground);

                linearNote = FindViewById<RelativeLayout>(Resource.Id.linearNote);

                txtEsito = FindViewById<TextView>(Resource.Id.txtEsito);
                lblAppOggiEsito = FindViewById<TextView>(Resource.Id.lblAppOggiEsito);

                detailsApuntamenti = FindViewById<LinearLayout>(Resource.Id.detailsAputtamenti);
                rlForListView = FindViewById<RelativeLayout>(Resource.Id.rlForListView);
                txtLocation = FindViewById<TextView>(Resource.Id.txtLocation);
                txtName = FindViewById<TextView>(Resource.Id.txtName);
                txtPhone = FindViewById<TextView>(Resource.Id.txtPhone);
                txtMail = FindViewById<TextView>(Resource.Id.txtMail);
                boxMaps = FindViewById<RelativeLayout>(Resource.Id.boxMaps);
                imgStatusArrow = FindViewById<ImageView>(Resource.Id.imgStatusArrow);
                editNote = FindViewById<EditText>(Resource.Id.editNote);
                btnSalva = FindViewById<LinearLayout>(Resource.Id.btnSalva);
                btnEmail = FindViewById<LinearLayout>(Resource.Id.btnEmail);
                btnPhone = FindViewById<LinearLayout>(Resource.Id.btnPhone);
                TextView txtSalva = FindViewById<TextView>(Resource.Id.txtSalva);
                map = FindViewById<FrameLayout>(Resource.Id.map);
                imgAppdiOgiNavigatore = FindViewById<ImageView>(Resource.Id.imgAppdiOgiNavigatore);
                boxButtons = FindViewById<LinearLayout>(Resource.Id.boxButtons);
                imgChangeAppHour = FindViewById<ImageView>(Resource.Id.imgChangeAppHour);
                //new app di oggi details

                boxDetails = FindViewById<LinearLayout>(Resource.Id.boxDetails);
                BoxAppuntamentiMoreDetails = FindViewById<LinearLayout>(Resource.Id.BoxAppuntamentiMoreDetails);

                txtAppCodChiamataLabel = FindViewById<TextView>(Resource.Id.txtAppCodChiamataLabel);
                txtAppCodChiamataValue = FindViewById<TextView>(Resource.Id.txtAppCodChiamataValue);
                txtAppDataChiamataLabel = FindViewById<TextView>(Resource.Id.txtAppDataChiamataLabel);
                txtAppDataChiamataValue = FindViewById<TextView>(Resource.Id.txtAppDataChiamataValue);
                txtAppDataAppuntamentoLabel = FindViewById<TextView>(Resource.Id.txtAppDataAppuntamentoLabel);
                txtAppDataAppuntamentoValue = FindViewById<TextView>(Resource.Id.txtAppDataAppuntamentoValue);
                txtAppOraAppuntamentoLabel = FindViewById<TextView>(Resource.Id.txtAppOraAppuntamentoLabel);
                txtAppOraAppuntamentoValue = FindViewById<TextView>(Resource.Id.txtAppOraAppuntamentoValue);
                txtAppNoteAgenteLabel = FindViewById<TextView>(Resource.Id.txtAppNoteAgenteLabel);
                txtAppNoteAgenteValue = FindViewById<TextView>(Resource.Id.txtAppNoteAgenteValue);
                txtAppNoteChiamataLabel = FindViewById<TextView>(Resource.Id.txtAppNoteChiamataLabel);
                txtAppNoteChiamataValue = FindViewById<TextView>(Resource.Id.txtAppNoteChiamataValue);
                txtAppNoteSegretariaLabel = FindViewById<TextView>(Resource.Id.txtAppNoteSegretariaLabel);
                txtAppNoteSegretariaValue = FindViewById<TextView>(Resource.Id.txtAppNoteSegretariaValue);
                txtAppMotiveLabel = FindViewById<TextView>(Resource.Id.txtAppMotiveLabel);
                txtAppMotiveValue = FindViewById<TextView>(Resource.Id.txtAppMotiveValue);
                txtAppDatiClienteLabel = FindViewById<TextView>(Resource.Id.txtAppDatiClienteLabel);
                txtAppRagioneSocialeLabel = FindViewById<TextView>(Resource.Id.txtAppRagioneSocialeLabel);
                txtAppRagioneSocialeValue = FindViewById<TextView>(Resource.Id.txtAppRagioneSocialeValue);
                txtAppCategoriaLabel = FindViewById<TextView>(Resource.Id.txtAppCategoriaLabel);
                txtAppCategoriaValue = FindViewById<TextView>(Resource.Id.txtAppCategoriaValue);
                txtAppIndirizzoLabel = FindViewById<TextView>(Resource.Id.txtAppIndirizzoLabel);
                txtAppIndirizzoValue = FindViewById<TextView>(Resource.Id.txtAppIndirizzoValue);
                txtAppContattiLabel = FindViewById<TextView>(Resource.Id.txtAppContattiLabel);
                txtAppContattiValue = FindViewById<TextView>(Resource.Id.txtAppContattiValue);

                txtAppDescription = FindViewById<TextView>(Resource.Id.txtAppDescription);

                txtAppIndirizzoLine2 = FindViewById<TextView>(Resource.Id.txtAppIndirizzoLine2);
                lyContattiLine2 = FindViewById<LinearLayout>(Resource.Id.lyContattiLine2);
                lyContattiLine3 = FindViewById<LinearLayout>(Resource.Id.lyContattiLine3);
                lyContattiLine4 = FindViewById<LinearLayout>(Resource.Id.lyContattiLine4);

                txtAppContattiLine2 = FindViewById<TextView>(Resource.Id.txtAppContattiLine2);
                txtAppContattiLine3 = FindViewById<TextView>(Resource.Id.txtAppContattiLine3);
                txtAppContattiLine4 = FindViewById<TextView>(Resource.Id.txtAppContattiLine4);

                //impegno personale details
                LaydetailsImpPersonale = FindViewById<LinearLayout>(Resource.Id.LaydetailsImpPersonale);
                lblDetailsAggImpDataDal = FindViewById<TextView>(Resource.Id.lblDetailsAggImpDataDal);
                lblDetailsAggImpDataDalValue = FindViewById<TextView>(Resource.Id.lblDetailsAggImpDataDalValue);    
                lblDetailsAggImpDataAl = FindViewById<TextView>(Resource.Id.lblDetailsAggImpDataAl); 
                lblDetailsAggImpDataAlValue = FindViewById<TextView>(Resource.Id.lblDetailsAggImpDataAlValue); 
                lblDetailsAggImpOra = FindViewById<TextView>(Resource.Id.lblDetailsAggImpOra); 
                lblDetailsAggImpOraValue = FindViewById<TextView>(Resource.Id.lblDetailsAggImpOraValue); 
                lblDetailsAggImpLuogo = FindViewById<TextView>(Resource.Id.lblDetailsAggImpLuogo); 
                lblDetailsAggImpLuogoValue = FindViewById<TextView>(Resource.Id.lblDetailsAggImpLuogoValue);
                txtAppImpPersDescription = FindViewById<TextView>(Resource.Id.txtAppImpPersDescription);
				txtAppImpPersEvento_Pers=FindViewById<TextView>(Resource.Id.txtAppImpPersEvento_Pers);

                //memo details
                LaydetailsAppMemo = FindViewById<LinearLayout>(Resource.Id.LaydetailsAppMemo);
                lblDetailsAggImpTitle = FindViewById<TextView>(Resource.Id.lblDetailsAggImpTitle);
				lblDetailsAggMemoTitle = FindViewById<TextView>(Resource.Id.lblDetailsAggMemoTitle);
                lblDetailsAppMemoPlace = FindViewById<TextView>(Resource.Id.lblDetailsAppMemoPlace);
                lblDetailsAppMemoPlaceValue = FindViewById<TextView>(Resource.Id.lblDetailsAppMemoPlaceValue);
                lblDetailsAppMemoTitle = FindViewById<TextView>(Resource.Id.lblDetailsAppMemoTitle);
                lblDetailsAppMemoTitleValue = FindViewById<TextView>(Resource.Id.lblDetailsAppMemoTitleValue);
                txtAppMemoDescription = FindViewById<TextView>(Resource.Id.txtAppMemoDescription);
                txtMemovaiAiClient = FindViewById<TextView>(Resource.Id.txtMemovaiAiClient);
                boxMemoGottoClientDetail = FindViewById<LinearLayout>(Resource.Id.boxMemoGottoClientDetail);

                scrlAppOggDetails = FindViewById<ScrollView>(Resource.Id.scrlAppOggDetails);

                //set fonts
                txtLocation.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtName.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtPhone.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtMail.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtEsito.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                lblAppOggiEsito.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                editNote.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtSalva.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);

                txtAppCodChiamataLabel.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtAppDataChiamataLabel.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtAppDataAppuntamentoLabel.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtAppOraAppuntamentoLabel.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtAppNoteAgenteLabel.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtAppNoteChiamataLabel.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtAppNoteSegretariaLabel.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtAppMotiveLabel.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtAppDatiClienteLabel.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtAppRagioneSocialeLabel.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtAppCategoriaLabel.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtAppIndirizzoLabel.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtAppContattiLabel.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);

                txtAppCodChiamataValue.SetTypeface(OswaldLight, TypefaceStyle.Normal);
                txtAppDataChiamataValue.SetTypeface(OswaldLight, TypefaceStyle.Normal);
                txtAppDataAppuntamentoValue.SetTypeface(OswaldLight, TypefaceStyle.Normal);
                txtAppOraAppuntamentoValue.SetTypeface(OswaldLight, TypefaceStyle.Normal);
                txtAppNoteAgenteValue.SetTypeface(OswaldLight, TypefaceStyle.Normal);
                txtAppNoteChiamataValue.SetTypeface(OswaldLight, TypefaceStyle.Normal);
                txtAppNoteSegretariaValue.SetTypeface(OswaldLight, TypefaceStyle.Normal);
                txtAppMotiveValue.SetTypeface(OswaldLight, TypefaceStyle.Normal);
                txtAppRagioneSocialeValue.SetTypeface(OswaldLight, TypefaceStyle.Normal);
                txtAppCategoriaValue.SetTypeface(OswaldLight, TypefaceStyle.Normal);
                txtAppIndirizzoValue.SetTypeface(OswaldLight, TypefaceStyle.Normal);
                txtAppContattiValue.SetTypeface(OswaldLight, TypefaceStyle.Normal);
                txtAppDescription.SetTypeface(OswaldLight, TypefaceStyle.Normal);

                lblDetailsAggImpDataDal.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                lblDetailsAggImpDataDalValue.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                lblDetailsAggImpDataAl.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                lblDetailsAggImpDataAlValue.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                lblDetailsAggImpOra.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                lblDetailsAggImpOraValue.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                lblDetailsAggImpLuogo.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                lblDetailsAggImpLuogoValue.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);

                lblDetailsAggImpTitle.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                lblDetailsAppMemoPlace.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                lblDetailsAppMemoPlaceValue.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                lblDetailsAppMemoTitle.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                lblDetailsAppMemoTitleValue.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtAppMemoDescription.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtMemovaiAiClient.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
				lblDetailsAggMemoTitle.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);

                txtAppImpPersDescription.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
				txtAppImpPersEvento_Pers.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);

                txtAppIndirizzoLine2.SetTypeface(OswaldLight, TypefaceStyle.Normal);

                txtAppContattiLine2.SetTypeface(OswaldLight, TypefaceStyle.Normal);
                txtAppContattiLine3.SetTypeface(OswaldLight, TypefaceStyle.Normal);
                txtAppContattiLine4.SetTypeface(OswaldLight, TypefaceStyle.Normal);

                rlForListView.SetBackgroundColor(Resources.GetColor(Resource.Color.bluedark));
                lyBackground.Visibility = ViewStates.Gone;


//                boxEsito.Click += OnClickEsito;
//                btnSalva.Click += OnClickbtnSalva;
                //            btnEmail.Click += OnClickbtnEmail;
                //            btnPhone.Click += OnClickbtnPhone;
                detailsApuntamenti.Visibility = ViewStates.Invisible;
                boxButtons.Visibility = ViewStates.Gone;

                RunOnUiThread(async () =>
                    {
                        await System.Threading.Tasks.Task.Run(() =>
                            {
								loadAppuntamentiForDay(DateTime.Now.Date, DateTime.Now.Date, false, string.Empty);
                            });
                    });

                AndroidEnvironment.UnhandledExceptionRaiser += AndroidEnvironmentUnhandledExceptionRaiser;
                InitMapEvent();

                imgAppdiOgiNavigatore.Click -= OnImgAppDiOggiNavigatoreClick;
                imgAppdiOgiNavigatore.Click += OnImgAppDiOggiNavigatoreClick;

//                imgAppdiOgiNavigatore.Click+= (object sender, EventArgs e) => 
//                    {
//                        try 
//                        {
//                            if(_map!=null)
//                            {
//                                CameraPosition position = new CameraPosition.Builder()
//                                    .Target(new LatLng(_map.MyLocation.Latitude,_map.MyLocation.Longitude))
//                                    .Zoom(14).Build();
//
//                                _map.AnimateCamera(CameraUpdateFactory.NewCameraPosition(position));
//                            }
//                        } 
//                        catch (Exception ex) 
//                        {
//                            Utils.writeToDeviceLog(ex);
//                        }
//                    };

                BoxAppuntamentiMoreDetails.Visibility = ViewStates.Gone;
                LaydetailsImpPersonale.Visibility = ViewStates.Gone;
                LaydetailsAppMemo.Visibility = ViewStates.Gone;
                boxDetails.Click += BoxDetails_Click;

                ScrollTouchListner scrlListener = new ScrollTouchListner(scrlAppOggDetails);
                // editNote.SetOnTouchListener(scrlListener);

                txtAppDescription.SetOnTouchListener(scrlListener);
                editNote.TextChanged += (object sender, TextChangedEventArgs e) =>
                {
                    if (editNote.Text.Length > 150)
                    {
                        Toast.MakeText(this, "max input reached", ToastLength.Short).Show();
                    }
                };

//                editNote.SetBackgroundColor(Color.Rgb(255,0,0));
//                editNote.Text="Un text mai ";
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
//            AddMeetingMarkersToMap();
        }

        void BoxDetails_Click(object sender, EventArgs e)
        {
            try
            {
                if (BoxAppuntamentiMoreDetails.Visibility == ViewStates.Gone)
                {
                    BoxAppuntamentiMoreDetails.Visibility = ViewStates.Visible;
                }
                else
                {
                    BoxAppuntamentiMoreDetails.Visibility = ViewStates.Gone;
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }


        private void AndroidEnvironmentUnhandledExceptionRaiser(object sender, RaiseThrowableEventArgs e)
        {
            Utils.writeToDeviceLog(e.Exception);
        }

        private void InitMapEvent()
        {

            try
            {
                //if(androidPhoneVersion<14)
                {
                    _mapFragmentASupport = SupportFragmentManager.FindFragmentByTag("map") as SupportMapFragment;
                    if (_mapFragment == null)
                    {
                        GoogleMapOptions mapOptions = new GoogleMapOptions()
                            .InvokeMapType(GoogleMap.MapTypeNormal)
                            .InvokeZoomControlsEnabled(false)
                            .InvokeCompassEnabled(true);

                        Android.Support.V4.App.FragmentTransaction fragTx = SupportFragmentManager.BeginTransaction();
                        _mapFragmentASupport = SupportMapFragment.NewInstance(mapOptions);
                        fragTx.Add(Resource.Id.map, _mapFragmentASupport, "map");
                        fragTx.Commit();
                    }
                }
//                else
//                {
//                    Utils.GooglePlayServicesEnum gpsStatusA = Utils.getGooglePlayServicesStatus();
//                    _mapFragmentA = FragmentManager.FindFragmentByTag("map") as MapFragment;
//                    if (_mapFragmentA == null)
//                    {
//                        if (gpsStatusA == Utils.GooglePlayServicesEnum.good)
//                        {
//                            GoogleMapOptions mapOptionsA = new GoogleMapOptions()
//                                .InvokeMapType(GoogleMap.MapTypeNormal)
//                                .InvokeZoomControlsEnabled(true)
//                                .InvokeCompassEnabled(true);
//
//                            Android.App.FragmentTransaction fragTxA = FragmentManager.BeginTransaction();
//
//                            _mapFragmentA = MapFragment.NewInstance(mapOptionsA);
//
//                            fragTxA.Add(Resource.Id.map, _mapFragmentA, "map");
//
//                            fragTxA.Commit();
//
//                            checkForGpsA = false;
//                        }
//                        else if (gpsStatusA == Utils.GooglePlayServicesEnum.outOfDate)
//                        {                   
//                            alertDialog = new AlertDialog.Builder(this).Create();
//                            alertDialog.SetTitle("Alert");
//                            alertDialog.SetMessage("'Google play services' non è aggiornato. Si prega di aggiornare.");
//                            alertDialog.SetButton("OK", (sender1, e1) =>
//                                {
//                                    alertDialog.Dismiss();
//                                });
//                            alertDialog.Show();
//                        }
//                        else
//                        {
//                            alertDialog = new AlertDialog.Builder(this).Create();
//                            alertDialog.SetTitle("Alert");
//                            alertDialog.SetMessage("'Google play services' non è installato. Si prega di installare.");
//                            alertDialog.SetButton("OK", (sender1, e1) =>
//                                {
//                                    alertDialog.Dismiss();
//                                });
//                            alertDialog.Show();
//                        }
//                    }
//                }
               

            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }


        }

        void OnClickEsito(object sender, EventArgs e)
        {
            try
            {
                LinearLayout btnSender = (LinearLayout)sender;
                var AppointmentTag = ((AppointmentObjTag)btnSender.Tag).Appointment;
                AppuntamentiItem appItem = (AppuntamentiItem)AppointmentTag;

                List<string> status = new List<string>();//{ "status 1", "status 2", "status 3", "status 4", "status 5" };
                if (Utils.AllStatuses != null)
                {
                    foreach (var item in Utils.AllStatuses)
                    {
                        status.Add(WebUtility.HtmlDecode(item.Value.ToString()));
                    }
                }
                List<string> allowedStatuses;//"Trattativa" "Chiuso" "Non si è creato interesse" "Disdetto" "Fuori strategia" "Annullato"
                switch (WebUtility.HtmlDecode(appItem.Status.ToLower()))
                {
                    case "":
                        allowedStatuses = new List<string>(){ "Trattativa", "Chiuso", "Non si è creato interesse", "Disdetto", "Fuori strategia", "Annullato" };
                        break;
                    case "non si è creato interesse":
                        allowedStatuses = new List<string>(){ "Trattativa", "Chiuso", "Non si è creato interesse" };
                        break;
                    case "trattativa":
                        allowedStatuses = new List<string>(){ "Trattativa", "Chiuso", "Non si è creato interesse" };
                        break;
                    case "chiuso":
                        allowedStatuses = new List<string>(){ "Chiuso" };
                        break;
                    default:
                        allowedStatuses = new List<string>(){ "Trattativa", "Chiuso", "Non si è creato interesse", "Disdetto", "Fuori strategia", "Annullato" };
                        break;

                }


                if (window != null && window.IsShowing == true)
                {
                    window.Dismiss();
                }
                else
                {
                    imgStatusArrow.SetImageResource(Resource.Drawable.whiteArrowDown);

                    LayoutInflater inflater = (LayoutInflater)this.GetSystemService(Context.LayoutInflaterService);
                    View popup = inflater.Inflate(Resource.Layout.DropDownLayout, null);

                    ListView listDropDown = popup.FindViewById<ListView>(Resource.Id.dropDownListView);
                    listDropDown.Adapter = new StatusDropDownListAdapter(allowedStatuses, this);
                    listDropDown.ItemClick += (object sender1, AdapterView.ItemClickEventArgs e1) =>
                    {
                        txtEsito.Text = (((ListView)sender1).Adapter).GetItem(e1.Position).ToString().ToUpper();
                        lblAppOggiEsito.Text = "ESITO - ";
                            statusChanged=true;
                        window.Dismiss();
                    };

                    window = new PopupWindow(popup, ((LinearLayout)sender).Width, WindowManagerLayoutParams.WrapContent);
                    window.Touchable = true;
                    window.Focusable = true;
                    window.OutsideTouchable = true;
                    window.DismissEvent += (object sender1, EventArgs e1) =>
                    {
                        imgStatusArrow.SetImageResource(Resource.Drawable.whiteArrowDown);
                    };

                    window.SetBackgroundDrawable(new BitmapDrawable());
                    window.ShowAsDropDown((LinearLayout)sender, 0, 0);
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        void OnClickbtnSalva(object sender, EventArgs e)
        {
            try
            {
                LinearLayout btnSender = (LinearLayout)sender;
                var AppointmentTag = ((AppointmentObjTag)btnSender.Tag).Appointment;
                AppuntamentiItem appItem = (AppuntamentiItem)AppointmentTag;

                progresV.Show(this, "Attendere prego", -1, MaskType.Clear, TimeSpan.FromSeconds(120), null, true, null); 
                if (mWakeLock != null)
                {
                    mWakeLock.Acquire();
                }
                if(txtEsito.Text!=string.Empty)
                    statusChanged=true;
                if(statusChanged)
                {
                    if(editNote.Text!=string.Empty)
                    {
                        UpdateDetailAppuntamenti(appItem);
                    }
                    else
                    {
                        Toast.MakeText(this, "Inserire il motivo dell’esito per procedere", ToastLength.Long).Show();
                        RunOnUiThread(() =>
                            {
                                if (progresV != null)
                                {
                                    progresV.Dismiss();
                                }

                                if (mWakeLock != null)
                                {
                                    mWakeLock.Release();
                                }
                            });
                    }
                }
                else
                {
                    UpdateDetailAppuntamenti(appItem);
                }
                statusChanged=false;

            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void UpdateDetailAppuntamenti(AppuntamentiItem appItem)
        {
            try
            {
                ThreadPool.QueueUserWorkItem(state =>
                    {
                        try
                        {
                            if (Utils.CheckForInternetConn())
                            {  
                                appItem.AppuntamentiDateHour=appItem.appuntamentiChangedTime;
                                string data = appItem.AppuntamentiDateHour.ToString("yyyy-MM-dd", CultureInfo.CurrentCulture);
                                string hour = appItem.AppuntamentiDateHour.ToString("HH:mm:ss", CultureInfo.CurrentCulture);

                                bool rasp = ApiCalls.updateAppuntamenti(Utils.UserToken, appItem.Id, txtEsito.Text, editNote.Text, appItem.Source, data, hour,string.Empty,string.Empty,false);

                                RunOnUiThread(() =>
                                    {
                                        if (rasp)
                                        {
                                            Toast.MakeText(this, "Appuntamento aggiornato con successo", ToastLength.Long).Show();
                                            appItem.Status = txtEsito.Text;
                                            appItem.Motivo = editNote.Text;
											appItem.DateChanged=false;

                                            if (lastLoadedDay != null)
                                            {
                                                try
                                                {
                                                    appuntamentiList = appuntamentiList.Where(x => x.AppuntamentiDateHour.Day == lastLoadedDay.Value.Day && x.AppuntamentiDateHour.Month == lastLoadedDay.Value.Month && x.AppuntamentiDateHour.Year == lastLoadedDay.Value.Year).OrderBy(x => x.AppuntamentiDateHour).ToList();
                                                }
                                                catch (Exception ex)
                                                {
                                                    Utils.writeToDeviceLog(ex);
                                                }
                                            }

                                            timeListAdapter.Items = appuntamentiList;
                                            timeListAdapter.SelectedIndex = -1;
                                            OnRowFromListClickEvent(0);
                                            AddMeetingMarkersToMapEvents();

												imgChangeAppHour.SetImageResource(Resource.Drawable.memo_calendarIcon);
		
                                        }
                                        else
                                        {
                                            if (editNote.Text != "")
                                            {
                                                Toast.MakeText(this, "Impossibile aggiornare le informazioni dell’appuntamento", ToastLength.Long).Show();
                                            }
                                            else
                                            {
                                                Toast.MakeText(this, "Inserire il motivo dell’esito per procedere", ToastLength.Long).Show();
                                            }
                                        }
                                    });
                            }
                            else
                            {
                                RunOnUiThread(() =>
                                    {
                                        Toast.MakeText(this, "Nessuna connessione Internet", ToastLength.Long).Show();
                                    });
                            }
                        }
                        catch (Exception ex)
                        {
                            Utils.writeToDeviceLog(ex);
                        }
                        finally
                        {
                            RunOnUiThread(() =>
                                {
                                    if (progresV != null)
                                    {
                                        progresV.Dismiss();
                                    }

                                    if (mWakeLock != null)
                                    {
                                        mWakeLock.Release();
                                    }
                                });
                        }
                    });
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        void OnClickbtnEmail(object sender, EventArgs e)
        {
            try
            {
                LinearLayout btnSender = (LinearLayout)sender;
                var AppointmentTag = ((AppointmentObjTag)btnSender.Tag).Appointment;
                AppuntamentiItem appItem = (AppuntamentiItem)AppointmentTag;

                if (appItem.EmailList.Count > 1)
                {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    LayoutInflater inflater = (LayoutInflater)GetSystemService(Context.LayoutInflaterService);
                    View popupView = inflater.Inflate(Resource.Layout.ContactItemView, screenBackground, false);
                    builder.SetView(popupView);
                    ListView listView = popupView.FindViewById<ListView>(Resource.Id.listView1);
                    ListCallAdapter contactListAdapter = new ListCallAdapter(this, appItem.EmailList);
                    listView.Adapter = contactListAdapter;
                    builder.Show();
                    listView.ItemClick += (object sender3, AdapterView.ItemClickEventArgs e3) =>
                    {
                        string emailToSendTo = appItem.EmailList[e3.Position];
                        Intent emailIntent = new Intent(Intent.ActionSend); 
                        emailIntent.SetType("plain/text");
                        emailIntent.PutExtra(Intent.ExtraEmail, new string[]{ emailToSendTo });  
                        StartActivity(Intent.CreateChooser(emailIntent, "Seleziona mail:"));
                    };
                }
                else if (appItem.EmailList.Count > 0)
                {
                    if (appItem.EmailList[0] != "")
                    {
                        Intent emailIntent = new Intent(Intent.ActionSend); 
                        emailIntent.SetType("plain/text");
                        emailIntent.PutExtra(Intent.ExtraEmail, new string[]{ appItem.EmailList[0] });   
                        StartActivity(Intent.CreateChooser(emailIntent, "Seleziona mail:"));
                    }
                    else
                    {
                        Toast.MakeText(this, "Nessun indirizzo mail presente", ToastLength.Short).Show();
                    }
                }
                else
                {
                    Toast.MakeText(this, "Nessun indirizzo mail presente", ToastLength.Short).Show();
                }

            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        void OnClickbtnPhone(object sender, EventArgs e)
        {
            try
            {

                LinearLayout btnSender = (LinearLayout)sender;
                var AppointmentTag = ((AppointmentObjTag)btnSender.Tag).Appointment;
                AppuntamentiItem appItem = (AppuntamentiItem)AppointmentTag;

                if (appItem.PhoneList.Count > 1)
                {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    LayoutInflater inflater = (LayoutInflater)GetSystemService(Context.LayoutInflaterService);
                    View popupView = inflater.Inflate(Resource.Layout.ContactItemView, screenBackground, false);
                    builder.SetView(popupView);
                    ListView listView = popupView.FindViewById<ListView>(Resource.Id.listView1);
                    ListCallAdapter contactListAdapter = new ListCallAdapter(this, appItem.PhoneList);
                    listView.Adapter = contactListAdapter;
                    builder.Show();
                    listView.ItemClick += (object sender3, AdapterView.ItemClickEventArgs e3) =>
                    {
                        string numberToCall = appItem.PhoneList[e3.Position];
                        AlertDialog.Builder builderCall = new AlertDialog.Builder(this);   
                        builderCall.SetTitle("Chiama");                   
                        builderCall.SetMessage("Conferma chiamata " + numberToCall.Trim()); 

                        builderCall.SetPositiveButton("Yes", (sender1, e1) =>
                            {                       
                                Intent telIntent = new Intent(Intent.ActionCall);
                                telIntent.SetData(Android.Net.Uri.Parse("tel:" + numberToCall.Trim()));
                                StartActivity(telIntent);
                            });
                        builderCall.SetNegativeButton("No", (sender1, e1) =>
                            {

                            });

                        builderCall.Show();
                    };

                }
                else if (appItem.PhoneList.Count > 0)
                {
//                    Intent telIntent = new Intent(Intent.ActionCall);
//                    telIntent.SetData(Android.Net.Uri.Parse("tel:" + appItem.PhoneList[0]));                                                                             
//                    StartActivity(telIntent);
                    AlertDialog.Builder builderCall = new AlertDialog.Builder(this);   
                    builderCall.SetTitle("Chiama");                   
                    builderCall.SetMessage("Conferma chiamata " + appItem.PhoneList[0].Trim()); 

                    builderCall.SetPositiveButton("Yes", (sender1, e1) =>
                        {                       
                            Intent telIntent = new Intent(Intent.ActionCall);
                            telIntent.SetData(Android.Net.Uri.Parse("tel:" + appItem.PhoneList[0].Trim()));
                            StartActivity(telIntent);
                        });
                    builderCall.SetNegativeButton("No", (sender1, e1) =>
                        {

                        });

                    builderCall.Show();
                }
                else
                {
                    Toast.MakeText(this, "Nessun numero di telefono presente", ToastLength.Short).Show();
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void OnClickFromListClickEvent(int position)
        {
            try
            {
                var metrics = Resources.DisplayMetrics;
                int widthInDp = ConvertDpToPixels(80);
                int heightInDp = ConvertPixelsToDp(metrics.HeightPixels);

                Animation myAnimation = AnimationUtils.LoadAnimation(this, Resource.Animation.scaleListViewToLeft);
                rlForListView.StartAnimation(myAnimation);

                myAnimation.AnimationStart += (object sender, Animation.AnimationStartEventArgs e) =>
                {
                    timeListAdapter.ItemClickIdEvent -= OnClickFromListClickEvent;
                    timeListAdapter.RowClickEvent -= OnRowFromListClickEvent;
                    if (appuntamentiList[position].AppBloccati)
                    {
                        detailsApuntamenti.Visibility = ViewStates.Gone;
                        LaydetailsImpPersonale.Visibility = ViewStates.Gone;
                        boxButtons.Visibility = ViewStates.Gone;
                        LaydetailsAppMemo.Visibility = ViewStates.Gone;
                    }
                    else
                    {
                        if (appuntamentiList[position].Id_appto_memo != "")
                        {
                            detailsApuntamenti.Visibility = ViewStates.Gone;
                            LaydetailsImpPersonale.Visibility = ViewStates.Gone;
                            boxButtons.Visibility = ViewStates.Gone;
                            LaydetailsAppMemo.Visibility = ViewStates.Visible;
                        }
                        else
                        {
                            if (appuntamentiList[position].TipAppuntamenti == "business")
                            {
                                detailsApuntamenti.Visibility = ViewStates.Visible;
                                LaydetailsImpPersonale.Visibility = ViewStates.Gone;
                                boxButtons.Visibility = ViewStates.Visible;
                                LaydetailsAppMemo.Visibility = ViewStates.Gone;
                            }
                            else if (appuntamentiList[position].TipAppuntamenti == "personal")
                            {
                                detailsApuntamenti.Visibility = ViewStates.Gone;
                                LaydetailsImpPersonale.Visibility = ViewStates.Visible;
                                boxButtons.Visibility = ViewStates.Gone;
                                LaydetailsAppMemo.Visibility = ViewStates.Gone;
                            }
                        }
                    }

                };

                myAnimation.AnimationEnd += (object sender, Animation.AnimationEndEventArgs e) =>
                {
                    //Setters
                    RelativeLayout.LayoutParams rel_btn = new RelativeLayout.LayoutParams(widthInDp, ViewGroup.LayoutParams.MatchParent);
                    rel_btn.AddRule(LayoutRules.AlignParentLeft);
                    rlForListView.LayoutParameters = rel_btn;

                    timeListAdapter = new ApputamentiTimeAdapter(this, appuntamentiList, position);
                    listApputamenti.Adapter = timeListAdapter;

                    timeListAdapter.RowClickEvent -= OnRowFromListClickEvent;
                    timeListAdapter.RowClickEvent += OnRowFromListClickEvent;
                    if (position > 4)
                    {
                        listApputamenti.SetSelection(position);
                    }
                    

                    rlForListView.SetBackgroundColor(Resources.GetColor(Resource.Color.trans));
                    lyBackground.Visibility = ViewStates.Visible;
                };

                if (appuntamentiList[position].AppBloccati)
                {
                    detailsApuntamenti.Visibility = ViewStates.Gone;
                    boxButtons.Visibility = ViewStates.Gone;
                    LaydetailsImpPersonale.Visibility = ViewStates.Gone;
                    LaydetailsAppMemo.Visibility = ViewStates.Gone;
                }
                else
                {
//                  appuntamentiList[position].TipAppuntamenti="xyz";
                    if (appuntamentiList[position].Id_appto_memo != "")
                    {
						boxMemoGottoClientDetail.Visibility = ViewStates.Visible;
                        lblDetailsAppMemoPlaceValue.Text = appuntamentiList[position].Indirizzo; 
                        lblDetailsAppMemoTitleValue.Text = appuntamentiList[position].Evento_pers;
                        txtAppMemoDescription.Text = appuntamentiList[position].Note_agent;

                        boxMemoGottoClientDetail.Click -= BoxMemoGottoClientDetail_Click;
                        boxMemoGottoClientDetail.Click += BoxMemoGottoClientDetail_Click;

						if(appuntamentiList[position].Id_appto_memo.Contains("cus"))
						{
							txtMemovaiAiClient.Text="VAI AL CLIENTE";
						}
						else
						{
							txtMemovaiAiClient.Text="VAI ALL’APPUNTAMENTO";
						}

                        AppointmentObjTag itemNav = new AppointmentObjTag();
                        itemNav.Appointment = appuntamentiList[position];
                        boxMemoGottoClientDetail.Tag = itemNav;
                    }
                    else
                    {
                        if (appuntamentiList[position].TipAppuntamenti == "business")
                        {
                            txtLocation.Text = appuntamentiList[position].RegSociale;
                            txtName.Text = appuntamentiList[position].Contact_name;
                            txtPhone.Text = appuntamentiList[position].Phone;
                            txtMail.Text = appuntamentiList[position].Mail;

							if (appuntamentiList[position].DateChanged)
                            {
                                imgChangeAppHour.SetImageResource(Resource.Drawable.memo_calendarIconRed);
                            }
                            else
                            {
                                imgChangeAppHour.SetImageResource(Resource.Drawable.memo_calendarIcon);
                            }

                            txtEsito.Text = (WebUtility.HtmlDecode(appuntamentiList[position].Status)).ToUpper();
							if (appuntamentiList[position].Status != "")
							{
								lblAppOggiEsito.Text = "ESITO - ";
							}
							else
							{
								lblAppOggiEsito.Text = "ESITO";
							}
                            txtAppDescription.Text = appuntamentiList[position].Note_chiamata;

                            editNote.Text = appuntamentiList[position].Motivo;

                            AppointmentObjTag item0 = new AppointmentObjTag();
                            item0.Appointment = appuntamentiList[position];
                            btnSalva.Tag = item0;


                            //new 11.06.2016
                            if (!string.IsNullOrEmpty(appuntamentiList[position].Status))
                            {
                                imgChangeAppHour.Visibility = ViewStates.Invisible;
                            }
                            else
                            {
                                imgChangeAppHour.Visibility = ViewStates.Visible;
                            }
                            imgChangeAppHour.Click -= ImgChangeAppHour_Click;
                            imgChangeAppHour.Click += ImgChangeAppHour_Click;
                            imgChangeAppHour.Tag = item0;

                            if (appuntamentiList[position].Latitude != 0 && appuntamentiList[position].Longitude != 0)
                            {
                                if (_map != null)
                                {
                                    CameraPosition positionCameraMap = new CameraPosition.Builder()
                                        .Target(new LatLng(appuntamentiList[position].Latitude, appuntamentiList[position].Longitude))
                                        .Zoom(14).Build();

                                    _map.AnimateCamera(CameraUpdateFactory.NewCameraPosition(positionCameraMap));
                                }
                            }
                            else
                            {
                                Toast.MakeText(this, "Appuntamento senza coordinate. Impossibile visualizzare il percorso", ToastLength.Long).Show(); 
                            }

                            //TODO NEW 
                            txtAppCodChiamataValue.Text = appuntamentiList[position].Id_Chiamata;
                            txtAppDataChiamataValue.Text = appuntamentiList[position].Date_chiamata.ToString("dd/MM/yyyy", CultureInfo.CurrentCulture);
                            txtAppDataAppuntamentoValue.Text = appuntamentiList[position].AppuntamentiDateHour.ToString("dd/MM/yyyy", CultureInfo.CurrentCulture);
                            txtAppOraAppuntamentoValue.Text = appuntamentiList[position].AppuntamentiDateHour.ToString("HH:mm", CultureInfo.CurrentCulture);
                            txtAppNoteAgenteValue.Text = appuntamentiList[position].Note_agent;
                            txtAppNoteChiamataValue.Text = appuntamentiList[position].Note_chiamata;
                            txtAppNoteSegretariaValue.Text = appuntamentiList[position].Note_segretaria;
                            txtAppMotiveValue.Text = appuntamentiList[position].Motivo;
                            txtAppRagioneSocialeValue.Text = appuntamentiList[position].RegSociale;
                            txtAppCategoriaValue.Text = appuntamentiList[position].Category;

                            txtAppContattiValue.Text = appuntamentiList[position].ContattiInfo;
                            //                        txtAppContattiLine2.Text="Telefono: "+appuntamentiList[position].Contact_phone;
                            //                        txtAppContattiLine3.Text="Email: "+appuntamentiList[position].Contact_mail;
                            //                        txtAppContattiLine4.Text="Note: "+appuntamentiList[position].Note;


                            txtAppIndirizzoValue.Text = appuntamentiList[position].Indirizzo;
                            if (appuntamentiList[position].Civico != "")
                            {
                                txtAppIndirizzoValue.Text += " " + appuntamentiList[position].Civico;
                            } 
                            txtAppIndirizzoLine2.Text = "";
                            if (appuntamentiList[position].Cap != "")
                            {
                                txtAppIndirizzoLine2.Text += appuntamentiList[position].Cap;
                            }
                            if (appuntamentiList[position].City != "")
                            {
                                txtAppIndirizzoLine2.Text += " " + appuntamentiList[position].City;
                            }
                            if (appuntamentiList[position].Provincia != "")
                            {
                                txtAppIndirizzoLine2.Text += " " + appuntamentiList[position].Provincia;
                            }
                            if (appuntamentiList[position].Acronim != "")
                            {
                                txtAppIndirizzoLine2.Text += " (" + appuntamentiList[position].Acronim + ")";
                            }
                            BoxAppuntamentiMoreDetails.Visibility = ViewStates.Gone;

                            AppointmentObjTag itemNav = new AppointmentObjTag();
                            itemNav.Appointment = appuntamentiList[position];
                            imgAppdiOgiNavigatore.Tag = itemNav;

                            btnPhone.Click -= OnClickbtnPhone;
                            btnPhone.Click += OnClickbtnPhone;

                            btnEmail.Click -= OnClickbtnEmail;
                            btnEmail.Click += OnClickbtnEmail;

                            btnSalva.Click -= OnClickbtnSalva;
                            btnSalva.Click += OnClickbtnSalva;

                            btnPhone.Tag = itemNav;
                            btnEmail.Tag = itemNav;

                            boxEsito.Click -= OnClickEsito;
                            boxEsito.Click += OnClickEsito;
                            boxEsito.Tag = itemNav;
                        }
                        else if (appuntamentiList[position].TipAppuntamenti == "personal")
                        {
                            lblDetailsAggImpDataDalValue.Text = appuntamentiList[position].Date_chiamata.ToString("dd/MM/yyyy", CultureInfo.CurrentCulture);    
                            lblDetailsAggImpDataAlValue.Text = appuntamentiList[position].AppuntamentiDateHour.ToString("dd/MM/yyyy", CultureInfo.CurrentCulture);   
                            lblDetailsAggImpOraValue.Text = appuntamentiList[position].AppuntamentiDateHour.ToString("HH:mm", CultureInfo.CurrentCulture);   
                            lblDetailsAggImpLuogoValue.Text = appuntamentiList[position].Indirizzo;    

                            txtAppImpPersDescription.Text = appuntamentiList[position].Note_agent;    
							txtAppImpPersEvento_Pers.Text = appuntamentiList[position].Evento_pers;
                        } 
                    }
                }

               
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        void ImgChangeAppHour_Click(object sender, EventArgs e)
        {
            try
            {
                ImageView btnSender = (ImageView)sender;
                var AppointmentTag = ((AppointmentObjTag)btnSender.Tag).Appointment;
                AppuntamentiItem appItem = (AppuntamentiItem)AppointmentTag;

                OnClickPickDate(appItem,true);
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void OnImgAppDiOggiNavigatoreClick(object sender, EventArgs e)
        {
            try
            {
                ImageView btnSender = (ImageView)sender;
                var AppointmentTag = ((AppointmentObjTag)btnSender.Tag);
                if (AppointmentTag != null)
                {
                    AppuntamentiItem appItem = (AppuntamentiItem)AppointmentTag.Appointment;
                    if (appItem.Latitude != 0 && appItem.Longitude != 0)
                    {
                        //show course
                        if (routeShow)
                        {
                            routeShow = false;
                            if (routeA != null)
                            {
                                routeA.Remove();
                            }
                            if (_map != null)
                            {
                                CameraPosition positionCameraMap = new CameraPosition.Builder()
                                        .Target(new LatLng(appItem.Latitude, appItem.Longitude))
                                        .Zoom(14).Build();

                                _map.AnimateCamera(CameraUpdateFactory.NewCameraPosition(positionCameraMap));
                            }
   
                        }
                        else
                        {
                            ShowRoute(appItem.Latitude, appItem.Longitude);
                        }

                    }
                    else
                    {
                        Toast.MakeText(this, "Appuntamento senza coordinate. Impossibile visualizzare il percorso", ToastLength.Long).Show(); 
                    }
                }
                else
                {
                    Toast.MakeText(this, "Seleziona un appuntamento per visualizzare il percorso", ToastLength.Long).Show();
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void OnRowFromListClickEvent(int position)
        {
            try
            {
                if (timeListAdapter != null && appuntamentiList != null && appuntamentiList.Count > 0)
                {
                    int selPos = timeListAdapter.selectItemPositon();
                    Console.WriteLine(selPos.ToString() + "------" + position);

                    if (selPos == position)
                    {
                        OnRefreshListClickEvent();
                    }
                    else
                    {
                        timeListAdapter = new ApputamentiTimeAdapter(this, appuntamentiList, position);
                        listApputamenti.Adapter = timeListAdapter;
                        timeListAdapter.RowClickEvent -= OnRowFromListClickEvent;
                        timeListAdapter.RowClickEvent += OnRowFromListClickEvent;
                        if (position > 4)
                        {
                            listApputamenti.SetSelection(position);
                        }
                    }
                    if (appuntamentiList[position].AppBloccati)
                    {
                        detailsApuntamenti.Visibility = ViewStates.Gone;
                        boxButtons.Visibility = ViewStates.Gone;
                        LaydetailsImpPersonale.Visibility = ViewStates.Gone;
                        LaydetailsAppMemo.Visibility = ViewStates.Gone;
                    }
                    else
                    {
                        if (appuntamentiList[position].Id_appto_memo != "")
                        {
                            detailsApuntamenti.Visibility = ViewStates.Gone;
                            boxButtons.Visibility = ViewStates.Gone;
                            LaydetailsImpPersonale.Visibility = ViewStates.Gone;
                            LaydetailsAppMemo.Visibility = ViewStates.Visible;

                            lblDetailsAppMemoPlaceValue.Text = appuntamentiList[position].Indirizzo; 
                            lblDetailsAppMemoTitleValue.Text = appuntamentiList[position].Evento_pers;
                            txtAppMemoDescription.Text = appuntamentiList[position].Note_agent;

                            boxMemoGottoClientDetail.Click -= BoxMemoGottoClientDetail_Click;
                            boxMemoGottoClientDetail.Click += BoxMemoGottoClientDetail_Click;

							if(appuntamentiList[position].Id_appto_memo.Contains("cus"))
							{
								txtMemovaiAiClient.Text="VAI AL CLIENTE";
							}
							else
							{
								txtMemovaiAiClient.Text="VAI ALL’APPUNTAMENTO";
							}

                            AppointmentObjTag itemNav = new AppointmentObjTag();
                            itemNav.Appointment = appuntamentiList[position];
                            boxMemoGottoClientDetail.Tag = itemNav; 
                        }
                        else
                        {
                            if (appuntamentiList[position].TipAppuntamenti == "business")
                            {
                                detailsApuntamenti.Visibility = ViewStates.Visible;
                                boxButtons.Visibility = ViewStates.Visible;
                                LaydetailsImpPersonale.Visibility = ViewStates.Gone;
                                LaydetailsAppMemo.Visibility = ViewStates.Gone;

                                txtLocation.Text = appuntamentiList[position].RegSociale;
                                txtName.Text = appuntamentiList[position].Contact_name;
                                txtPhone.Text = appuntamentiList[position].Phone;
                                txtMail.Text = appuntamentiList[position].Mail;

								if (appuntamentiList[position].DateChanged)
                                {
                                    imgChangeAppHour.SetImageResource(Resource.Drawable.memo_calendarIconRed);
                                }
                                else
                                {
                                    imgChangeAppHour.SetImageResource(Resource.Drawable.memo_calendarIcon);
                                }

                                txtEsito.Text = (WebUtility.HtmlDecode(appuntamentiList[position].Status)).ToUpper();
								if (appuntamentiList[position].Status != "")
								{
									lblAppOggiEsito.Text = "ESITO - ";
								}
								else
								{
									lblAppOggiEsito.Text = "ESITO";
								}
                                txtAppDescription.Text = appuntamentiList[position].Note_chiamata;
                                editNote.Text = appuntamentiList[position].Motivo;

                                if (!string.IsNullOrEmpty(appuntamentiList[position].Status))
                                {
                                    imgChangeAppHour.Visibility = ViewStates.Invisible;
                                }
                                else
                                {
                                    imgChangeAppHour.Visibility = ViewStates.Visible;
                                }

                                if (appuntamentiList[position].Latitude != 0 && appuntamentiList[position].Longitude != 0)
                                {
                                    if (_map != null)
                                    {
                                        CameraPosition positionCameraMap = new CameraPosition.Builder()
                                            .Target(new LatLng(appuntamentiList[position].Latitude, appuntamentiList[position].Longitude))
                                            .Zoom(14).Build();

                                        _map.AnimateCamera(CameraUpdateFactory.NewCameraPosition(positionCameraMap));
                                    }
                                }
                                else
                                {
                                    Toast.MakeText(this, "Appuntamento senza coordinate. Impossibile visualizzare il percorso", ToastLength.Long).Show(); 
                                }

                                //TODO NEW 
                                txtAppCodChiamataValue.Text = appuntamentiList[position].Id_Chiamata;
                                txtAppDataChiamataValue.Text = appuntamentiList[position].Date_chiamata.ToString("dd/MM/yyyy", CultureInfo.CurrentCulture);
                                txtAppDataAppuntamentoValue.Text = appuntamentiList[position].AppuntamentiDateHour.ToString("dd/MM/yyyy", CultureInfo.CurrentCulture);
                                txtAppOraAppuntamentoValue.Text = appuntamentiList[position].AppuntamentiDateHour.ToString("HH:mm", CultureInfo.CurrentCulture);
                                txtAppNoteAgenteValue.Text = appuntamentiList[position].Note_agent;
                                txtAppNoteChiamataValue.Text = appuntamentiList[position].Note_chiamata;
                                txtAppNoteSegretariaValue.Text = appuntamentiList[position].Note_segretaria;
                                txtAppMotiveValue.Text = appuntamentiList[position].Motivo;
                                txtAppRagioneSocialeValue.Text = appuntamentiList[position].RegSociale;
                                txtAppCategoriaValue.Text = appuntamentiList[position].Category;

                                txtAppContattiValue.Text = appuntamentiList[position].ContattiInfo;
                                //                            txtAppContattiLine2.Text="Telefono: "+appuntamentiList[position].Contact_phone;
                                //                            txtAppContattiLine3.Text="Email: "+appuntamentiList[position].Contact_mail;
                                //                            txtAppContattiLine4.Text="Note: "+appuntamentiList[position].Note;

                                txtAppIndirizzoValue.Text = appuntamentiList[position].Indirizzo;
                                if (appuntamentiList[position].Civico != "")
                                {
                                    txtAppIndirizzoValue.Text += " " + appuntamentiList[position].Civico;
                                }

                                txtAppIndirizzoLine2.Text = "";
                                if (appuntamentiList[position].Cap != "")
                                {
                                    txtAppIndirizzoLine2.Text += appuntamentiList[position].Cap;
                                }
                                if (appuntamentiList[position].City != "")
                                {
                                    txtAppIndirizzoLine2.Text += " " + appuntamentiList[position].City;
                                }
                                if (appuntamentiList[position].Provincia != "")
                                {
                                    txtAppIndirizzoLine2.Text += " " + appuntamentiList[position].Provincia;
                                }
                                if (appuntamentiList[position].Acronim != "")
                                {
                                    txtAppIndirizzoLine2.Text += " (" + appuntamentiList[position].Acronim + ")";
                                }
                                //                    editNote.Text = appuntamentiList[position].Note;

                                BoxAppuntamentiMoreDetails.Visibility = ViewStates.Gone;

                                btnPhone.Click -= OnClickbtnPhone;
                                btnPhone.Click += OnClickbtnPhone;

                                btnEmail.Click -= OnClickbtnEmail;
                                btnEmail.Click += OnClickbtnEmail;

                                btnSalva.Click -= OnClickbtnSalva;
                                btnSalva.Click += OnClickbtnSalva;

                                imgChangeAppHour.Click -= ImgChangeAppHour_Click;
                                imgChangeAppHour.Click += ImgChangeAppHour_Click;


                                AppointmentObjTag itemNav = new AppointmentObjTag();
                                itemNav.Appointment = appuntamentiList[position];
                                imgAppdiOgiNavigatore.Tag = itemNav; 

                                btnPhone.Tag = itemNav;
                                btnEmail.Tag = itemNav;
                                boxEsito.Tag = itemNav;
                                imgChangeAppHour.Tag = itemNav;

                                AppointmentObjTag item0 = new AppointmentObjTag();
                                item0.Appointment = appuntamentiList[position];
                                btnSalva.Tag = item0;
                            }
                            else if (appuntamentiList[position].TipAppuntamenti == "personal")
                            {
                                detailsApuntamenti.Visibility = ViewStates.Gone;
                                boxButtons.Visibility = ViewStates.Gone;
                                LaydetailsImpPersonale.Visibility = ViewStates.Visible;
                                LaydetailsAppMemo.Visibility = ViewStates.Gone;

                                lblDetailsAggImpDataDalValue.Text = appuntamentiList[position].Date_chiamata.ToString("dd/MM/yyyy", CultureInfo.CurrentCulture);    
                                lblDetailsAggImpDataAlValue.Text = appuntamentiList[position].AppuntamentiDateHour.ToString("dd/MM/yyyy", CultureInfo.CurrentCulture);   
                                lblDetailsAggImpOraValue.Text = appuntamentiList[position].AppuntamentiDateHour.ToString("HH:mm", CultureInfo.CurrentCulture);   
                                lblDetailsAggImpLuogoValue.Text = appuntamentiList[position].Indirizzo;    

                                txtAppImpPersDescription.Text = appuntamentiList[position].Note_agent;  
								txtAppImpPersEvento_Pers.Text = appuntamentiList[position].Evento_pers; 
                            }
                        }
                    }

                }
                else
                {
                    ResetValues();

                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        void BoxMemoGottoClientDetail_Click(object sender, EventArgs e)
        {
            try
            {
                LinearLayout btnSender = (LinearLayout)sender;
                var AppointmentTag = ((AppointmentObjTag)btnSender.Tag).Appointment;
                AppuntamentiItem appItem = (AppuntamentiItem)AppointmentTag;

				if(appItem.Id_appto_memo.Contains("cus"))
				{
					Intent clientiIntent = new Intent(this, typeof(ClientiActivity));
					clientiIntent.PutExtra("fromAppuntamentiMap", true);
					clientiIntent.PutExtra("customer_id", appItem.Id_customer);
					clientiIntent.SetFlags(ActivityFlags.ReorderToFront);
					this.StartActivity(clientiIntent);
				}
				else
				{
					string[] words = appItem.Id_appto_memo.Split('|');
					if(words!=null && words.Length>0)
					{
						RunOnUiThread(async () =>
							{
								await System.Threading.Tasks.Task.Run(() =>
									{
										loadAppuntamentiForDay(DateTime.Now.Date, DateTime.Now.Date, true,words[1]);
									});
							});
					}
				}
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void ResetValues()
        {
            try
            {
                imgChangeAppHour.SetImageResource(Resource.Drawable.memo_calendarIcon);
                if(imgSearchChangeAppHour!=null)
                {
                    imgSearchChangeAppHour.SetImageResource(Resource.Drawable.memo_calendarIcon);
                }

                txtLocation.Text = "";
                txtName.Text = "";
                txtPhone.Text = "";
                txtMail.Text = "";
                txtEsito.Text = "";
                txtAppDescription.Text = "";

                //TODO NEW 
                txtAppCodChiamataValue.Text = "";
                txtAppDataChiamataValue.Text = "";
                txtAppDataAppuntamentoValue.Text = "";
                txtAppOraAppuntamentoValue.Text = "";
                txtAppNoteAgenteValue.Text = "";
                txtAppNoteChiamataValue.Text = "";
                txtAppNoteSegretariaValue.Text = "";
                txtAppMotiveValue.Text = "";
                txtAppRagioneSocialeValue.Text = "";
                txtAppCategoriaValue.Text = "";

                txtAppContattiValue.Text = "";
                txtAppContattiLine2.Text = "Telefono: ";
                txtAppContattiLine3.Text = "Email: ";
                txtAppContattiLine4.Text = "Note: ";

                txtAppIndirizzoValue.Text = "";
                txtAppIndirizzoLine2.Text = "";

                lblDetailsAggImpDataDalValue.Text = "";    
                lblDetailsAggImpDataAlValue.Text = "";    
                lblDetailsAggImpOraValue.Text = "";    
                lblDetailsAggImpLuogoValue.Text = "";    

                txtAppImpPersDescription.Text = "";   
				txtAppImpPersEvento_Pers.Text = "";

                OnRefreshListClickEvent();
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void OnRefreshListClickEvent()
        {
            try
            {
                Animation myAnimation = AnimationUtils.LoadAnimation(this, Resource.Animation.scaleListViewToRight);
                rlForListView.StartAnimation(myAnimation);

                myAnimation.AnimationStart += (object sender, Animation.AnimationStartEventArgs e) =>
                {
                    if (timeListAdapter != null)
                    {
                        timeListAdapter.ItemClickIdEvent -= OnClickFromListClickEvent;
                        timeListAdapter.RowClickEvent -= OnRowFromListClickEvent;
                    }


                    timeListAdapter = new ApputamentiTimeAdapter(this, appuntamentiList, -1);
                    listApputamenti.Adapter = timeListAdapter;

                    rlForListView.SetBackgroundColor(Resources.GetColor(Resource.Color.bluedark));
                    lyBackground.Visibility = ViewStates.Gone;

                };

                myAnimation.AnimationEnd += (object sender, Animation.AnimationEndEventArgs e) =>
                {
                    //Setters
                    timeListAdapter.ItemClickIdEvent += OnClickFromListClickEvent;

                    RelativeLayout.LayoutParams rel_btn = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent, ViewGroup.LayoutParams.MatchParent);
                    rel_btn.AddRule(LayoutRules.AlignParentLeft);
                    rlForListView.LayoutParameters = rel_btn;

                    detailsApuntamenti.Visibility = ViewStates.Gone;
                    boxButtons.Visibility = ViewStates.Gone;
                    LaydetailsImpPersonale.Visibility = ViewStates.Gone;
                    LaydetailsAppMemo.Visibility = ViewStates.Gone;
                };

            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        private void SetupMapIfNeededEvents()
        {

            try
            {
                string err = "Error ";

                System.Threading.ThreadPool.QueueUserWorkItem(state =>
                    {
                        try
                        {
//                            currLocation = Utils.requestNewLocation(this).Location;
//                            location = new LatLng(currLocation.Latitude, currLocation.Longitude);
                            RunOnUiThread(() =>
                                {
                                    location = new LatLng(currLocation.Latitude, currLocation.Longitude);
                                    if (location != null)
                                    {
//                                        _currentLocationA = currLocation;
                                        MarkerOptions marker;
                                        int type;
//                                        _map = _mapFragmentA.Map;
                                        //if(androidPhoneVersion<14)
//                                        {
//                                            _map = _mapFragmentASupport.Map;
//                                        }
//                                        else
//                                        {
//                                            _map = _mapFragmentA.Map;
//                                        }

                                        if (_map != null)
                                        {
                                            _map.Clear();

                                            marker = new MarkerOptions();
                                            marker.SetPosition(location);
                                            marker.SetTitle("La mia posizione");
                                            marker.Draggable(true);
                                            marker.InvokeIcon(BitmapDescriptorFactory.FromResource(Resource.Drawable.map_man));
                                            _map.AddMarker(marker);

                                            CameraUpdate cameraUpdate = CameraUpdateFactory.NewLatLngZoom(location, (float)11.5);
                                            _map.MoveCamera(cameraUpdate);
                                            _map.MyLocationEnabled = true;
                                            _map.UiSettings.MyLocationButtonEnabled = false;

                                            RunOnUiThread(async () =>
                                                {
                                                    await System.Threading.Tasks.Task.Run(() =>
                                                        {
                                                            AddMeetingMarkersToMapEvents();
                                                        });
                                                });
                                           
                                            _map.MarkerClick += MapOnMarkerClickEvents;
                                        }
                                    }
                                    else
                                    {
                                        alertDialog = new AlertDialog.Builder(this).Create();
                                        alertDialog.SetTitle("Errore");
                                        alertDialog.SetMessage("La posizione non è stato trovato!");
                                        alertDialog.SetButton("OK", (sender1, e1) =>
                                            {
                                                alertDialog.Dismiss();
                                            });
                                        alertDialog.Show();
                                    }
                                });
                        }
                        catch (Exception ex)
                        {
                            Utils.writeToDeviceLog(ex);
                        }
                        finally
                        {
                            if (progresV != null)
                            {
                                progresV.Dismiss();
                            }
                        }
                    });
                
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        private async Task AddMeetingMarkersToMapEvents()
        {
            try
            {
                BitmapDescriptor descriptor;

                if (appuntamentiList != null)
                {
                    if (_map != null)
                    {
                        RunOnUiThread(() =>
                            {
                                try
                                {
                                    _map.Clear();
                                }
                                catch (Exception ex)
                                {
                                    Utils.writeToDeviceLog(ex);
                                }
                           
                            });

                    }
                    for (int i = 0; i < appuntamentiList.Count; i++)
                    {
                        if (appuntamentiList[i].Latitude != 0 && appuntamentiList[i].Longitude != 0)
                        {
                            descriptor = BitmapDescriptorFactory.FromBitmap(CreateIconEvents(appuntamentiList[i].AppuntamentiDateHour.ToString("HH:mm", CultureInfo.InvariantCulture)));
                            //                     BitmapDescriptor icon = BitmapDescriptorFactory.FromResource(Resource.Drawable.customPin_icon);
                            MarkerOptions markerOptions = new MarkerOptions()
                                .SetPosition(new LatLng(appuntamentiList[i].Latitude, appuntamentiList[i].Longitude))
                                .InvokeIcon(descriptor)
                                .SetSnippet(appuntamentiList[i].AppuntamentiDateHour.Date.ToString("HH:mm", CultureInfo.InvariantCulture))
                                .SetTitle(String.Format("Marker {0}", i));
                            RunOnUiThread(() =>
                                {
                                    try
                                    {
                                        if (_map != null)
                                        {
                                            _map.AddMarker(markerOptions);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Utils.writeToDeviceLog(ex);
                                    }

                                });
                        }
                    } 
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }

        }

        private void MapOnMarkerClickEvents(object sender, GoogleMap.MarkerClickEventArgs markerClickEventArgs)
        {
            try
            {
                markerClickEventArgs.Handled = true;

                Marker marker = markerClickEventArgs.Marker;
                ShowRoute(marker.Position.Latitude, marker.Position.Longitude);
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        private void ShowRoute(double lat_finish, double lon_finish)
        {
            try
            {
                routeShow = true;
                // Show route
                string startLat = _currentLocation.Latitude.ToString().Replace(",", ".");
                string startLon = _currentLocation.Longitude.ToString().Replace(",", ".");
                string finishLat = lat_finish.ToString().Replace(",", ".");
                string finishLon = lon_finish.ToString().Replace(",", ".");
                string requestString = @"http://maps.googleapis.com/maps/api/directions/xml?origin=" + startLat + "," + startLon + "&destination=" + finishLat + "," + finishLon + "&sensor=true" + "&alternatives=false";

                // http request for the google api web service
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(requestString);
                request.ContentType = "text/xml";
                request.Method = "GET";

                // get the response from server
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        Utils.displayMessage("Errore dati di recupero", "Server codice di stato restituito:" + response.StatusCode, this);
                    }
                    else
                    {
                        // read the content returned
                        using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                        {
                            string content = reader.ReadToEnd();

                            if (string.IsNullOrEmpty(content))
                            {
                                Utils.displayError("La risposta è vuoto:", this);
                            }
                            else
                            {
                                // save content as xml file
                                XmlDocument doc = new XmlDocument();
                                doc.LoadXml(content);

                                XmlNode statusCode = doc.GetElementsByTagName("status")[0];

                                if (!statusCode.InnerText.ToLower().Equals("ok"))
                                {                       
                                    Utils.displayError("Il percorso non è stato trovato", this);

                                    return;
                                }

                                XmlNode routeNode = doc.GetElementsByTagName("route")[0];

                                //the node wich contains the entire polyline
                                XmlNode overViewPolyline = routeNode.SelectSingleNode("./overview_polyline");

                                //the node wich contains the entire route encoded points
                                XmlNode points = overViewPolyline.FirstChild;

                                // create coordinate array
                                LatLng[] coordArray = Utils.DecodePolylinePoints(points.InnerText).ToArray();

                                //0xff000000 - black        

                                if (routeA != null)
                                {
                                    routeA.Remove();
                                }

                                using (PolylineOptions polyLnOp = new PolylineOptions().InvokeColor(unchecked((int)0xff0000ff)).InvokeWidth(8f))
                                {
                                    polyLnOp.Add(coordArray);
                                    routeA = _map.AddPolyline(polyLnOp);
                                }

                                XmlNode boundsNode = doc.GetElementsByTagName("bounds")[0];
                                updateCameraView(boundsNode);       

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Utils.displayError(ex.Message + "   ---   " + ex.StackTrace, this);
            }
        }


        private void updateCameraView(XmlNode boundsNode)
        {
            try
            {
                // south west
                XmlNode southWestNode = boundsNode.FirstChild;
                XmlNode southWestLat = southWestNode.FirstChild; // south west -> latitude
                XmlNode southWestLon = southWestNode.LastChild;  // south west -> longitude

                // north east
                XmlNode northEastNode = boundsNode.LastChild;
                XmlNode northEastLat = northEastNode.FirstChild; // north east -> latitude
                XmlNode northEastLon = northEastNode.LastChild;  // north east -> longitude

                double latDouble;
                double longDouble;
                double nlatDouble;
                double nlongDouble;
                double.TryParse(southWestLat.InnerText, NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingSign, CultureInfo.InvariantCulture, out latDouble);
                double.TryParse(southWestLon.InnerText, NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingSign, CultureInfo.InvariantCulture, out longDouble);
                double.TryParse(northEastLat.InnerText, NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingSign, CultureInfo.InvariantCulture, out nlatDouble);
                double.TryParse(northEastLon.InnerText, NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingSign, CultureInfo.InvariantCulture, out nlongDouble);


                LatLng southWestCoord = new LatLng(latDouble, longDouble);
                LatLng northEastCoord = new LatLng(nlatDouble, nlongDouble);


                LatLngBounds cameraBounds = new LatLngBounds(southWestCoord, northEastCoord);

                //CameraUpdate cameraUpdate = CameraUpdateFactory.NewLatLngBounds(cameraBounds, 10);
                Display display = this.WindowManager.DefaultDisplay;
                float ScreenWidth = display.Width;
                float ScreenHeight = display.Height - getStatusBarHeight();
                //progressDialog1.Dismiss();
                foundA = true;
                //CameraUpdate cameraUpdate =CameraUpdateFactory.NewLatLngZoom(new LatLng(46.1428492, 26.6019322), 15); 
                //using (CameraUpdate cameraUpdate = CameraUpdateFactory.NewLatLngBounds(cameraBounds, (int)ScreenWidth, (int)ScreenHeight, 0))
                //{
                //    // We create an instance of CameraUpdate, and move the map to it.
                //    _map.MoveCamera(cameraUpdate);
                //}

				CameraUpdate cameraUpdate = CameraUpdateFactory.NewLatLngZoom(location, (float)zoomLevel);
				_map.MoveCamera(cameraUpdate);
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        private int getStatusBarHeight()
        {
            int result = 0;
            int resourceId = Resources.GetIdentifier("status_bar_height", "dimen", "android");
            if (resourceId > 0)
            {
                result = Resources.GetDimensionPixelSize(resourceId);
            }
            return result;
        }

        public Bitmap CreateIconEvents(string text)
        {
            Bitmap bitmap = null;
            try
            {
                EnsureInitEvents();

                // Update the icon
                //                iconView.SetImageDrawable(drawable);
                //                drawable.Dispose();

                // Update the text
                textViewEvents.Text = text;
                textViewEvents.Visibility = !String.IsNullOrEmpty(text) ? ViewStates.Visible : ViewStates.Gone;
                if (textViewEvents.Text == " ")
                    textViewEvents.SetBackgroundColor(Color.Transparent);

                // Lay out size and measure
                var measureSpec = ViewGroup.MeasureSpec.MakeMeasureSpec(0, MeasureSpecMode.Unspecified);
                containerEvents.Measure(measureSpec, measureSpec);
                int measuredWidth = containerEvents.MeasuredWidth;
                int measuredHeight = containerEvents.MeasuredHeight;
                containerEvents.Layout(0, 0, measuredWidth, measuredHeight);

                // Take current layout and draw to a bitmap
                bitmap = Bitmap.CreateBitmap(measuredWidth, measuredHeight, Bitmap.Config.Argb4444); // According to API docs, this is deprecated to Argb8888; however, testing shows a reduction in memory pressure
                bitmap.EraseColor(Color.Transparent);
                using (var canvas = new Canvas(bitmap))
                {
                    containerEvents.Draw(canvas);
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }

            return bitmap;
        }

        public void EnsureInitEvents()
        {
            try
            {
                if (containerEvents == null)
                {
                    containerEvents = this.LayoutInflater.Inflate(Resource.Layout.mapPin, null) as ViewGroup;
                    iconViewEvents = containerEvents.FindViewById<ImageView>(Resource.Id.imgCustomPin);
                    textViewEvents = containerEvents.FindViewById<TextView>(Resource.Id.textView1);
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        #endregion


        #region Calendar

        LinearLayout bodyCalendar;
        bool visibleCalendar;
        ViewAnimator viewAnimator;
        TextView txtTitleCalendar;
        Button btnAddEventCalendar;
        int count = 1;
        public Java.Util.Calendar month;
        public CalendarAdapter adapter;
        public ArrayList items;
        public Handler handler;
        GridSwipeAdapter nmAdapter;

        void CalendarLayout()
        {
            try
            {
                txtTitleCalendar = FindViewById<TextView>(Resource.Id.txtTitleCalendar);
                btnAddEventCalendar = FindViewById<Button>(Resource.Id.btnAddEventCalendar);
                bodyCalendar = FindViewById<LinearLayout>(Resource.Id.bodyCalendar);
                visibleCalendar = Intent.GetBooleanExtra("visibleCalendar", false);
                calendarPager = FindViewById<ViewPager>(Resource.Id.calendarViewPager);

                btnAddEventCalendar.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtTitleCalendar.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                btnAddEventCalendar.Click += OnAddEventToCalendarClick;
                //            calendar.DateChange += OnDateFromCalendarClick;

                TextView txtLun = FindViewById<TextView>(Resource.Id.txtLun);
                TextView txtMar = FindViewById<TextView>(Resource.Id.txtMar);
                TextView txtMer = FindViewById<TextView>(Resource.Id.txtMer);
                TextView txtGio = FindViewById<TextView>(Resource.Id.txtGio);
                TextView txtVen = FindViewById<TextView>(Resource.Id.txtVen);
                TextView txtSab = FindViewById<TextView>(Resource.Id.txtSab);
                TextView txtDom = FindViewById<TextView>(Resource.Id.txtDom);

                txtLun.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtMar.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtMer.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtGio.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtVen.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtSab.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtDom.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);

                initCalendarGrid();


            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void initCalendarGrid()
        {
            try
            {
                /*
                AndHUD progressCalendar = new AndHUD();
                progressCalendar.Show(this, "Attendere prego", -1, MaskType.Clear, TimeSpan.FromSeconds(120), null, true, null); 

                try
                {
                    System.Threading.ThreadPool.QueueUserWorkItem(state =>
                        { 
                            nmAdapter = new GridSwipeAdapter(SupportFragmentManager, this, calendarPager);
                           
                            RunOnUiThread(() =>
                                {
                                    calendarPager.Adapter = nmAdapter;
                                    calendarPager.CurrentItem = 58;
                                });
                        });
                }
                catch (Exception ex)
                {
                    Utils.writeToDeviceLog(ex);
                }
                finally
                {
                    if (progressCalendar != null)
                    {
                        progressCalendar.Dismiss();
                    }
                }
                */
                nmAdapter = new GridSwipeAdapter(SupportFragmentManager, this, calendarPager);

                calendarPager.Adapter = nmAdapter;
                calendarPager.CurrentItem = 58;
                
                if (nmAdapter != null)
                {
                    nmAdapter.GridClickCalendar += async (DateTime date_from_to) =>
                    {

                        AndHUD progress = new AndHUD();
                        progress.Show(this, "Attendere prego", -1, MaskType.Clear, TimeSpan.FromSeconds(120), null, true, null); 
                        try
                        {
                            System.Threading.ThreadPool.QueueUserWorkItem(state =>
                                {   
                                    RunOnUiThread(async () =>
                                        {

                                            if (map.Parent == null)
                                            {
                                                boxMaps.AddView(map);

                                            }
                                            if (imgAppdiOgiNavigatore.Parent == null)
                                            {
                                                boxMaps.AddView(imgAppdiOgiNavigatore);
                                            }

                                            boxMapSearch.RemoveView(mapBox);

                                            Map.Visibility = ViewStates.Gone;
                                            Search.Visibility = ViewStates.Gone;
                                            Events.Visibility = ViewStates.Visible;
                                            ItemMenuSelected("events");

                                            if (Calendar.Visibility == ViewStates.Visible)
                                            {
                                                expandOrCollapse(Calendar, "as");
                                                AddEvent.Visibility = ViewStates.Gone;
                                            }

                                            await System.Threading.Tasks.Task.Run(() =>
                                                {
													loadAppuntamentiForDay(date_from_to, date_from_to, true, string.Empty);
                                                });
                                        });

                                });
                        }
                        catch (Exception ex)
                        {
                            Utils.writeToDeviceLog(ex);
                            progress.Dismiss();
                        }
                        finally
                        {
                            progress.Dismiss();
                        }

                    };
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        /*
        public async System.Threading.Tasks.Task OnDateFromCalendarClick(DateTime date_from_to)
        {
            try
            {
//                try
//                {
//                    InputMethodManager inputManager = (InputMethodManager)this.GetSystemService(Context.InputMethodService);
//                    inputManager.HideSoftInputFromWindow(this.CurrentFocus.WindowToken, HideSoftInputFlags.NotAlways);
//                }
//                catch (Exception ex)
//                {
//                    Utils.writeToDeviceLog(ex);
//                }
                if (map.Parent == null)
                {
                    boxMaps.AddView(map);

                }
                if (imgAppdiOgiNavigatore.Parent == null)
                {
                    boxMaps.AddView(imgAppdiOgiNavigatore);
                }

                boxMapSearch.RemoveView(mapBox);

                Map.Visibility = ViewStates.Gone;
                Search.Visibility = ViewStates.Gone;
                Events.Visibility = ViewStates.Visible;
                ItemMenuSelected("events");

                if (Calendar.Visibility == ViewStates.Visible)
                {
                    expandOrCollapse(Calendar, "as");
                    AddEvent.Visibility = ViewStates.Gone;
                }

                await System.Threading.Tasks.Task.Run(() =>
                    {
                        loadAppuntamentiForDay(date_from_to, date_from_to, true);
                    });
//                loadAppuntamentiForDay(date_from_to, date_from_to, true);
                OnRowFromListClickEvent(0);
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }
        */

        void OnAddEventToCalendarClick(object sender, EventArgs e)
        {
            try
            {
                AddEvent.Visibility = ViewStates.Visible;
                ItemMenuSelected(null);

            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        #endregion

        #region AddEvent

        LinearLayout layAddEventToCalendar;

        TextView txtTitleAddEvent;
        TextView txtDataDalEvent;
        TextView txtDataAlEvent;
        TextView txtOraEvent;
        TextView txtLuogoEvent;

        TextView txtDataDalEventValue;
        TextView txtDataAlEventValue;
        TextView txtOraEventValue;
        EditText editTxtLuogo;
        EditText editTxtDescription;
        ImageView imgCalendarDataDalPick;
        ImageView imgCalendarDataAlPick;
        ImageView imgCalendarOraAlPick;
        Button btnSaveEvent;
        DateTime _date;
        DateTime _date2;
        private int hour;
        private int minute;
        const int DATE_DIALOG_ID1 = 1;
        const int DATE_DIALOG_ID2 = 2;
        const int TIME_DIALOG_ID = 0;


        void AddEventLayout()
        {
            try
            {
                txtTitleAddEvent = FindViewById<TextView>(Resource.Id.txtTitleAddEvent);
                txtDataDalEvent = FindViewById<TextView>(Resource.Id.txtDataDalEvent);
                txtDataAlEvent = FindViewById<TextView>(Resource.Id.txtDataAlEvent);
                txtOraEvent = FindViewById<TextView>(Resource.Id.txtOraEvent);
                txtLuogoEvent = FindViewById<TextView>(Resource.Id.txtLuogoEvent);
                txtDataDalEventValue = FindViewById<TextView>(Resource.Id.txtDataDalEventValue);
                txtDataAlEventValue = FindViewById<TextView>(Resource.Id.txtDataAlEventValue);
                txtOraEventValue = FindViewById<TextView>(Resource.Id.txtOraEventValue);
                editTxtLuogo = FindViewById<EditText>(Resource.Id.editTxtLuogo);
                editTxtDescription = FindViewById<EditText>(Resource.Id.editTxtDescription);
                imgCalendarDataDalPick = FindViewById<ImageView>(Resource.Id.imgCalendarDataDalPick);
                imgCalendarDataAlPick = FindViewById<ImageView>(Resource.Id.imgCalendarDataAlPick);
                imgCalendarOraAlPick = FindViewById<ImageView>(Resource.Id.imgCalendarOraAlPick);
                LinearLayout lyDateBegin = FindViewById<LinearLayout>(Resource.Id.lyDateBegin);
                LinearLayout lyDateFinale = FindViewById<LinearLayout>(Resource.Id.lyDateFinale);
                LinearLayout lyHour = FindViewById<LinearLayout>(Resource.Id.lyHour);
                btnSaveEvent = FindViewById<Button>(Resource.Id.btnSaveEvent);

                txtTitleAddEvent.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtDataDalEvent.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtDataAlEvent.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtOraEvent.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtLuogoEvent.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtDataDalEventValue.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtDataAlEventValue.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtOraEventValue.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                editTxtLuogo.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                editTxtDescription.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                btnSaveEvent.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);

                _date = DateTime.Today;
                _date2 = DateTime.Today;
                hour = DateTime.Now.Hour;
                minute = DateTime.Now.Minute;
                Thread.CurrentThread.CurrentCulture = new CultureInfo("it-IT");
                txtDataDalEventValue.Text = DateTime.Today.ToString("dd/MM/yyyy", CultureInfo.CurrentCulture);
                txtDataAlEventValue.Text = DateTime.Today.ToString("dd/MM/yyyy", CultureInfo.CurrentCulture);
                txtOraEventValue.Text = string.Format("{0}:{1}", hour, minute.ToString().PadLeft(2, '0'));


                lyDateBegin.Click += delegate
                {
                    ShowDialog(DATE_DIALOG_ID1);
                };
                lyDateFinale.Click += delegate
                {
                    ShowDialog(DATE_DIALOG_ID2);
                };
                lyHour.Click += delegate
                {
                    ShowDialog(TIME_DIALOG_ID);
                };
                btnSaveEvent.Click += OnBtnSaveEventClick;

            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }

        }

        protected override Dialog OnCreateDialog(int id)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("it-IT");
            switch (id)
            {
                case DATE_DIALOG_ID1:
                    return new DatePickerDialog(this, HandleDateSet, _date.Year, _date.Month - 1, _date.Day); 
                case DATE_DIALOG_ID2:
                    return new DatePickerDialog(this, HandleDateSet2, _date2.Year, _date2.Month - 1, _date2.Day); 
                case TIME_DIALOG_ID:
					return new TimePickerDialog(this, TimePickerCallback, hour, minute, true);
            }
            return null;
        }

        void HandleDateSet(object sender, DatePickerDialog.DateSetEventArgs e)
        {
            try
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("it-IT");
                _date = e.Date;
                txtDataDalEventValue.Text = _date.ToString("dd/MM/yyyy", CultureInfo.CurrentCulture);
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        void HandleDateSet2(object sender, DatePickerDialog.DateSetEventArgs e)
        {
            try
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("it-IT");
                _date2 = e.Date;
                txtDataAlEventValue.Text = _date2.ToString("dd/MM/yyyy", CultureInfo.CurrentCulture);
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        private void TimePickerCallback(object sender, TimePickerDialog.TimeSetEventArgs e)
        {
            try
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("it-IT");
                hour = e.HourOfDay;
                minute = e.Minute; 
				string time = string.Format("{0}:{1}", hour.ToString().PadLeft(2, '0'), minute.ToString().PadLeft(2, '0'));
                txtOraEventValue.Text = time;
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void OnBtnSaveEventClick(object sender, EventArgs e)
        {
            AndHUD progress = new AndHUD();
            progress.Show(this, "Attendere prego", -1, MaskType.Clear, TimeSpan.FromSeconds(120), null, true, null); 
            try
            {
//                if (_date <= _date2)
                {
                    if (mWakeLock != null)
                    {
                        mWakeLock.Acquire();
                    }
                    System.Threading.ThreadPool.QueueUserWorkItem(state =>
                        {  
                            try
                            {
                                bool postEvent = ApiCalls.postEventToCalendar(Utils.UserToken, _date.ToString("yyyy-MM-dd", CultureInfo.CurrentCulture), _date.ToString("yyyy-MM-dd", CultureInfo.CurrentCulture), txtOraEventValue.Text, editTxtLuogo.Text, editTxtDescription.Text);
                                if (postEvent)
                                {
                                    RunOnUiThread(() =>
                                        {
                                            Toast.MakeText(this, "Impegno personale salvato nel calendario", ToastLength.Long).Show();

                                            //clean the module
                                            txtDataDalEventValue.Text = DateTime.Today.ToString("dd/MM/yyyy", CultureInfo.CurrentCulture);
                                            txtDataAlEventValue.Text = DateTime.Today.ToString("dd/MM/yyyy", CultureInfo.CurrentCulture);
                                            txtOraEventValue.Text = DateTime.Now.ToString("HH:mm", CultureInfo.CurrentCulture);//string.Format("{0}:{1}", hour, minute.ToString().PadLeft(2, '0'));
                                            editTxtLuogo.Text = "";
                                            editTxtDescription.Text = "";

                                            AddEvent.Visibility = ViewStates.Gone;
                                            progress.Dismiss();
                                        });
                                }
                                else
                                {
                                    RunOnUiThread(() =>
                                        {
                                            Toast.MakeText(this, "Compilare i campi per salvare l’impegno personale", ToastLength.Long).Show();
                                            progress.Dismiss();
                                        });
                                } 
                            }
                            catch (Exception ex)
                            {
                                Utils.writeToDeviceLog(ex);
                            }
                            finally
                            {
                                RunOnUiThread(() =>
                                    {
                                        if (mWakeLock != null)
                                        {
                                            mWakeLock.Release();
                                        }
                                    });

                            }
                        });
                }
//                else
//                {
//                    RunOnUiThread(()=>{
//                        Toast.MakeText(this,"Data inserita non valida: la data finale deve essere maggiore di quella iniziale",ToastLength.Short).Show();
//                    });
//                }

            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
                progress.Dismiss();
            }
            finally
            {
                progress.Dismiss();
            }
        }

        #endregion

        #region Map

        RelativeLayout butAppuntamenti;
        RelativeLayout butClienti;
        LinearLayout boxCheckAppuntamenti;
        LinearLayout boxCheckClienti;
        ImageView imageAppuntamenti;
        ImageView imageClienti;
        CheckBox checkDiOggi;
        CheckBox checkNonEsiati;
        CheckBox checkInTratativa;
        CheckBox checkInBorsellino;
        CheckBox checkConvergenti;
        CheckBox checkNonConvergenti;
        CheckBox checkDeact;
        private GoogleMap _map;
        private MapFragment _mapFragment;
        private SupportMapFragment _mapFragmentSupport;

        bool checkForGps = true;
        private Location _currentLocation;
        ViewGroup container;
        ImageView iconView;
        TextView textView;

        LinearLayout boxSearchMapBtn;
        Button btnSearchOptionsMap;
        FrameLayout mapBox;
        LinearLayout boxMapSearch;

        ImageView imgMapNavigtoLoc;
        TextView txtMapSearchPlus;
        TextView txtMapSearchMinus;

        void MapLayout()
        {
            try
            {
                butAppuntamenti = FindViewById<RelativeLayout>(Resource.Id.butAppuntamenti);
                butClienti = FindViewById<RelativeLayout>(Resource.Id.butClienti);
                boxCheckAppuntamenti = FindViewById<LinearLayout>(Resource.Id.boxCheckAppuntamenti);
                boxCheckClienti = FindViewById<LinearLayout>(Resource.Id.boxCheckClienti);
                imageAppuntamenti = FindViewById<ImageView>(Resource.Id.imageAppuntamenti);
                imageClienti = FindViewById<ImageView>(Resource.Id.imageClienti);

                checkDiOggi = FindViewById<CheckBox>(Resource.Id.checkDiOggi);
                checkNonEsiati = FindViewById<CheckBox>(Resource.Id.checkNonEsiati);
                checkInTratativa = FindViewById<CheckBox>(Resource.Id.checkInTratativa);
                checkInBorsellino = FindViewById<CheckBox>(Resource.Id.checkInBorsellino);
                checkConvergenti = FindViewById<CheckBox>(Resource.Id.checkConvergenti);
                checkNonConvergenti = FindViewById<CheckBox>(Resource.Id.checkNonConvergenti);
                checkDeact = FindViewById<CheckBox>(Resource.Id.checkDeact);
                TextView textClien = FindViewById<TextView>(Resource.Id.textClien);
                TextView textAppun = FindViewById<TextView>(Resource.Id.textAppun);

                boxSearchMapBtn = FindViewById<LinearLayout>(Resource.Id.boxSearchMapBtn);
                btnSearchOptionsMap = FindViewById<Button>(Resource.Id.btnSearchOptionsMap);
                mapBox = FindViewById<FrameLayout>(Resource.Id.mapBox);
                boxMapSearch = FindViewById<LinearLayout>(Resource.Id.boxMapSearch);

                imgMapNavigtoLoc = FindViewById<ImageView>(Resource.Id.imgMapNavigtoLoc);
                txtMapSearchPlus = FindViewById<TextView>(Resource.Id.txtMapSearchPlus);
                txtMapSearchMinus = FindViewById<TextView>(Resource.Id.txtMapSearchMinus);


                textClien.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                textAppun.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                checkDiOggi.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                checkNonEsiati.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                checkInTratativa.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                checkInBorsellino.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                checkConvergenti.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                checkNonConvergenti.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                checkDeact.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                btnSearchOptionsMap.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);


                checkDiOggi.Checked = true;
                checkNonEsiati.Checked = false;
                checkInTratativa.Checked = false;
                checkInBorsellino.Checked = false;
                checkConvergenti.Checked = false;
                checkNonConvergenti.Checked = false;
                checkDeact.Checked = false;



                boxCheckAppuntamenti.Visibility = ViewStates.Gone;
                boxCheckClienti.Visibility = ViewStates.Gone;

                butAppuntamenti.Click += OnClickAppuntamenti;
                butClienti.Click += OnClickClienti;

                InitMap();

                btnSearchOptionsMap.Click += (object sender, EventArgs e) =>
                {
                    AddMeetingMarkersToMap();
                }; 

                txtMapSearchPlus.Click += (sender, e) =>
                {
                    _mapSearchAppuntamenti.AnimateCamera(CameraUpdateFactory.ZoomIn());
                };

                txtMapSearchMinus.Click += (sender, e) =>
                {
                    _mapSearchAppuntamenti.AnimateCamera(CameraUpdateFactory.ZoomOut());
                };
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        void ImgMapNavigtoLoc_Click(object sender, EventArgs e)
        {
            try
            {
                
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        void OnClickAppuntamenti(object sender, EventArgs e)
        {
            try
            {
                if (boxCheckAppuntamenti.Visibility == ViewStates.Gone)
                {
                    boxCheckAppuntamenti.Visibility = ViewStates.Visible;
                    imageAppuntamenti.SetImageResource(Resource.Drawable.arrowDown);
                    boxCheckClienti.Visibility = ViewStates.Gone;
                    imageClienti.SetImageResource(Resource.Drawable.arrowTop);
                    butAppuntamenti.SetBackgroundColor(Color.Rgb(0, 95, 167));//0,95,167//242,113,48
                    butClienti.SetBackgroundColor(Color.Rgb(242, 113, 48));
                }
                else
                {
                    boxCheckAppuntamenti.Visibility = ViewStates.Gone;
                    imageAppuntamenti.SetImageResource(Resource.Drawable.arrowTop);
                    butAppuntamenti.SetBackgroundColor(Color.Rgb(242, 113, 48));
                }

            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        void OnClickClienti(object sender, EventArgs e)
        {
            try
            {
                if (boxCheckClienti.Visibility == ViewStates.Gone)
                {
                    boxCheckClienti.Visibility = ViewStates.Visible;
                    imageClienti.SetImageResource(Resource.Drawable.arrowDown);
                    boxCheckAppuntamenti.Visibility = ViewStates.Gone;
                    imageAppuntamenti.SetImageResource(Resource.Drawable.arrowTop);
                    butClienti.SetBackgroundColor(Color.Rgb(0, 95, 167));
                    butAppuntamenti.SetBackgroundColor(Color.Rgb(242, 113, 48));
                }
                else
                {
                    boxCheckClienti.Visibility = ViewStates.Gone;
                    imageClienti.SetImageResource(Resource.Drawable.arrowTop);
                    butClienti.SetBackgroundColor(Color.Rgb(242, 113, 48));
                }

            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        private void SetupMapIfNeeded()
        {

            try
            {
                string err = "Error ";

                System.Threading.ThreadPool.QueueUserWorkItem(state =>
                    {
                        try
                        {
                            RunOnUiThread(() =>
                                {
                                    location = new LatLng(currLocation.Latitude, currLocation.Longitude);
                                    if (location != null)
                                    {
                                        _currentLocation = currLocation;
                                        MarkerOptions marker;
                                        int type;
                                        if (_mapSearchAppuntamenti != null)
                                        {
                                            _mapSearchAppuntamenti.Clear();

                                            marker = new MarkerOptions();
                                            marker.SetPosition(location);
                                            marker.SetTitle("La mia posizione");
                                            marker.Draggable(true);
                                            marker.InvokeIcon(BitmapDescriptorFactory.FromResource(Resource.Drawable.map_man));
                                            _mapSearchAppuntamenti.AddMarker(marker);


                                            CameraUpdate cameraUpdate = CameraUpdateFactory.NewLatLngZoom(location, (float)zoomLevel);
                                            _mapSearchAppuntamenti.MoveCamera(cameraUpdate);
                                            _mapSearchAppuntamenti.MyLocationEnabled = true;

                                            _mapSearchAppuntamenti.UiSettings.MyLocationButtonEnabled = false;
                                        }
                                    }
                                });
                        }
                        catch (Exception ex)
                        {
                            Utils.writeToDeviceLog(ex);
                        }
                        finally
                        {
                            if (progresV != null)
                            {
                                progresV.Dismiss();
                            }
                        }
                    });
               
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }


        }

        private void InitMap()
        {
            try
            {
                //if(androidPhoneVersion<14)
                {
                    _mapFragmentSupport = SupportFragmentManager.FindFragmentByTag("mapBox") as SupportMapFragment;
                    if (_mapFragmentSupport == null)
                    {
                        GoogleMapOptions mapOptions = new GoogleMapOptions()
                            .InvokeMapType(GoogleMap.MapTypeNormal)
                            .InvokeZoomControlsEnabled(false)
                            .InvokeCompassEnabled(true);

                        Android.Support.V4.App.FragmentTransaction fragTx = SupportFragmentManager.BeginTransaction();
                        _mapFragmentSupport = SupportMapFragment.NewInstance(mapOptions);
                        fragTx.Add(Resource.Id.mapBox, _mapFragmentSupport, "mapBox");
                        fragTx.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }

        }

        private LatLng getCurrentSelectedMarkerLocation(List<Marker> markersList)
        {
            try
            {
                if (markersList == null || markersList.Count < 1)
                {
                    return null;
                }

                foreach (Marker mrk in markersList)
                {
                    if (mrk.IsInfoWindowShown == true)
                    {
                        return mrk.Position;
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
            return null;
        }

        List<Marker> markersList;

        private void AddMeetingMarkersToMap()
        {
            try
            {
                
                //loadAppuntamentiForDay(DateTime.Now.Date,DateTime.Now.Date,false);
                List<AppuntamentiItem> listOfSelectedAppuntaments = new List<AppuntamentiItem>();
                string status_array_clienti = "";
                string status_array_appuntamenti = "";
                if (checkDiOggi.Checked)
                {
                    if (status_array_appuntamenti != "")
                    {
                        status_array_appuntamenti += ",";
                    }
                    status_array_appuntamenti += "di_oggi";
                }
                if (checkNonEsiati.Checked)
                {
                    if (status_array_appuntamenti != "")
                    {
                        status_array_appuntamenti += ",";
                    }
                    status_array_appuntamenti += "non_esitati";
                }
                if (checkInTratativa.Checked)
                {
                    if (status_array_appuntamenti != "")
                    {
                        status_array_appuntamenti += ",";
                    }
                    status_array_appuntamenti += "in_trattativa";
                }

                if (checkInBorsellino.Checked)
                {
                    if (status_array_clienti != "")
                    {
                        status_array_clienti += ",";
                    }
                    status_array_clienti += "in_borsellino";
                }
                if (checkConvergenti.Checked)
                {
                    if (status_array_clienti != "")
                    {
                        status_array_clienti += ",";
                    }
                    status_array_clienti += "convergenti";
                }
                if (checkNonConvergenti.Checked)
                {
                    if (status_array_clienti != "")
                    {
                        status_array_clienti += ",";
                    }
                    status_array_clienti += "non_convergenti";
                }
                if (checkDeact.Checked)
                {
                    if (status_array_clienti != "")
                    {
                        status_array_clienti += ",";
                    }
                    status_array_clienti += "deact";
                }

                List<ClientInfoItem> listOfSelectedClienti = new List<ClientInfoItem>();


                if (progresV != null)
                {
                    progresV.Show(this, "Attendere prego", -1, MaskType.Clear, TimeSpan.FromSeconds(120), null, true, null);
                }

                LatLng selectedMarkerLocation = getCurrentSelectedMarkerLocation(markersList);

                if (_mapSearchAppuntamenti != null)
                {
                    _mapSearchAppuntamenti.Clear();
                }
                System.Threading.ThreadPool.QueueUserWorkItem(state =>
                    {
                        try
                        {//44.6399408,10.8975089
                            double lat = 0;
                            double lng = 0;
                            if (status_array_appuntamenti != "")
                            {

                                if (checkDiOggi.Checked && !checkInTratativa.Checked && !checkNonEsiati.Checked)
                                {
									listOfSelectedAppuntaments = GetListOFAppuntamenti(DateTime.Now.Date, DateTime.Now.Date, "", "", "1", "", "", 0, 0, string.Empty);
                                    foreach (var item in listOfSelectedAppuntaments)
                                    {
                                        item.Status = "di_oggi";
                                    }
                                }
                                else
                                {
                                    if (selectedMarkerLocation != null)
                                    {
                                        lat = selectedMarkerLocation.Latitude;
                                        lng = selectedMarkerLocation.Longitude;                                        
                                    }
                                    else if (currLocation != null)
                                    {
                                        lat = currLocation.Latitude;
                                        lng = currLocation.Longitude;
                                    }

									listOfSelectedAppuntaments = GetListOFAppuntamenti(DateTime.Now.Date, DateTime.Now.Date, "", "", "2", status_array_appuntamenti, "", lat, lng, string.Empty);//44.851049, 11.582951
                                    foreach (var item in listOfSelectedAppuntaments)
                                    {
                                        if (item.AppuntamentiDateHour.Date == DateTime.Now.Date)
                                        {
                                            item.Status = "di_oggi";
                                        }
                                    }
                                }
                            }

                            double clientLat = 0;
                            double clientLng = 0;

                            if (status_array_clienti != "")
                            {
                                if (apiClient.IsConnected)
                                {
                                    if (currLocation != null)
                                    {
                                        clientLat = currLocation.Latitude;
                                        clientLng = currLocation.Longitude;
                                    
                                        //location is knowed
                                        ManualResetEvent resetEvent = new ManualResetEvent(false);  
                                        listOfSelectedClienti = LoadALlClients(true, "status", status_array_clienti, resetEvent, clientLng, clientLat, false);

                                        resetEvent.WaitOne();
                                    }
                                    else
                                    {
                                        foreach (var item in listOfSelectedAppuntaments)
                                        {
                                            if (item.Latitude != 0 && item.Longitude != 0)
                                            {
                                                clientLat = item.Latitude;
                                                clientLng = item.Longitude;
                                                break;
                                            }
                                        }

                                        if (clientLat != 0 && clientLng != 0)
                                        {

                                            ManualResetEvent resetEvent = new ManualResetEvent(false);  
                                            listOfSelectedClienti = LoadALlClients(true, "status", status_array_clienti, resetEvent, clientLng, clientLat, false);

                                            resetEvent.WaitOne();
                                        }
                                        else
                                        {
                                            //no appuntamenti so no clients
                                            ManualResetEvent resetEvent = new ManualResetEvent(false);  
                                            listOfSelectedClienti = LoadALlClients(true, "status", status_array_clienti, resetEvent, clientLng, clientLat, true);

                                            resetEvent.WaitOne();
                                        }
                                    }

                                }
                            }

                            RunOnUiThread(() =>
                                {
                                    try
                                    {
                                        markersList.Clear();

                                        if (_mapSearchAppuntamenti != null)
                                        {
                                            _mapSearchAppuntamenti.Clear();


                                            BitmapDescriptor descriptor;
                                            if (listOfSelectedAppuntaments != null)
                                            {

                                                for (int i = 0; i < listOfSelectedAppuntaments.Count; i++)
                                                {
                                                    if (listOfSelectedAppuntaments[i].Latitude != 0 && listOfSelectedAppuntaments[i].Longitude != 0)
                                                    {
                                                        descriptor = BitmapDescriptorFactory.FromBitmap(CreateIcon(listOfSelectedAppuntaments[i].AppuntamentiDateHour.ToString("HH:mm", CultureInfo.InvariantCulture), listOfSelectedAppuntaments[i].Status));
                                                        MarkerOptions markerOptions = new MarkerOptions()
                                                            .SetPosition(new LatLng(listOfSelectedAppuntaments[i].Latitude, listOfSelectedAppuntaments[i].Longitude))
                                                            .InvokeIcon(descriptor)
                                                            .SetSnippet(listOfSelectedAppuntaments[i].AppuntamentiDateHour.ToString("HH:mm", CultureInfo.InvariantCulture))
                                                            .SetTitle(listOfSelectedAppuntaments[i].RegSociale);
//                                                        _mapSearchAppuntamenti.AddMarker(markerOptions);
                                                        markersList.Add(_mapSearchAppuntamenti.AddMarker(markerOptions));
//                                                        ((Marker)markerOptions).IsInfoWindowShown 

                                                    }
                                                } 
                                            }
                                            BitmapDescriptor descriptorC;
                                            for (int i = 0; i < listOfSelectedClienti.Count; i++)
                                            {
                                                if (listOfSelectedClienti[i].Latitude != 0 && listOfSelectedClienti[i].Longitude != 0)
                                                {
                                                    descriptorC = BitmapDescriptorFactory.FromBitmap(CreateIcon(listOfSelectedClienti[i].Date_clienti.ToString("HH:mm", CultureInfo.InvariantCulture), listOfSelectedClienti[i].Status));
                                                    //                     BitmapDescriptor icon = BitmapDescriptorFactory.FromResource(Resource.Drawable.customPin_icon);
                                                    MarkerOptions markerOptionsC = new MarkerOptions()
                                                        .SetPosition(new LatLng(listOfSelectedClienti[i].Latitude, listOfSelectedClienti[i].Longitude))
                                                        .InvokeIcon(descriptorC)
                                                        .SetSnippet("Dettagli")//listOfSelectedClienti[i].Date_clienti.ToString("HH:mm", CultureInfo.InvariantCulture))
                                                        .SetTitle(listOfSelectedClienti[i].Regione_sociale);
//                                                    _mapSearchAppuntamenti.AddMarker(markerOptionsC);
                                                    markersList.Add(_mapSearchAppuntamenti.AddMarker(markerOptionsC));
                                                }
                                            }

                                            //map info window click
                                            _mapSearchAppuntamenti.InfoWindowClick += (object sender, GoogleMap.InfoWindowClickEventArgs e) =>
                                            {
                                                try
                                                {
                                                    if (listOfSelectedClienti != null && listOfSelectedClienti.Count > 0)
                                                    {
                                                        int index = listOfSelectedClienti.FindIndex(element => element.Latitude == e.Marker.Position.Latitude && element.Longitude == e.Marker.Position.Longitude);
                                                        if (index != -1)
                                                        {
                                                            Console.WriteLine(listOfSelectedClienti[index].Regione_sociale);
                                                            Intent clientiIntent = new Intent(this, typeof(ClientiActivity));
                                                            clientiIntent.PutExtra("fromAppuntamentiMap", true);
                                                            clientiIntent.PutExtra("ragioneSociale", listOfSelectedClienti[index].Regione_sociale);
                                                            clientiIntent.SetFlags(ActivityFlags.ReorderToFront);
                                                            this.StartActivity(clientiIntent);
                                                        }
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    Utils.writeToDeviceLog(ex);
                                                }
                                            };

                                            if (markersList.Count > 0)
                                            {
                                                if (_mapSearchAppuntamenti.MyLocationEnabled && _mapSearchAppuntamenti.MyLocation != null)
                                                {
                                                    MarkerOptions markerOptionsC = new MarkerOptions()
                                                    .SetPosition(new LatLng(_mapSearchAppuntamenti.MyLocation.Latitude, _mapSearchAppuntamenti.MyLocation.Longitude))
                                                        .Visible(false);
                                                
                                                    markersList.Add(_mapSearchAppuntamenti.AddMarker(markerOptionsC));
                                                }

                                                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                                                foreach (Marker marker in markersList)
                                                {
                                                    builder.Include(marker.Position);
                                                }
                                                LatLngBounds bounds = builder.Build();
                                                int padding = 0; // offset from edges of the map in pixels
//                                                CameraUpdate cu = CameraUpdateFactory.NewLatLngBounds(bounds, padding);
//                                                _mapSearchAppuntamenti.AnimateCamera(cu);
                                                CameraUpdate cameraUpdate = CameraUpdateFactory.NewLatLngZoom(location, (float)zoomLevel);
                                                _mapSearchAppuntamenti.MoveCamera(cameraUpdate);
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Utils.writeToDeviceLog(ex);
                                    }

                                });
                        }
                        catch (Exception ex)
                        {
                            Utils.writeToDeviceLog(ex);
                        }
                        finally
                        {
                            if (progresV != null)
                            {
                                progresV.Dismiss();
                            }
                        }
                    });
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public DateTime? lastLoadedDay = null;

		public async System.Threading.Tasks.Task loadAppuntamentiForDay(DateTime date_from, DateTime date_to, bool fromCalendar, string appAppMemoId)
        {
            try
            {
                lastLoadedDay = date_from;
                
                AndHUD progressVaiew = new AndHUD();
                
                progressVaiew.Show(this, "Attendere prego", -1, MaskType.Clear, TimeSpan.FromSeconds(120), null, true, null); 

                System.Threading.ThreadPool.QueueUserWorkItem(state =>
                    {	
                        try
                        {
                            Geocoder geocoder = new Geocoder(this, Java.Util.Locale.Default);
							string search_type="1";
							if(appAppMemoId!=string.Empty)
								search_type="3";
							else
								search_type="1";
                            appuntamentiList = new List<AppuntamentiItem>();                
							appuntamentiList = GetListOFAppuntamenti(date_from, date_to, "", "", search_type, "", "", 0, 0, appAppMemoId);
                            if (appuntamentiList != null && appuntamentiList.Count > 0)
                            {
                                if (appuntamentiList.Count == 2 && appuntamentiList[0].AppuntamentiDateHour.Hour == 0 && appuntamentiList[1].AppuntamentiDateHour.Hour == 23)
                                {
                                    //app blocate
                                    RunOnUiThread(() =>
                                        {
                                            timeListAdapter = new ApputamentiTimeAdapter(this, appuntamentiList, -1);
                                            if (timeListAdapter != null)
                                            {
                                                listApputamenti.Adapter = timeListAdapter;
                                            }

                                            OnRefreshListClickEvent();

                                            if (progressVaiew != null)
                                            {
                                                progressVaiew.Dismiss();
                                            }
//                                    Toast.MakeText(this, "Appuntamento blocato", ToastLength.Short).Show();
                                        });

                                }
                                else
                                {
                                    if (fromCalendar)
                                    { 
                                        timeListAdapter = new ApputamentiTimeAdapter(this, appuntamentiList, -1);
                                        RunOnUiThread(() =>
                                            {

                                                listApputamenti.Adapter = timeListAdapter;
                                                timeListAdapter.RowClickEvent += OnRowFromListClickEvent;
                                                OnClickFromListClickEvent(0);
                                                timeListAdapter.ItemClickIdEvent += OnClickFromListClickEvent;
                                                OnRowFromListClickEvent(0);

                                                if (progressVaiew != null)
                                                {
                                                    progressVaiew.Dismiss();
                                                }
                                            });
                                    }
                                    else
                                    {
                                        RunOnUiThread(() =>
                                            {  
                                                try
                                                {
                                                    timeListAdapter = new ApputamentiTimeAdapter(this, appuntamentiList, -1);
                                                    if (timeListAdapter != null)
                                                    {
                                                        listApputamenti.Adapter = timeListAdapter;
                                                    }
//                                            timeListAdapter.RowClickEvent += OnRowFromListClickEvent;
                                                    timeListAdapter.ItemClickIdEvent += OnClickFromListClickEvent;

                                                }
                                                catch (Exception ex)
                                                {
                                                    Utils.writeToDeviceLog(ex);
                                                }
                                                finally
                                                {
                                                    if (progressVaiew != null)
                                                    {
                                                        progressVaiew.Dismiss();
                                                    }
                                                }
                                            });
                                    }
                                }
                            }
                            else
                            {
                                if (aagInBlocco != "")
                                {
                                    RunOnUiThread(() =>
                                        {
                                            Toast.MakeText(this, aagInBlocco, ToastLength.Long).Show();
                                            ResetValues();
                                            listApputamenti.Adapter = null; 

                                            OnMenuCalendarClick(null, null);

                                            if (progressVaiew != null)
                                            {
                                                progressVaiew.Dismiss();
                                            }
                                        });

                                }
                                else
                                {
                                    //no appuntamenti
                                    RunOnUiThread(() =>
                                        {
                                            if (!this.IsFinishing)
                                            {
                                                Toast.MakeText(this, "Nessun appuntamento per il giorno selezionato", ToastLength.Short).Show();
                                                ResetValues();
                                                listApputamenti.Adapter = null; 

                                                if (fromCalendar)
                                                {
                                                    OnMenuCalendarClick(null, null);
                                                }
                                            }

                                            if (progressVaiew != null)
                                            {
                                                progressVaiew.Dismiss();
                                            }
                                        });
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Utils.writeToDeviceLog(ex);

                            if (progressVaiew != null)
                            {
                                progressVaiew.Dismiss();
                            }
                        }	                
                    });
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

		public List<AppuntamentiItem> GetListOFAppuntamenti(DateTime date_from, DateTime date_to, string regSociale, string localita, string search_type, string status_array_appuntamenti, string status_array_clienti, double lat, double lang, string appIdForMemo)
        {
            List<AppuntamentiItem> listAppuntamentiReturn = new List<AppuntamentiItem>();

            try
            {
                RunOnUiThread(() =>
                    {
                        if (mWakeLock != null)
                        {
                            mWakeLock.Acquire();
                        }
                    });

                if (Utils.CheckForInternetConn())
                {
					JToken appuntamentiJtoken = ApiCalls.getAppuntamentiInPeriod(Utils.UserToken, date_from.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture), date_to.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture), regSociale, localita, search_type, status_array_appuntamenti, status_array_clienti, lat, lang,appIdForMemo);
                    if (appuntamentiJtoken.ToString().Contains("blocked"))
                    {
                        aagInBlocco = "AGENTE IN BLOCCO";
                    }
                    else
                    {
                        if (appuntamentiJtoken != null && appuntamentiJtoken.HasValues)
                        {
                            foreach (var itemJtoken in appuntamentiJtoken)
                            {
                                List<string> emailListApp = new List<string>();
                                List<string> phoneListApp = new List<string>();
                                AppuntamentiItem item = new AppuntamentiItem();
                                item.Id = itemJtoken["ID"].ToString();
                                try
                                {
                                    DateTime appDateHour;
                                    DateTime.TryParseExact(itemJtoken["date_appointment"].ToString() + " " + itemJtoken["ora_appointment"].ToString(), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out appDateHour);

                                    item.AppuntamentiDateHour = appDateHour;
                                    item.appuntamentiChangedTime = appDateHour;
                                }
                                catch (Exception ex)
                                {
                                    Utils.writeToDeviceLog(ex);                                         
                                }
                                if (!string.IsNullOrEmpty(itemJtoken["data_appuntamento_originale"].ToString()))
                                {
                                    try
                                    {
                                        DateTime original_appDateHour;
                                        DateTime.TryParseExact(itemJtoken["data_appuntamento_originale"].ToString(), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out original_appDateHour);
                                        item.original_appuntamentiDateHour = original_appDateHour;
                                    }
                                    catch (Exception ex)
                                    {
                                        Utils.writeToDeviceLog(ex);                                         
                                    }
                                }

                                item.Mail = itemJtoken["contact_mail"].ToString();
                                item.Note = itemJtoken["note"].ToString();
                                if (itemJtoken["contact_phone"].ToString() != "")
                                {
                                    item.Phone = itemJtoken["contact_phone"].ToString().Substring(1);
                                }

                                if (item.AppuntamentiDateHour.Hour == 0 || item.AppuntamentiDateHour.Hour == 23)
                                {
									item.RegSociale = "STOP APPUNTAMENTI";
                                    item.AppBloccati = true;
                                }
                                else
                                {
                                    item.RegSociale = itemJtoken["ragione_sociale"].ToString();
                                    item.AppBloccati = false;
                                }

                                item.RepresentatnteLegale = itemJtoken["contact_name"].ToString();
                                item.Status = itemJtoken["esito"].ToString();
                                item.TipAppuntamenti = itemJtoken["type"].ToString();
                                item.City = itemJtoken["location"].ToString();

                                if (itemJtoken["lat"] != null && itemJtoken["lat"].ToString() != "")
                                {
                                    double latDouble;
                                    double.TryParse(itemJtoken["lat"].ToString(), NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingSign, CultureInfo.InvariantCulture, out latDouble);
                                    item.Latitude = latDouble;
                                }
                                if (itemJtoken["lng"] != null && itemJtoken["lng"].ToString() != "")
                                {
                                    double longDouble;
                                    double.TryParse(itemJtoken["lng"].ToString(), NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingSign, CultureInfo.InvariantCulture, out longDouble);
                                    item.Longitude = longDouble;
                                }

                                item.TipClient = itemJtoken["status_customer"].ToString();

                                item.Id_Chiamata = itemJtoken["id_chiamata"].ToString();

                                if (itemJtoken["date_chiamata"].ToString() != "")
                                {
                                    try
                                    {
                                        DateTime appDateChiamataHour;
                                        DateTime.TryParseExact(itemJtoken["date_chiamata"].ToString().Replace(",", ""), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out appDateChiamataHour);

                                        item.Date_chiamata = appDateChiamataHour;
                                    }
                                    catch (Exception ex)
                                    {
                                        Utils.writeToDeviceLog(ex);                                         
                                    }
                                }

                                item.Note_agent = itemJtoken["note_agent"].ToString();
//                                item.Note_chiamata = itemJtoken["note_chiamata"].ToString();

                                Encoding iso = Encoding.GetEncoding("ISO-8859-1");
                                Encoding utf8 = Encoding.UTF8;
                                byte[] utfBytes = iso.GetBytes(itemJtoken["note_chiamata"].ToString());
                                byte[] isoBytes = Encoding.Convert(iso, utf8, utfBytes);
                                item.Note_chiamata = iso.GetString(isoBytes);

                                item.Note_segretaria = itemJtoken["note_segreteria"].ToString();
                                item.Motivo = itemJtoken["motivo"].ToString();
                                item.Category = itemJtoken["categoria"].ToString();
                                item.Indirizzo = itemJtoken["indirizzo"].ToString();
                                item.Civico = itemJtoken["civico"].ToString();
                                item.Cap = itemJtoken["zipcode"].ToString();
                                //                            item.Comune = itemJtoken["comune"].ToString();
                                item.Provincia = itemJtoken["province"].ToString();
                                //                            item.Sigla = itemJtoken["sigla"].ToString();
                                item.Contact_name = itemJtoken["contact_name"].ToString();
                                if (itemJtoken["contact_phone"].ToString() != "")
                                {
                                    item.Contact_phone = itemJtoken["contact_phone"].ToString().Substring(1);
                                }

                                item.Contact_mail = itemJtoken["contact_mail"].ToString();
                                item.Id_customer = itemJtoken["id_customer"].ToString();
                                item.Source = itemJtoken["source"].ToString();
                                item.Acronim = itemJtoken["acronym"].ToString();
                                item.Id_appto_memo = itemJtoken["id_appto_memo"].ToString();
                                item.Evento_pers = itemJtoken["evento_pers"].ToString();
								item.DateChanged=false;

                                if (item.Contact_name != "")
                                {
                                    item.ContattiInfo = item.Contact_name;
                                }
                                if (item.Contact_phone != null && item.Contact_phone != "")
                                {
                                    item.ContattiInfo += "\n" + "Telefono: " + item.Contact_phone;
                                    bool has = phoneListApp.Any(cus => cus == item.Contact_phone);
                                    if (!has)
                                        phoneListApp.Add(item.Contact_phone);
                                }
                                if (item.Contact_mail != null && item.Contact_mail != "")
                                {
                                    item.ContattiInfo += "\n" + item.Contact_mail;
                                    bool has = emailListApp.Any(cus => cus == item.Contact_mail);
                                    if (!has)
                                        emailListApp.Add(item.Contact_mail);
                                }
                                if (item.Note != "")
                                {
                                    item.ContattiInfo += "\n" + "Note: " + item.Note;
                                }
                                item.ContattiInfo += "\n";

                                item.PhoneList = phoneListApp;
                                item.EmailList = emailListApp;

                                int index = listAppuntamentiReturn.FindIndex(element => element.Id == itemJtoken["ID"].ToString());
                                if (index != -1)
                                {

                                    if (item.Contact_phone != null && item.Contact_phone != "")
                                    {
                                        bool has = listAppuntamentiReturn[index].PhoneList.Any(cus => cus == item.Contact_phone);
                                        if (!has)
                                            listAppuntamentiReturn[index].PhoneList.Add(item.Contact_phone);
                                    }
                                    if (item.Contact_mail != null && item.Contact_mail != "")
                                    {
                                        bool has = listAppuntamentiReturn[index].EmailList.Any(cus => cus == item.Contact_mail);
                                        if (!has)
                                            listAppuntamentiReturn[index].EmailList.Add(item.Contact_mail);
                                    }

                                    listAppuntamentiReturn[index].ContattiInfo += "\n" + item.ContattiInfo;
//                                    if(item.Contact_phone!="")
//                                    {
//                                        listAppuntamentiReturn[index].Contact_phone+= "\n"+item.Contact_phone;
//                                    }
//                                    if(item.Contact_mail!="")
//                                    {
//                                        listAppuntamentiReturn[index].Contact_mail+= "\n"+item.Contact_mail;
//                                    }
//                                    if(item.Contact_name!="")
//                                    {
//                                        listAppuntamentiReturn[index].Contact_name+= "\n"+item.Contact_name;
//                                    }

                                    continue;
                                }
                                else
                                {
                                    listAppuntamentiReturn.Add(item);
                                }

                            }
                        }
                    }

                    //after get appuntamenti-save to db
					SaveAppToDbOnOtherThread(listAppuntamentiReturn);
                }
                else
                {
                    //no internet connection -get app from db-- TODO

                    RunOnUiThread(() =>
                        {
                            Toast.MakeText(this, "Nessuna connessione Internet", ToastLength.Short).Show();
                        });
                    listAppuntamentiReturn = db.getAllAppuntamenti(date_from.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture), date_to.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture), regSociale, localita);
                }				
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
                listAppuntamentiReturn = null;
            }
            finally
            {
                RunOnUiThread(() =>
                    {
                        if (search_type == "1")
                        {
                            Task.Run(async () => await AddMeetingMarkersToMapEvents());
                        }
						else if(search_type == "3")
						{
							if(appIdForMemo!=string.Empty)
							{
								Task.Run(async () => await AddMeetingMarkersToMapEvents());
							}
						}
                        if (mWakeLock != null)
                        {
                            mWakeLock.Release();
                        }
                    });
            }
            return listAppuntamentiReturn;
        }


		public void SaveAppToDbOnOtherThread(List<AppuntamentiItem> listAppuntamentiReturn)
		{
			new Thread(() => 
				{
					Thread.CurrentThread.IsBackground = true; 
					foreach (var item in listAppuntamentiReturn)
					{
						if (db.CheckIfExiststAppuntamentiDB(item.Id))
						{
							db.UpdateAppuntamentiInfo(item);
						}
						else
						{
							db.InsertAppuntamentiToTable(item);
						}
					}
				}).Start();
		}

        public Bitmap CreateIcon(string text, string type)
        {
            Bitmap bitmap = null;
            try
            {
                EnsureInit();

                // Update the icon
                //                iconView.SetImageDrawable(drawable);
                //                drawable.Dispose();

                // Update the text
                //(in_borsellino, convergenti,non_convergenti,deact)

                switch (type.ToLower())
                {
                    case "di_oggi":
                        iconView.SetImageResource(Resource.Drawable.app_dioggi);
                        break;
                    case "":
                        iconView.SetImageResource(Resource.Drawable.app_nonesitati);
                        break;
                    case "trattativa":
                        iconView.SetImageResource(Resource.Drawable.app_intratativa);
                        break;
                    case "borsellino":
                        iconView.SetImageResource(Resource.Drawable.clienti_inborcellino);
                        break;
                    case "convergente":
                        iconView.SetImageResource(Resource.Drawable.clienti_convergenti);
                        break;
                    case "nonconvergente":
                        iconView.SetImageResource(Resource.Drawable.clienti_nonconvergenti);
                        break;
                    case "deactivated":
                        iconView.SetImageResource(Resource.Drawable.clienti_deact);
                        break;
                } 

                textView.Text = text;
                textView.Visibility = !String.IsNullOrEmpty(text) ? ViewStates.Visible : ViewStates.Gone;
                if (text == " ")
                    textView.SetBackgroundColor(Color.Transparent);

                // Lay out size and measure
                var measureSpec = ViewGroup.MeasureSpec.MakeMeasureSpec(0, MeasureSpecMode.Unspecified);
                container.Measure(measureSpec, measureSpec);
                int measuredWidth = container.MeasuredWidth;
                int measuredHeight = container.MeasuredHeight;
                container.Layout(0, 0, measuredWidth, measuredHeight);

                // Take current layout and draw to a bitmap
                bitmap = Bitmap.CreateBitmap(measuredWidth, measuredHeight, Bitmap.Config.Argb4444); // According to API docs, this is deprecated to Argb8888; however, testing shows a reduction in memory pressure
                bitmap.EraseColor(Color.Transparent);
                using (var canvas = new Canvas(bitmap))
                {
                    container.Draw(canvas);
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }

            return bitmap;
        }

        public void EnsureInit()
        {
            try
            {
                if (container == null)
                {
                    container = this.LayoutInflater.Inflate(Resource.Layout.mapPinForMapActivity, null) as ViewGroup;
                    iconView = container.FindViewById<ImageView>(Resource.Id.imgCustomPin);
                    textView = container.FindViewById<TextView>(Resource.Id.textView1);
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        private void MapOnMarkerClick(object sender, GoogleMap.MarkerClickEventArgs markerClickEventArgs)
        {
            try
            {
                markerClickEventArgs.Handled = true;

                Marker marker = markerClickEventArgs.Marker;
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        #endregion

        #region Search

        ListView listEvents;
        RadioButton radioLOC;
        RadioButton radioRS;
        List<AppuntamentiItem> itemsSearch;
        RelativeLayout boxListSea;
        ApputamentiSearchAdapter timeListSearchAdapter;
        LinearLayout detailsAputtamentiSea;
        ImageView imgStatusArrowSea;
        TextView txtEsitoSea;
        RelativeLayout rlForListViewSea;
        EditText editSearch;
        TextView txtLocationSea;
        TextView txtNameSea;
        TextView txtPhoneSea;
        TextView txtMailSea;
        EditText editNote1;
        LinearLayout lyBackgroundSea;
        LinearLayout baraBack;
        LinearLayout boxEsitoSea;
        LinearLayout btnSalvaSea;
        LinearLayout btnEmailSea;
        LinearLayout btnPhoneSea;
        Button btnSearchAppuntamenti;
        TextView lblEsitoSea;

        LinearLayout LaydetailsImpPersonaleSea;
        TextView lblDetailsAggImpDataDalSea;
        TextView lblDetailsAggImpDataDalValueSea;
        TextView lblDetailsAggImpDataAlSea;
        TextView lblDetailsAggImpDataAlValueSea;
        TextView lblDetailsAggImpOraSea;
        TextView lblDetailsAggImpOraValueSea;
        TextView lblDetailsAggImpLuogoSea;
        TextView lblDetailsAggImpLuogoValueSea;

        TextView txtAppImpPersDescriptionSea;

        LinearLayout boxDetailsSearch;
        LinearLayout BoxAppuntamentiMoreDetailsSea;
        TextView txtAppCodChiamataLabelSea;
        TextView txtAppCodChiamataValueSea;
        TextView txtAppDataChiamataLabelSea;
        TextView txtAppDataChiamataValueSea;
        TextView txtAppDataAppuntamentoLabelSea;
        TextView txtAppDataAppuntamentoValueSea;
        TextView txtAppOraAppuntamentoLabelSea;
        TextView txtAppOraAppuntamentoValueSea;
        TextView txtAppNoteAgenteLabelSea;
        TextView txtAppNoteAgenteValueSea;
        TextView txtAppNoteChiamataLabelSea;
        TextView txtAppNoteChiamataValueSea;
        TextView txtAppNoteSegretariaLabelSea;
        TextView txtAppNoteSegretariaValueSea;
        TextView txtAppMotiveLabelSea;
        TextView txtAppMotiveValueSea;
        TextView txtAppDatiClienteLabelSea;
        TextView txtAppRagioneSocialeLabelSea;
        TextView txtAppRagioneSocialeValueSea;
        TextView txtAppCategoriaLabelSea;
        TextView txtAppCategoriaValueSea;
        TextView txtAppIndirizzoLabelSea;
        TextView txtAppIndirizzoValueSea;
        TextView txtAppContattiLabelSea;
        TextView txtAppContattiValueSea;
        TextView txtAppDescriptionSea;

        TextView txtAppIndirizzoLine2Sea;
        TextView txtAppContattiLine2Sea;
        TextView txtAppContattiLine3Sea;
        TextView txtAppContattiLine4Sea;
        LinearLayout boxButtonsSearch;

        LinearLayout LaydetailsAppMemoSea;
        TextView lblDetailsAggImpTitleSea;
        TextView lblDetailsAppMemoPlaceSea;
        TextView lblDetailsAppMemoPlaceValueSea;
        TextView lblDetailsAppMemoTitleSea;
        TextView lblDetailsAppMemoTitleValueSea;
        TextView txtAppMemoDescriptionSea;
        TextView txtMemovaiAiClientSea;
        LinearLayout boxMemoGottoClientDetailSea;
        ImageView imgSearchChangeAppHour;
		TextView txtAppImpPersEvent_PersSea;

        void SearchLayout()
        {
            try
            {
                listEvents = FindViewById<ListView>(Resource.Id.listEvents);
                boxListSea = FindViewById<RelativeLayout>(Resource.Id.boxListSea);
                editSearch = FindViewById<EditText>(Resource.Id.editSearch);
                txtLocationSea = FindViewById<TextView>(Resource.Id.txtLocationSea);
                txtNameSea = FindViewById<TextView>(Resource.Id.txtNameSea);
                txtPhoneSea = FindViewById<TextView>(Resource.Id.txtPhoneSea);
                txtMailSea = FindViewById<TextView>(Resource.Id.txtMailSea);
                lyBackgroundSea = FindViewById<LinearLayout>(Resource.Id.lyBackgroundSea);
                baraBack = FindViewById<LinearLayout>(Resource.Id.baraBack);
                btnSearchAppuntamenti = FindViewById<Button>(Resource.Id.btnSearchAppuntamenti);

                itemsSearch = new List<AppuntamentiItem>();

                //            timeListSearchAdapter = new ApputamentiSearchAdapter(this, appuntamentiList, -1);
                //            listEvents.Adapter = timeListSearchAdapter;
                //            timeListSearchAdapter.ItemClickIdEvent += (position) => OnClickItem(position, appuntamentiList);

                radioLOC = FindViewById<RadioButton>(Resource.Id.radioLOC);
                radioRS = FindViewById<RadioButton>(Resource.Id.radioRS);
                detailsAputtamentiSea = FindViewById<LinearLayout>(Resource.Id.detailsAputtamentiSea);
                radioRS.SetTextColor(new Android.Graphics.Color(255, 255, 255));
                radioLOC.SetTextColor(new Android.Graphics.Color(43, 55, 69));
                imgStatusArrowSea = FindViewById<ImageView>(Resource.Id.imgStatusArrowSea);
                txtEsitoSea = FindViewById<TextView>(Resource.Id.txtEsitoSea);
                lblEsitoSea = FindViewById<TextView>(Resource.Id.lblEsitoSea);
                boxEsitoSea = FindViewById<LinearLayout>(Resource.Id.boxEsitoSea);
                btnSalvaSea = FindViewById<LinearLayout>(Resource.Id.btnSalvaSea);
                btnEmailSea = FindViewById<LinearLayout>(Resource.Id.btnEmailSea);
                btnPhoneSea = FindViewById<LinearLayout>(Resource.Id.btnPhoneSea);
                rlForListViewSea = FindViewById<RelativeLayout>(Resource.Id.rlForListViewSea);
                editNote1 = FindViewById<EditText>(Resource.Id.editNote1);
                TextView txtSalvaSea = FindViewById<TextView>(Resource.Id.txtSalvaSea);
                //rlForListView.SetBackgroundColor(Color.White);
                baraBack.SetBackgroundColor(Color.White);


                LaydetailsImpPersonaleSea = FindViewById<LinearLayout>(Resource.Id.LaydetailsImpPersonaleSea);
                lblDetailsAggImpDataDalSea = FindViewById<TextView>(Resource.Id.lblDetailsAggImpDataDalSea);
                lblDetailsAggImpDataDalValueSea = FindViewById<TextView>(Resource.Id.lblDetailsAggImpDataDalValueSea);    
                lblDetailsAggImpDataAlSea = FindViewById<TextView>(Resource.Id.lblDetailsAggImpDataAlSea); 
                lblDetailsAggImpDataAlValueSea = FindViewById<TextView>(Resource.Id.lblDetailsAggImpDataAlValueSea); 
                lblDetailsAggImpOraSea = FindViewById<TextView>(Resource.Id.lblDetailsAggImpOraSea); 
                lblDetailsAggImpOraValueSea = FindViewById<TextView>(Resource.Id.lblDetailsAggImpOraValueSea); 
                lblDetailsAggImpLuogoSea = FindViewById<TextView>(Resource.Id.lblDetailsAggImpLuogoSea); 
                lblDetailsAggImpLuogoValueSea = FindViewById<TextView>(Resource.Id.lblDetailsAggImpLuogoValueSea);
                txtAppImpPersDescriptionSea = FindViewById<TextView>(Resource.Id.txtAppImpPersDescriptionSea);

                boxDetailsSearch = FindViewById<LinearLayout>(Resource.Id.boxDetailsSearch);
                BoxAppuntamentiMoreDetailsSea = FindViewById<LinearLayout>(Resource.Id.BoxAppuntamentiMoreDetailsSea);
                txtAppCodChiamataLabelSea = FindViewById<TextView>(Resource.Id.txtAppCodChiamataLabelSea);
                txtAppCodChiamataValueSea = FindViewById<TextView>(Resource.Id.txtAppCodChiamataValueSea);
                txtAppDataChiamataLabelSea = FindViewById<TextView>(Resource.Id.txtAppDataChiamataLabelSea);
                txtAppDataChiamataValueSea = FindViewById<TextView>(Resource.Id.txtAppDataChiamataValueSea);
                txtAppDataAppuntamentoLabelSea = FindViewById<TextView>(Resource.Id.txtAppDataAppuntamentoLabelSea);
                txtAppDataAppuntamentoValueSea = FindViewById<TextView>(Resource.Id.txtAppDataAppuntamentoValueSea);
                txtAppOraAppuntamentoLabelSea = FindViewById<TextView>(Resource.Id.txtAppOraAppuntamentoLabelSea);
                txtAppOraAppuntamentoValueSea = FindViewById<TextView>(Resource.Id.txtAppOraAppuntamentoValueSea);
                txtAppNoteAgenteLabelSea = FindViewById<TextView>(Resource.Id.txtAppNoteAgenteLabelSea);
                txtAppNoteAgenteValueSea = FindViewById<TextView>(Resource.Id.txtAppNoteAgenteValueSea);
                txtAppNoteChiamataLabelSea = FindViewById<TextView>(Resource.Id.txtAppNoteChiamataLabelSea);
                txtAppNoteChiamataValueSea = FindViewById<TextView>(Resource.Id.txtAppNoteChiamataValueSea);
                txtAppNoteSegretariaLabelSea = FindViewById<TextView>(Resource.Id.txtAppNoteSegretariaLabelSea);
                txtAppNoteSegretariaValueSea = FindViewById<TextView>(Resource.Id.txtAppNoteSegretariaValueSea);
                txtAppMotiveLabelSea = FindViewById<TextView>(Resource.Id.txtAppMotiveLabelSea);
                txtAppMotiveValueSea = FindViewById<TextView>(Resource.Id.txtAppMotiveValueSea);
                txtAppDatiClienteLabelSea = FindViewById<TextView>(Resource.Id.txtAppDatiClienteLabelSea);
                txtAppRagioneSocialeLabelSea = FindViewById<TextView>(Resource.Id.txtAppRagioneSocialeLabelSea);
                txtAppRagioneSocialeValueSea = FindViewById<TextView>(Resource.Id.txtAppRagioneSocialeValueSea);

                txtAppCategoriaLabelSea = FindViewById<TextView>(Resource.Id.txtAppCategoriaLabelSea);
                txtAppCategoriaValueSea = FindViewById<TextView>(Resource.Id.txtAppCategoriaValueSea);
                txtAppIndirizzoLabelSea = FindViewById<TextView>(Resource.Id.txtAppIndirizzoLabelSea);
                txtAppIndirizzoValueSea = FindViewById<TextView>(Resource.Id.txtAppIndirizzoValueSea);
                txtAppContattiLabelSea = FindViewById<TextView>(Resource.Id.txtAppContattiLabelSea);
                txtAppContattiValueSea = FindViewById<TextView>(Resource.Id.txtAppContattiValueSea);
                txtAppDescriptionSea = FindViewById<TextView>(Resource.Id.txtAppDescriptionSearch);

                txtAppIndirizzoLine2Sea = FindViewById<TextView>(Resource.Id.txtAppIndirizzoLine2Sea);
                txtAppContattiLine2Sea = FindViewById<TextView>(Resource.Id.txtAppContattiLine2Sea);
                txtAppContattiLine3Sea = FindViewById<TextView>(Resource.Id.txtAppContattiLine3Sea);
                txtAppContattiLine4Sea = FindViewById<TextView>(Resource.Id.txtAppContattiLine4Sea);
                boxButtonsSearch = FindViewById<LinearLayout>(Resource.Id.boxButtonsSearch);
				txtAppImpPersEvent_PersSea=FindViewById<TextView>(Resource.Id.txtAppImpPersEvent_PersSea);

                //memo details
                LaydetailsAppMemoSea = FindViewById<LinearLayout>(Resource.Id.LaydetailsAppMemoSea);
                lblDetailsAggImpTitleSea = FindViewById<TextView>(Resource.Id.lblDetailsAggImpTitleSea);
                lblDetailsAppMemoPlaceSea = FindViewById<TextView>(Resource.Id.lblDetailsAppMemoPlaceSea);
                lblDetailsAppMemoPlaceValueSea = FindViewById<TextView>(Resource.Id.lblDetailsAppMemoPlaceValueSea);
                lblDetailsAppMemoTitleSea = FindViewById<TextView>(Resource.Id.lblDetailsAppMemoTitleSea);
                lblDetailsAppMemoTitleValueSea = FindViewById<TextView>(Resource.Id.lblDetailsAppMemoTitleValueSea);
                txtAppMemoDescriptionSea = FindViewById<TextView>(Resource.Id.txtAppMemoDescriptionSea);
                txtMemovaiAiClientSea = FindViewById<TextView>(Resource.Id.txtMemovaiAiClientSea);
                boxMemoGottoClientDetailSea = FindViewById<LinearLayout>(Resource.Id.boxMemoGottoClientDetailSea);

//                imgSearchChangeAppHour=FindViewById<ImageView>(Resource.Id.imgSearchChangeAppHour);

                txtLocationSea.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtNameSea.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtPhoneSea.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtMailSea.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                editSearch.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                radioRS.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                radioLOC.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtEsitoSea.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                lblEsitoSea.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                editNote1.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtSalvaSea.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);

                lblDetailsAggImpDataDalSea.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                lblDetailsAggImpDataDalValueSea.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                lblDetailsAggImpDataAlSea.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                lblDetailsAggImpDataAlValueSea.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                lblDetailsAggImpOraSea.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                lblDetailsAggImpOraValueSea.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                lblDetailsAggImpLuogoSea.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                lblDetailsAggImpLuogoValueSea.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);

				txtAppImpPersDescriptionSea.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
				txtAppImpPersEvent_PersSea.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);

                txtAppCodChiamataLabelSea.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtAppDataChiamataLabelSea.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtAppDataAppuntamentoLabelSea.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtAppOraAppuntamentoLabelSea.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtAppNoteAgenteLabelSea.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtAppNoteChiamataLabelSea.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtAppNoteSegretariaLabelSea.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtAppMotiveLabelSea.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtAppDatiClienteLabelSea.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtAppRagioneSocialeLabelSea.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtAppCategoriaLabelSea.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtAppIndirizzoLabelSea.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtAppContattiLabelSea.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);

                txtAppCodChiamataValueSea.SetTypeface(OswaldLight, TypefaceStyle.Normal);
                txtAppDataChiamataValueSea.SetTypeface(OswaldLight, TypefaceStyle.Normal);
                txtAppDataAppuntamentoValueSea.SetTypeface(OswaldLight, TypefaceStyle.Normal);
                txtAppOraAppuntamentoValueSea.SetTypeface(OswaldLight, TypefaceStyle.Normal);
                txtAppNoteAgenteValueSea.SetTypeface(OswaldLight, TypefaceStyle.Normal);
                txtAppNoteChiamataValueSea.SetTypeface(OswaldLight, TypefaceStyle.Normal);
                txtAppNoteSegretariaValueSea.SetTypeface(OswaldLight, TypefaceStyle.Normal);
                txtAppMotiveValueSea.SetTypeface(OswaldLight, TypefaceStyle.Normal);
                txtAppRagioneSocialeValueSea.SetTypeface(OswaldLight, TypefaceStyle.Normal);
                txtAppCategoriaValueSea.SetTypeface(OswaldLight, TypefaceStyle.Normal);
                txtAppIndirizzoValueSea.SetTypeface(OswaldLight, TypefaceStyle.Normal);
                txtAppIndirizzoLine2Sea.SetTypeface(OswaldLight, TypefaceStyle.Normal);
                txtAppContattiValueSea.SetTypeface(OswaldLight, TypefaceStyle.Normal);
                txtAppContattiLine2Sea.SetTypeface(OswaldLight, TypefaceStyle.Normal);
                txtAppContattiLine3Sea.SetTypeface(OswaldLight, TypefaceStyle.Normal);
                txtAppContattiLine4Sea.SetTypeface(OswaldLight, TypefaceStyle.Normal);
                txtAppDescriptionSea.SetTypeface(OswaldLight, TypefaceStyle.Normal);

                lblDetailsAggImpTitleSea.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                lblDetailsAppMemoPlaceSea.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                lblDetailsAppMemoPlaceValueSea.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                lblDetailsAppMemoTitleSea.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                lblDetailsAppMemoTitleValueSea.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtAppMemoDescriptionSea.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtMemovaiAiClientSea.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);


                radioLOC.CheckedChange += OnClickRadio;
                radioRS.CheckedChange += OnClickRadio;
//                boxEsitoSea.Click += OnClickEsitoSea;

                btnSearchAppuntamenti.Click += OnTextChangeSearch;
                boxDetailsSearch.Click += BoxDetailsSearch_Click;

                LaydetailsImpPersonaleSea.Visibility = ViewStates.Gone;
                BoxAppuntamentiMoreDetailsSea.Visibility = ViewStates.Gone;
                boxButtonsSearch.Visibility = ViewStates.Gone;
                LaydetailsAppMemoSea.Visibility = ViewStates.Gone;


            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }

           
//            editSearch.TextChanged += new EventHandler<TextChangedEventArgs>(OnTextChangeSearch);
        }

        void BoxDetailsSearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (BoxAppuntamentiMoreDetailsSea.Visibility == ViewStates.Gone)
                {
                    BoxAppuntamentiMoreDetailsSea.Visibility = ViewStates.Visible;
                }
                else
                {
                    BoxAppuntamentiMoreDetailsSea.Visibility = ViewStates.Gone;
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        void OnClickRadio(object sender, EventArgs e)
        {
            try
            {
                if (radioLOC.Checked)
                {
                    radioLOC.SetTextColor(new Android.Graphics.Color(255, 255, 255));
                    radioRS.SetTextColor(new Android.Graphics.Color(53, 53, 53));
                    if (!string.IsNullOrEmpty(editSearch.Text))
                    {

                        RelativeLayout.LayoutParams rel_btn = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent, ViewGroup.LayoutParams.MatchParent);
                        rel_btn.AddRule(LayoutRules.AlignParentLeft);
                        rlForListViewSea.LayoutParameters = rel_btn;
                        detailsAputtamentiSea.Visibility = ViewStates.Gone;
//                        itemsSearch.Clear();
//                        itemsSearch = GetListOFAppuntamenti(DateTime.MinValue, DateTime.Now.Date, editSearch.Text.ToLower(), "", "3", "", "");
                        List<AppuntamentiItem> itemsNewSearchFilter = new List<AppuntamentiItem>();
                        if (itemsSearch != null && itemsSearch.Count > 0)
                        {
//                            timeListSearchAdapter = new ApputamentiSearchAdapter(this, itemsSearch, -1);
//                            listEvents.Adapter = timeListSearchAdapter;
//                            ((BaseAdapter)this.listEvents.Adapter).NotifyDataSetChanged();
                            for (int i = 0; i < itemsSearch.Count; i++)
                                if (itemsSearch[i].Provincia.ToLower().Contains(editSearch.Text.ToLower()))
                                {
                                    itemsNewSearchFilter.Add(itemsSearch[i]);

                                }
                            timeListSearchAdapter = new ApputamentiSearchAdapter(this, itemsNewSearchFilter, -1);
                            listEvents.Adapter = timeListSearchAdapter;
                            ((BaseAdapter)this.listEvents.Adapter).NotifyDataSetChanged();

                            timeListSearchAdapter.ItemClickIdEvent += (position) => OnClickItem(position, itemsSearch);
                            timeListSearchAdapter.RowClickEvent += (position) => OnRowFromListClick(position, itemsSearch);
                        }
                    }
                }
                else if (radioRS.Checked)
                {
                    radioRS.SetTextColor(new Android.Graphics.Color(255, 255, 255));
                    radioLOC.SetTextColor(new Android.Graphics.Color(53, 53, 53));
                    if (!string.IsNullOrEmpty(editSearch.Text))
                    {

                        RelativeLayout.LayoutParams rel_btn = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent, ViewGroup.LayoutParams.MatchParent);
                        rel_btn.AddRule(LayoutRules.AlignParentLeft);
                        rlForListViewSea.LayoutParameters = rel_btn;
                        detailsAputtamentiSea.Visibility = ViewStates.Gone;
//                        itemsSearch.Clear();
//                        itemsSearch = GetListOFAppuntamenti(DateTime.MinValue, DateTime.Now.Date, "", editSearch.Text.ToLower(), "3", "", "");
                        List<AppuntamentiItem> itemsNewSearchFilter = new List<AppuntamentiItem>();
                        if (itemsSearch != null && itemsSearch.Count > 0)
                        {
//                            timeListSearchAdapter = new ApputamentiSearchAdapter(this, itemsSearch, -1);
//                            listEvents.Adapter = timeListSearchAdapter;
//                            ((BaseAdapter)this.listEvents.Adapter).NotifyDataSetChanged();
                            for (int i = 0; i < itemsSearch.Count; i++)
                                if (itemsSearch[i].RegSociale.ToLower().Contains(editSearch.Text.ToLower()))
                                {
                                    itemsNewSearchFilter.Add(itemsSearch[i]);
                                }
                            
                            timeListSearchAdapter = new ApputamentiSearchAdapter(this, itemsNewSearchFilter, -1);
                            listEvents.Adapter = timeListSearchAdapter;
                            ((BaseAdapter)this.listEvents.Adapter).NotifyDataSetChanged();

                            timeListSearchAdapter.ItemClickIdEvent += (position) => OnClickItem(position, itemsSearch);
                            timeListSearchAdapter.RowClickEvent += (position) => OnRowFromListClick(position, itemsSearch);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        private int ConvertPixelsToDp(float pixelValue)
        {
            var dp = (int)((pixelValue) / Resources.DisplayMetrics.Density);
            return dp;
        }

        private int ConvertDpToPixels(float dp)
        {
            var pixelValue = (int)(dp * Resources.DisplayMetrics.Density);
            return pixelValue;
        }

        void OnClickItem(int position, List<AppuntamentiItem> list)
        {
            try
            {
                var metrics = Resources.DisplayMetrics;
                int widthInDp = ConvertDpToPixels(80);
                int heightInDp = ConvertPixelsToDp(metrics.HeightPixels);

                Animation myAnimation = AnimationUtils.LoadAnimation(this, Resource.Animation.scaleListViewToLeft);
                rlForListViewSea.StartAnimation(myAnimation);

                myAnimation.AnimationStart += (object sender, Animation.AnimationStartEventArgs e) =>
                {
                    timeListSearchAdapter.ItemClickIdEvent -= (position1) => OnClickItem(position1, list);

                    if (list[position].Id_appto_memo != "")
                    {
                        detailsAputtamentiSea.Visibility = ViewStates.Gone;
                        boxButtonsSearch.Visibility = ViewStates.Gone;
                        LaydetailsImpPersonaleSea.Visibility = ViewStates.Visible;
                        LaydetailsAppMemoSea.Visibility = ViewStates.Visible;
                    }
                    else
                    {
                        if (list[position].TipAppuntamenti == "business")
                        {
                            detailsAputtamentiSea.Visibility = ViewStates.Visible;
                            LaydetailsImpPersonaleSea.Visibility = ViewStates.Gone;
                            boxButtonsSearch.Visibility = ViewStates.Visible;
                            LaydetailsAppMemoSea.Visibility = ViewStates.Gone;
                        }
                        else if (list[position].TipAppuntamenti == "personal")
                        {
                            detailsAputtamentiSea.Visibility = ViewStates.Gone;
                            LaydetailsImpPersonaleSea.Visibility = ViewStates.Visible;
                            boxButtonsSearch.Visibility = ViewStates.Gone;
                            LaydetailsAppMemoSea.Visibility = ViewStates.Gone;
                        }
                    }
                };
                myAnimation.AnimationEnd += (object sender, Animation.AnimationEndEventArgs e) =>
                {
                    //Setters
                    RelativeLayout.LayoutParams rel_btn = new RelativeLayout.LayoutParams(widthInDp, ViewGroup.LayoutParams.MatchParent);
                    rel_btn.AddRule(LayoutRules.AlignParentLeft);
                    rlForListViewSea.LayoutParameters = rel_btn;

                    timeListSearchAdapter = new ApputamentiSearchAdapter(this, list, position);
                    listEvents.Adapter = timeListSearchAdapter;
//                        listEvents.SetSelector(position);
                    //View c = listEvents.GetChildAt(0);
                    //	int scrolly = -c.Top+ listEvents.FirstVisiblePosition * c.Height;
                    //listEvents.SmoothScrollToPosition(position);
//                    listEvents.SmoothScrollToPositionFromTop(position, 20);
                    timeListSearchAdapter.RowClickEvent += (position2) => OnRowFromListClick(position2, list);
					listEvents.SetSelectionFromTop(position,Utils.dpToPx(60));
                    //rlForListViewSea.SetBackgroundColor(Color.Transparent);
                    baraBack.SetBackgroundColor(new Android.Graphics.Color(76, 76, 76));


                };
//                list[position].TipAppuntamenti="xyz";
                if (list[position].Id_appto_memo != "")
                {
                    lblDetailsAppMemoPlaceValueSea.Text = list[position].Indirizzo; 
                    lblDetailsAppMemoTitleValueSea.Text = list[position].Evento_pers;
                    txtAppMemoDescriptionSea.Text = list[position].Note_agent; 

					if(list[position].Id_appto_memo.Contains("cus"))
					{
						txtMemovaiAiClientSea.Text="VAI AL CLIENTE";
					}
					else
					{
						txtMemovaiAiClientSea.Text="VAI ALL’APPUNTAMENTO";
					}

                    AppointmentObjTag item0 = new AppointmentObjTag();
                    item0.Appointment = list[position];
                    boxMemoGottoClientDetailSea.Tag = item0;

                    boxMemoGottoClientDetailSea.Click -= BoxMemoGottoClientDetailSea_Click;
                    boxMemoGottoClientDetailSea.Click += BoxMemoGottoClientDetailSea_Click;
                }
                else
                {
                    if (list[position].TipAppuntamenti == "business")
                    {
                        txtLocationSea.Text = list[position].RegSociale;
                        txtNameSea.Text = list[position].RepresentatnteLegale;
                        txtPhoneSea.Text = list[position].Phone;
                        txtMailSea.Text = list[position].Mail;
                        txtEsitoSea.Text = (WebUtility.HtmlDecode(list[position].Status)).ToUpper();
						if (list[position].Status != "")
						{
							lblEsitoSea.Text = "ESITO - ";
						}
						else
						{
							lblEsitoSea.Text = "ESITO";
						}
                        editNote1.Text = list[position].Motivo;

                        txtAppDescriptionSea.Text = list[position].Note_chiamata;

                        txtAppCodChiamataValueSea.Text = list[position].Id_Chiamata;
                        txtAppDataChiamataValueSea.Text = list[position].Date_chiamata.ToString("dd/MM/yyyy", CultureInfo.CurrentCulture);
                        txtAppDataAppuntamentoValueSea.Text = list[position].AppuntamentiDateHour.ToString("dd/MM/yyyy", CultureInfo.CurrentCulture);
                        txtAppOraAppuntamentoValueSea.Text = list[position].AppuntamentiDateHour.ToString("HH:mm", CultureInfo.CurrentCulture);
                        txtAppNoteAgenteValueSea.Text = list[position].Note_agent;
                        txtAppNoteChiamataValueSea.Text = list[position].Note_chiamata;
                        txtAppNoteSegretariaValueSea.Text = list[position].Note_segretaria;
                        txtAppMotiveValueSea.Text = list[position].Motivo;
                        txtAppRagioneSocialeValueSea.Text = list[position].RegSociale;
                        txtAppCategoriaValueSea.Text = list[position].Category;

                        txtAppContattiValueSea.Text = list[position].ContattiInfo;
                        //                    txtAppContattiLine2Sea.Text="Telefono: "+list[position].Contact_phone;
                        //                    txtAppContattiLine3Sea.Text="Email: "+list[position].Contact_mail;
                        //                    txtAppContattiLine4Sea.Text="Note: "+list[position].Note;


                        txtAppIndirizzoValueSea.Text = list[position].Indirizzo;
                        if (list[position].Civico != "")
                        {
                            txtAppIndirizzoValue.Text += " " + list[position].Civico;
                        }

                        txtAppIndirizzoLine2Sea.Text = "";
                        if (list[position].Cap != "")
                        {
                            txtAppIndirizzoLine2Sea.Text += list[position].Cap;
                        }
                        if (list[position].City != "")
                        {
                            txtAppIndirizzoLine2Sea.Text += " " + list[position].City;
                        }
                        if (list[position].Provincia != "")
                        {
                            txtAppIndirizzoLine2Sea.Text += " " + list[position].Provincia;
                        }
                        if (list[position].Acronim != "")
                        {
                            txtAppIndirizzoLine2Sea.Text += " (" + list[position].Acronim + ")";
                        }

                        BoxAppuntamentiMoreDetailsSea.Visibility = ViewStates.Gone;
                        AppointmentObjTag item0 = new AppointmentObjTag();
                        item0.Appointment = list[position];
                        btnSalvaSea.Tag = item0;

                        //new 01.06.2016
						if (list[position].DateChanged)
                        {
                            imgSearchChangeAppHour.SetImageResource(Resource.Drawable.memo_calendarIconRed);
                        }
                        else
                        {
                            imgSearchChangeAppHour.SetImageResource(Resource.Drawable.memo_calendarIcon);
                        }

                        if (!string.IsNullOrEmpty(list[position].Status))
                        {
                            imgSearchChangeAppHour.Visibility = ViewStates.Invisible;
                        }
                        else
                        {
                            imgSearchChangeAppHour.Visibility = ViewStates.Visible;
                        }
                        imgSearchChangeAppHour.Click -= ImgSearchChangeAppHour_Click;
                        imgSearchChangeAppHour.Click += ImgSearchChangeAppHour_Click;

                        btnSalvaSea.Click -= OnClickbtnSalvaSea;
                        btnEmailSea.Click -= OnClickbtnEmailSea;
                        btnPhoneSea.Click -= OnClickbtnPhoneSea;

                        btnSalvaSea.Click += OnClickbtnSalvaSea;
                        btnEmailSea.Click += OnClickbtnEmailSea;
                        btnPhoneSea.Click += OnClickbtnPhoneSea;

                        btnEmailSea.Tag = item0;
                        btnPhoneSea.Tag = item0;
                        imgSearchChangeAppHour.Tag = item0;

                        boxEsitoSea.Click -= OnClickEsitoSea;
                        boxEsitoSea.Click += OnClickEsitoSea;
                        boxEsitoSea.Tag = item0;
                    }
                    else if (list[position].TipAppuntamenti == "personal")
                    {
                        lblDetailsAggImpDataDalValueSea.Text = list[position].Date_chiamata.ToString("dd/MM/yyyy", CultureInfo.CurrentCulture);    
                        lblDetailsAggImpDataAlValueSea.Text = list[position].AppuntamentiDateHour.ToString("dd/MM/yyyy", CultureInfo.CurrentCulture);   
                        lblDetailsAggImpOraValueSea.Text = list[position].AppuntamentiDateHour.ToString("HH:mm", CultureInfo.CurrentCulture);   
                        lblDetailsAggImpLuogoValueSea.Text = list[position].Indirizzo;    

                        txtAppImpPersDescriptionSea.Text = list[position].Note_agent;   
						txtAppImpPersEvent_PersSea.Text = list[position].Evento_pers;   
                    }
                }

            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        void ImgSearchChangeAppHour_Click(object sender, EventArgs e)
        {
            try
            {
                ImageView btnSender = (ImageView)sender;
                var AppointmentTag = ((AppointmentObjTag)btnSender.Tag).Appointment;
                AppuntamentiItem appItem = (AppuntamentiItem)AppointmentTag;

                OnClickPickDate(appItem,false);
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        void OnClickEsitoSea(object sender, EventArgs e)
        {
            try
            {
                LinearLayout btnSender = (LinearLayout)sender;
                var AppointmentTag = ((AppointmentObjTag)btnSender.Tag).Appointment;
                AppuntamentiItem appItem = (AppuntamentiItem)AppointmentTag;

                List<string> status = new List<string>();//{ "status 1", "status 2", "status 3", "status 4", "status 5" };

                if (Utils.AllStatuses != null)
                {
                    foreach (var item in Utils.AllStatuses)
                    {
                        status.Add(WebUtility.HtmlDecode(item.Value.ToString()));
                    }
                }

                List<string> allowedStatuses;//"Trattativa" "Chiuso" "Non si è creato interesse" "Disdetto" "Fuori strategia" "Annullato"
                switch (WebUtility.HtmlDecode(appItem.Status.ToLower()))
                {
                    case "":
                        allowedStatuses = new List<string>(){ "Trattativa", "Chiuso", "Non si è creato interesse", "Disdetto", "Fuori strategia", "Annullato" };
                        break;
                    case "non si è creato interesse":
                        allowedStatuses = new List<string>(){ "Trattativa", "Chiuso", "Non si è creato interesse" };
                        break;
                    case "trattativa":
                        allowedStatuses = new List<string>(){ "Trattativa", "Chiuso", "Non si è creato interesse" };
                        break;
                    case "chiuso":
                        allowedStatuses = new List<string>(){ "Chiuso" };
                        break;
                    default:
                        allowedStatuses = new List<string>(){ "Trattativa", "Chiuso", "Non si è creato interesse", "Disdetto", "Fuori strategia", "Annullato" };
                        break;

                }

                if (window != null && window.IsShowing == true)
                {
                    window.Dismiss();
                }
                else
                {
                    imgStatusArrowSea.SetImageResource(Resource.Drawable.whiteArrowDown);

                    LayoutInflater inflater = (LayoutInflater)this.GetSystemService(Context.LayoutInflaterService);
                    View popup = inflater.Inflate(Resource.Layout.DropDownLayout, null);

                    ListView listDropDown = popup.FindViewById<ListView>(Resource.Id.dropDownListView);
                    listDropDown.Adapter = new StatusDropDownListAdapter(allowedStatuses, this);
                    listDropDown.ItemClick += (object sender1, AdapterView.ItemClickEventArgs e1) =>
                    {
                        txtEsitoSea.Text = (((ListView)sender1).Adapter).GetItem(e1.Position).ToString().ToUpper();
                        lblEsitoSea.Text = "ESITO - ";
                            statusChangedSearched=true;
                        window.Dismiss();
                    };

                    window = new PopupWindow(popup, ((LinearLayout)sender).Width, WindowManagerLayoutParams.WrapContent);
                    window.Touchable = true;
                    window.Focusable = true;
                    window.OutsideTouchable = true;
                    window.DismissEvent += (object sender1, EventArgs e1) =>
                    {
                        imgStatusArrowSea.SetImageResource(Resource.Drawable.whiteArrowDown);
                    };

                    window.SetBackgroundDrawable(new BitmapDrawable());
                    window.ShowAsDropDown((LinearLayout)sender, 0, 0);
                }

            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }


        }

        void OnClickbtnSalvaSea(object sender, EventArgs e)
        {
            try
            {
                LinearLayout btnSender = (LinearLayout)sender;
                var AppointmentTag = ((AppointmentObjTag)btnSender.Tag).Appointment;
                AppuntamentiItem appItem = (AppuntamentiItem)AppointmentTag;

                progresV.Show(this, "Attendere prego", -1, MaskType.Clear, TimeSpan.FromSeconds(120), null, true, null); 
                if (mWakeLock != null)
                {
                    mWakeLock.Acquire();
                }

                if(txtEsitoSea.Text!=string.Empty)
                    statusChangedSearched=true;
                if(statusChangedSearched)
                {
                    if(editNote1.Text!=string.Empty)
                    {
                        updateAppuntamentiSearch(appItem);
                    }
                    else
                    {
                        Toast.MakeText(this, "Inserire il motivo dell’esito per procedere", ToastLength.Long).Show();
                        RunOnUiThread(() =>
                            {
                                if (progresV != null)
                                {
                                    progresV.Dismiss();
                                }

                                if (mWakeLock != null)
                                {
                                    mWakeLock.Release();
                                }
                            });
                    }
                }
                else
                {
                    updateAppuntamentiSearch(appItem);
                }
                statusChangedSearched=false;
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void updateAppuntamentiSearch(AppuntamentiItem appItem)
        {
            try
            {
                ThreadPool.QueueUserWorkItem(state =>
                    {
                        try
                        {
                            if (Utils.CheckForInternetConn())
                            {  
                                appItem.AppuntamentiDateHour=appItem.appuntamentiChangedTime;
                                string data = appItem.AppuntamentiDateHour.ToString("yyyy-MM-dd", CultureInfo.CurrentCulture);
                                string hour = appItem.AppuntamentiDateHour.ToString("HH:mm:ss", CultureInfo.CurrentCulture);

                                bool rasp = ApiCalls.updateAppuntamenti(Utils.UserToken, appItem.Id, txtEsitoSea.Text, editNote1.Text, appItem.Source, data, hour,string.Empty,string.Empty,false);
                                appItem.Status = txtEsitoSea.Text;
                                appItem.Motivo = editNote1.Text;

                                RunOnUiThread(() =>
                                    {
                                        if (rasp)
                                        {
                                            Toast.MakeText(this, "Appuntamento aggiornato con successo", ToastLength.Long).Show();

											appItem.DateChanged=false;
                                            if (lastLoadedDay != null)
                                            {
                                                try
                                                {
                                                    itemsSearch = itemsSearch.OrderByDescending(x => x.AppuntamentiDateHour).ToList();
                                                }
                                                catch (Exception ex)
                                                {
                                                    Utils.writeToDeviceLog(ex);
                                                }
                                            }
                                            //                                            timeListSearchAdapter.NotifyDataSetChanged();
                                            timeListSearchAdapter.Items = itemsSearch;
                                            timeListSearchAdapter.SelectedIndex = -1;
                                            OnRowFromListClick(0,itemsSearch);

											imgSearchChangeAppHour.SetImageResource(Resource.Drawable.memo_calendarIcon);

                                        }
                                        else
                                        {
                                            if (editNote1.Text != "")
                                            {
                                                Toast.MakeText(this, "Impossibile aggiornare le informazioni dell’appuntamento", ToastLength.Long).Show();
                                            }
                                            else
                                            {
                                                Toast.MakeText(this, "Inserire il motivo dell’esito per procedere", ToastLength.Long).Show();
                                            }
                                        }
                                    });
                            }
                            else
                            {
                                RunOnUiThread(() =>
                                    {
                                        Toast.MakeText(this, "Nessuna connessione Internet", ToastLength.Short).Show();
                                    });
                            }
                        }
                        catch (Exception ex)
                        {
                            Utils.writeToDeviceLog(ex);
                        }
                        finally
                        {
                            RunOnUiThread(() =>
                                {
                                    if (progresV != null)
                                    {
                                        progresV.Dismiss();
                                    }

                                    if (mWakeLock != null)
                                    {
                                        mWakeLock.Release();
                                    }
                                });
                        }
                    });
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        void OnClickbtnEmailSea(object sender, EventArgs e)
        {
            try
            {

                LinearLayout btnSender = (LinearLayout)sender;
                var AppointmentTag = ((AppointmentObjTag)btnSender.Tag).Appointment;
                AppuntamentiItem appItem = (AppuntamentiItem)AppointmentTag;

                if (appItem.EmailList.Count > 1)
                {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    LayoutInflater inflater = (LayoutInflater)GetSystemService(Context.LayoutInflaterService);
                    View popupView = inflater.Inflate(Resource.Layout.ContactItemView, screenBackground, false);
                    builder.SetView(popupView);
                    ListView listView = popupView.FindViewById<ListView>(Resource.Id.listView1);
                    ListCallAdapter contactListAdapter = new ListCallAdapter(this, appItem.EmailList);
                    listView.Adapter = contactListAdapter;
                    builder.Show();
                    listView.ItemClick += (object sender3, AdapterView.ItemClickEventArgs e3) =>
                    {
                        string emailToSendTo = appItem.EmailList[e3.Position];
                        Intent emailIntent = new Intent(Intent.ActionSend); 
                        emailIntent.SetType("plain/text");
                        emailIntent.PutExtra(Intent.ExtraEmail, new string[]{ emailToSendTo });  
                        StartActivity(Intent.CreateChooser(emailIntent, "Seleziona mail:"));
                    };
                }
                else if (appItem.EmailList.Count > 0)
                {
                    if (appItem.EmailList[0] != "")
                    {
                        Intent emailIntent = new Intent(Intent.ActionSend); 
                        emailIntent.SetType("plain/text");
                        emailIntent.PutExtra(Intent.ExtraEmail, new string[]{ appItem.EmailList[0] });   
                        StartActivity(Intent.CreateChooser(emailIntent, "Seleziona mail:"));
                    }
                    else
                    {
                        Toast.MakeText(this, "Nessun indirizzo mail presente", ToastLength.Short).Show();
                    }
                }
                else
                {
                    Toast.MakeText(this, "Nessun indirizzo mail presente", ToastLength.Short).Show();
                }

            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        void OnClickbtnPhoneSea(object sender, EventArgs e)
        {
            try
            {
                LinearLayout btnSender = (LinearLayout)sender;
                var AppointmentTag = ((AppointmentObjTag)btnSender.Tag).Appointment;
                AppuntamentiItem appItem = (AppuntamentiItem)AppointmentTag;

                if (appItem.PhoneList.Count > 1)
                {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    LayoutInflater inflater = (LayoutInflater)GetSystemService(Context.LayoutInflaterService);
                    View popupView = inflater.Inflate(Resource.Layout.ContactItemView, screenBackground, false);
                    builder.SetView(popupView);
                    ListView listView = popupView.FindViewById<ListView>(Resource.Id.listView1);
                    ListCallAdapter contactListAdapter = new ListCallAdapter(this, appItem.PhoneList);
                    listView.Adapter = contactListAdapter;
                    builder.Show();
                    listView.ItemClick += (object sender3, AdapterView.ItemClickEventArgs e3) =>
                    {
                        string numberToCall = appItem.PhoneList[e3.Position];
                        AlertDialog.Builder builderCall = new AlertDialog.Builder(this);   
                        builderCall.SetTitle("Chiama");                   
                        builderCall.SetMessage("Conferma chiamata " + numberToCall.Trim()); 

                        builderCall.SetPositiveButton("Yes", (sender1, e1) =>
                            {                       
                                Intent telIntent = new Intent(Intent.ActionCall);
                                telIntent.SetData(Android.Net.Uri.Parse("tel:" + numberToCall.Trim()));
                                StartActivity(telIntent);
                            });
                        builderCall.SetNegativeButton("No", (sender1, e1) =>
                            {

                            });

                        builderCall.Show();
                    };

                }
                else if (appItem.PhoneList.Count > 0)
                {
                    //                    Intent telIntent = new Intent(Intent.ActionCall);
                    //                    telIntent.SetData(Android.Net.Uri.Parse("tel:" + appItem.PhoneList[0]));                                                                             
                    //                    StartActivity(telIntent);
                    AlertDialog.Builder builderCall = new AlertDialog.Builder(this);   
                    builderCall.SetTitle("Chiama");                   
                    builderCall.SetMessage("Conferma chiamata " + appItem.PhoneList[0].Trim()); 

                    builderCall.SetPositiveButton("Yes", (sender1, e1) =>
                        {                       
                            Intent telIntent = new Intent(Intent.ActionCall);
                            telIntent.SetData(Android.Net.Uri.Parse("tel:" + appItem.PhoneList[0].Trim()));
                            StartActivity(telIntent);
                        });
                    builderCall.SetNegativeButton("No", (sender1, e1) =>
                        {

                        });

                    builderCall.Show();
                }
                else
                {
                    Toast.MakeText(this, "Nessun numero di telefono presente", ToastLength.Short).Show();
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void OnRowFromListClick(int position, List<AppuntamentiItem> list)
        {
            try
            {
                int selPos = timeListSearchAdapter.selectItemPositon();
                if (position == selPos)
                {
                    OnRefreshListClick(list);
                }
                else
                {
                    timeListSearchAdapter = new ApputamentiSearchAdapter(this, list, position);//position
                    listEvents.Adapter = timeListSearchAdapter;

                    timeListSearchAdapter.RowClickEvent += (position2) => OnRowFromListClick(position2, list);
					listEvents.SetSelectionFromTop(position,Utils.dpToPx(60));
                }

//                list[position].TipAppuntamenti="xyz";
                if (list[position].Id_appto_memo != "")
                {
                    detailsAputtamentiSea.Visibility = ViewStates.Gone;
                    boxButtonsSearch.Visibility = ViewStates.Gone;
                    LaydetailsImpPersonaleSea.Visibility = ViewStates.Visible;
                    LaydetailsAppMemoSea.Visibility = ViewStates.Visible;

					if(list[position].Id_appto_memo.Contains("cus"))
					{
						txtMemovaiAiClientSea.Text="VAI AL CLIENTE";
					}
					else
					{
						txtMemovaiAiClientSea.Text="VAI ALL’APPUNTAMENTO";
					}

                    AppointmentObjTag item0 = new AppointmentObjTag();
                    item0.Appointment = list[position];
                    boxMemoGottoClientDetailSea.Tag = item0;

                    lblDetailsAppMemoPlaceValueSea.Text = list[position].Indirizzo; 
                    lblDetailsAppMemoTitleValueSea.Text = list[position].Evento_pers;
                    txtAppMemoDescriptionSea.Text = list[position].Note_agent; 

                }
                else
                {
                    if (list[position].TipAppuntamenti == "business")
                    {
                        detailsAputtamentiSea.Visibility = ViewStates.Visible;
                        boxButtonsSearch.Visibility = ViewStates.Visible;
                        LaydetailsImpPersonaleSea.Visibility = ViewStates.Gone;
                        LaydetailsAppMemoSea.Visibility = ViewStates.Gone;

                        txtLocationSea.Text = list[position].RegSociale;
                        txtNameSea.Text = list[position].RepresentatnteLegale;
                        txtPhoneSea.Text = list[position].Phone;
                        txtMailSea.Text = list[position].Mail;
                        txtEsitoSea.Text = (WebUtility.HtmlDecode(list[position].Status)).ToUpper();
						if (list[position].Status != "")
						{
							lblEsitoSea.Text = "ESITO - ";
						}
						else
						{
							lblEsitoSea.Text = "ESITO";
						}
                        txtAppDescriptionSea.Text = list[position].Note_chiamata;
                        editNote1.Text = list[position].Motivo;

						if (list[position].DateChanged)
                        {
                            imgSearchChangeAppHour.SetImageResource(Resource.Drawable.memo_calendarIconRed);
                        }
                        else
                        {
                            imgSearchChangeAppHour.SetImageResource(Resource.Drawable.memo_calendarIcon);
                        }

                        if (!string.IsNullOrEmpty(list[position].Status))
                        {
                            imgSearchChangeAppHour.Visibility = ViewStates.Invisible;
                        }
                        else
                        {
                            imgSearchChangeAppHour.Visibility = ViewStates.Visible;
                        }

                        AppointmentObjTag item0 = new AppointmentObjTag();
                        item0.Appointment = list[position];

                        btnEmailSea.Tag = item0;
                        btnPhoneSea.Tag = item0;
                        boxEsitoSea.Tag = item0;
                        btnSalvaSea.Tag = item0;
                        imgSearchChangeAppHour.Tag = item0;

                        txtAppCodChiamataValueSea.Text = list[position].Id_Chiamata;
                        txtAppDataChiamataValueSea.Text = list[position].Date_chiamata.ToString("dd/MM/yyyy", CultureInfo.CurrentCulture);
                        txtAppDataAppuntamentoValueSea.Text = list[position].AppuntamentiDateHour.ToString("dd/MM/yyyy", CultureInfo.CurrentCulture);
                        txtAppOraAppuntamentoValueSea.Text = list[position].AppuntamentiDateHour.ToString("HH:mm", CultureInfo.CurrentCulture);
                        txtAppNoteAgenteValueSea.Text = list[position].Note_agent;
                        txtAppNoteChiamataValueSea.Text = list[position].Note_chiamata;
                        txtAppNoteSegretariaValueSea.Text = list[position].Note_segretaria;
                        txtAppMotiveValueSea.Text = list[position].Motivo;
                        txtAppRagioneSocialeValueSea.Text = list[position].RegSociale;
                        txtAppCategoriaValueSea.Text = list[position].Category;

                        txtAppContattiValueSea.Text = list[position].ContattiInfo;
                        //                    txtAppContattiLine2Sea.Text="Telefono: "+list[position].Contact_phone;
                        //                    txtAppContattiLine3Sea.Text="Email: "+list[position].Contact_mail;
                        //                    txtAppContattiLine4Sea.Text="Note: "+list[position].Note;

                        txtAppIndirizzoValueSea.Text = list[position].Indirizzo;
                        if (list[position].Civico != "")
                        {
                            txtAppIndirizzoValue.Text += " " + list[position].Civico;
                        }

                        txtAppIndirizzoLine2Sea.Text = "";
                        if (list[position].Cap != "")
                        {
                            txtAppIndirizzoLine2Sea.Text += list[position].Cap;
                        }
                        if (list[position].City != "")
                        {
                            txtAppIndirizzoLine2Sea.Text += " " + list[position].City;
                        }
                        if (list[position].Provincia != "")
                        {
                            txtAppIndirizzoLine2Sea.Text += " " + list[position].Provincia;
                        }
                        if (list[position].Acronim != "")
                        {
                            txtAppIndirizzoLine2Sea.Text += " (" + list[position].Acronim + ")";
                        }

                        BoxAppuntamentiMoreDetailsSea.Visibility = ViewStates.Gone;
                    }
                    else if (list[position].TipAppuntamenti == "personal")
                    {
                        detailsAputtamentiSea.Visibility = ViewStates.Gone;
                        boxButtonsSearch.Visibility = ViewStates.Gone;
                        LaydetailsImpPersonaleSea.Visibility = ViewStates.Visible;
                        LaydetailsAppMemoSea.Visibility = ViewStates.Gone;

                        lblDetailsAggImpDataDalValueSea.Text = list[position].Date_chiamata.ToString("dd/MM/yyyy", CultureInfo.CurrentCulture);    
                        lblDetailsAggImpDataAlValueSea.Text = list[position].AppuntamentiDateHour.ToString("dd/MM/yyyy", CultureInfo.CurrentCulture);   
                        lblDetailsAggImpOraValueSea.Text = list[position].AppuntamentiDateHour.ToString("HH:mm", CultureInfo.CurrentCulture);   
                        lblDetailsAggImpLuogoValueSea.Text = list[position].Indirizzo;    

                        txtAppImpPersDescriptionSea.Text = list[position].Note_agent;  
						txtAppImpPersEvent_PersSea.Text = list[position].Evento_pers;
                    }
                }

            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        void BoxMemoGottoClientDetailSea_Click(object sender, EventArgs e)
        {
            try
            {
                LinearLayout btnSender = (LinearLayout)sender;
                var AppointmentTag = ((AppointmentObjTag)btnSender.Tag).Appointment;
                AppuntamentiItem appItem = (AppuntamentiItem)AppointmentTag;

				if(appItem.Id_appto_memo.Contains("cus"))
				{

					Intent clientiIntent = new Intent(this, typeof(ClientiActivity));
					clientiIntent.PutExtra("fromAppuntamentiMap", true);
					clientiIntent.PutExtra("customer_id", appItem.Id_customer);
					clientiIntent.SetFlags(ActivityFlags.ReorderToFront);
					this.StartActivity(clientiIntent);

				}
				else
				{
					string[] words = appItem.Id_appto_memo.Split('|');
					if(words!=null && words.Length>0)
					{
						RunOnUiThread(async () =>
							{
								await System.Threading.Tasks.Task.Run(() =>
									{
										loadAppuntamentiForDay(DateTime.Now.Date, DateTime.Now.Date, true,words[1]);
									});
							});
					}
				}
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void OnRefreshListClick(List<AppuntamentiItem> list)
        {
            try
            {
                Animation myAnimation = AnimationUtils.LoadAnimation(this, Resource.Animation.scaleListViewToRight);
                rlForListViewSea.StartAnimation(myAnimation);

                myAnimation.AnimationStart += (object sender, Animation.AnimationStartEventArgs e) =>
                {
                    timeListSearchAdapter = new ApputamentiSearchAdapter(this, list, -1);
                    listEvents.Adapter = timeListSearchAdapter;
                    timeListSearchAdapter.ItemClickIdEvent += (position) => OnClickItem(position, list);

                    //rlForListViewSea.SetBackgroundColor(Color.White);
                    baraBack.SetBackgroundColor(Color.White);

                };

                myAnimation.AnimationEnd += (object sender, Animation.AnimationEndEventArgs e) =>
                {
                    //Setters
                    RelativeLayout.LayoutParams rel_btn = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent, ViewGroup.LayoutParams.MatchParent);
                    rel_btn.AddRule(LayoutRules.AlignParentLeft);
                    rlForListViewSea.LayoutParameters = rel_btn;

                    detailsAputtamentiSea.Visibility = ViewStates.Gone;


                };


            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        void OnTextChangeSearch(object senders, EventArgs e)
        {
            try
            {
                string s = editSearch.Text.Trim();
                if (s != "")
                {
                    RelativeLayout.LayoutParams rel_btn = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent, ViewGroup.LayoutParams.MatchParent);
                    rel_btn.AddRule(LayoutRules.AlignParentLeft);
                    rlForListViewSea.LayoutParameters = rel_btn;
                    detailsAputtamentiSea.Visibility = ViewStates.Gone;
                    LaydetailsImpPersonaleSea.Visibility = ViewStates.Gone;
                    boxButtonsSearch.Visibility = ViewStates.Gone;
                    LaydetailsAppMemoSea.Visibility = ViewStates.Gone;
                    itemsSearch.Clear();
                    progresV.Show(this, "Attendere prego", -1, MaskType.Clear, TimeSpan.FromSeconds(120), null, true, null); 
                    //                timeListSearchAdapter = new ApputamentiSearchAdapter(this, itemsSearch, -1);
                    //                listEvents.Adapter = timeListSearchAdapter;
                    //                ((BaseAdapter)this.listEvents.Adapter).NotifyDataSetChanged();
                    System.Threading.ThreadPool.QueueUserWorkItem(state =>
                        {
                            if (radioRS.Checked)
                            {
                                //                    for (int i = 0; i < appuntamentiList.Count; i++)
                                //                        if (appuntamentiList[i].RegSociale.ToLower().Contains(s))
                                //                        {
                                //                            itemsSearch.Add(appuntamentiList[i]);
								itemsSearch = GetListOFAppuntamenti(DateTime.MinValue, DateTime.Now.Date, s, "", "3", "", "", 0, 0, string.Empty);
                                if(itemsSearch!=null)
                                {
                                    itemsSearch = itemsSearch.OrderByDescending(x => x.AppuntamentiDateHour).ThenByDescending(x => x.RegSociale).ToList();
                                }

                                if (itemsSearch != null && itemsSearch.Count > 0)
                                {
                                    timeListSearchAdapter = new ApputamentiSearchAdapter(this, itemsSearch, -1);
                                    RunOnUiThread(() =>
                                        {
                                            listEvents.Adapter = timeListSearchAdapter;
                                            ((BaseAdapter)this.listEvents.Adapter).NotifyDataSetChanged();
                                        });
                                }
                                else
                                {
                                    timeListSearchAdapter = new ApputamentiSearchAdapter(this, itemsSearch, -1);
                                    RunOnUiThread(() =>
                                        {
                                            listEvents.Adapter = timeListSearchAdapter;
                                            ((BaseAdapter)this.listEvents.Adapter).NotifyDataSetChanged();

                                        });
                                    RunOnUiThread(() =>
                                        {  
                                            Toast.MakeText(this, "Nessun appuntamento presente per la ragione sociale scelta!", ToastLength.Short).Show();
                                        });
                                }

                                RunOnUiThread(() =>
                                    {  
                                        if (progresV != null)
                                        {
                                            progresV.Dismiss();
                                        }
                                    });

                                //                        }
                            }
                            else if (radioLOC.Checked)
                            {
                                //                    for (int i = 0; i < appuntamentiList.Count; i++)
                                //                        if (appuntamentiList[i].RepresentatnteLegale.ToLower().Contains(s))
                                //                        {
								itemsSearch = GetListOFAppuntamenti(DateTime.MinValue, DateTime.Now.Date, "", s, "3", "", "", 0, 0, string.Empty);

                                itemsSearch = itemsSearch.OrderByDescending(x => x.AppuntamentiDateHour).ThenByDescending(x => x.RegSociale).ToList();

                                if (itemsSearch != null && itemsSearch.Count > 0)
                                {
                                    timeListSearchAdapter = new ApputamentiSearchAdapter(this, itemsSearch, -1);

                                    RunOnUiThread(() =>
                                        {    
                                            listEvents.Adapter = timeListSearchAdapter;
                                            ((BaseAdapter)this.listEvents.Adapter).NotifyDataSetChanged();
                                        });
                                }
                                else
                                {
                                    RunOnUiThread(() =>
                                        {  

                                            timeListSearchAdapter = new ApputamentiSearchAdapter(this, itemsSearch, -1);

                                            listEvents.Adapter = timeListSearchAdapter;
                                            ((BaseAdapter)this.listEvents.Adapter).NotifyDataSetChanged();
                                            Toast.MakeText(this, "Nessun appuntamento presente nella localita' scelta!", ToastLength.Short).Show();
                                        });

                                }
                                RunOnUiThread(() =>
                                    {  
                                        if (progresV != null)
                                        {
                                            progresV.Dismiss();
                                        }
                                    });
                            }
                            RunOnUiThread(() =>
                                {
                                    if (timeListSearchAdapter != null)
                                    {
                                        timeListSearchAdapter.ItemClickIdEvent += (position) => OnClickItem(position, itemsSearch);
                                        timeListSearchAdapter.RowClickEvent += (position) => OnRowFromListClick(position, itemsSearch);
                                    }
                                });

                            InputMethodManager manager = (InputMethodManager)GetSystemService(InputMethodService);
                            manager.HideSoftInputFromWindow(editSearch.WindowToken, 0);
                        });
                }
                else
                {
                    string warningText = "inserisci un testo da ricercare";
                    Toast.MakeText(this, warningText, ToastLength.Short).Show();
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }


        #endregion

        #region TopMenu

        void addMenuEventsForAll()
        {
            try
            {

                boxMenuBack.Click += OnBackPressedClick;
                boxMenuAppointments.Click += OnMenuAppontmentsClick;
                boxMenuCalendar.Click += OnMenuCalendarClick;
                boxMenuMap.Click += OnMenuMapClick;
                boxMenuSearch.Click += OnMenuSearchClick;
                ItemMenuSelected("events");
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        void ItemMenuSelected(string sel)
        {
            try
            {
                boxMenuAppointments.SetBackgroundResource(Resource.Drawable.SelectorTopMenu);
                imgMenuAppointments.SetImageResource(Resource.Drawable.menuApput);
                boxMenuCalendar.SetBackgroundResource(Resource.Drawable.SelectorTopMenu);
                imgMenuCalendar.SetImageResource(Resource.Drawable.menuCalendar);
                boxMenuMap.SetBackgroundResource(Resource.Drawable.SelectorTopMenu);
                imgMenuMap.SetImageResource(Resource.Drawable.menuLocation);
                boxMenuSearch.SetBackgroundResource(Resource.Drawable.SelectorTopMenu);
                imgMenuSearch.SetImageResource(Resource.Drawable.menuSearch);

                switch (sel)
                {
                    case "events": 
                        boxMenuAppointments.SetBackgroundColor(Resources.GetColor(Resource.Color.graySelectMenu));
                        imgMenuAppointments.SetImageResource(Resource.Drawable.menuApputSel);
                        break;
                    case "calendar":
                        boxMenuCalendar.SetBackgroundColor(Resources.GetColor(Resource.Color.graySelectMenu));
                        imgMenuCalendar.SetImageResource(Resource.Drawable.menuCalendarSel);
                        break;
                    case "map":
                        boxMenuMap.SetBackgroundColor(Resources.GetColor(Resource.Color.graySelectMenu));
                        imgMenuMap.SetImageResource(Resource.Drawable.menuLocationSel);
                        break;
                    case "search":
                        boxMenuSearch.SetBackgroundColor(Resources.GetColor(Resource.Color.graySelectMenu));
                        imgMenuSearch.SetImageResource(Resource.Drawable.menuSearchSel);
                        break;
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void OnMenuAppontmentsClick(object sender, EventArgs e)
        {
            try
            { 
//                try
//                {
//                    InputMethodManager inputManager = (InputMethodManager)this.GetSystemService(Context.InputMethodService);
//                    inputManager.HideSoftInputFromWindow(this.CurrentFocus.WindowToken, HideSoftInputFlags.NotAlways);
//                }
//                catch (Exception ex)
//                {
//                    Utils.writeToDeviceLog(ex);
//                }
                if (map.Parent == null)
                {
                    boxMaps.AddView(map);
                }

                if (imgAppdiOgiNavigatore.Parent == null)
                {
                    boxMaps.AddView(imgAppdiOgiNavigatore);
                }

                boxMapSearch.RemoveView(mapBox);

                Map.Visibility = ViewStates.Gone;
                Search.Visibility = ViewStates.Gone;
                Events.Visibility = ViewStates.Visible;
                ItemMenuSelected("events");
				
                if (Calendar.Visibility == ViewStates.Visible)
                {
                    expandOrCollapse(Calendar, "as");
                    AddEvent.Visibility = ViewStates.Gone;
                }

                RunOnUiThread(async () =>
                    {
                        await System.Threading.Tasks.Task.Run(() =>
                            {
								loadAppuntamentiForDay(DateTime.Now.Date, DateTime.Now.Date, false, string.Empty);
                            });
                    });
                OnRefreshListClickEvent();
                //EventsLayout();
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void OnMenuCalendarClick(object sender, EventArgs e)
        {
            try
            {
                try
                {
//                    InputMethodManager inputManager = (InputMethodManager)this.GetSystemService(Context.InputMethodService);
//                    inputManager.HideSoftInputFromWindow(this.CurrentFocus.WindowToken, HideSoftInputFlags.NotAlways);
                }
                catch (Exception ex)
                {
                    Utils.writeToDeviceLog(ex);
                }

                if (Calendar.Visibility == ViewStates.Gone)
                {
                    expandOrCollapse(Calendar, "expand");
                    ItemMenuSelected("calendar");
                }
                if (AddEvent.Visibility == ViewStates.Visible)
                {
                    AddEvent.Visibility = ViewStates.Gone;
                    ItemMenuSelected("calendar");
                }
                //initCalendarGrid();
                calendarPager.CurrentItem = 58;
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void OnMenuMapClick(object sender, EventArgs e)
        {
            try
            {
                boxMaps.RemoveView(map);
                boxMaps.RemoveView(imgAppdiOgiNavigatore);

                if (mapBox.Parent == null)
                {
                    boxMapSearch.AddView(mapBox);
                }
//                boxMaps.Visibility = ViewStates.Gone;
                Events.Visibility = ViewStates.Gone;
                Map.Visibility = ViewStates.Visible;
                Search.Visibility = ViewStates.Gone;
                ItemMenuSelected("map");
                if (Calendar.Visibility == ViewStates.Visible)
                {
                    expandOrCollapse(Calendar, "as");
                    AddEvent.Visibility = ViewStates.Gone;
                }

                checkDiOggi.Checked = true;
                checkNonEsiati.Checked = false;
                checkInTratativa.Checked = false;
                checkInBorsellino.Checked = false;
                checkConvergenti.Checked = false;
                checkNonConvergenti.Checked = false;
                checkDeact.Checked = false;
                AddMeetingMarkersToMap();

            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void OnMenuSearchClick(object sender, EventArgs e)
        {
            try
            {
                Events.Visibility = ViewStates.Gone;
                Map.Visibility = ViewStates.Gone;
                Search.Visibility = ViewStates.Visible;
                ItemMenuSelected("search");
				
                if (Calendar.Visibility == ViewStates.Visible)
                {
                    expandOrCollapse(Calendar, "as");
                    AddEvent.Visibility = ViewStates.Gone;
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void OnBackPressedClick(object sender, EventArgs e)
        {
            try
            {
//                if (AddEvent.Visibility == ViewStates.Visible || Map.Visibility == ViewStates.Visible || Search.Visibility == ViewStates.Visible || Calendar.Visibility == ViewStates.Visible)
//                {
//                    if (Calendar.Visibility == ViewStates.Visible)
//                    {
//                        expandOrCollapse(Calendar, "as");
//
//                    }
//
//                    if(map.Parent==null)
//                    {
//                        boxMaps.AddView(map);
//                       
//                    }
//
//                    if(imgAppdiOgiNavigatore.Parent==null)
//                    {
//                        boxMaps.AddView(imgAppdiOgiNavigatore);
//                    }
//
//                    boxMapSearch.RemoveView(mapBox);
//
//                    Map.Visibility = ViewStates.Gone;
//                    Search.Visibility = ViewStates.Gone;
//                    AddEvent.Visibility = ViewStates.Gone;
//                    Events.Visibility = ViewStates.Visible;
//                    ItemMenuSelected("events");
//
//                }
//                else
//                {
//                    this.Finish();
//                }
                this.Finish();
				
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void expandOrCollapse(View v, string exp_or_colpse)
        {
            try
            {
                TranslateAnimation anim = null;
                if (exp_or_colpse.Equals("expand"))
                {
                    v.Visibility = ViewStates.Visible;
                    anim = new TranslateAnimation(0.0f, 0.0f, -v.Height, 0.0f);

                }
                else
                {
                    anim = new TranslateAnimation(0.0f, 0.0f, 0.0f, -v.Height);
                    AnimationViewVisibleGone collapselistener = new AnimationViewVisibleGone(v);
                    anim.SetAnimationListener(collapselistener);
                }

                anim.Duration = 300;
                anim.Interpolator = new AccelerateInterpolator(0.5f);
                v.StartAnimation(anim);
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }﻿

        public List<ClientInfoItem> LoadALlClients(bool searchFlag, string param, string value, ManualResetEvent resetEvent, double lng, double lat, bool city)
        {
            List<ClientInfoItem> clientsList = new List<ClientInfoItem>();
            try
            {
                JToken objClients = null;

                if (mWakeLock != null)
                {
                    mWakeLock.Acquire();
                }
                ThreadPool.QueueUserWorkItem(state =>
                    {
                        try
                        {
                            if (Utils.CheckForInternetConn())
                            {  
                                objClients = ApiCalls.getAllClients(Utils.UserToken, lat.ToString(), lng.ToString(), city, searchFlag, param, value);

                                if (objClients != null && objClients.HasValues)
                                {
                                    if (!objClients.First.Contains("error"))
                                    {
                                        foreach (var item in objClients)
                                        {
                                            ClientInfoItem client = new ClientInfoItem();

                                            client.Customer_id = item["id_customer"].ToString();
                                            client.Address = item["address"].ToString();
                                            if (item["fax"].ToString() != "")
                                            {
                                                client.Fax = item["fax"].ToString().Substring(1);
                                            }

                                            client.Location = item["location"].ToString();
                                            client.Mail = item["mail"].ToString();
                                            if (item["mobile_phone"].ToString() != "")
                                            {
                                                client.Mobile_phone = item["mobile_phone"].ToString().Substring(1);
                                            }

                                            client.Partita_iva = item["partita_iva"].ToString();
                                            if (item["phone"].ToString() != "")
                                            {
                                                client.Phone = item["phone"].ToString().Substring(1);
                                            }
                                            client.Province = item["province"].ToString();
                                            client.Referente = item["referente"].ToString();
                                            client.Regione_sociale = item["regione_sociale"].ToString();
                                            client.Status = item["status"].ToString();
                                            client.Date_clienti = DateTime.Now.Date;

                                            if (item["latitude"] != null && item["latitude"].ToString() != "")
                                            {
                                                double latDouble;
                                                double.TryParse(item["latitude"].ToString(), NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingSign, CultureInfo.InvariantCulture, out latDouble);

                                                client.Latitude = latDouble;
                                            }
                                            if (item["longitude"] != null && item["longitude"].ToString() != "")
                                            {
                                                double longDouble;
                                                double.TryParse(item["longitude"].ToString(), NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingSign, CultureInfo.InvariantCulture, out longDouble);

                                                client.Longitude = longDouble;
                                            }


                                            DateTime dataInserimento;
                                            DateTime.TryParseExact(item["data_inserimento"].ToString(), "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dataInserimento);

                                            client.Data_inserimento = dataInserimento;
                                            client.Tipo_contratto = item["tipo_contratto"].ToString();
                                            client.Codice_cliente = item["codice_cliente"].ToString();
                                            client.Email_shop = item["email_shop"].ToString();
                                            int result;
                                            int.TryParse(item["punti_disponibili"].ToString(), out result);
                                            client.Punti_disponibili = result;
                                            client.Stato_rid = item["stato_rid"].ToString();
                                            client.Numero_referenze = (item["numero_referenze"].ToString()) != null ? (item["numero_referenze"].ToString()) : "";
                                            client.Flag_borsellino = item["flag_borsellino"].ToString();
                                            client.Flag_preso = item["flag_preso"].ToString();

                                            DateTime dataConvergence;
                                            DateTime.TryParseExact(item["date_convergence"].ToString(), "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dataConvergence);
                                            client.Date_convergence = dataConvergence;

                                            List<ClientPraticheItemInfo> listofPratiche = new List<ClientPraticheItemInfo>();
                                            foreach (var itemPrat in item["pratiche"])
                                            {
                                                ClientPraticheItemInfo pratItem = new ClientPraticheItemInfo();
                                                if (itemPrat["data_creazione"].ToString() != "")
                                                {
                                                    DateTime dataCreazione;
                                                    DateTime.TryParseExact(itemPrat["data_creazione"].ToString(), "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dataCreazione);
                                                    pratItem.Pratiche_date = dataConvergence;
                                                }
                                                pratItem.Gnt_inserita = itemPrat["quantity"].ToString();
                                                pratItem.Gnt_attiva = itemPrat["quantity_active"].ToString();
                                                pratItem.Tipo_prodotto = itemPrat["product_type"].ToString();
                                                pratItem.Prodotto = itemPrat["product"].ToString();

                                                if (itemPrat["force_date_active"].ToString() != "")
                                                {
                                                    DateTime dataForceActive;
                                                    DateTime.TryParseExact(itemPrat["force_date_active"].ToString(), "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dataForceActive);
                                                    pratItem.Data_inserimento = dataForceActive;
                                                    //                                                pratItem.Data_inserimento = DateTime.Parse(itemPrat["date_insert"].ToString(), CultureInfo.InvariantCulture);
                                                }
                                                if (itemPrat["data_creazione"].ToString() != "")
                                                {
                                                    DateTime dataCreazione;
                                                    DateTime.TryParseExact(itemPrat["data_creazione"].ToString(), "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dataCreazione);
                                                    pratItem.Data_creazione = dataCreazione;
                                                    //                                                pratItem.Data_creazione = DateTime.Parse(itemPrat["data_creazione"].ToString(), CultureInfo.InvariantCulture);
                                                }
                                                if (itemPrat["quantity"].ToString() != "")
                                                {
                                                    pratItem.Qta_inserita = Int32.Parse(itemPrat["quantity"].ToString());
                                                }
                                                if (itemPrat["quantity_active"].ToString() != "")
                                                {
                                                    pratItem.Qta_attiva = Int32.Parse(itemPrat["quantity_active"].ToString());
                                                }
                                                if (itemPrat["linee"].ToString() != "")
                                                {
                                                    pratItem.Linee = itemPrat["linee"].ToString();
                                                }
                                                pratItem.Seriale = itemPrat["serial"].ToString();
                                                pratItem.Tipo_contratto = itemPrat["contract_type"].ToString();

                                                if (itemPrat["canone"] != null && itemPrat["canone"].ToString() != "")
                                                {
                                                    double canon;
                                                    double.TryParse(itemPrat["canone"].ToString(), NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingSign, CultureInfo.InvariantCulture, out canon);

                                                    pratItem.Canone = canon;
                                                }

                                                if (itemPrat["force_date_active"].ToString() != "")
                                                {
                                                    DateTime dataAttivazione;
                                                    DateTime.TryParseExact(itemPrat["data_creazione"].ToString(), "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dataAttivazione);
                                                    pratItem.Data_attivazione = dataAttivazione;
                                                    //                                                pratItem.Data_attivazione = DateTime.Parse(itemPrat["force_date_active"].ToString(), CultureInfo.InvariantCulture);//
                                                }
                                                pratItem.Stato_ordine = "";
                                                pratItem.Stato_pratica = "";
                                                pratItem.Codice_cliente = item["id_customer"].ToString();
                                                pratItem.Id_product = itemPrat["id_product"].ToString();

                                                pratItem.Routing = itemPrat["routing"].ToString();
                                                pratItem.Notes = itemPrat["notes"].ToString();

                                                listofPratiche.Add(pratItem);
                                                client.Practiche_list = listofPratiche;    
                                            }


                                            clientsList.Add(client);
                                        }
                                    }
                                    else
                                    {
                                        RunOnUiThread(() =>
                                            {
                                                AndHUD.Shared.ShowError(this, objClients["error"].ToString(), MaskType.Black, TimeSpan.FromSeconds(3));
                                            });
                                    }
                                    //insert info to db
									RunOnOtherThread(clientsList);

                                }
                            }
                            else
                            {
                                clientsList = db.getAllClients();
                                foreach (var item in clientsList)
                                {
                                    //TODO Add pratiche to clients
                                    item.Practiche_list = (db.getPraticheListForCustomer(item.Customer_id));
                                }

                                RunOnUiThread(() =>
                                    {
                                        AndHUD.Shared.ShowError(this, "Nessuna connessione Internet", MaskType.Black, TimeSpan.FromSeconds(3));
                                    });
                            }
                        }
                        catch (Exception ex)
                        {
                            Utils.writeToDeviceLog(ex);
                        }
                        finally
                        {
                            resetEvent.Set();

                            if (mWakeLock != null)
                            {
                                mWakeLock.Release();
                            }
                        }
                    });

            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
            finally
            {
                RunOnUiThread(() =>
                    {
                    });
            }
            return clientsList;
        }

        public void AddPraticheToDb(List<ClientPraticheItemInfo> listPratiche)
        {
            try
            {
                if (listPratiche != null)
                {
                    foreach (var itemprat in listPratiche)
                    {
                        if (db.CheckIfClientPraticheExistinDB(itemprat.Id_product))
                        {
                            db.UpdatePraticheInfo(itemprat);
                        }
                        else
                        {
                            db.InsertPraticheToTable(itemprat);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

		public void RunOnOtherThread(List<ClientInfoItem> clientsList)
		{
			new Thread(() => 
				{
					Thread.CurrentThread.IsBackground = true; 
					foreach (var item in clientsList)
					{
						if (db.CheckIfExiststClientInDB(item.Partita_iva))
						{
							db.UpdateClientItem(item);
						}
						else
						{
							db.InsertClientsToTable(item);
						}
						AddPraticheToDb(item.Practiche_list);
					}
				}).Start();
		}


        void OnClickPickDate(AppuntamentiItem itemAppuntamenti, bool flagAppointmentFromDiOggi)
        {

            try
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                AlertDialog builder = alertDialogBuilder.Create();
                View view = this.LayoutInflater.Inflate(Resource.Layout.DateAndTimePicker, null);
                builder.SetView(view, 0, 0, 0, 0);

                TimePicker orderTimePicker = view.FindViewById<TimePicker>(Resource.Id.orderTimePicker);
				orderTimePicker.SetIs24HourView(Java.Lang.Boolean.True);
                orderTimePicker.Visibility = ViewStates.Visible;
                orderTimePicker.CurrentHour = new Java.Lang.Integer(itemAppuntamenti.appuntamentiChangedTime.Hour);
                orderTimePicker.CurrentMinute = new Java.Lang.Integer(itemAppuntamenti.appuntamentiChangedTime.Minute);
                DatePicker datePicker = view.FindViewById<DatePicker>(Resource.Id.orderDatePicker);
                LinearLayout test = (LinearLayout)datePicker.GetChildAt(0);
                CalendarView test2 = (CalendarView)test.GetChildAt(1);
                LinearLayout untest = (LinearLayout)test2.GetChildAt(0);
                untest.DescendantFocusability = DescendantFocusability.BlockDescendants;
                TextView txtMonth = (TextView)untest.GetChildAt(0);


                DateTime date0 = new DateTime(1970, 1, 1);
                datePicker.MinDate = (long)(itemAppuntamenti.original_appuntamentiDateHour - date0).TotalMilliseconds;
                datePicker.MaxDate = (long)(itemAppuntamenti.original_appuntamentiDateHour.AddDays(30) - date0).TotalMilliseconds;
                datePicker.DateTime = itemAppuntamenti.appuntamentiChangedTime;

                Android.Views.InputMethods.InputMethodManager imm = (Android.Views.InputMethods.InputMethodManager)GetSystemService(Context.InputMethodService);
                imm.HideSoftInputFromWindow(txtMonth.WindowToken, Android.Views.InputMethods.HideSoftInputFlags.NotAlways);

                EditText textDummy = view.FindViewById<EditText>(Resource.Id.textDummy);
                Button btn_OK = view.FindViewById<Button>(Resource.Id.step1DateTimeBtnOK);
                Button btn_Cancel = view.FindViewById<Button>(Resource.Id.step1DateTimeBtnCancel);

                btn_OK.Touch += (object sender2, View.TouchEventArgs e2) =>
                {
                    try
                    {
                        Button b = (Button)sender2;
                        if (e2.Event.Action == MotionEventActions.Down)
                        {
                            b.Background.SetColorFilter(new Color(100, 100, 100), PorterDuff.Mode.Multiply);
                        }
                        if (e2.Event.Action == MotionEventActions.Cancel)
                        {
                            b.Background.ClearColorFilter();
                        }
                        if (e2.Event.Action == MotionEventActions.Up)
                        {
                            b.Background.ClearColorFilter();
                            textDummy.RequestFocus();
                            Thread.CurrentThread.CurrentCulture = new CultureInfo("it-IT");
                                itemAppuntamenti.appuntamentiChangedTime = datePicker.DateTime.Date.AddHours(orderTimePicker.CurrentHour.DoubleValue()).AddMinutes((double)orderTimePicker.CurrentMinute.DoubleValue());
//                            itemAppuntamenti.AppuntamentiDateHour = datePicker.DateTime.Date.AddHours(orderTimePicker.CurrentHour.DoubleValue()).AddMinutes((double)orderTimePicker.CurrentMinute.DoubleValue());
//                            timeListAdapter.NotifyDataSetChanged();
//                            txtAppDataAppuntamentoValue.Text = itemAppuntamenti.AppuntamentiDateHour.ToString("dd/MM/yyyy", CultureInfo.CurrentCulture);
//                            txtAppOraAppuntamentoValue.Text = itemAppuntamenti.AppuntamentiDateHour.ToString("HH:mm", CultureInfo.CurrentCulture);
//                            AddMeetingMarkersToMapEvents();
							itemAppuntamenti.DateChanged=true;
							if (itemAppuntamenti.AppuntamentiDateHour.Year > 1970 && itemAppuntamenti.AppuntamentiDateHour != itemAppuntamenti.appuntamentiChangedTime)
                                {
                                    if(flagAppointmentFromDiOggi)
                                    {
                                        imgChangeAppHour.SetImageResource(Resource.Drawable.memo_calendarIconRed);
                                    }
                                    else
                                    {
                                        imgSearchChangeAppHour.SetImageResource(Resource.Drawable.memo_calendarIconRed);
                                    }
                                }
                                else
                                {
                                    if(flagAppointmentFromDiOggi)
                                    {
                                        imgChangeAppHour.SetImageResource(Resource.Drawable.memo_calendarIcon);
                                    }
                                    else
                                    {
                                        imgSearchChangeAppHour.SetImageResource(Resource.Drawable.memo_calendarIcon);
                                    }
                                }
                            builder.Dismiss();
                        }
                    }
                    catch (Exception ex)
                    {
                        Utils.writeToDeviceLog(ex);
                    }
                };

                btn_Cancel.Touch += (object sender1, View.TouchEventArgs e1) =>
                {
                    try
                    {
                        Button b = (Button)sender1;
                        if (e1.Event.Action == MotionEventActions.Down)
                        {
                            b.Background.SetColorFilter(new Color(100, 100, 100), PorterDuff.Mode.Multiply);
                        }
                        if (e1.Event.Action == MotionEventActions.Cancel)
                        {
                            b.Background.ClearColorFilter();
                        }
                        if (e1.Event.Action == MotionEventActions.Up)
                        {
                            b.Background.ClearColorFilter();
                            builder.Dismiss();
                        }
                    }
                    catch (Exception ex)
                    {
                        Utils.writeToDeviceLog(ex);
                    }
                };

                builder.Show();
                builder.Window.SetLayout(RelativeLayout.LayoutParams.MatchParent, RelativeLayout.LayoutParams.WrapContent);


            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }


        protected override void OnDestroy()
        {
            base.OnDestroy();
            GC.Collect();
            try
            {
                boxEsito.Click -= OnClickEsito;
                btnSalva.Click -= OnClickbtnSalva;
                btnEmail.Click -= OnClickbtnEmail;
                btnPhone.Click -= OnClickbtnPhone;
                if (timeListAdapter != null)
                {
                    timeListAdapter.ItemClickIdEvent -= OnClickFromListClickEvent;
                }
                btnAddEventCalendar.Click -= OnAddEventToCalendarClick;
                btnSaveEvent.Click -= OnBtnSaveEventClick;
                butAppuntamenti.Click -= OnClickAppuntamenti;
                butClienti.Click -= OnClickClienti;
                radioLOC.CheckedChange -= OnClickRadio;
                radioRS.CheckedChange -= OnClickRadio;
                if (timeListSearchAdapter != null)
                {
                    timeListSearchAdapter.ItemClickIdEvent -= (position) => OnClickItem(position, null);
                }
                boxEsitoSea.Click -= OnClickEsitoSea;
                btnSalvaSea.Click -= OnClickbtnSalvaSea;
                btnEmailSea.Click -= OnClickbtnEmailSea;
                btnPhoneSea.Click -= OnClickbtnPhoneSea;
                editSearch.TextChanged -= OnTextChangeSearch;
                boxMenuBack.Click -= OnBackPressedClick;
                boxMenuAppointments.Click -= OnMenuAppontmentsClick;
                boxMenuCalendar.Click -= OnMenuCalendarClick;
                boxMenuMap.Click -= OnMenuMapClick;
                boxMenuSearch.Click -= OnMenuSearchClick;

                if (_mapSearchAppuntamenti != null)
                {
                    _mapSearchAppuntamenti.Dispose();
                }
               
                if (_map != null)
                {
                    _map.Dispose();
                }

                if (_mapFragment != null)
                {
                    _mapFragment.Dispose();
                }
                if (_mapFragmentA != null)
                {
                    _mapFragmentA.Dispose();
                }
                 
                if (_mapFragmentSupport != null)
                {
                    _mapFragmentSupport.Dispose();
                }
                if (_mapFragmentASupport != null)
                {
                    _mapFragmentASupport.Dispose();
                }

//                _mapA.Dispose();
               
                if (screenBackground != null)
                {
                    unbindDrawables(screenBackground);
                }

            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }

        }

        protected override void OnStop()
        {
            base.OnStop();
            try
            {
                GC.Collect();
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }

        }

        private void unbindDrawables(View rootView)
        {
            try
            {
                if (rootView.Background != null)
                {
                    rootView.Background.SetCallback(null);
                }
                if (rootView is ViewGroup)
                {
                    for (int i = 0; i < ((ViewGroup)rootView).ChildCount; i++)
                    {
                        unbindDrawables(((ViewGroup)rootView).GetChildAt(i));

                        ((IDisposable)((ViewGroup)rootView).GetChildAt(i)).Dispose();
                    }

                    try
                    {
                        ((ViewGroup)rootView).RemoveAllViews();
                    }
                    catch (Java.Lang.UnsupportedOperationException mayHappen)
                    {
                        // AdapterViews, ListViews and potentially other ViewGroups don抰 support the removeAllViews operation
                        //Utils.writeToDeviceLog (mayHappen);
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        void Android.Gms.Location.ILocationListener.OnLocationChanged(Location location)
        {
        }

        void GoogleApiClient.IConnectionCallbacks.OnConnected(Bundle connectionHint)
        {
            try
            {
                if (apiClient.IsConnected)
                {
                    Location locationCurr = LocationServices.FusedLocationApi.GetLastLocation(apiClient);
                    if (locationCurr != null)
                    {
                        currLocation = locationCurr;
                        SetupMapIfNeededEvents();
                        SetupMapIfNeeded();
                        Log.Debug("LocationClient", "Last location printed----" + locationCurr.Latitude.ToString() + "-----" + locationCurr.Longitude.ToString());
                    }
                    else
                    {
                        if (progresV != null)
                        {
                            progresV.Dismiss();
                            Toast.MakeText(this, "Impossibile ottenere posizione", ToastLength.Short).Show();
                        }
                    }
                }
                Log.Info("LocationClient", "Now connected to client");
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }

        }

        void GoogleApiClient.IConnectionCallbacks.OnConnectionSuspended(int cause)
        {
            Log.Info("LocationClient", "Now disconnected from client");
        }

        void GoogleApiClient.IOnConnectionFailedListener.OnConnectionFailed(Android.Gms.Common.ConnectionResult result)
        {
            Log.Info("LocationClient", "Connection failed, attempting to reach google play services");
        }

        bool IsGooglePlayServicesInstalled()
        {
            int queryResult = GoogleApiAvailability.Instance.IsGooglePlayServicesAvailable(this);
            if (queryResult == ConnectionResult.Success)
            {
                Log.Info("MainActivity", "Google Play Services is installed on this device.");
                return true;
            }

            if (GoogleApiAvailability.Instance.IsUserResolvableError(queryResult))
            {
                string errorString = GoogleApiAvailability.Instance.GetErrorString(queryResult);
                Log.Error("ManActivity", "There is a problem with Google Play Services on this device: {0} - {1}", queryResult, errorString);

                // Show error dialog to let user debug google play services
            }
            return false;
        }

        public class AnimationViewVisibleGone:Animation,Android.Views.Animations.Animation.IAnimationListener
        {
            View v;

            public AnimationViewVisibleGone(View _v)
            {
                this.v = _v;
            }

            void IAnimationListener.OnAnimationEnd(Animation animation)
            {
                v.Visibility = ViewStates.Gone;
            }

            void IAnimationListener.OnAnimationRepeat(Animation animation)
            {
            }

            void IAnimationListener.OnAnimationStart(Animation animation)
            {
            }



        }

        #endregion

    }

    public class AppointmentObjTag:Java.Lang.Object
    {
        public AppuntamentiItem appointment;



        public AppuntamentiItem Appointment
        {
            get
            {
                return appointment;
            }
            set
            {
                appointment = value;
            }
        }
    }

    public class MarkerLatLong:Java.Lang.Object
    {
        public double lat;
        public double lng;

        public double Lat
        {
            get
            {
                return lat;
            }
            set
            {
                lat = value;
            }
        }

        public double Lng
        {
            get
            {
                return lng;
            }
            set
            {
                lng = value;
            }
        }
    }
}

