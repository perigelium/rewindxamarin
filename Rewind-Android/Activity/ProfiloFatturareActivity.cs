﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading;
using System.Globalization;
using Android.Graphics;
using Android.Content.PM;
using AndroidHUD;
using Newtonsoft.Json.Linq;
using RewindShared;

namespace RewindAndroid
{
	[Activity (Label = "ProfiloFatturareActivity",ScreenOrientation = ScreenOrientation.Portrait, WindowSoftInputMode = SoftInput.StateAlwaysHidden)]			
	public class ProfiloFatturareActivity : BaseActivity
	{
		Typeface OswaldRegualr;
		List<FatturareItem> itemsFatturare;
		ProfiloFatturareAdapter fatturareAdapter;
		ListView listBill;
        LinearLayout screenBackground;
        LinearLayout boxMenuBack;
        UserLocalDB db=new UserLocalDB();
        TextView txtDateBegin;
        TextView txtDateFinal;
        AndHUD progresV=new AndHUD();
        private const string DATE_FORMAT="yyyy";
        PowerManager.WakeLock mWakeLock;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			SetContentView(Resource.Layout.ProfiloFatturareLayout);
			try {
//                this.Window.SetFlags(WindowManagerFlags.KeepScreenOn, WindowManagerFlags.KeepScreenOn);
                PowerManager pm = (PowerManager)GetSystemService(Context.PowerService);
                mWakeLock = pm.NewWakeLock(WakeLockFlags.ScreenDim | WakeLockFlags.OnAfterRelease, "My Tag");

                screenBackground=FindViewById<LinearLayout>(Resource.Id.screenBackground);
				TextView txtTitle=FindViewById<TextView>(Resource.Id.txtTitle);
				TextView textDB=FindViewById<TextView>(Resource.Id.textDB);
				TextView textDF=FindViewById<TextView>(Resource.Id.textDF);


				txtDateBegin=FindViewById<TextView>(Resource.Id.txtDateBegin);
				txtDateFinal=FindViewById<TextView>(Resource.Id.txtDateFinal);
				boxMenuBack=FindViewById<LinearLayout>(Resource.Id.boxMenuBack);
				LinearLayout lyBeginDate=FindViewById<LinearLayout>(Resource.Id.lyBeginDate);
				LinearLayout lyFinalDate=FindViewById<LinearLayout>(Resource.Id.lyFinalDate);
				listBill=FindViewById<ListView>(Resource.Id.listBill);


				Thread.CurrentThread.CurrentCulture = new CultureInfo("it-IT");
                txtDateBegin.Text=DateTime.Now.Date.ToString(DATE_FORMAT, CultureInfo.CurrentCulture);
                txtDateFinal.Text=DateTime.Now.Date.ToString(DATE_FORMAT, CultureInfo.CurrentCulture);

				OswaldRegualr =  TypeFaces.getTypeface(this, "Oswald-Regular.ttf");
				txtTitle.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
				textDB.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
				textDF.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
				txtDateBegin.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
				txtDateFinal.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);

                int year = DateTime.Now.Year;
                DateTime firstDay = new DateTime(year , 1, 1);
                DateTime lastDay = new DateTime(year , 12, 31);
                LoadInvoicesData(firstDay,lastDay,false);
//				itemsFatturare=new List<FatturareItem>();
//                itemsFatturare=getInvoicesList(DateTime.Now.Date,DateTime.Now.Date);
//				FatturareItem item1 ;
//				for (int i=1;i<4;i++){
//					item1 = new FatturareItem();
//					item1.Id=i.ToString();
                //					item1.Data=DateTime.Now.Date.ToString("MMMMM yyyy", CultureInfo.InvariantCulture);
//					item1.Total=i.ToString()+"00";
//					item1.Check=true;
//					item1.Storni="Storni";
//					item1.Storniconsumare="Storni consumare";
//					item1.Bonus="Bonus";
//					item1.Gara="Gara";
//					item1.Anticipo="Anticipo";
//					itemsFatturare.Add(item1);
//				}
//				fatturareAdapter=new ProfiloFatturareAdapter(this, itemsFatturare);
//				listBill.Adapter=fatturareAdapter;

				lyBeginDate.Click += (object sender, EventArgs e) => {
					OnClickPickDate(txtDateBegin);
				};

				lyFinalDate.Click += (object sender, EventArgs e) => {
					OnClickPickDate(txtDateFinal);
				};
				boxMenuBack.Click += OnBackPressedClick;

		
			} catch (Exception ex) {
				Utils.writeToDeviceLog (ex);
			}
		}


        public void LoadInvoicesData(DateTime begin, DateTime end,bool init)
        {
            try
            {
                progresV.Show(this, "Attendere prego", -1, MaskType.Clear, TimeSpan.FromSeconds(120), null, true, null); 
                if(mWakeLock!=null)
                {
                    mWakeLock.Acquire();
                }
                System.Threading.ThreadPool.QueueUserWorkItem(state =>
                    { 
                        try 
                        {
//                            if(init)
//                            {
                                //true when select other period then initial one
                                if (Utils.CheckForInternetConn())
                                {
                                    itemsFatturare=getInvoicesList(begin,end);
                                }
                                else
                                {
                                    //no internet connection
                                    RunOnUiThread(()=>
                                        {
                                            Toast.MakeText(this,"Nessuna connessione Internet",ToastLength.Short).Show();
                                        });
                                    itemsFatturare=db.getLastInsertedInvoices();
                                }
//                            }
//                            else
//                            {
//                                if (!db.CheckIfTableIsEmpty(Consts.PROFILE_INVOICES_TABLE_NAME))
//                                {
//                                    //if table is empty get classifications
//                                    if (Utils.CheckForInternetConn())
//                                    {
//                                        itemsFatturare=getInvoicesList(begin,end);
//                                    }
//                                    else
//                                    {
//                                        //no internet connection
//                                        RunOnUiThread(()=>
//                                            {
//                                                Toast.MakeText(this,"Nessuna connessione Internet",ToastLength.Short).Show();
//                                            });
//                                    }
//                                }
//                                else
//                                {
//                                    itemsFatturare=db.getLastInsertedInvoices();
//                                } 
//                            }

                            RunOnUiThread(()=>
                                {
                                    List<FatturarePeriodItem> factPerPeriod=new List<FatturarePeriodItem>();
                                    if(itemsFatturare!=null && itemsFatturare.Count>0)
                                    {
                                        foreach (var item in itemsFatturare)
                                        {
                                            int index=factPerPeriod.FindIndex(element=>element.Period==item.Period);
                                            if(index!=-1)
                                            {
                                                factPerPeriod[index].Total+=item.Total;
                                                if(factPerPeriod[index].Id!="")
                                                {
                                                    factPerPeriod[index].Id+=","+item.Id.ToString();
                                                }
                                                int indexDictionary=factPerPeriod[index].Facturi.FindIndex(element=>element.ContainsKey(item.Status));
                                                if(indexDictionary!=-1)
                                                {
                                                    //same group name new fatture name
                                                    (factPerPeriod[index].Facturi[indexDictionary])[item.Status].Add(item);
                                                }
                                                else
                                                {
                                                    //new Group Name
                                                    Dictionary<string,List<FatturareItem>>dictGroup_Name=new Dictionary<string, List<FatturareItem>>();
                                                    List<FatturareItem>fattureName=new List<FatturareItem>();
                                                    fattureName.Add(item);
                                                    dictGroup_Name.Add(item.Status,fattureName.OrderBy(x=>x.Id).ToList());
                                                    factPerPeriod[index].Facturi.Add(dictGroup_Name);
                                                }
                                                if(item.Check!="true")
                                                {
                                                    factPerPeriod[index].Check="false"; 
                                                }
                                            }
                                            else
                                            {
                                                //new period
                                                FatturarePeriodItem itm=new FatturarePeriodItem();
                                                itm.Period=item.Period;
                                                itm.Total=item.Total;
                                                List<Dictionary<string,List<FatturareItem>>>group_Name=new List<Dictionary<string, List<FatturareItem>>>();
                                                Dictionary<string,List<FatturareItem>>dictGroup_Name=new Dictionary<string, List<FatturareItem>>();
                                                List<FatturareItem>fattureName=new List<FatturareItem>();
                                                fattureName.Add(item);
                                                dictGroup_Name.Add(item.Status,fattureName);
                                                group_Name.Add(dictGroup_Name);
                                                itm.Facturi=group_Name;
                                                itm.Check=item.Check;
                                                itm.Data=item.Data;
                                                itm.Id=item.Id.ToString();
                                                factPerPeriod.Add(itm);
                                            }
                                        }
                                    }
                                    try {

//                                        foreach (var item in factPerPeriod) {
//                                            foreach (var item2 in item.Facturi) {
//                                                foreach (var item3 in item2.Values) {
//                                                    item3=item3.OrderBy(x=>x.Id).ToList();
//                                                }
//                                            }
//                                        }
                                        fatturareAdapter=new ProfiloFatturareAdapter(this, factPerPeriod,listBill);
                                        listBill.Adapter=fatturareAdapter;
                                    } catch (Exception ex) {
                                        Utils.writeToDeviceLog(ex);
                                    }

                                });
                        }
                        catch(Exception ex)
                        {
                            Utils.writeToDeviceLog (ex);
                        }
                        finally
                        {
                            RunOnUiThread(()=>
                                {
                                    if(progresV != null)
                                    {
                                        progresV.Dismiss();
                                    }

                                    if(mWakeLock!=null)
                                    {
                                        mWakeLock.Release();
                                    }
                                });
                        }
                    });
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public List<FatturareItem> getInvoicesList(DateTime begin, DateTime end)
        {
            List<FatturareItem> listInvoices = new List<FatturareItem>();
            string period = string.Empty;
            try
            {
                if (begin <= end)
                {
                    if (begin == end)
                    {
                        period = begin.ToString("yyyyMM", CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        int diffMonths = (end.Month + end.Year * 12) - (begin.Month + begin.Year * 12);
                        for (int i = diffMonths + 1; i > 1; i--)
                        {
                            period += end.ToString("yyyyMM", CultureInfo.InvariantCulture);
                            end = end.AddMonths(-1);
                            period += ",";
                        }
                        period += end.ToString("yyyyMM", CultureInfo.InvariantCulture);
                    }
                    //                    itemsGare = new List<GareItem>();

                    JToken invoicesJtoken = ApiCalls.getProfilClasificationandBonus(Utils.UserToken, period, Consts.API_PROFILE_INVOICES_URL);
                    if (invoicesJtoken != null && invoicesJtoken.HasValues)
                    {
                        foreach (var item in invoicesJtoken)
                        {
                            FatturareItem item1;
                            item1 = new FatturareItem();
                            int resultOfParse;
                            Int32.TryParse(item["id"].ToString(),out resultOfParse);
                            item1.Id =resultOfParse;
                            item1.Period = item["period"].ToString();
                            if(item["date"].ToString()!="")
                            {
                                DateTime fattureDateHour;
                                DateTime.TryParseExact(item["date"].ToString(), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out fattureDateHour);

                                item1.Data = fattureDateHour;
                            }
//                            item1.Total = item["total"].ToString();
                            if (item["total"].ToString() != "")
                            {
                                float total;
                                float.TryParse(item["total"].ToString(), NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingSign, CultureInfo.InvariantCulture, out total);

                                item1.Total = total;
                            }
                            item1.Status=item["group"].ToString();
                            item1.Title_invoice=item["name"].ToString();
                            string checkString=item["checked"].ToString();
                            if(checkString!="")
                            {
                                item1.Check="true";
                            }
                            else
                            {
                                item1.Check="false";    
                            }
                            listInvoices.Add(item1);
                        }
                    }

                    db.deleteTableName(Consts.PROFILE_INVOICES_TABLE_NAME);
                    Console.WriteLine(db.CheckIfTableIsEmpty(Consts.PROFILE_INFO_TABLE_NAME).ToString());
                    foreach (var item in listInvoices) {
                        db.InsertInvoicesToTable(item);
                    }
                }
                else
                {
                    //invalid input- end has to be grater than begin
                    RunOnUiThread(()=>{
                        Toast.MakeText(this,"Data inserita non valida: la data finale deve essere maggiore di quella iniziale",ToastLength.Short).Show();
                    });
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
                listInvoices = null;
            }
            return listInvoices;
        }

		public void OnBackPressedClick(object sender, EventArgs e)
		{
			try
			{
				OnBackPressed();
			}
			catch (Exception ex)
			{
				Utils.writeToDeviceLog(ex);
			}
		}

		void OnClickPickDate(TextView label)
		{

			try {

				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder (this);
				AlertDialog builder = alertDialogBuilder.Create ();
				View view = this.LayoutInflater.Inflate(Resource.Layout.DateAndTimePicker, null);
				builder.SetView (view, 0, 0, 0, 0);

				TimePicker timePicker = view.FindViewById<TimePicker> (Resource.Id.orderTimePicker);
				DatePicker datePicker = view.FindViewById<DatePicker> (Resource.Id.orderDatePicker);
				datePicker.Visibility=ViewStates.Visible;
				timePicker.Visibility=ViewStates.Gone;
                if(label.Text!="")
                {
                    DateTime pickerDateHour;
                    DateTime.TryParseExact(label.Text, DATE_FORMAT, CultureInfo.CurrentCulture, DateTimeStyles.None, out pickerDateHour);
                    datePicker.DateTime=pickerDateHour;//DateTime.Parse(label.Text, CultureInfo.InvariantCulture);
                }
				timePicker.SetIs24HourView (Java.Lang.Boolean.True);
				Button btn_OK = view.FindViewById<Button> (Resource.Id.step1DateTimeBtnOK);
				Button btn_Cancel = view.FindViewById<Button> (Resource.Id.step1DateTimeBtnCancel);

                datePicker.FindViewById(Android.Content.Res.Resources.System.GetIdentifier("day", "id", "android")).Visibility=ViewStates.Gone;
                datePicker.FindViewById(Android.Content.Res.Resources.System.GetIdentifier("month", "id", "android")).Visibility=ViewStates.Gone;

				btn_OK.Touch += (object sender2, View.TouchEventArgs e2) => {
					try {
						Button b = (Button)sender2;
						if (e2.Event.Action == MotionEventActions.Down) {
							b.Background.SetColorFilter (new Color (100, 100, 100), PorterDuff.Mode.Multiply);
						}
						if (e2.Event.Action == MotionEventActions.Cancel) {
							b.Background.ClearColorFilter ();
						}
						if (e2.Event.Action == MotionEventActions.Up) {
							b.Background.ClearColorFilter ();
//							Thread.CurrentThread.CurrentCulture = new CultureInfo("it-IT");
                            label.Text=datePicker.DateTime.ToString (DATE_FORMAT, CultureInfo.CurrentCulture);

							builder.Dismiss ();

                            DateTime pickerStartDateHour;
                            DateTime.TryParseExact(txtDateBegin.Text, DATE_FORMAT, CultureInfo.CurrentCulture, DateTimeStyles.None, out pickerStartDateHour);

                            DateTime pickerEndDateHour;
                            DateTime.TryParseExact(txtDateFinal.Text, DATE_FORMAT, CultureInfo.CurrentCulture, DateTimeStyles.None, out pickerEndDateHour);

                            LoadInvoicesData(pickerStartDateHour, pickerStartDateHour.AddMonths(11),true);

						}
					} catch (Exception ex) {
						Utils.writeToDeviceLog (ex);
					}
				};

				btn_Cancel.Touch += (object sender1, View.TouchEventArgs e1) => {
					try {
						Button b = (Button)sender1;
						if (e1.Event.Action == MotionEventActions.Down) {
							b.Background.SetColorFilter (new Color (100, 100, 100), PorterDuff.Mode.Multiply);
						}
						if (e1.Event.Action == MotionEventActions.Cancel) {
							b.Background.ClearColorFilter ();
						}
						if (e1.Event.Action == MotionEventActions.Up) {
							b.Background.ClearColorFilter ();
							builder.Dismiss ();
						}
					} catch (Exception ex) {
						Utils.writeToDeviceLog (ex);
					}
				};

				builder.Show ();
				builder.Window.SetLayout (RelativeLayout.LayoutParams.MatchParent, RelativeLayout.LayoutParams.WrapContent);


			} catch (Exception ex) {
				Utils.writeToDeviceLog (ex);
			}


		}
        protected override void OnPause()
        {
            base.OnPause();
            if (progresV != null)
            {
                progresV.Dismiss();
            }
        }

        protected override void OnStop()
        {
            base.OnStop();
            try
            {
                GC.Collect();
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            try
            {
                GC.Collect();
                boxMenuBack.Click -= OnBackPressedClick;
                if (screenBackground != null)
                {
                    unbindDrawables(screenBackground);
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        private void unbindDrawables(View rootView)
        {
            try
            {
                if (rootView.Background != null)
                {
                    rootView.Background.SetCallback(null);
                }
                if (rootView is ViewGroup)
                {
                    for (int i = 0; i < ((ViewGroup)rootView).ChildCount; i++)
                    {
                        unbindDrawables(((ViewGroup)rootView).GetChildAt(i));

                        ((IDisposable)((ViewGroup)rootView).GetChildAt(i)).Dispose();
                    }

                    try
                    {
                        ((ViewGroup)rootView).RemoveAllViews();
                    }
                    catch (Java.Lang.UnsupportedOperationException mayHappen)
                    {
                        // AdapterViews, ListViews and potentially other ViewGroups don抰 support the removeAllViews operation
                        //Utils.writeToDeviceLog (mayHappen);
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

//		[Java.Interop.Export ("btnDetailsClickHandler")]
//		public void basketBtnClickHandler (View v1)
//		{
//			try {
//				fatturareAdapter.OnItemClick;
//
//			} catch (Exception ex) {
//				Utils.writeToDeviceLog (ex);
//			} 
//		}
	}
}

