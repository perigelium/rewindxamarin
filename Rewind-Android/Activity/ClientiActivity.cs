﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Gms.Maps;

//using Android.Locations;
using Android.Gms.Maps.Model;
using AndroidHUD;
using Android.Support.V4.App;
using Android.Content.PM;
using System.Threading;
using System.Globalization;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using Android.Util;
using Android.Gms.Location;
using Android.Gms.Common;
using Android.Gms.Common.Apis;
using Android.Locations;
using RewindShared;

namespace RewindAndroid
{
    [Activity(Label = "ClientiActivity", ScreenOrientation = ScreenOrientation.Portrait, WindowSoftInputMode = SoftInput.StateAlwaysHidden)]			
	public class ClientiActivity : BaseFragmentActivity,Android.Gms.Location.ILocationListener,GoogleApiClient.IConnectionCallbacks,GoogleApiClient.IOnConnectionFailedListener
    {

        int kilimometers=120;
        TextView txtClientiTitle;
        LinearLayout boxMenuBack;
        EditText editTxtSearch;
        LinearLayout layFIlter;
        TextView txtFilterText;
        TextView txtSearchLabel;
        ListView listviewResultSearch;
        TextView txtClientiMailLabel;
        TextView txtClientiMailValue;
        TextView txtClientiMobilePhoneLable;
        TextView txtClientiMobilePhoneValue;
        TextView txtClientiAddressLabel;
        TextView txtClientiAddressValue;
        TextView txtClientiPartitaLabel;
        TextView txtClientiPartitaivaValue;
        TextView txtClientiTelefonoLabel;
        TextView txtClientiTelefonoValue;
        TextView txtClientiFaxLabel;
        TextView txtClientiFaxValue;
        TextView txtClientiReferenteLabel;
        TextView txtClientiReferenteValue;
        LinearLayout lyAltuofianco;
        LinearLayout lyPratiche;
        LinearLayout lyClientiCall;
        LinearLayout lyClientiEmail;
        LinearLayout lyCLientiMemo;
        LinearLayout lyClientiContent;
        LinearLayout laySearch;

        //altuofianco screen
        LinearLayout lyAltuofiancoScreen;
        TextView txtAltuofiancoDataLabel;
        TextView txtAltuofiancoDataValue;
        TextView txtAltuofiancoTipoConLabel;
        TextView txtAltuofiancoTipoConValue;
        TextView txtAltuofiancoCodiceClLabel;
        TextView txtAltuofiancoCodiceClValue;
        TextView txtAltuofiancoEmailShopLabel;
        TextView txtAltuofiancoEmailShopValue;
        TextView txtAltuofiancoPuntiDispLabel;
        TextView txtAltuofiancoPuntiDispValue;
        TextView txtAltuofiancoStatoRidLabel;
        TextView txtAltuofiancoStatoRidValue;
        TextView txtAltuofiancoNumeroRefLabel;
        TextView txtAltuofiancoNumeroRefValue;
        LinearLayout lyaltuofiancoBottomBack;
        LinearLayout lyaltuofiancoMenuBack;

        //pratiche Screen
        LinearLayout lyPraticheScreen;
        LinearLayout lyBackFomPratiche;
        TextView txtPraticheTitle;
        TextView txtPraticheSearchLabel;
        EditText editTxtPraticheSearch;
        ListView listviewPratiche;

        //pratiche details screen
        LinearLayout lyPraticheDetailsScreen;
        TextView txtPraticheDetailsTitle;
        LinearLayout lyBackFomPraticheDetails;
        TextView txtPraticheDetailAgente;
        TextView txtPraticheDetailApriTicket;
        LinearLayout lyCreateApriTicket;
        TextView txtPraticheDetailDataInserLabel;
        TextView txtPraticheDetailDataInserValue;
        TextView txtPraticheDetailsDataCreazioneLabel;
        TextView txtPraticheDetailsDataCreazioneValue;
        TextView txtPraticheDetailsQtaInseritaLabel;
        TextView txtPraticheDetailsQtaInseritaValue;
        TextView txtPraticheDetailsQtaAttivaLabel;
        TextView txtPraticheDetailsQtaAttivaValue;
        TextView txtPraticheDetailsTipoProdottoLabel;
        TextView txtPraticheDetailsTipoProdottoValue;
        TextView txtPraticheDetailsProdottoLabel;
        TextView txtPraticheDetailsProdottoValue;
        TextView txtPraticheDetailsSerialiLabel;
        TextView txtPraticheDetailsSerialiValue;
        TextView txtPraticheDetailsLineeLabel;
        TextView txtPraticheDetailsLineeValue;
        TextView txtPraticheDetailsTipoContrattoLabel;
        TextView txtPraticheDetailsTipoContrattoValue;
        TextView txtPraticheDetailsCanoneLabel;
        TextView txtPraticheDetailsCanoneValue;
        TextView txtPraticheDetailsDataAttivaLabel;
        TextView txtPraticheDetailsDataAttivaValue;
        TextView txtPraticheDetailsStatoOrdineLabel;
        TextView txtPraticheDetailsStatoOrdineValue;
        TextView txtPraticheDetailsStatoPraticaLabel;
        TextView txtPraticheDetailsStatoPraticaValue;

        TextView txtPraticheDetailsRoutingLabel;
        TextView txtPraticheDetailsRoutingValue;
        TextView txtPraticheDetailsNotesLabel;
        TextView txtPraticheDetailsNotesValue;

        //new ticket screen
        LinearLayout lyNewTicket;
        LinearLayout lyBackFomNewTicket;
        TextView txtNewTicketTitle;
        TextView txtNewTicketDestinatoarioLabel;
        TextView txtNewTicketDestinatoarioValue;
        TextView txtNewTicketTipoLabel;
        TextView txtNewTicketRegSocialeLabel;
        TextView txtNewTicketRegSocialeValue;
        TextView txtNewTicketPartitaIvaLabel;
        TextView txtNewTicketPartitaIvaValue;
        TextView txtNewTicketOggettoLabel;
        EditText txtEditOggetto;
        EditText txtEditNewTicketDescription;
        TextView txtNewTicketInviaLabel;
        LinearLayout lyNewTicketInviaBtn;
        LinearLayout lyNewTicketTipo;

        //memo screen
        LinearLayout lyClientiMemoScreen;
        ListView listviewMemo;
        TextView txtMemoTitle;
        EditText txtEditMemoTitle;
        TextView txtMemoDataLabel;
        TextView txtMemoDataValue;
        ImageView imgMemoCalendar;
        LinearLayout linearMemoDataValue;
        LinearLayout linearMemoHourValue;
        LinearLayout linearDetails;
        TextView txtMemoOraLabel;
        TextView txtMemoOraValue;
        ImageView imgMemoClock;
        EditText txtEditMemoDescription;
        TextView txtMemoSalvaBtn;
        LinearLayout lyMemoSalvaBtn;
        EditText txtEditMemoPlace;

        ImageView imgMapNavigtoLoc;
        TextView txtMapSearchPlus;
        TextView txtMapSearchMinus;
        LinearLayout memoPlusLay;

        //popup window
        PopupWindow window;
        ImageView imgStatusArrow;

        Typeface OswaldLight;
        Typeface OswaldRegualr;

        private GoogleMap _map;
        private MapFragment _mapFragment;
        private SupportMapFragment _mapFragmentSupport;
        AlertDialog alertDialog;
        bool checkForGps = true;
        private Location _currentLocation;
        ViewGroup container;
        ImageView iconView;
        TextView textView;
        ClientiSearchListAdapter clientiListSearchAdapter;
        PraticheListAdapter praticheAdapter;
        ClientiMemoAdapter adapterClenti;
        List<ClientInfoItem> clientsList;
        List<ClientiMemoItem> clientMemo;
        AndHUD test = new AndHUD();
        LinearLayout clientiLayoutBg;
        int posMemoList = -1;
        UserLocalDB db = new UserLocalDB();
        List<TicketTypteItem> listTicketTipes;
        bool gpsEnabled = false;
        bool networkEnabled = false;
        LocationManager _locationManager;
        string _locationProvider;
        int countSeconds = 10;
        System.Timers.Timer timer;
        bool found = false;
        bool _isGooglePlayServicesInstalled;
        GoogleApiClient apiClient;
        LocationRequest locRequest;
        int androidPhoneVersion;
        TextView lblTicketLabelTipo;
        PowerManager.WakeLock mWakeLock;

        Button btnSearchClients;

        TextView txtClientiMemo;
        TextView txtClientiPratiche;
        TextView txtMemoPlace;
        bool fromAppuntamentiMap;
        string ragioneSocialeClient;
        string customerid_from_appuntamenti;
		double zoomLevel=1;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            try
            {
                SetContentView(Resource.Layout.ClientiLayout);

                fromAppuntamentiMap = Intent.GetBooleanExtra("fromAppuntamentiMap", false);
                ragioneSocialeClient = Intent.GetStringExtra("ragioneSociale");
                customerid_from_appuntamenti = Intent.GetStringExtra("customer_id");

//                this.Window.SetFlags(WindowManagerFlags.KeepScreenOn, WindowManagerFlags.KeepScreenOn);
                PowerManager pm = (PowerManager)GetSystemService(Context.PowerService);
                mWakeLock = pm.NewWakeLock(WakeLockFlags.ScreenDim | WakeLockFlags.OnAfterRelease, "My Tag");

                this.Window.SetSoftInputMode(SoftInput.StateAlwaysHidden | SoftInput.AdjustPan);
                test.Show(this, "Attendere prego", -1, MaskType.Clear, TimeSpan.FromSeconds(120), null, true, null); 

                clientiLayoutBg = FindViewById<LinearLayout>(Resource.Id.clientiLayoutBg);
                txtClientiTitle = FindViewById<TextView>(Resource.Id.txtClientiTitle);
                boxMenuBack = FindViewById<LinearLayout>(Resource.Id.boxMenuBack);
                editTxtSearch = FindViewById<EditText>(Resource.Id.editTxtSearch);
                layFIlter = FindViewById<LinearLayout>(Resource.Id.layFIlter);
                txtFilterText = FindViewById<TextView>(Resource.Id.txtFilterText);
                imgStatusArrow = FindViewById<ImageView>(Resource.Id.imgStatusArrow);
                txtSearchLabel = FindViewById<TextView>(Resource.Id.txtSearchLabel);
                listviewResultSearch = FindViewById<ListView>(Resource.Id.listviewResultSearch);
                txtClientiMailLabel = FindViewById<TextView>(Resource.Id.txtClientiMailLabel);
                txtClientiMailValue = FindViewById<TextView>(Resource.Id.txtClientiMailValue);
                txtClientiMobilePhoneLable = FindViewById<TextView>(Resource.Id.txtClientiMobilePhoneLable);
                txtClientiMobilePhoneValue = FindViewById<TextView>(Resource.Id.txtClientiMobilePhoneValue);
                txtClientiAddressLabel = FindViewById<TextView>(Resource.Id.txtClientiAddressLabel);
                txtClientiAddressValue = FindViewById<TextView>(Resource.Id.txtClientiAddressValue);
                txtClientiPartitaLabel = FindViewById<TextView>(Resource.Id.txtClientiPartitaLabel);
                txtClientiPartitaivaValue = FindViewById<TextView>(Resource.Id.txtClientiPartitaivaValue);
                txtClientiTelefonoLabel = FindViewById<TextView>(Resource.Id.txtClientiTelefonoLabel);
                txtClientiTelefonoValue = FindViewById<TextView>(Resource.Id.txtClientiTelefonoValue);
                txtClientiFaxLabel = FindViewById<TextView>(Resource.Id.txtClientiFaxLabel);
                txtClientiFaxValue = FindViewById<TextView>(Resource.Id.txtClientiFaxValue);
                txtClientiReferenteLabel = FindViewById<TextView>(Resource.Id.txtClientiReferenteLabel);
                txtClientiReferenteValue = FindViewById<TextView>(Resource.Id.txtClientiReferenteValue);
                lyAltuofianco = FindViewById<LinearLayout>(Resource.Id.lyAltuofianco);
                lyPratiche = FindViewById<LinearLayout>(Resource.Id.lyPratiche);
                lyClientiCall = FindViewById<LinearLayout>(Resource.Id.lyClientiCall);
                lyClientiEmail = FindViewById<LinearLayout>(Resource.Id.lyClientiEmail);
                lyCLientiMemo = FindViewById<LinearLayout>(Resource.Id.lyCLientiMemo);
                lyClientiContent = FindViewById<LinearLayout>(Resource.Id.lyClientiContent);
                laySearch = FindViewById<LinearLayout>(Resource.Id.laySearch);
                linearDetails = FindViewById<LinearLayout>(Resource.Id.linearDetails);
                //altuofianco screen
                lyAltuofiancoScreen = FindViewById<LinearLayout>(Resource.Id.lyAltuofiancoScreen);
                txtAltuofiancoDataLabel = FindViewById<TextView>(Resource.Id.txtAltuofiancoDataLabel);
                txtAltuofiancoDataValue = FindViewById<TextView>(Resource.Id.txtAltuofiancoDataValue);
                txtAltuofiancoTipoConLabel = FindViewById<TextView>(Resource.Id.txtAltuofiancoTipoConLabel);
                txtAltuofiancoTipoConValue = FindViewById<TextView>(Resource.Id.txtAltuofiancoTipoConValue);
                txtAltuofiancoCodiceClLabel = FindViewById<TextView>(Resource.Id.txtAltuofiancoCodiceClLabel);
                txtAltuofiancoCodiceClValue = FindViewById<TextView>(Resource.Id.txtAltuofiancoCodiceClValue);
                txtAltuofiancoEmailShopLabel = FindViewById<TextView>(Resource.Id.txtAltuofiancoEmailShopLabel);
                txtAltuofiancoEmailShopValue = FindViewById<TextView>(Resource.Id.txtAltuofiancoEmailShopValue);
                txtAltuofiancoPuntiDispLabel = FindViewById<TextView>(Resource.Id.txtAltuofiancoPuntiDispLabel);
                txtAltuofiancoPuntiDispValue = FindViewById<TextView>(Resource.Id.txtAltuofiancoPuntiDispValue);
                txtAltuofiancoStatoRidLabel = FindViewById<TextView>(Resource.Id.txtAltuofiancoStatoRidLabel);
                txtAltuofiancoStatoRidValue = FindViewById<TextView>(Resource.Id.txtAltuofiancoStatoRidValue);
                txtAltuofiancoNumeroRefLabel = FindViewById<TextView>(Resource.Id.txtAltuofiancoNumeroRefLabel);
                txtAltuofiancoNumeroRefValue = FindViewById<TextView>(Resource.Id.txtAltuofiancoNumeroRefValue);
                lyaltuofiancoBottomBack = FindViewById<LinearLayout>(Resource.Id.lyaltuofiancoBottomBack);
                lyaltuofiancoMenuBack = FindViewById<LinearLayout>(Resource.Id.lyaltuofiancoMenuBack);

                txtClientiMemo = FindViewById<TextView>(Resource.Id.txtClientiMemo);
                txtClientiPratiche = FindViewById<TextView>(Resource.Id.txtClientiPratiche);

                btnSearchClients = FindViewById<Button>(Resource.Id.btnSearchClients);

                btnSearchClients.Click += BtnSearchClients_Click;

                //pratiche Screen
                lyPraticheScreen = FindViewById<LinearLayout>(Resource.Id.lyPraticheScreen);
                lyBackFomPratiche = FindViewById<LinearLayout>(Resource.Id.lyBackFomPratiche);
                txtPraticheTitle = FindViewById<TextView>(Resource.Id.txtPraticheTitle);
                txtPraticheSearchLabel = FindViewById<TextView>(Resource.Id.txtPraticheSearchLabel);
                editTxtPraticheSearch = FindViewById<EditText>(Resource.Id.editTxtPraticheSearch);
                listviewPratiche = FindViewById<ListView>(Resource.Id.listviewPratiche);

                //pratiche details screen
                lyPraticheDetailsScreen = FindViewById<LinearLayout>(Resource.Id.lyPraticheDetailsScreen);
                txtPraticheDetailsTitle = FindViewById<TextView>(Resource.Id.txtPraticheDetailsTitle);
                lyBackFomPraticheDetails = FindViewById<LinearLayout>(Resource.Id.lyBackFomPraticheDetails);
                txtPraticheDetailAgente = FindViewById<TextView>(Resource.Id.txtPraticheDetailAgente);
                txtPraticheDetailApriTicket = FindViewById<TextView>(Resource.Id.txtPraticheDetailApriTicket);
                lyCreateApriTicket = FindViewById<LinearLayout>(Resource.Id.lyCreateApriTicket);
                txtPraticheDetailDataInserLabel = FindViewById<TextView>(Resource.Id.txtPraticheDetailDataInserLabel);
                txtPraticheDetailDataInserValue = FindViewById<TextView>(Resource.Id.txtPraticheDetailDataInserValue);
                txtPraticheDetailsDataCreazioneLabel = FindViewById<TextView>(Resource.Id.txtPraticheDetailsDataCreazioneLabel);
                txtPraticheDetailsDataCreazioneValue = FindViewById<TextView>(Resource.Id.txtPraticheDetailsDataCreazioneValue);
                txtPraticheDetailsQtaInseritaLabel = FindViewById<TextView>(Resource.Id.txtPraticheDetailsQtaInseritaLabel);
                txtPraticheDetailsQtaInseritaValue = FindViewById<TextView>(Resource.Id.txtPraticheDetailsQtaInseritaValue);
                txtPraticheDetailsQtaAttivaLabel = FindViewById<TextView>(Resource.Id.txtPraticheDetailsQtaAttivaLabel);
                txtPraticheDetailsQtaAttivaValue = FindViewById<TextView>(Resource.Id.txtPraticheDetailsQtaAttivaValue);
                txtPraticheDetailsTipoProdottoLabel = FindViewById<TextView>(Resource.Id.txtPraticheDetailsTipoProdottoLabel);
                txtPraticheDetailsTipoProdottoValue = FindViewById<TextView>(Resource.Id.txtPraticheDetailsTipoProdottoValue);
                txtPraticheDetailsProdottoLabel = FindViewById<TextView>(Resource.Id.txtPraticheDetailsProdottoLabel);
                txtPraticheDetailsProdottoValue = FindViewById<TextView>(Resource.Id.txtPraticheDetailsProdottoValue);
                txtPraticheDetailsSerialiLabel = FindViewById<TextView>(Resource.Id.txtPraticheDetailsSerialiLabel);
                txtPraticheDetailsSerialiValue = FindViewById<TextView>(Resource.Id.txtPraticheDetailsSerialiValue);
                txtPraticheDetailsLineeLabel = FindViewById<TextView>(Resource.Id.txtPraticheDetailsLineeLabel);
                txtPraticheDetailsLineeValue = FindViewById<TextView>(Resource.Id.txtPraticheDetailsLineeValue);
                txtPraticheDetailsTipoContrattoLabel = FindViewById<TextView>(Resource.Id.txtPraticheDetailsTipoContrattoLabel);
                txtPraticheDetailsTipoContrattoValue = FindViewById<TextView>(Resource.Id.txtPraticheDetailsTipoContrattoValue);
                txtPraticheDetailsCanoneLabel = FindViewById<TextView>(Resource.Id.txtPraticheDetailsCanoneLabel);
                txtPraticheDetailsCanoneValue = FindViewById<TextView>(Resource.Id.txtPraticheDetailsCanoneValue);
                txtPraticheDetailsDataAttivaLabel = FindViewById<TextView>(Resource.Id.txtPraticheDetailsDataAttivaLabel);
                txtPraticheDetailsDataAttivaValue = FindViewById<TextView>(Resource.Id.txtPraticheDetailsDataAttivaValue);
                txtPraticheDetailsStatoOrdineLabel = FindViewById<TextView>(Resource.Id.txtPraticheDetailsStatoOrdineLabel);
                txtPraticheDetailsStatoOrdineValue = FindViewById<TextView>(Resource.Id.txtPraticheDetailsStatoOrdineValue);
                txtPraticheDetailsStatoPraticaLabel = FindViewById<TextView>(Resource.Id.txtPraticheDetailsStatoPraticaLabel);
                txtPraticheDetailsStatoPraticaValue = FindViewById<TextView>(Resource.Id.txtPraticheDetailsStatoPraticaValue);
                txtPraticheDetailsRoutingLabel = FindViewById<TextView>(Resource.Id.txtPraticheDetailsRoutingLabel);
                txtPraticheDetailsRoutingValue = FindViewById<TextView>(Resource.Id.txtPraticheDetailsRoutingValue);
                txtPraticheDetailsNotesLabel = FindViewById<TextView>(Resource.Id.txtPraticheDetailsNotesLabel);
                txtPraticheDetailsNotesValue = FindViewById<TextView>(Resource.Id.txtPraticheDetailsNotesValue);

                //new ticket screen
                lyNewTicket = FindViewById<LinearLayout>(Resource.Id.lyNewTicket);
                lyBackFomNewTicket = FindViewById<LinearLayout>(Resource.Id.lyBackFomNewTicket);
                txtNewTicketTitle = FindViewById<TextView>(Resource.Id.txtNewTicketTitle);
                txtNewTicketDestinatoarioLabel = FindViewById<TextView>(Resource.Id.txtNewTicketDestinatoarioLabel);
                txtNewTicketDestinatoarioValue = FindViewById<TextView>(Resource.Id.txtNewTicketDestinatoarioValue);
                txtNewTicketTipoLabel = FindViewById<TextView>(Resource.Id.txtNewTicketTipoLabel);
                lblTicketLabelTipo = FindViewById<TextView>(Resource.Id.lblTicketLabelTipo);
                txtNewTicketRegSocialeLabel = FindViewById<TextView>(Resource.Id.txtNewTicketRegSocialeLabel);
                txtNewTicketRegSocialeValue = FindViewById<TextView>(Resource.Id.txtNewTicketRegSocialeValue);
                txtNewTicketPartitaIvaLabel = FindViewById<TextView>(Resource.Id.txtNewTicketPartitaIvaLabel);
                txtNewTicketPartitaIvaValue = FindViewById<TextView>(Resource.Id.txtNewTicketPartitaIvaValue);
                txtNewTicketOggettoLabel = FindViewById<TextView>(Resource.Id.txtNewTicketOggettoLabel);
                txtEditOggetto = FindViewById<EditText>(Resource.Id.txtEditOggetto);
                txtEditNewTicketDescription = FindViewById<EditText>(Resource.Id.txtEditNewTicketDescription);
                txtNewTicketInviaLabel = FindViewById<TextView>(Resource.Id.txtNewTicketInviaLabel);
                lyNewTicketInviaBtn = FindViewById<LinearLayout>(Resource.Id.lyNewTicketInviaBtn);
                lyNewTicketTipo = FindViewById<LinearLayout>(Resource.Id.lyNewTicketTipo);

                //memo screen
                lyClientiMemoScreen = FindViewById<LinearLayout>(Resource.Id.lyClientiMemoScreen);
                listviewMemo = FindViewById<ListView>(Resource.Id.listviewMemo);
                txtMemoTitle = FindViewById<TextView>(Resource.Id.txtMemoTitle);
                txtEditMemoTitle = FindViewById<EditText>(Resource.Id.txtEditMemoTitle);
                txtMemoDataLabel = FindViewById<TextView>(Resource.Id.txtMemoDataLabel);
                txtMemoDataValue = FindViewById<TextView>(Resource.Id.txtMemoDataValue);
                imgMemoCalendar = FindViewById<ImageView>(Resource.Id.imgMemoCalendar);
                linearMemoDataValue = FindViewById<LinearLayout>(Resource.Id.linearMemoDataValue);
                linearMemoHourValue = FindViewById<LinearLayout>(Resource.Id.linearMemoHourValue);
                txtMemoOraLabel = FindViewById<TextView>(Resource.Id.txtMemoOraLabel);
                txtMemoOraValue = FindViewById<TextView>(Resource.Id.txtMemoOraValue);
                imgMemoClock = FindViewById<ImageView>(Resource.Id.imgMemoClock);
                txtEditMemoDescription = FindViewById<EditText>(Resource.Id.txtEditMemoDescription);
                txtMemoSalvaBtn = FindViewById<TextView>(Resource.Id.txtMemoSalvaBtn);
                lyMemoSalvaBtn = FindViewById<LinearLayout>(Resource.Id.lyMemoSalvaBtn);
                txtEditMemoPlace = FindViewById<EditText>(Resource.Id.txtEditMemoPlace);
                txtMemoPlace = FindViewById<TextView>(Resource.Id.txtMemoPlaceHolder);
                memoPlusLay = FindViewById<LinearLayout>(Resource.Id.memoPlusLay);

                imgMapNavigtoLoc = FindViewById<ImageView>(Resource.Id.imgMapNavigtoLoc);
                txtMapSearchPlus = FindViewById<TextView>(Resource.Id.txtMapSearchPlus);
                txtMapSearchMinus = FindViewById<TextView>(Resource.Id.txtMapSearchMinus);


                //set fonts
                OswaldLight = TypeFaces.getTypeface(this, "Oswald-Light.ttf");
                OswaldRegualr = TypeFaces.getTypeface(this, "Oswald-Regular.ttf");

                txtClientiTitle.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtFilterText.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                editTxtSearch.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtSearchLabel.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtClientiMailLabel.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtClientiMailValue.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtClientiMobilePhoneLable.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtClientiMobilePhoneValue.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtClientiAddressLabel.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtClientiAddressValue.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtClientiPartitaLabel.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtClientiPartitaivaValue.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtClientiTelefonoLabel.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtClientiTelefonoValue.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtClientiFaxLabel.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtClientiFaxValue.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtClientiReferenteLabel.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtClientiReferenteValue.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtAltuofiancoDataLabel.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtAltuofiancoDataValue.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtAltuofiancoTipoConLabel.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtAltuofiancoTipoConValue.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtAltuofiancoCodiceClLabel.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtAltuofiancoCodiceClValue.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtAltuofiancoEmailShopLabel.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtAltuofiancoEmailShopValue.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtAltuofiancoPuntiDispLabel.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtAltuofiancoPuntiDispValue.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtAltuofiancoStatoRidLabel.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtAltuofiancoStatoRidValue.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtAltuofiancoNumeroRefLabel.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtAltuofiancoNumeroRefValue.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtPraticheTitle.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtPraticheSearchLabel.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                editTxtPraticheSearch.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);

                txtPraticheDetailsTitle.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtPraticheDetailAgente.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtPraticheDetailApriTicket.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtPraticheDetailDataInserLabel.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtPraticheDetailDataInserValue.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtPraticheDetailsDataCreazioneLabel.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtPraticheDetailsDataCreazioneValue.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtPraticheDetailsQtaInseritaLabel.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtPraticheDetailsQtaInseritaValue.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtPraticheDetailsQtaAttivaLabel.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtPraticheDetailsQtaAttivaValue.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtPraticheDetailsTipoProdottoLabel.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtPraticheDetailsTipoProdottoValue.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtPraticheDetailsProdottoLabel.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtPraticheDetailsProdottoValue.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtPraticheDetailsSerialiLabel.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtPraticheDetailsSerialiValue.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtPraticheDetailsLineeLabel.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtPraticheDetailsLineeValue.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtPraticheDetailsTipoContrattoLabel.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtPraticheDetailsTipoContrattoValue.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtPraticheDetailsCanoneLabel.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtPraticheDetailsCanoneValue.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtPraticheDetailsDataAttivaLabel.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtPraticheDetailsDataAttivaValue.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtPraticheDetailsStatoOrdineLabel.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtPraticheDetailsStatoOrdineValue.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtPraticheDetailsStatoPraticaLabel.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtPraticheDetailsStatoPraticaValue.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtPraticheDetailsRoutingLabel.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtPraticheDetailsRoutingValue.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtPraticheDetailsNotesLabel.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtPraticheDetailsNotesValue.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);

                txtNewTicketTitle.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtNewTicketDestinatoarioLabel.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtNewTicketDestinatoarioValue.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtNewTicketTipoLabel.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                lblTicketLabelTipo.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtNewTicketRegSocialeLabel.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtNewTicketRegSocialeValue.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtNewTicketPartitaIvaLabel.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtNewTicketPartitaIvaValue.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtNewTicketOggettoLabel.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtEditOggetto.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtEditNewTicketDescription.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtNewTicketInviaLabel.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);

                txtMemoTitle.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtEditMemoTitle.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtMemoDataLabel.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtMemoDataValue.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtMemoOraLabel.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtMemoOraValue.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtEditMemoDescription.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtMemoSalvaBtn.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtClientiMemo.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtClientiPratiche.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtEditMemoPlace.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);
                txtMemoPlace.SetTypeface(OswaldRegualr, TypefaceStyle.Normal);


                //events click
                layFIlter.Click += SelectFilter;
                boxMenuBack.Click += OnBackMenuClick;
                lyaltuofiancoBottomBack.Click += OnBackFromAltuofiancoClick1;
                lyaltuofiancoMenuBack.Click += OnBackFromAltuofiancoClick1;
                lyBackFomPratiche.Click += OnBackFromPraticheClick;
                lyBackFomPraticheDetails.Click += OnBackFromPraticheDetailsClick;
                lyBackFomNewTicket.Click += OnBackFromNewTicket;
//                lyClientiCall.Click += OnClickCallBut;
//                lyClientiEmail.Click += OnClickEmailBut;
//                lyMemoSalvaBtn.Click += OnClickAddMemo;
                linearMemoDataValue.Touch += ShowCalendarPicker;
                linearMemoHourValue.Touch += ShowHourPicker;
                listviewMemo.ItemClick += OnClcikItemMemoList;
                memoPlusLay.Click += MemoPlusLay_Click;

				zoomLevel = Utils.calculateZoomLevel((int)Utils.getScreenWidth());
                InitializeLocationManager();
                InitMap();
//                LoadALlClients();

                _isGooglePlayServicesInstalled = IsGooglePlayServicesInstalled();

                if (_isGooglePlayServicesInstalled)
                {
                    apiClient = new GoogleApiClient.Builder(this, this, this).AddApi(LocationServices.Api).Build();
                    locRequest = new LocationRequest();
                }
                else
                {
                    Log.Error("OnCreate", "Google Play Services is not installed");
                    Toast.MakeText(this, "Google Play Services non è installato", ToastLength.Long).Show();

                    if (test != null)
                    {
                        test.Dismiss();
                    }
                }

                String androidOS = Build.VERSION.Sdk;
                androidPhoneVersion = Int32.Parse(androidOS);
                Console.WriteLine(androidOS + "---------versione name-------");


                txtMapSearchPlus.Click += (sender, e) =>
                {
                    try
                    {
                        _map.AnimateCamera(CameraUpdateFactory.ZoomIn());
                    }
                    catch (Exception ex)
                    {
                        Utils.writeToDeviceLog(ex);
                    }
                   
                };

                txtMapSearchMinus.Click += (sender, e) =>
                {
                    try
                    {
                        _map.AnimateCamera(CameraUpdateFactory.ZoomOut());
                    }
                    catch (Exception ex)
                    {
                        Utils.writeToDeviceLog(ex);
                    }
                   
                };


                imgMapNavigtoLoc.Click += (sender, e) =>
                {
                    try
                    {
                        if (_map != null)
                        {
                            CameraPosition position = new CameraPosition.Builder()
                                .Target(new LatLng(_map.MyLocation.Latitude, _map.MyLocation.Longitude))
                                    .Zoom((float)zoomLevel).Build();

                            _map.AnimateCamera(CameraUpdateFactory.NewCameraPosition(position));
                        }
                    }
                    catch (Exception ex)
                    {
                        Utils.writeToDeviceLog(ex);
                    }

                };


            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        void BtnSearchClients_Click(object sender, EventArgs e)
        {
            try
            {
                List<ClientInfoItem> searchedClients = new List<ClientInfoItem>();
                string param = "";
                //if(clientsList!=null && clientsList.Count>0)
                {
                    switch (txtFilterText.Text.ToLower())
                    {
                        case "ragione sociale":
                            param = "regione_sociale";
                            //                                        Task.Run(async () => await  LoadALlClients(true,"regione_sociale",editTxtSearch.Text.ToLower()));
                            ////                                        LoadALlClients(true,"regione_sociale",editTxtSearch.Text.ToLower());
                            //                                        searchedClients = clientsList;//.Where(x => x.Regione_sociale.ToLower().Contains(editTxtSearch.Text.ToLower())).ToList();
                            break;
                        case "partita iva":
                            param = "partita_iva";
                            //                                        LoadALlClients(true,"partita_iva",editTxtSearch.Text.ToLower());
                            //                                        searchedClients = clientsList;//.Where(x => x.Partita_iva.ToLower().Contains(editTxtSearch.Text.ToLower())).ToList();
                            break;
                        case "telefono":
                            param = "phone";
                            //                                        LoadALlClients(true,"phone",editTxtSearch.Text.ToLower());
                            //                                        searchedClients = clientsList;//.Where(x => x.Phone.ToLower().Contains(editTxtSearch.Text.ToLower())).ToList();
                            break;
                        case "localita'":
                            param = "location";
                            //                                        LoadALlClients(true,"location",editTxtSearch.Text.ToLower());
                            //                                        searchedClients = clientsList;//.Where(x => x.Location.ToLower().Contains(editTxtSearch.Text.ToLower())).ToList();
                            break;
                        case "cellulare":
                            param = "mobile_phone";
                            //                                        LoadALlClients(true,"mobile_phone",editTxtSearch.Text.ToLower());
                            //                                        searchedClients = clientsList;//.Where(x => x.Mobile_phone.ToLower().Contains(editTxtSearch.Text.ToLower())).ToList();
                            break;
                        case "provincia":
                            param = "province";
                            //                                        LoadALlClients(true,"province",editTxtSearch.Text.ToLower());
                            //                                        searchedClients = clientsList;//.Where(x => x.Province.ToLower().Contains(editTxtSearch.Text.ToLower())).ToList();
                            break;
                    }
                }
                if (editTxtSearch.Text != "")
                {
                    LoadALlClients(true, param, editTxtSearch.Text.ToLower());
                }
                else
                {
                    Toast.MakeText(this, "Inserisci un testo da ricercare", ToastLength.Short).Show();
                }

            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        private void OnTimedEvent(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                countSeconds--;
                if (countSeconds == 0)
                {
                    if (!found)
                    {
                        RunOnUiThread(() =>
                            {
//                                Utils.displayMessage("Warning", "Gps could not determine current location", this);
                                Toast.MakeText(this, "Gps could not determine current location", ToastLength.Long).Show();
                                //if (networkEnabled)
                                // _locationManager.RequestLocationUpdates(LocationManager.NetworkProvider, 0, 0, this);
                            });
                    }
                    timer.Stop();
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        void InitializeLocationManager()
        {
            _locationManager = (LocationManager)GetSystemService(LocationService);

//            Criteria criteriaForLocationService = new Criteria
//                {
//                    Accuracy = Accuracy.Fine
//                };
//            IList<string> acceptableLocationProviders = _locationManager.GetProviders(criteriaForLocationService, true);
//
//            if (acceptableLocationProviders.Any())
//            {
//                _locationProvider = acceptableLocationProviders.First();
//            }
//            else
//            {
//                _locationProvider = String.Empty;
//            }
//            Log.Debug("LOG", "Using " + _locationProvider + ".");

            try
            {
                gpsEnabled = _locationManager.IsProviderEnabled(LocationManager.GpsProvider);
            }
            catch (Exception ex)
            {
            }


            try
            {
                networkEnabled = _locationManager.IsProviderEnabled(LocationManager.NetworkProvider);
            }
            catch (Exception ex)
            {
            }  

//            timer = new System.Timers.Timer();
//            timer.Interval = 1000;
//            timer.Elapsed += OnTimedEvent;
//            timer.Enabled = true;

//            if (gpsEnabled)
//                _locationManager.RequestLocationUpdates(LocationManager.GpsProvider, 0, 0, this);
//
//            if (networkEnabled)
//                _locationManager.RequestLocationUpdates(LocationManager.NetworkProvider, 0, 0, this);

        }

        protected override void OnResume()
        {
            base.OnResume();
            try
            {
                if (apiClient != null)
                {
                    apiClient.Connect();
                }
//                Thread.Sleep(5000);
//                locRequest.SetPriority(100);
//                locRequest.SetFastestInterval(500);
//                locRequest.SetInterval(1000);

//                if (apiClient.IsConnected)
//                {
//                    Location location = LocationServices.FusedLocationApi.GetLastLocation (apiClient);
//                    if (location != null)
//                    {
////                        latitude.Text = "Latitude: " + location.Latitude.ToString();
////                        longitude.Text = "Longitude: " + location.Longitude.ToString();
////                        provider.Text = "Provider: " + location.Provider.ToString();
//                        Log.Debug ("LocationClient", "Last location printed----"+location.Latitude.ToString()+"-----"+location.Longitude.ToString());
//                    }
//                }

//                LocationServices.FusedLocationApi.RequestLocationUpdates (apiClient, locRequest, this);
//                _locationManager.RequestLocationUpdates(LocationManager.GpsProvider, 0, 0, this);
//                _locationManager.RequestLocationUpdates(LocationManager.NetworkProvider, 0, 0, this);
//                if(androidPhoneVersion<14)
                if (_mapFragmentSupport != null)
                {
                    _map = _mapFragmentSupport.Map;
                }
//                else
//                {
//                    _map = _mapFragment.Map;
//                }


//                Object obj = new Object();
//                lock (obj)
//                { 
//                    SetupMapIfNeeded();
//                }
//                System.Threading.ThreadPool.QueueUserWorkItem(state =>
//                    {
//                        try
//                        {
//                            Object obj = new Object();
//                            lock (obj)
//                            {
//                                if (gpsEnabled)
//                                    _locationManager.RequestLocationUpdates(LocationManager.GpsProvider, 0, 0, this);
//
//                                if (networkEnabled)
//                                    _locationManager.RequestLocationUpdates(LocationManager.NetworkProvider, 0, 0, this);
//                            }
//                        }
//                        catch (Exception ex)
//                        {
//                            Utils.writeToDeviceLog(ex);
//                        }
//                        finally
//                        {
//                            Thread.Sleep(100);
//                            LoadALlClients();
////                            SetupMapIfNeeded();
//                        }
//
//                    });
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        protected override async void OnPause()
        {
            base.OnPause();
            try
            {
                if (apiClient.IsConnected)
                {
                    // stop location updates, passing in the LocationListener
                    await LocationServices.FusedLocationApi.RemoveLocationUpdates(apiClient, this);

                    apiClient.Disconnect();
                }
//                _locationManager.RemoveUpdates(this);
                if (_map != null)
                {
                    _map.MyLocationEnabled = false;
                }

                if (test != null)
                {
                    test.Dismiss();
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        private void InitMap()
        {

            try
            {
//                if(androidPhoneVersion<14)
                {
                    _mapFragmentSupport = SupportFragmentManager.FindFragmentByTag("map") as SupportMapFragment;
                    if (_mapFragmentSupport == null)
                    {
                        GoogleMapOptions mapOptions = new GoogleMapOptions()
                            .InvokeMapType(GoogleMap.MapTypeNormal)
                            .InvokeZoomControlsEnabled(false)
                            .InvokeCompassEnabled(true);

                        Android.Support.V4.App.FragmentTransaction fragTx = SupportFragmentManager.BeginTransaction();
                        _mapFragmentSupport = SupportMapFragment.NewInstance(mapOptions);
                        fragTx.Add(Resource.Id.map, _mapFragmentSupport, "map");
                        fragTx.Commit();
                    }
                }
//                else
//                {
//                    Utils.GooglePlayServicesEnum gpsStatus = Utils.getGooglePlayServicesStatus();
//                    _mapFragment = FragmentManager.FindFragmentByTag("map") as MapFragment;
//                    if (_mapFragment == null)
//                    {
//                        if (gpsStatus == Utils.GooglePlayServicesEnum.good)
//                        {
//                            GoogleMapOptions mapOptions = new GoogleMapOptions()
//                                .InvokeMapType(GoogleMap.MapTypeNormal)
//                                .InvokeZoomControlsEnabled(true)
//                                .InvokeCompassEnabled(true);
//
//                            Android.App.FragmentTransaction fragTx = FragmentManager.BeginTransaction();
//
//                            _mapFragment = MapFragment.NewInstance(mapOptions);
//
//                            fragTx.Add(Resource.Id.map, _mapFragment, "map");
//
//                            fragTx.Commit();
//
//                            checkForGps = false;
//                        }
//                        else if (gpsStatus == Utils.GooglePlayServicesEnum.outOfDate)
//                        {                   
//                            alertDialog = new AlertDialog.Builder(this).Create();
//                            alertDialog.SetTitle("Alert");
//                            alertDialog.SetMessage("'Google play services' non è aggiornato. Si prega di aggiornare.");
//                            alertDialog.SetButton("OK", (sender1, e1) =>
//                                {
//                                    alertDialog.Dismiss();
//                                });
//                            alertDialog.Show();
//                        }
//                        else
//                        {
//                            alertDialog = new AlertDialog.Builder(this).Create();
//                            alertDialog.SetTitle("Alert");
//                            alertDialog.SetMessage("'Google play services' non è installato. Si prega di installare.");
//                            alertDialog.SetButton("OK", (sender1, e1) =>
//                                {
//                                    alertDialog.Dismiss();
//                                });
//                            alertDialog.Show();
//                        }
//                    }
//                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }


        }

        public void SetupMapIfNeeded()
        {
            try
            {
                System.Threading.ThreadPool.QueueUserWorkItem(state =>
                    {
                        try
                        {
                            string err = "Error ";
//                            Location currLocation = null;
//                            Object obj = new Object();
//                            lock (obj)
//                            { 
//                                if(_currentLocation==null)
//                                {
//                                    currLocation = Utils.requestNewLocation(this).Location;
//                                }
//                                else
//                                {
//                                    currLocation=_currentLocation;
//                                }
//
//                            }
                            if (_currentLocation != null)
                            {
                                setLocation = true;
                                LatLng location = new LatLng(_currentLocation.Latitude, _currentLocation.Longitude);
                                RunOnUiThread(() =>
                                    {
                                        if (location != null)
                                        {
                                            MarkerOptions marker;
                                            int type;
//                                            _map = _mapFragment.Map;
                                            if (_map != null)
                                            {

                                                _map.Clear();

                                                marker = new MarkerOptions();
                                                marker.SetPosition(location);
                                                marker.SetTitle("La mia posizione");
                                                marker.Draggable(true);
                                                marker.InvokeIcon(BitmapDescriptorFactory.FromResource(Resource.Drawable.map_man));
                                                _map.AddMarker(marker);
                                                _map.MapType = GoogleMap.MapTypeNormal;
                                                _map.UiSettings.MyLocationButtonEnabled = false;

                                                CameraUpdate cameraUpdate = CameraUpdateFactory.NewLatLngZoom(location, (float)zoomLevel);
                                                _map.MoveCamera(cameraUpdate);


                                                _map.MyLocationEnabled = true;
//                                                AddMeetingMarkersToMap();

                                            }
                                        }
                                        else
                                        {
                                            alertDialog = new AlertDialog.Builder(this).Create();
                                            alertDialog.SetTitle("Error");
                                            alertDialog.SetMessage(err);
                                            alertDialog.SetButton("OK", (sender1, e1) =>
                                                {
                                                    alertDialog.Dismiss();
                                                });
                                            alertDialog.Show();
                                        }
                                    });
                            }
                            else
                            {
                                AndHUD.Shared.ShowError(this, "Impossibile determinare la tua posizione", MaskType.Black, TimeSpan.FromSeconds(3));
                            }
                        }
                        catch (Exception ex)
                        {
                            Utils.writeToDeviceLog(ex);
                        }
                        finally
                        {
                            RunOnUiThread(() =>
                                {
//                                    test.Dismiss();
                                    // _locationManager.RemoveUpdates(this);
                                });
                        }

                    });
                
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }

        }

        public Bitmap CreateIcon(string text, string type, ClientInfoItem clientItem)
        {
            Bitmap bitmap = null;
            try
            {
                EnsureInit();

                switch (type)
                {
                    case "borsellino":
                        iconView.SetImageResource(Resource.Drawable.tab_borsellino);
                        break;
                    case "convergente_atf_special":
                        iconView.SetImageResource(Resource.Drawable.customers_conv_atf_special);
                        break;
                    case "convergente_special":
                        iconView.SetImageResource(Resource.Drawable.customers_conv_special);
                        break;
                    case "convergente_atf":
                        iconView.SetImageResource(Resource.Drawable.customers_conv_atf);
                        break;
                    case "convergente":
                        iconView.SetImageResource(Resource.Drawable.customers_conv);
                        break;
                    case "nonconvergente_atf_special":
                        iconView.SetImageResource(Resource.Drawable.customers_noConv_atf_special);
                        break;
                    case "nonconvergente_special":
                        iconView.SetImageResource(Resource.Drawable.customers_noConv_special);
                        break;
                    case "nonconvergente_atf":
                        iconView.SetImageResource(Resource.Drawable.customers_noConv_atf);
                        break;
                    case "nonconvergente":
                        iconView.SetImageResource(Resource.Drawable.customers_noConv);
                        break;
                    case "deactivated":
                        iconView.SetImageResource(Resource.Drawable.customers_deact);
                        break;
                    default:
                        iconView.SetImageResource(Resource.Drawable.customers_deact);
                        break;
                }

                textView.Text = text;
                textView.Visibility = !String.IsNullOrEmpty(text) ? ViewStates.Visible : ViewStates.Gone;
                if (text == " ")
                    textView.SetBackgroundColor(Color.Transparent);

                // Lay out size and measure
                var measureSpec = ViewGroup.MeasureSpec.MakeMeasureSpec(0, MeasureSpecMode.Unspecified);
                container.Measure(measureSpec, measureSpec);
                int measuredWidth = container.MeasuredWidth;
                int measuredHeight = container.MeasuredHeight;
                container.Layout(0, 0, measuredWidth, measuredHeight);

                // Take current layout and draw to a bitmap
                bitmap = Bitmap.CreateBitmap(measuredWidth, measuredHeight, Bitmap.Config.Argb4444); // According to API docs, this is deprecated to Argb8888; however, testing shows a reduction in memory pressure
                bitmap.EraseColor(Color.Transparent);
                using (var canvas = new Canvas(bitmap))
                {
                    container.Draw(canvas);
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }

            return bitmap;
        }

        public void EnsureInit()
        {
            try
            {
                if (container == null)
                {
                    container = this.LayoutInflater.Inflate(Resource.Layout.mapPinForMapActivity, null) as ViewGroup;
                    iconView = container.FindViewById<ImageView>(Resource.Id.imgCustomPin);
                    textView = container.FindViewById<TextView>(Resource.Id.textView1);
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        private async Task AddMeetingMarkersToMap()
        {
            try
            {
                BitmapDescriptor descriptor;
                List<Marker> markersList = new List<Marker>();
                List<ClientInfoItem> listOfSelectedClients = new List<ClientInfoItem>();
                if (clientsList != null && clientsList.Count > 0)
                {
                    listOfSelectedClients = clientsList.Where(x => x.Date_clienti.Date == DateTime.Now.Date).ToList();
                }
                if (_map != null)
                {
                    RunOnUiThread(() =>
                        {
						try
							{
								_map.Clear();
								for (int i = 0; i < listOfSelectedClients.Count; i++)
								{
									if (listOfSelectedClients[i].Latitude != 0 && listOfSelectedClients[i].Longitude != 0)
									{
										descriptor = BitmapDescriptorFactory.FromBitmap(CreateIcon(listOfSelectedClients[i].Date_clienti.Date.ToString("HH:mm", CultureInfo.InvariantCulture), listOfSelectedClients[i].Status, listOfSelectedClients[i]));
										//                     BitmapDescriptor icon = BitmapDescriptorFactory.FromResource(Resource.Drawable.customPin_icon);
										MarkerOptions markerOptions = new MarkerOptions()
											.SetPosition(new LatLng(listOfSelectedClients[i].Latitude, listOfSelectedClients[i].Longitude))
											.InvokeIcon(descriptor)
											.SetSnippet("Dettagli")//listOfSelectedClients[i].Date_clienti.Date.ToString("HH:mm", CultureInfo.InvariantCulture))
											.SetTitle(listOfSelectedClients[i].Regione_sociale);
										_map.AddMarker(markerOptions);

										markersList.Add(_map.AddMarker(markerOptions));
									}
								}


								_map.InfoWindowClick += (object sender, GoogleMap.InfoWindowClickEventArgs e) =>
								{
								try
									{
										int index = listOfSelectedClients.FindIndex(element => element.Latitude == e.Marker.Position.Latitude && element.Longitude == e.Marker.Position.Longitude);
										Console.WriteLine(listOfSelectedClients[index].Regione_sociale);

										linearDetails.Visibility = ViewStates.Visible;
										editTxtSearch.ClearFocus();
										lyClientiContent.Visibility = ViewStates.Visible;

										List<ClientInfoItem> listClientiFromPin = new List<ClientInfoItem>();
										listClientiFromPin.Add(listOfSelectedClients[index]);
										clientiListSearchAdapter = new ClientiSearchListAdapter(this, listClientiFromPin, 0);
										listviewResultSearch.Adapter = clientiListSearchAdapter;
										OnClickFromListClick(0, listClientiFromPin);
										clientiListSearchAdapter.ItemClickIdEvent += (index2) => OnClickFromListClick(index2, listClientiFromPin);
									}
									catch (Exception ex)
									{
										Utils.writeToDeviceLog(ex);
									}

								};

								if (markersList.Count > 0)
								{
									LatLngBounds.Builder builder = new LatLngBounds.Builder();
									foreach (Marker marker in markersList)
									{
										builder.Include(marker.Position);
									}
									LatLngBounds bounds = builder.Build();
									int padding = 0; // offset from edges of the map in pixels
													 //                                CameraUpdate cu = CameraUpdateFactory.NewLatLngBounds(bounds, padding);
													 //                                _map.AnimateCamera(cu);
									CameraUpdate cameraUpdate = CameraUpdateFactory.NewLatLngZoom(new LatLng(_currentLocation.Latitude, _currentLocation.Longitude), (float)zoomLevel);
									_map.MoveCamera(cameraUpdate);
								}
							}
							catch (Exception ex)
							{
								Utils.writeToDeviceLog(ex);
							}


                        });
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }


        void SelectFilter(object sender, EventArgs e)
        {
            try
            {
                List<string> status = new List<string>(){ "RAGIONE SOCIALE", "PARTITA IVA", "TELEFONO", "LOCALITA'", "CELLULARE", "PROVINCIA" };
                if (txtFilterText.Text != "")
                {
                    if (status != null)
                    {
                        status.Remove(txtFilterText.Text);
                    }
                }

                if (window != null && window.IsShowing == true)
                {
                    window.Dismiss();
                }
                else
                {
                    imgStatusArrow.SetImageResource(Resource.Drawable.arrowDown);

                    LayoutInflater inflater = (LayoutInflater)this.GetSystemService(Context.LayoutInflaterService);
                    View popup = inflater.Inflate(Resource.Layout.DropDownClientiFilter, null);

                    ListView listDropDown = popup.FindViewById<ListView>(Resource.Id.dropDownListView);
                    listDropDown.Adapter = new ClientiFilterDropDownListAdapter(status, this);
                    listDropDown.ItemClick += (object sender1, AdapterView.ItemClickEventArgs e1) =>
                    {
                        txtFilterText.Text = (((ListView)sender1).Adapter).GetItem(e1.Position).ToString();
                        window.Dismiss();

                        List<ClientInfoItem> searchedClients = new List<ClientInfoItem>();
                        string param = "";
                        //if(clientsList!=null && clientsList.Count>0)
                        {
                            switch ((((ListView)sender1).Adapter).GetItem(e1.Position).ToString().ToLower())
                            {
                                case "ragione sociale":
                                    param = "regione_sociale";
//                                        Task.Run(async () => await  LoadALlClients(true,"regione_sociale",editTxtSearch.Text.ToLower()));
////                                        LoadALlClients(true,"regione_sociale",editTxtSearch.Text.ToLower());
//                                        searchedClients = clientsList;//.Where(x => x.Regione_sociale.ToLower().Contains(editTxtSearch.Text.ToLower())).ToList();
                                    break;
                                case "partita iva":
                                    param = "partita_iva";
//                                        LoadALlClients(true,"partita_iva",editTxtSearch.Text.ToLower());
//                                        searchedClients = clientsList;//.Where(x => x.Partita_iva.ToLower().Contains(editTxtSearch.Text.ToLower())).ToList();
                                    break;
                                case "telefono":
                                    param = "phone";
//                                        LoadALlClients(true,"phone",editTxtSearch.Text.ToLower());
//                                        searchedClients = clientsList;//.Where(x => x.Phone.ToLower().Contains(editTxtSearch.Text.ToLower())).ToList();
                                    break;
                                case "localita'":
                                    param = "location";
//                                        LoadALlClients(true,"location",editTxtSearch.Text.ToLower());
//                                        searchedClients = clientsList;//.Where(x => x.Location.ToLower().Contains(editTxtSearch.Text.ToLower())).ToList();
                                    break;
                                case "cellulare":
                                    param = "mobile_phone";
//                                        LoadALlClients(true,"mobile_phone",editTxtSearch.Text.ToLower());
//                                        searchedClients = clientsList;//.Where(x => x.Mobile_phone.ToLower().Contains(editTxtSearch.Text.ToLower())).ToList();
                                    break;
                                case "provincia":
                                    param = "province";
//                                        LoadALlClients(true,"province",editTxtSearch.Text.ToLower());
//                                        searchedClients = clientsList;//.Where(x => x.Province.ToLower().Contains(editTxtSearch.Text.ToLower())).ToList();
                                    break;
                            }
                        }
//                            Task taskA = Task.Factory.StartNew(() => LoadALlClients(true,param,editTxtSearch.Text.ToLower()));
//                            taskA.Wait();


//                            ManualResetEvent resetEvent = new ManualResetEvent(false);  
                        if (editTxtSearch.Text != "")
                        {
                            LoadALlClients(true, param, editTxtSearch.Text.ToLower());
                        }
                        else
                        {
                            Toast.MakeText(this, "Inserisci un testo da ricercare", ToastLength.Short).Show();
                        }

                                        
                        //resetEvent.WaitOne();
//                            searchedClients = clientsList;
//                        if (searchedClients.Count > 0)
//                        {
//                            linearDetails.Visibility = ViewStates.Visible;
//                            editTxtSearch.ClearFocus();
//                            lyClientiContent.Visibility = ViewStates.Visible;
//                            clientiListSearchAdapter = new ClientiSearchListAdapter(this, searchedClients, 0);
//                            listviewResultSearch.Adapter = clientiListSearchAdapter;
//                            OnClickFromListClick(0, searchedClients);
//                            clientiListSearchAdapter.ItemClickIdEvent += (index) => OnClickFromListClick(index, searchedClients);
//
//                        }
//                        else
//                        {
//                            Toast.MakeText(this, "Nessun cliente trovato.", ToastLength.Long).Show();
//                            listviewResultSearch.Adapter = null;
//                            linearDetails.Visibility = ViewStates.Invisible;
//
//                        }
//
//                        if (lyClientiMemoScreen.Visibility == ViewStates.Visible)
//                        {
//                            lyClientiMemoScreen.Visibility = ViewStates.Gone;
//                        }
                       
                    };

                    window = new PopupWindow(popup, ((LinearLayout)sender).Width, WindowManagerLayoutParams.WrapContent);
                    window.Touchable = true;
                    window.Focusable = true;
                    window.OutsideTouchable = true;
                    window.DismissEvent += (object sender1, EventArgs e1) =>
                    {
                        imgStatusArrow.SetImageResource(Resource.Drawable.arrowTop);
                    };

                    window.SetBackgroundDrawable(new BitmapDrawable());
                    window.ShowAsDropDown((LinearLayout)sender, 0, 0);
                }

            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void OnClickFromListClick(int position, List<ClientInfoItem> searchedList)
        {
            try
            {
                txtClientiMailValue.Text = searchedList[position].Mail;
                txtClientiMobilePhoneValue.Text = searchedList[position].Mobile_phone;
                txtClientiAddressValue.Text = searchedList[position].Address + ", " + searchedList[position].Location + ", " + searchedList[position].Province;

                txtClientiPartitaivaValue.Text = searchedList[position].Partita_iva;
                txtClientiTelefonoValue.Text = searchedList[position].Phone;
                txtClientiFaxValue.Text = searchedList[position].Fax;
                txtClientiReferenteValue.Text = searchedList[position].Referente;

                clientiListSearchAdapter = new ClientiSearchListAdapter(this, searchedList, position);
                listviewResultSearch.Adapter = clientiListSearchAdapter;
                clientiListSearchAdapter.ItemClickIdEvent += (index) => OnClickFromListClick(index, searchedList);

                CustomTagClientInfo item0 = new CustomTagClientInfo();
                item0.Clientinf = searchedList[position];
                lyPratiche.Tag = item0;

                lyCLientiMemo.Tag = item0;
                lyAltuofianco.Tag = item0;


                lyPratiche.Click -= OnShowPraticheForClient;//(sender, e, searchedList[position]);
                lyCLientiMemo.Click -= OnShowAllMemosForClient;// searchedList[position]);

                lyAltuofianco.Click -= OnShowAltuofiancoInfo;
                lyAltuofianco.Click += OnShowAltuofiancoInfo;

                if (searchedList[position].Atf_id_customer != "")
                {
                    lyAltuofianco.SetBackgroundResource(Resource.Drawable.SelectorBtnOrangetoblue);
//                    lyAltuofianco.Click -=OnShowAltuofiancoInfo;
//                    lyAltuofianco.Click +=OnShowAltuofiancoInfo;
                }
                else
                {
//                    lyAltuofianco.Click -=OnShowAltuofiancoInfo;
                    lyAltuofianco.SetBackgroundColor(Color.Gray);
                }

                lyPratiche.Click += OnShowPraticheForClient;//(sender, e, searchedList[position]);
                lyCLientiMemo.Click += OnShowAllMemosForClient;

                lyClientiCall.Tag = item0;
                lyClientiEmail.Tag = item0;

                lyClientiCall.Click -= OnClickCallBut;
                lyClientiEmail.Click -= OnClickEmailBut;

                lyClientiCall.Click += OnClickCallBut;
                lyClientiEmail.Click += OnClickEmailBut;

                lyMemoSalvaBtn.Tag = item0;
                lyMemoSalvaBtn.Click -= OnClickAddMemo;
                lyMemoSalvaBtn.Click += OnClickAddMemo;

                listviewResultSearch.SetSelection(position);
                //listviewResultSearch.SetSelector(position);
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        ProgressDialog asd;

        public void LoadALlClients(bool searchFlag, string param, string value)
        {
            try
            {
                JToken objClients = null;
                LocationManager locationManager = (LocationManager)GetSystemService(LocationService);

                clientsList = new List<ClientInfoItem>();  

                test.Show(this, "Attendere prego", -1, MaskType.Clear, TimeSpan.FromSeconds(120), null, true, null); 
//                test = null;
//                asd =  ProgressDialog.Show(this, "asdasd", "asda");
                if (mWakeLock != null)
                {
                    mWakeLock.Acquire();
                }
                ThreadPool.QueueUserWorkItem(state =>
                    {
                        try
                        {
                            //Thread.Sleep(3000);
                            if (Utils.CheckForInternetConn())
                            {  
                                if (searchFlag)
                                {
                                    objClients = ApiCalls.getAllClients(Utils.UserToken, "0", "0", false, searchFlag, param, value);
                                }
                                else
                                {
                                    if (locationManager.IsProviderEnabled(LocationManager.GpsProvider))
                                    {
                                        //GPS enabled
                                        double lat = 0;
                                        double lng = 0;
                                        if (_currentLocation != null)
                                        {
                                            lat = _currentLocation.Latitude;
                                            lng = _currentLocation.Longitude;
                                        }
                                        objClients = ApiCalls.getAllClients(Utils.UserToken, lat.ToString(), lng.ToString(), false, searchFlag, param, value);//_currentLocation.Longitude.ToString(),_currentLocation.Latitude.ToString());
                                    }
                                    else
                                    {
                                        //GPS disabled
                                        objClients = ApiCalls.getAllClients(Utils.UserToken, "0", "0", true, searchFlag, param, value);
                                    }
                                }


                                if (objClients != null && objClients.HasValues)
                                {
                                    if (!objClients.First.Contains("error"))
                                    {
                                        foreach (var item in objClients)
                                        {
                                            ClientInfoItem client = new ClientInfoItem();
                                            client.Customer_id = item["id_customer"].ToString();
                                            client.Address = item["address"].ToString();

                                            if (item["fax"].ToString() != "")
                                            {
                                                client.Fax = item["fax"].ToString().Substring(1);
                                            }

                                            client.Location = item["location"].ToString();
                                            client.Mail = item["mail"].ToString();

                                       
                                            client.Partita_iva = item["partita_iva"].ToString();
                                            if (item["phone"].ToString() != "")
                                            {
                                                client.Phone = item["phone"].ToString().Substring(1);
                                            }
                                            client.Province = item["province"].ToString();
                                            client.Referente = item["referente"].ToString();
                                            if (client.Referente != "")
                                            {
                                                client.Mobile_phone = item["ref_mobile"].ToString();
                                            }
                                            else
                                            {
                                                client.Mobile_phone = item["mobile_phone"].ToString();
                                            }

                                            client.Regione_sociale = item["regione_sociale"].ToString();
                                            client.Status = item["status"].ToString();
                                            client.Date_clienti = DateTime.Now.Date;

                                            if (item["latitude"] != null && item["latitude"].ToString() != "")
                                            {
                                                double latDouble;
                                                double.TryParse(item["latitude"].ToString(), NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingSign, CultureInfo.InvariantCulture, out latDouble);

                                                client.Latitude = latDouble;
                                            }
                                            if (item["longitude"] != null && item["longitude"].ToString() != "")
                                            {
                                                double longDouble;
                                                double.TryParse(item["longitude"].ToString(), NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingSign, CultureInfo.InvariantCulture, out longDouble);

                                                client.Longitude = longDouble;
                                            }

                                            DateTime dataInserimento;
                                            DateTime.TryParseExact(item["data_inserimento"].ToString(), "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dataInserimento);

                                            client.Data_inserimento = dataInserimento;
                                            client.Tipo_contratto = item["tipo_contratto"].ToString();
                                            client.Codice_cliente = item["codice_cliente"].ToString();
                                            client.Email_shop = item["email_shop"].ToString();
                                            int result;
                                            int.TryParse(item["punti_disponibili"].ToString(), out result);
                                            client.Punti_disponibili = result;
                                            client.Stato_rid = item["stato_rid"].ToString();
                                            client.Numero_referenze = (item["numero_referenze"].ToString()) != null ? (item["numero_referenze"].ToString()) : "";
                                            client.Flag_borsellino = item["flag_borsellino"].ToString();
                                            client.Flag_preso = item["flag_preso"].ToString();

                                            DateTime dataConvergence;
                                            DateTime.TryParseExact(item["date_convergence"].ToString(), "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dataConvergence);
                                            client.Date_convergence = dataConvergence;

                                            client.Atf_id_customer = item["atf_id_customer"].ToString();

                                            List<ClientPraticheItemInfo> listofPratiche = new List<ClientPraticheItemInfo>();
                                            foreach (var itemPrat in item["pratiche"])
                                            {
                                                ClientPraticheItemInfo pratItem = new ClientPraticheItemInfo();
                                                if (itemPrat["data_creazione"].ToString() != "")
                                                {
                                                    DateTime dataCreazione;
                                                    DateTime.TryParseExact(itemPrat["data_creazione"].ToString(), "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dataCreazione);
                                                    pratItem.Pratiche_date = dataCreazione;
                                                }
                                                pratItem.Gnt_inserita = itemPrat["quantity"].ToString();
                                                pratItem.Gnt_attiva = itemPrat["quantity_active"].ToString();
                                                pratItem.Tipo_prodotto = itemPrat["product_type"].ToString();
                                                pratItem.Prodotto = itemPrat["product"].ToString();

                                                if (itemPrat["date_insert"].ToString() != "")
                                                {
                                                    DateTime dataForceActive;
                                                    DateTime.TryParseExact(itemPrat["date_insert"].ToString(), "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dataForceActive);
                                                    pratItem.Data_inserimento = dataForceActive;
//                                                pratItem.Data_inserimento = DateTime.Parse(itemPrat["date_insert"].ToString(), CultureInfo.InvariantCulture);
                                                }
                                                if (itemPrat["data_creazione"].ToString() != "")
                                                {
                                                    DateTime dataCreazione;
                                                    DateTime.TryParseExact(itemPrat["data_creazione"].ToString(), "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dataCreazione);
                                                    pratItem.Data_creazione = dataCreazione;
//                                                pratItem.Data_creazione=client.Data_inserimento;
//                                                pratItem.Data_creazione = DateTime.Parse(itemPrat["data_creazione"].ToString(), CultureInfo.InvariantCulture);
                                                }
                                                if (itemPrat["quantity"].ToString() != "")
                                                {
                                                    pratItem.Qta_inserita = Int32.Parse(itemPrat["quantity"].ToString());
                                                }
                                                if (itemPrat["quantity_active"].ToString() != "")
                                                {
                                                    pratItem.Qta_attiva = Int32.Parse(itemPrat["quantity_active"].ToString());
                                                }
                                                //if (itemPrat["linee"].ToString() != "")
                                                {
                                                    pratItem.Linee = itemPrat["linee"].ToString();

                                                    if (pratItem.Linee.Contains("\\r\\n"))
                                                    {
                                                        pratItem.Linee = pratItem.Linee.Replace(@"\r\n", "\n");
                                                    }
//                                                pratItem.Linee="90532771318 \n\r90532772030 \n\r90532978705";
                                                }

                                                pratItem.Seriale = itemPrat["serial"].ToString();
                                                if (pratItem.Seriale.Contains("\\r\\n"))
                                                {
                                                    pratItem.Seriale = pratItem.Seriale.Replace(@"\r\n", "\n");
                                                }

                                                pratItem.Tipo_contratto = itemPrat["contract_type"].ToString();


                                                if (itemPrat["canone"] != null && itemPrat["canone"].ToString() != "")
                                                {
                                                    double canon;
                                                    double.TryParse(itemPrat["canone"].ToString(), NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingSign, CultureInfo.InvariantCulture, out canon);

                                                    pratItem.Canone = canon;
                                                }

                                                if (itemPrat["force_date_active"].ToString() != "")
                                                {
                                                    DateTime dataAttivazione;
                                                    DateTime.TryParseExact(itemPrat["force_date_active"].ToString(), "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out dataAttivazione);
                                                    pratItem.Data_attivazione = dataAttivazione;
//                                                pratItem.Data_attivazione = DateTime.Parse(itemPrat["force_date_active"].ToString(), CultureInfo.InvariantCulture);//
                                                }

                                                pratItem.Stato_ordine = "";
                                                pratItem.Stato_pratica = "";
                                                pratItem.Codice_cliente = item["id_customer"].ToString();
                                                pratItem.Id_product = itemPrat["id_product"].ToString();

                                                pratItem.Routing = itemPrat["routing"].ToString();
                                                pratItem.Notes = itemPrat["notes"].ToString();

                                                listofPratiche.Add(pratItem);
                                              
                                            }
                                            client.Practiche_list = listofPratiche.OrderBy(x => x.Data_inserimento).ToList();
                                            clientsList.Add(client);
                                        }
                                        clientsList = clientsList.OrderBy(x => x.Regione_sociale).ToList();
                                    }
                                    else
                                    {
                                        RunOnUiThread(() =>
                                            {
                                                AndHUD.Shared.ShowError(this, objClients["error"].ToString(), MaskType.Black, TimeSpan.FromSeconds(3));
                                            });
                                    }

                                    //insert info to db
									RunOnOtherThread();
//                                    foreach (var item in clientsList)
//                                    {
//                                        if (db.CheckIfExiststClientInDB(item.Partita_iva))
//                                        {
//                                            db.UpdateClientItem(item);
//                                        }
//                                        else
//                                        {
//                                            db.InsertClientsToTable(item);
//                                        }
//                                        AddPraticheToDb(item.Practiche_list);
//                                    }
                                }
                            }
                            else
                            {
                                clientsList = db.getAllClients();
                                foreach (var item in clientsList)
                                {
                                    //TODO Add pratiche to clients
                                    item.Practiche_list = (db.getPraticheListForCustomer(item.Customer_id));
                                }
                                clientsList = clientsList.OrderBy(x => x.Regione_sociale).ToList();
                                RunOnUiThread(() =>
                                    {
                                        AndHUD.Shared.ShowError(this, "Nessuna connessione Internet", MaskType.Black, TimeSpan.FromSeconds(3));
                                    });
                            }
                            if (searchFlag)
                            {
                                RunOnUiThread(() =>
                                    {
                                        if (clientsList.Count > 0)
                                        {
                                            linearDetails.Visibility = ViewStates.Visible;
                                            editTxtSearch.ClearFocus();
                                            lyClientiContent.Visibility = ViewStates.Visible;
                                            clientiListSearchAdapter = new ClientiSearchListAdapter(this, clientsList, 0);
                                            listviewResultSearch.Adapter = clientiListSearchAdapter;
                                            OnClickFromListClick(0, clientsList);
                                            clientiListSearchAdapter.ItemClickIdEvent += (index) => OnClickFromListClick(index, clientsList);

                                        }
                                        else
                                        {
                                            Toast.MakeText(this, "Nessun cliente trovato.", ToastLength.Long).Show();
                                            listviewResultSearch.Adapter = null;
                                            linearDetails.Visibility = ViewStates.Invisible;

                                        }

                                        if (lyClientiMemoScreen.Visibility == ViewStates.Visible)
                                        {
                                            lyClientiMemoScreen.Visibility = ViewStates.Gone;
                                        }
                                    });
                            }
                        }
                        catch (Exception ex)
                        {
                            Utils.writeToDeviceLog(ex);
                        }
                        finally
                        {
                        
                            RunOnUiThread(() =>
                                {
                                    if (test != null)
                                    {
                                        test.Dismiss();
                                    }

                                    Task.Run(async () => await AddMeetingMarkersToMap());

                                    if (mWakeLock != null)
                                    {
                                        mWakeLock.Release();
                                    }

                                });
                        }
                    });

            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

		public void RunOnOtherThread()
		{
			new Thread(() => 
				{
					Thread.CurrentThread.IsBackground = true; 
					foreach (var item in clientsList)
					{
						if (db.CheckIfExiststClientInDB(item.Partita_iva))
						{
							db.UpdateClientItem(item);
						}
						else
						{
							db.InsertClientsToTable(item);
						}
						AddPraticheToDb(item.Practiche_list);
					}
				}).Start();
		}

        public void AddPraticheToDb(List<ClientPraticheItemInfo> listPratiche)
        {
            try
            {
                if (listPratiche != null)
                {
                    foreach (var itemprat in listPratiche)
                    {
                        if (db.CheckIfClientPraticheExistinDB(itemprat.Id_product))
                        {
                            db.UpdatePraticheInfo(itemprat);
                        }
                        else
                        {
                            db.InsertPraticheToTable(itemprat);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void OnShowAltuofiancoInfo(object sender, EventArgs e)
        {
            try
            {
                LinearLayout lyprat = (LinearLayout)sender;
                var clientTag = ((CustomTagClientInfo)lyprat.Tag).Clientinf;
                ClientInfoItem client = (ClientInfoItem)clientTag;

                if (client.Atf_id_customer != "")
                {
                    hideAllViews();
                    lyAltuofiancoScreen.Visibility = ViewStates.Visible;
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("it-IT");
                    txtAltuofiancoDataValue.Text = client.Data_inserimento.ToString("dd/MM/yyyy", CultureInfo.CurrentCulture);
                    txtAltuofiancoTipoConValue.Text = client.Tipo_contratto;
                    txtAltuofiancoCodiceClValue.Text = client.Codice_cliente;
                    txtAltuofiancoEmailShopValue.Text = client.Email_shop;
                    txtAltuofiancoPuntiDispValue.Text = client.Punti_disponibili.ToString();
                    txtAltuofiancoStatoRidValue.Text = client.Stato_rid;
                    txtAltuofiancoNumeroRefValue.Text = client.Numero_referenze;
                }
                else
                {
                    Toast.MakeText(this, "Nessun contratto ATF", ToastLength.Short).Show();
                }

            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void OnShowPraticheForClient(object sender, EventArgs e)
        {
            try
            {
                LinearLayout lyprat = (LinearLayout)sender;
                var clientTag = ((CustomTagClientInfo)lyprat.Tag).Clientinf;
                ClientInfoItem client = (ClientInfoItem)clientTag;
                if (client.Practiche_list != null && client.Practiche_list.Count > 0)
                { 
                    editTxtPraticheSearch.Text = "";
                    hideAllViews();
                    lyPraticheScreen.Visibility = ViewStates.Visible;
                    praticheAdapter = new PraticheListAdapter(this, client.Practiche_list);
                    listviewPratiche.Adapter = praticheAdapter;
                    praticheAdapter.DetailsClickEvent += (iteminPos) => OnShowDetailsForPratiche(iteminPos);

                    editTxtPraticheSearch.TextChanged += (sender2, e2) =>
                    {
                        ((PraticheListAdapter)listviewPratiche.Adapter).FilterName(editTxtPraticheSearch.Text, client.Practiche_list.ToArray());
                        ((PraticheListAdapter)listviewPratiche.Adapter).NotifyDataSetChanged();
                    };
                }
                else
                {
                    Toast.MakeText(this, "Nessun Pratiche per il cliente", ToastLength.Short).Show();
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void OnShowDetailsForPratiche(ClientPraticheItemInfo client)
        {
            try
            {
                hideAllViews();
                ClientPraticheItemInfo praticItem = client;
                lyPraticheDetailsScreen.Visibility = ViewStates.Visible;

                Thread.CurrentThread.CurrentCulture = new CultureInfo("it-IT");
                txtPraticheDetailsTitle.Text = "DETTAGLIO PRATICA";// + praticItem.Id_product;
                txtPraticheDetailAgente.Text = "AGENTE " + (db.getProfileInfo()).Name + " " + (db.getProfileInfo()).Surname;
                txtPraticheDetailDataInserValue.Text = praticItem.Data_inserimento.ToString("dd/MM/yyyy", CultureInfo.CurrentCulture);
                txtPraticheDetailsDataCreazioneValue.Text = praticItem.Data_creazione.ToString("dd/MM/yyyy", CultureInfo.CurrentCulture);
                txtPraticheDetailsQtaInseritaValue.Text = praticItem.Qta_inserita.ToString();
                txtPraticheDetailsQtaAttivaValue.Text = praticItem.Qta_attiva.ToString();
                txtPraticheDetailsTipoProdottoValue.Text = praticItem.Tipo_prodotto;
                txtPraticheDetailsProdottoValue.Text = praticItem.Prodotto;

                txtPraticheDetailsSerialiValue.Text = praticItem.Seriale;
                if (praticItem.Tipo_prodotto.ToLower() == "fisso")
                {
                    txtPraticheDetailsLineeValue.Text = (praticItem.Linee.Substring(1)).Replace("\n9", "\n");
                }
                else
                {
                    txtPraticheDetailsLineeValue.Text = praticItem.Linee;
                }
                txtPraticheDetailsTipoContrattoValue.Text = praticItem.Tipo_contratto;
                txtPraticheDetailsCanoneValue.Text = praticItem.Canone.ToString();
                if (praticItem.Data_attivazione > new DateTime(1, 1, 1))
                {
                    txtPraticheDetailsDataAttivaValue.Text = praticItem.Data_attivazione.ToString("dd/MM/yyyy", CultureInfo.CurrentCulture);
                }
                else
                {
                    txtPraticheDetailsDataAttivaValue.Text = "";
                }
                txtPraticheDetailsStatoOrdineValue.Text = praticItem.Stato_ordine;
                txtPraticheDetailsStatoPraticaValue.Text = praticItem.Stato_pratica;
                txtPraticheDetailsRoutingValue.Text = praticItem.Routing;
                txtPraticheDetailsNotesValue.Text = praticItem.Notes;

//                CustomTagClientTicketPosition item0 = new CustomTagClientTicketPosition();
//                item0.Client = client;
//                item0.Position = position;
//                lyCreateApriTicket.Tag = item0;
//
//                lyCreateApriTicket.Click -= OnCreateApriTicketClick;
//                lyCreateApriTicket.Click += OnCreateApriTicketClick;
//                lyCreateApriTicket.Click += (sender, e) => OnCreateApriTicketClick(sender, e, client, position);
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void OnCreateApriTicketClick(object sender, EventArgs e)
        {
            try
            {
                LinearLayout lyprat = (LinearLayout)sender;
                var clientTag = ((CustomTagClientTicketPosition)lyprat.Tag).Client;
                ClientInfoItem client = (ClientInfoItem)clientTag;

                var positionTag = ((CustomTagClientTicketPosition)lyprat.Tag).Position;
                int position = (int)positionTag;

                hideAllViews();
                lyNewTicket.Visibility = ViewStates.Visible;
                listTicketTipes = new List<TicketTypteItem>();

                if (Utils.AllTicketTipes != null)
                {
                    JToken ticketTypesJtoken = Utils.AllTicketTipes;
                    foreach (var item in ticketTypesJtoken)
                    {
                        if (item != null)
                        {
                            TicketTypteItem itm = new TicketTypteItem();
                            itm.Id_type_ticket = item["id_type_ticket"].ToString();
                            itm.Type = item["type"].ToString();
                            itm.Description = item["description"].ToString();
                            listTicketTipes.Add(itm);
                        }
                    }
                }

                if (db.CheckIfExistsProfile())
                {
                    ProfileInfoItem profil = db.getProfileInfo();

                    txtNewTicketDestinatoarioValue.Text = profil.Mail;
                }
               
//                txtNewTicketDestinatoarioValue.Text = client.Mail;
                txtNewTicketRegSocialeValue.Text = client.Regione_sociale;
                txtNewTicketPartitaIvaValue.Text = client.Partita_iva;

                txtEditOggetto.Text = "";
                txtEditNewTicketDescription.Text = "";
                txtNewTicketTipoLabel.Text = "";
                    
                CustomTagClientTicketPosition item0 = new CustomTagClientTicketPosition();
                item0.Client = client;
                item0.Position = position;
                lyNewTicketInviaBtn.Tag = item0;

                lyNewTicketInviaBtn.Click -= OnNewTicketCreatClick;
                lyNewTicketInviaBtn.Click += OnNewTicketCreatClick;//(sender2, e2) => OnNewTicketCreatClick(sender2, e2, client, position);
                lyNewTicketTipo.Click -= OnClickTicketType;
                lyNewTicketTipo.Click += OnClickTicketType;
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        void OnClickTicketType(object sender, EventArgs e)
        {
            try
            {
                if (window != null && window.IsShowing == true)
                {
                    window.Dismiss();
                }
                else
                {
                    imgStatusArrow.SetImageResource(Resource.Drawable.whiteArrowDown);

                    LayoutInflater inflater = (LayoutInflater)this.GetSystemService(Context.LayoutInflaterService);
                    View popup = inflater.Inflate(Resource.Layout.DropDownLayout, null);

                    ListView listDropDown = popup.FindViewById<ListView>(Resource.Id.dropDownListView);
                    listDropDown.Adapter = new TicketTypeDropDownAdapter(listTicketTipes, this);
                    listDropDown.ItemClick += (object sender1, AdapterView.ItemClickEventArgs e1) =>
                    {
                        txtNewTicketTipoLabel.Text = (((ListView)sender1).Adapter).GetItem(e1.Position).ToString();
                        window.Dismiss();
                    };

                    window = new PopupWindow(popup, ((LinearLayout)sender).Width, WindowManagerLayoutParams.WrapContent);
                    window.Touchable = true;
                    window.Focusable = true;
                    window.OutsideTouchable = true;
                    window.DismissEvent += (object sender1, EventArgs e1) =>
                    {
                        imgStatusArrow.SetImageResource(Resource.Drawable.whiteArrowDown);
                    };

                    window.SetBackgroundDrawable(new BitmapDrawable());
                    window.ShowAsDropDown((LinearLayout)sender, 0, 0);
                }

            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }


        }

        public void OnNewTicketCreatClick(object sender, EventArgs e)
        {
            try
            {
                LinearLayout lyprat = (LinearLayout)sender;
                var clientTag = ((CustomTagClientTicketPosition)lyprat.Tag).Client;
                ClientInfoItem client = (ClientInfoItem)clientTag;

                var positionTag = ((CustomTagClientTicketPosition)lyprat.Tag).Position;
                int position = (int)positionTag;

                TicketTypteItem itmType = listTicketTipes.FirstOrDefault(x => x.Type.ToLower() == txtNewTicketTipoLabel.Text);

                string tipoTicket = txtNewTicketTipoLabel.Text.Trim();
                string regSocialeTicket = txtNewTicketRegSocialeValue.Text.Trim();
                string partitaIvaTicket = txtNewTicketPartitaIvaValue.Text.Trim();
                string oggetoTicket = txtEditOggetto.Text.Trim();
                string descrTicket = txtEditNewTicketDescription.Text.Trim();

                //if (Utils.CheckForInternetConnection())
                // {
                if (string.IsNullOrEmpty(tipoTicket) && string.IsNullOrEmpty(regSocialeTicket) && string.IsNullOrEmpty(partitaIvaTicket) && string.IsNullOrEmpty(oggetoTicket) && string.IsNullOrEmpty(descrTicket))
                {
                    Utils.displayError("Si prega di inserire campi informazioni", this);
                }
                else if (string.IsNullOrEmpty(tipoTicket))
                {
                    Utils.displayError("Scegli tipo di biglietto", this);
                }
                else if (string.IsNullOrEmpty(partitaIvaTicket))
                {
                    Utils.displayError("Inserisci Partita iva", this);
                }
                else if (string.IsNullOrEmpty(oggetoTicket))
                {
                    Utils.displayError("Inserisci Oggeto", this);
                }
                else if (string.IsNullOrEmpty(descrTicket))
                {
                    Utils.displayError("Inserisci Descrizione", this);
                }

                if (tipoTicket != "" && regSocialeTicket != "" && partitaIvaTicket != "" && oggetoTicket != "" && descrTicket != "")
                {
                    test.Show(this, "Attendere prego", -1, MaskType.Clear, TimeSpan.FromSeconds(120), null, true, null); 
                    if (mWakeLock != null)
                    {
                        mWakeLock.Acquire();
                    }

                    System.Threading.ThreadPool.QueueUserWorkItem(state =>
                        {   
                            try
                            {
                                if (Utils.CheckForInternetConn())
                                {
                                    bool success = ApiCalls.postTicket(Utils.UserToken, client.Mail, itmType.Id_type_ticket, client.Customer_id, client.Practiche_list[position].Id_product, txtEditOggetto.Text, txtEditNewTicketDescription.Text);
                                    RunOnUiThread(() =>
                                        {

                                            if (success)
                                            {
                                                Toast.MakeText(this, "Si crea con successo nuovo ticket", ToastLength.Short).Show();
                                                OnBackPressed();
                                            }
                                            else
                                            {
                                                Toast.MakeText(this, "Impossibile creare nuovo tickett", ToastLength.Short).Show();
                                            }
                                        });
                                }
                                else
                                {
                                    RunOnUiThread(() =>
                                        {
                                            AndHUD.Shared.ShowError(this, "Nessuna connessione Internet", MaskType.Black, TimeSpan.FromSeconds(2));
                                        });
                                }


                            }
                            catch (Exception ex)
                            {
                                Utils.writeToDeviceLog(ex);
                            }
                            finally
                            {
                                RunOnUiThread(() =>
                                    {
                                        if (test != null)
                                        {
                                            test.Dismiss();
                                        }

                                        if (mWakeLock != null)
                                        {
                                            mWakeLock.Release();
                                        }
                                    });
                            }
                        });
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        // ChoiceListFragment fragment;

        public void OnShowAllMemosForClient(object sender, EventArgs e)
        {
            try
            {
                LinearLayout lyprat = (LinearLayout)sender;
                var clientTag = ((CustomTagClientInfo)lyprat.Tag).Clientinf;
                ClientInfoItem client = (ClientInfoItem)clientTag;

                hideAllViews();
                layFIlter.Visibility = ViewStates.Visible;
                laySearch.Visibility = ViewStates.Visible;
                lyClientiMemoScreen.Visibility = ViewStates.Visible;
                Thread.CurrentThread.CurrentCulture = new CultureInfo("it-IT");
                txtMemoDataValue.Text = DateTime.Now.Date.ToString("dd/MM/yyyy", CultureInfo.CurrentCulture);
                txtMemoOraValue.Text = DateTime.Now.AddHours(1).ToString("HH:mm", CultureInfo.InvariantCulture);
                txtEditMemoTitle.Text = "";
                txtEditMemoDescription.Text = "";
                txtEditMemoPlace.Text = "";
                posMemoList = -1;
                //txtMemoOraValue.Text=DateTime.Now.Hour.ToString()+":"+DateTime.Now.Minute.ToString("00");
//                clientMemo = new List<ClientiMemoItem>();


                test.Show(this, "Attendere prego", -1, MaskType.Clear, TimeSpan.FromSeconds(120), null, true, null); 
                if (mWakeLock != null)
                {
                    mWakeLock.Acquire();
                }

                ThreadPool.QueueUserWorkItem(state =>
                    {
                        try
                        {
                            GetAllMemos(client.Customer_id);

                            clientMemo = clientMemo.OrderByDescending(x => x.Date).ToList();
                            RunOnUiThread(() =>
                                {
                                    adapterClenti = new ClientiMemoAdapter(this, clientMemo,0);
                                    listviewMemo.Adapter = adapterClenti;
                                    if (clientMemo != null && clientMemo.Count > 0)
                                        listviewMemo.PerformItemClick(listviewMemo.GetChildAt(0), 0, listviewMemo.FirstVisiblePosition);
                                });
                        }
                        catch (Exception ex)
                        {
                            Utils.writeToDeviceLog(ex);
                        }
                        finally
                        {
                            RunOnUiThread(() =>
                                {
                                    if (test != null)
                                    {
                                        test.Dismiss();
                                    }

                                    if (mWakeLock != null)
                                    {
                                        mWakeLock.Release();
                                    }
                                });
                        }
                    });

            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void GetAllMemos(string Customer_id)
        {
            try
            {
                clientMemo = new List<ClientiMemoItem>();
                if (Utils.CheckForInternetConn())
                {
                    JToken memoTokenClient = ApiCalls.getAllMemos(Utils.UserToken, Customer_id);
                    foreach (var item in memoTokenClient)
                    {
                        ClientiMemoItem item1 = new ClientiMemoItem();
                        item1.Id = item["id_appuntamento"].ToString();
                        item1.Title = item["evento_pers"].ToString();
                        item1.Details = item["note"].ToString();
                        item1.Place = item["luogo_pers"].ToString();
                        try
                        {
                            DateTime dataFromMemo;
                            DateTime.TryParseExact(item["data"].ToString() + " " + item["ora"].ToString(), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out dataFromMemo);

                            item1.Date = dataFromMemo;
                        }
                        catch (Exception ex)
                        {
                            Utils.writeToDeviceLog(ex);
                        }
                        clientMemo.Add(item1);
                    }
                }
                else
                {
                    using (var streamReader = new StreamReader(Consts.pathJsonClientMemo))
                    {
                        string result = streamReader.ReadToEnd();

                        JObject memoReader = JObject.Parse(result);

                        if (memoReader["id_client"] != null)
                        {
                            foreach (var item in memoReader["id_client"])
                            {
                                ClientiMemoItem item1 = new ClientiMemoItem();
                                item1.Id = item["id"].ToString();
                                item1.Title = item["title"].ToString();
                                item1.Details = item["description"].ToString();
                                item1.Date = DateTime.Parse(item["date"].ToString() + " " + item["hour"].ToString(), CultureInfo.InvariantCulture);
                                clientMemo.Add(item1);

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public override void OnBackPressed()
        {
            try
            {
                if (lyClientiMemoScreen.Visibility == ViewStates.Visible)
                {
                    lyClientiMemoScreen.Visibility = ViewStates.Gone;
                    lyClientiContent.Visibility = ViewStates.Visible;
                    _map.UiSettings.ScrollGesturesEnabled = true;
                    _map.UiSettings.ZoomGesturesEnabled = true;
                }
                else if (lyPraticheScreen.Visibility == ViewStates.Visible)
                {
                    hideAllViews();
                    lyClientiContent.Visibility = ViewStates.Visible;
                    layFIlter.Visibility = ViewStates.Visible;
                    laySearch.Visibility = ViewStates.Visible;
                }
                else if (lyPraticheDetailsScreen.Visibility == ViewStates.Visible)
                {
                    hideAllViews();
                    lyPraticheScreen.Visibility = ViewStates.Visible;
                }
                else if (lyNewTicket.Visibility == ViewStates.Visible)
                {
                    hideAllViews();
                    lyPraticheDetailsScreen.Visibility = ViewStates.Visible;
                }
                else
                {
                    this.Finish();
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void OnBackMenuClick(object sender, EventArgs e)
        {
            try
            {
				if (lyClientiMemoScreen.Visibility == ViewStates.Visible)
				{
					lyClientiMemoScreen.Visibility = ViewStates.Gone;
					lyClientiContent.Visibility = ViewStates.Visible;
					_map.UiSettings.ScrollGesturesEnabled = true;
					_map.UiSettings.ZoomGesturesEnabled = true;
				}
				else if (lyClientiContent.Visibility == ViewStates.Visible)
				{
					lyClientiContent.Visibility = ViewStates.Gone;
					_map.UiSettings.ScrollGesturesEnabled = true;
					_map.UiSettings.ZoomGesturesEnabled = true;
				}
				else
				{
					this.Finish();
				}
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void OnBackFromAltuofiancoClick(object sender, EventArgs e)
        {
            try
            {
                this.Finish();
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void OnBackFromAltuofiancoClick1(object sender, EventArgs e)
        {
            try
            {
                hideAllViews();
                lyClientiContent.Visibility = ViewStates.Visible;
                layFIlter.Visibility = ViewStates.Visible;
                laySearch.Visibility = ViewStates.Visible;
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void OnBackFromPraticheClick(object sender, EventArgs e)
        {
            try
            {
                hideAllViews();
                lyClientiContent.Visibility = ViewStates.Visible;
                layFIlter.Visibility = ViewStates.Visible;
                laySearch.Visibility = ViewStates.Visible;
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void OnBackFromPraticheDetailsClick(object sender, EventArgs e)
        {
            try
            {
                hideAllViews();
                lyPraticheScreen.Visibility = ViewStates.Visible;
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void OnBackFromNewTicket(object sender, EventArgs e)
        {
            try
            {
                hideAllViews();
                lyPraticheDetailsScreen.Visibility = ViewStates.Visible;
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void ShowCalendarPicker(object sender, View.TouchEventArgs e)
        {
            try
            {
//                fragment = new ChoiceListFragment ();
//
//                SupportFragmentManager.BeginTransaction ()
//                    .Replace (Resource.Id.content, fragment)
//                    .Commit ();

                if (e.Event.Action == MotionEventActions.Down)
                {
                    linearMemoDataValue.SetBackgroundColor(Resources.GetColor(Resource.Color.orangedark));
                }
                if (e.Event.Action == MotionEventActions.Cancel)
                {
                    linearMemoDataValue.SetBackgroundColor(Resources.GetColor(Resource.Color.trans));
                }
                if (e.Event.Action == MotionEventActions.Up)
                {
                    linearMemoDataValue.SetBackgroundColor(Resources.GetColor(Resource.Color.trans));
                    OnClickPickDate(txtMemoDataValue);
                }

            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void ShowHourPicker(object sender, View.TouchEventArgs e)
        {
            try
            {
                if (e.Event.Action == MotionEventActions.Down)
                {
                    linearMemoHourValue.SetBackgroundColor(Resources.GetColor(Resource.Color.orangedark));
                }
                if (e.Event.Action == MotionEventActions.Cancel)
                {
                    linearMemoHourValue.SetBackgroundColor(Resources.GetColor(Resource.Color.trans));
                }
                if (e.Event.Action == MotionEventActions.Up)
                {
                    linearMemoHourValue.SetBackgroundColor(Resources.GetColor(Resource.Color.trans));
                    OnClickPickHour(txtMemoOraValue);
                }

            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        void OnClickPickDate(TextView label)
        {

            try
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                AlertDialog builder = alertDialogBuilder.Create();
                View view = this.LayoutInflater.Inflate(Resource.Layout.DateAndTimePicker, null);
                builder.SetView(view, 0, 0, 0, 0);

                DatePicker datePicker = view.FindViewById<DatePicker>(Resource.Id.orderDatePicker);
//                datePicker.DescendantFocusability= DescendantFocusability.BlockDescendants;
                LinearLayout test = (LinearLayout)datePicker.GetChildAt(0);
                CalendarView test2 = (CalendarView)test.GetChildAt(1);
                LinearLayout untest = (LinearLayout)test2.GetChildAt(0);
                untest.DescendantFocusability = DescendantFocusability.BlockDescendants;
                TextView txtMonth = (TextView)untest.GetChildAt(0);

//                    test3.TextChanged += (object sender, Android.Text.TextChangedEventArgs e) =>
//                    {
//                        ((TextView)test3).SetSelectAllOnFocus(true);
//                    };
//                    test3.EditorAction += (object sender, TextView.EditorActionEventArgs e) =>
//                    {
//                        ((TextView)test3).SetSelectAllOnFocus(true);
//                    };
//                test3.Click+= (object sender, EventArgs e) => 
//                    {
//                        ((TextView)test3).SetSelectAllOnFocus(true);
//                    };
//                test3.FocusableInTouchMode=false;
                test2.ClearChildFocus(txtMonth);
                txtMonth.Focusable = false;
                txtMonth.Text = "Dec";
                txtMonth.InputType = Android.Text.InputTypes.Null;
                txtMonth.EditorAction += (object sender, TextView.EditorActionEventArgs e) =>
                {
                    Console.WriteLine("ANA are mere");
                };
                Android.Views.InputMethods.InputMethodManager imm = (Android.Views.InputMethods.InputMethodManager)GetSystemService(Context.InputMethodService);
                imm.HideSoftInputFromWindow(txtMonth.WindowToken, Android.Views.InputMethods.HideSoftInputFlags.NotAlways);
//                test3.Clickable=false;
//                test3.LongClickable=false;
//                test3.KeyListener=null;
//                test3.Enabled=false;
//                test3.InputType= Android.Text.InputTypes.Null;
//                ClassTextViewExtend asd=new ClassTextViewExtend();
//                test3.SetOnEditorActionListener(asd);
                
                EditText textDummy = view.FindViewById<EditText>(Resource.Id.textDummy);
                Button btn_OK = view.FindViewById<Button>(Resource.Id.step1DateTimeBtnOK);
                Button btn_Cancel = view.FindViewById<Button>(Resource.Id.step1DateTimeBtnCancel);

                btn_OK.Touch += (object sender2, View.TouchEventArgs e2) =>
                {
                    try
                    {
                        Button b = (Button)sender2;
                        if (e2.Event.Action == MotionEventActions.Down)
                        {
                            b.Background.SetColorFilter(new Color(100, 100, 100), PorterDuff.Mode.Multiply);
                        }
                        if (e2.Event.Action == MotionEventActions.Cancel)
                        {
                            b.Background.ClearColorFilter();
                        }
                        if (e2.Event.Action == MotionEventActions.Up)
                        {
                            b.Background.ClearColorFilter();
                            textDummy.RequestFocus();
                            Thread.CurrentThread.CurrentCulture = new CultureInfo("it-IT");
                            label.Text = datePicker.DateTime.ToString("dd/MM/yyyy", CultureInfo.CurrentCulture);

                            builder.Dismiss();


                        }
                    }
                    catch (Exception ex)
                    {
                        Utils.writeToDeviceLog(ex);
                    }
                };

                btn_Cancel.Touch += (object sender1, View.TouchEventArgs e1) =>
                {
                    try
                    {
                        Button b = (Button)sender1;
                        if (e1.Event.Action == MotionEventActions.Down)
                        {
                            b.Background.SetColorFilter(new Color(100, 100, 100), PorterDuff.Mode.Multiply);
                        }
                        if (e1.Event.Action == MotionEventActions.Cancel)
                        {
                            b.Background.ClearColorFilter();
                        }
                        if (e1.Event.Action == MotionEventActions.Up)
                        {
                            b.Background.ClearColorFilter();
                            builder.Dismiss();
                        }
                    }
                    catch (Exception ex)
                    {
                        Utils.writeToDeviceLog(ex);
                    }
                };

                builder.Show();
                builder.Window.SetLayout(RelativeLayout.LayoutParams.MatchParent, RelativeLayout.LayoutParams.WrapContent);


            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }


        }

        void OnClickPickHour(TextView label)
        {
            try
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                AlertDialog builder = alertDialogBuilder.Create();
                View view = this.LayoutInflater.Inflate(Resource.Layout.DateAndTimePicker, null);
                builder.SetView(view, 0, 0, 0, 0);

                TimePicker timePicker = view.FindViewById<TimePicker>(Resource.Id.orderTimePicker);
                DatePicker datePicker = view.FindViewById<DatePicker>(Resource.Id.orderDatePicker);
                EditText textDummy = view.FindViewById<EditText>(Resource.Id.textDummy);
                datePicker.Visibility = ViewStates.Gone;
                timePicker.Visibility = ViewStates.Visible;
				timePicker.SetIs24HourView(Java.Lang.Boolean.True);

				DateTime pickerHour;
				DateTime.TryParseExact(label.Text, "HH:mm", CultureInfo.CurrentCulture, DateTimeStyles.None, out pickerHour);

				timePicker.CurrentHour = new Java.Lang.Integer(pickerHour.Hour);
				timePicker.CurrentMinute = new Java.Lang.Integer(pickerHour.Minute);
                Button btn_OK = view.FindViewById<Button>(Resource.Id.step1DateTimeBtnOK);
                Button btn_Cancel = view.FindViewById<Button>(Resource.Id.step1DateTimeBtnCancel);

                btn_OK.Touch += (object sender2, View.TouchEventArgs e2) =>
                {
                    try
                    {
                        Button b = (Button)sender2;
                        if (e2.Event.Action == MotionEventActions.Down)
                        {
                            b.Background.SetColorFilter(new Color(100, 100, 100), PorterDuff.Mode.Multiply);
                        }
                        if (e2.Event.Action == MotionEventActions.Cancel)
                        {
                            b.Background.ClearColorFilter();
                        }
                        if (e2.Event.Action == MotionEventActions.Up)
                        {
                            b.Background.ClearColorFilter();
                            textDummy.RequestFocus();
                            Thread.CurrentThread.CurrentCulture = new CultureInfo("it-IT");
							label.Text = timePicker.CurrentHour.ToString().PadLeft(2, '0') + ":" + timePicker.CurrentMinute.ToString().PadLeft(2, '0');
                            builder.Dismiss();
                        }
                    }
                    catch (Exception ex)
                    {
                        Utils.writeToDeviceLog(ex);
                    }
                };

                btn_Cancel.Touch += (object sender1, View.TouchEventArgs e1) =>
                {
                    try
                    {
                        Button b = (Button)sender1;
                        if (e1.Event.Action == MotionEventActions.Down)
                        {
                            b.Background.SetColorFilter(new Color(100, 100, 100), PorterDuff.Mode.Multiply);
                        }
                        if (e1.Event.Action == MotionEventActions.Cancel)
                        {
                            b.Background.ClearColorFilter();
                        }
                        if (e1.Event.Action == MotionEventActions.Up)
                        {
                            b.Background.ClearColorFilter();
                            builder.Dismiss();
                        }
                    }
                    catch (Exception ex)
                    {
                        Utils.writeToDeviceLog(ex);
                    }
                };

                builder.Show();
                builder.Window.SetLayout(RelativeLayout.LayoutParams.MatchParent, RelativeLayout.LayoutParams.WrapContent);


            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }


        }

        public void hideAllViews()
        {
            try
            {
                lyNewTicket.Visibility = ViewStates.Gone;
                lyPraticheDetailsScreen.Visibility = ViewStates.Gone;
                lyPraticheScreen.Visibility = ViewStates.Gone;
                lyAltuofiancoScreen.Visibility = ViewStates.Gone;
                lyClientiContent.Visibility = ViewStates.Gone;
                layFIlter.Visibility = ViewStates.Gone;
                laySearch.Visibility = ViewStates.Gone;
                lyClientiMemoScreen.Visibility = ViewStates.Gone;

                _map.UiSettings.ScrollGesturesEnabled = false;
                _map.UiSettings.ZoomGesturesEnabled = false;
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        void OnClickCallBut(object sender, EventArgs e)
        {
            try
            {
                LinearLayout lyprat = (LinearLayout)sender;
                var clientTag = ((CustomTagClientInfo)lyprat.Tag).Clientinf;
                ClientInfoItem client = (ClientInfoItem)clientTag;

                if (client.Phone != "")
                {
                    Intent telIntent = new Intent(Intent.ActionCall);
                    telIntent.SetData(Android.Net.Uri.Parse("tel:" + client.Phone));                                                                             
                    StartActivity(telIntent);
                }
                else
                {
                    AndHUD.Shared.ShowError(this, "Nessun numero di telefono presente ", MaskType.Black, TimeSpan.FromSeconds(2));
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        void MemoPlusLay_Click(object sender, EventArgs e)
        {
            try
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("it-IT");
                txtMemoDataValue.Text = DateTime.Now.Date.ToString("dd/MM/yyyy", CultureInfo.CurrentCulture);
                txtMemoOraValue.Text = DateTime.Now.AddHours(1).ToString("HH:mm", CultureInfo.InvariantCulture);
                txtEditMemoTitle.Text = "";
                txtEditMemoDescription.Text = "";
                txtEditMemoPlace.Text = "";
                posMemoList = -1;
                adapterClenti = new ClientiMemoAdapter(this, clientMemo,posMemoList);
                listviewMemo.Adapter = adapterClenti;
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        void OnClickAddMemo(object sender, EventArgs e)
        {
            try
            {
                LinearLayout lyprat = (LinearLayout)sender;
                var clientTag = ((CustomTagClientInfo)lyprat.Tag).Clientinf;
                ClientInfoItem client = (ClientInfoItem)clientTag;

                string txtEditMemoTitleText = txtEditMemoTitle.Text;
                string txtEditMemoDescriptionText = txtEditMemoDescription.Text;
                string txtMemoDataValueText = txtMemoDataValue.Text;
                string txtMemoOraValueText = txtMemoOraValue.Text;
                string txteditMemoPlaceText = txtEditMemoPlace.Text;

                DateTime dataMemo;
                Thread.CurrentThread.CurrentCulture = new CultureInfo("it-IT");
                DateTime.TryParseExact(txtMemoDataValueText + " " + txtMemoOraValueText, "dd/MM/yyyy HH:mm", CultureInfo.CurrentCulture, DateTimeStyles.None, out dataMemo);

                if (!string.IsNullOrEmpty(txtEditMemoTitleText))
                {
                    if (dataMemo >= DateTime.Now)
                    {
                        test.Show(this, "Attendere prego", -1, MaskType.Clear, TimeSpan.FromSeconds(120), null, true, null); 
                        if (mWakeLock != null)
                        {
                            mWakeLock.Acquire();
                        }

                        //txtEditMemoTitle.Hint = null;

                        ThreadPool.QueueUserWorkItem(state =>
                            {
                                try
                                {
                                    if (posMemoList > -1)
                                    {
                                        //update memo
                                        clientMemo[posMemoList].Title = txtEditMemoTitleText;
                                        clientMemo[posMemoList].Details = txtEditMemoDescriptionText;

                                        clientMemo[posMemoList].Date = dataMemo;
                                        clientMemo[posMemoList].Place = txteditMemoPlaceText;
//                                        posMemoList = -1;
                                        string data =  clientMemo[posMemoList].Date.ToString("yyyy-MM-dd", CultureInfo.CurrentCulture);
                                        string hour =  clientMemo[posMemoList].Date.ToString("HH:mm:ss", CultureInfo.CurrentCulture);
                                        bool rasp= ApiCalls.updateAppuntamenti(Utils.UserToken,clientMemo[posMemoList].Id,"",clientMemo[posMemoList].Details,"monitor_demo_app",data,hour,clientMemo[posMemoList].Place,txtEditMemoTitleText, true);
                                        RunOnUiThread(() =>
                                            {
                                                if (rasp)
                                                {
                                                    Toast.MakeText(this, "Memo aggiornato con successo", ToastLength.Long).Show();
                                                    clientMemo = clientMemo.OrderByDescending(x => x.Date).ToList();
                                                    adapterClenti = new ClientiMemoAdapter(this, clientMemo,posMemoList);
                                                    listviewMemo.Adapter = adapterClenti;
                                                }
                                                else
                                                {
                                                    Toast.MakeText(this, "Impossibile aggiornare le informazioni dell’memo", ToastLength.Long).Show();
                                                }
                                            });
                                    }
                                    else
                                    {
                                        ClientiMemoItem item1 = new ClientiMemoItem();
                                        item1.Id = client.Customer_id;
                                        item1.Title = txtEditMemoTitleText;
                                        item1.Details = txtEditMemoDescriptionText;
                                        item1.Place = txteditMemoPlaceText;

                                        item1.Date = dataMemo;

                                        if (Utils.CheckForInternetConn())
                                        { 
                                            //post memo to server
                                            bool success = ApiCalls.sendMemo(Utils.UserToken, txtEditMemoTitleText, dataMemo.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture), dataMemo.ToString("HH:mm", CultureInfo.InvariantCulture), txtEditMemoDescriptionText, client.Customer_id, txteditMemoPlaceText);
                                            RunOnUiThread(() =>
                                                {
                                                    if (success)
                                                    {
//                                                        clientMemo.Add(item1);
                                                        GetAllMemos(client.Customer_id);

                                                        RunOnUiThread(() =>
                                                            {
                                                                clientMemo = clientMemo.OrderByDescending(x => x.Date).ToList();
                                                                adapterClenti = new ClientiMemoAdapter(this, clientMemo,0);
                                                                listviewMemo.Adapter = adapterClenti;

                                                                using (var streamWriter = new StreamWriter(Consts.pathJsonClientMemo))
                                                                {
                                                                    var i = 0;
                                                                    string json = "{ \"id_client\" :  [";
                                                                    foreach (var itemMemo in clientMemo)
                                                                    {
                                                                        i++;
                                                                        json += "{\"id\": \" " + itemMemo.Id
                                                                        + "\", \"title\": \"" + itemMemo.Title
                                                                        + "\", \"date\": \" " + itemMemo.Date.ToString("dd/MM/yyyy", CultureInfo.CurrentCulture)
                                                                        + "\", \"hour\": \"" + itemMemo.Date.ToString("HH:mm", CultureInfo.CurrentCulture)
                                                                        + "\", \"description\": \"" + itemMemo.Details
                                                                        + "\"}";
                                                                        if (i != clientMemo.Count)
                                                                            json += ",";
                                                                    }
                                                                    json += "]}";
                                                                    streamWriter.Write(json);
                                                                    streamWriter.Flush();
                                                                    streamWriter.Close();

                                                                    using (var streamReader = new StreamReader(Consts.pathJsonClientMemo))
                                                                    {
                                                                        string result = streamReader.ReadToEnd();

                                                                        JObject memoReader = JObject.Parse(result);

                                                                        txtEditMemoTitle.Text = "";
                                                                        txtEditMemoDescription.Text = "";
                                                                        Thread.CurrentThread.CurrentCulture = new CultureInfo("it-IT");
                                                                        txtMemoDataValue.Text = DateTime.Now.Date.ToString("dd/MM/yyyy", CultureInfo.CurrentCulture);
                                                                        txtMemoOraValue.Text = DateTime.Now.AddHours(1).ToString("HH:mm", CultureInfo.InvariantCulture);
                                                                        txtEditMemoPlace.Text = "";
                                                                        posMemoList = -1;
                                                                    }  
                                                                    if (clientMemo != null && clientMemo.Count > 0)
                                                                        listviewMemo.PerformItemClick(listviewMemo.GetChildAt(0), 0, listviewMemo.FirstVisiblePosition);
                                                                }
                                                            });
                                                        
                                                        Toast.MakeText(this, "Memo aggiunto con successo", ToastLength.Short).Show();
                                                    }
                                                    else
                                                    {
                                                        Toast.MakeText(this, "Errore durante la creazione di memo", ToastLength.Short).Show();
                                                    }
                                                });
                                        }
                                        else
                                        {
                                            RunOnUiThread(() =>
                                                {
                                                    Toast.MakeText(this, "Nessuna connessione Internet", ToastLength.Short).Show();
                                                });
                                        }                           
                                    }     
                                }
                                catch (Exception ex)
                                {
                                    Utils.writeToDeviceLog(ex);
                                }
                                finally
                                {
                                    RunOnUiThread(() =>
                                        {
                                            if (test != null)
                                            {
                                                test.Dismiss();
                                            }

                                            if (mWakeLock != null)
                                            {
                                                mWakeLock.Release();
                                            }
                                        });
                                }
                            });
                    }
                    else
                    {
                        //data din memo trebuie sa fie mai mare sau egala cu data curenta
                        //TODO tradus
                        Toast.MakeText(this, "Inserire una data superiore a quella odierna", ToastLength.Long).Show();
                    }
                }
                else
                {
                    txtEditMemoTitle.Hint = "Inserire il titolo!";
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        void OnClickEmailBut(object sender, EventArgs e)
        {
            try
            {
                LinearLayout lyprat = (LinearLayout)sender;
                var clientTag = ((CustomTagClientInfo)lyprat.Tag).Clientinf;
                ClientInfoItem client = (ClientInfoItem)clientTag;

                if (client.Mail != "")
                {
                    Intent emailIntent = new Intent(Intent.ActionSend); 
                    emailIntent.SetType("plain/text");
                    emailIntent.PutExtra(Intent.ExtraEmail, new string[]{ client.Mail });   
                    StartActivity(Intent.CreateChooser(emailIntent, "Seleziona mail:"));
                }
                else
                {
                    AndHUD.Shared.ShowError(this, "Nessun indirizzo mail presente", MaskType.Black, TimeSpan.FromSeconds(2));
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            try
            {
                GC.Collect();

                layFIlter.Click -= SelectFilter;
                boxMenuBack.Click -= OnBackMenuClick;
                lyaltuofiancoBottomBack.Click -= OnBackFromAltuofiancoClick1;
                lyaltuofiancoMenuBack.Click -= OnBackFromAltuofiancoClick1;
                lyBackFomPratiche.Click -= OnBackFromPraticheClick;
                lyBackFomPraticheDetails.Click -= OnBackFromPraticheDetailsClick;
                lyCreateApriTicket.Click -= (sender, e) => OnCreateApriTicketClick(sender, e);
                lyBackFomNewTicket.Click -= OnBackFromNewTicket;

                lyAltuofianco.Click -= OnShowAltuofiancoInfo;
                lyPratiche.Click -= (sender, e) => OnShowPraticheForClient(sender, e);
                lyCLientiMemo.Click -= OnShowAllMemosForClient;

                if (_map != null)
                {
                    _map.Dispose();
                    if (_mapFragment != null)
                    {
                        _mapFragment.Dispose();
                    }
                    if (_mapFragmentSupport != null)
                    {
                        _mapFragmentSupport.Dispose();
                    }
                }

                if (clientiLayoutBg != null)
                {
                    unbindDrawables(clientiLayoutBg);
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        protected override void OnStop()
        {
            base.OnStop();
            try
            {
                GC.Collect();
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }

        }

        private void unbindDrawables(View rootView)
        {
            try
            {
                if (rootView.Background != null)
                {
                    rootView.Background.SetCallback(null);
                }
                if (rootView is ViewGroup)
                {
                    for (int i = 0; i < ((ViewGroup)rootView).ChildCount; i++)
                    {
                        unbindDrawables(((ViewGroup)rootView).GetChildAt(i));

                        ((IDisposable)((ViewGroup)rootView).GetChildAt(i)).Dispose();
                    }

                    try
                    {
                        ((ViewGroup)rootView).RemoveAllViews();
                    }
                    catch (Java.Lang.UnsupportedOperationException mayHappen)
                    {
                        // AdapterViews, ListViews and potentially other ViewGroups don抰 support the removeAllViews operation
                        //Utils.writeToDeviceLog (mayHappen);
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }


        void OnClcikItemMemoList(object sender, AdapterView.ItemClickEventArgs e)
        {
            try
            {
//                txtEditMemoTitle.Hint = null;
                int pos = e.Position;
                txtEditMemoTitle.Text = clientMemo[pos].Title; 
                txtEditMemoDescription.Text = clientMemo[pos].Details;
                Thread.CurrentThread.CurrentCulture = new CultureInfo("it-IT");
                txtMemoDataValue.Text = clientMemo[pos].Date.Date.ToString("dd/MM/yyyy", CultureInfo.CurrentCulture);
                txtMemoOraValue.Text = clientMemo[pos].Date.Hour.ToString("00") + ":" + clientMemo[pos].Date.Minute.ToString("00");
                txtEditMemoPlace.Text = clientMemo[pos].Place;
                posMemoList = pos;
//                adapterClenti.NotifyDataSetChanged();
                adapterClenti = new ClientiMemoAdapter(this, clientMemo,posMemoList);
                listviewMemo.Adapter = adapterClenti;

            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        bool setLocation = false;

        void Android.Gms.Location.ILocationListener.OnLocationChanged(Location location)
        {
            try
            {/*
                Console.WriteLine("--------------getLocation---------");
                Console.WriteLine("--------------getLocation---------" + location.Latitude.ToString());
                _currentLocation = location;
                found = true;
                SetupMapIfNeeded();
                LoadALlClients();

                Log.Debug("LocationClient", "Location updated");
                */
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        void GoogleApiClient.IConnectionCallbacks.OnConnected(Bundle connectionHint)
        {
            try
            {
                if (apiClient.IsConnected)
                {
                    Location location = LocationServices.FusedLocationApi.GetLastLocation(apiClient);
                    if (location != null)
                    {
                        _currentLocation = location;
                        found = true;
                        SetupMapIfNeeded();
                        if (fromAppuntamentiMap)
                        {
                            if (ragioneSocialeClient != null && ragioneSocialeClient != "")
                            {
                                LoadALlClients(true, "regione_sociale", ragioneSocialeClient);  
                            }
                            else if (customerid_from_appuntamenti != null && customerid_from_appuntamenti != "")
                            {
                                LoadALlClients(true, "id_customer", customerid_from_appuntamenti);  
                            }

                        }
                        else
                        {
                            LoadALlClients(false, "", "");
                        }

                        Log.Debug("LocationClient", "Last location printed----" + location.Latitude.ToString() + "-----" + location.Longitude.ToString());
                    }
                    else
                    {
                        if (test != null)
                        {
                            test.Dismiss();
                            Toast.MakeText(this, "Impossibile ottenere posizione", ToastLength.Short).Show();
                        }

                        if (fromAppuntamentiMap)
                        {
                            if (ragioneSocialeClient != null && ragioneSocialeClient != "")
                            {
                                LoadALlClients(true, "regione_sociale", ragioneSocialeClient);  
                            }
                            else if (customerid_from_appuntamenti != null && customerid_from_appuntamenti != "")
                            {
                                LoadALlClients(true, "id_customer", customerid_from_appuntamenti);  
                            }
                        }
                        else
                        {
                            LoadALlClients(false, "", "");
                        }
                    }
//                    LoadALlClients();
                }
                Log.Info("LocationClient", "Now connected to client");
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        void GoogleApiClient.IConnectionCallbacks.OnConnectionSuspended(int cause)
        {
            try
            {
                test.Dismiss();
                Log.Info("LocationClient", "Now disconnected from client");
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        void GoogleApiClient.IOnConnectionFailedListener.OnConnectionFailed(Android.Gms.Common.ConnectionResult result)
        {
            try
            {
                test.Dismiss();
                Log.Info("LocationClient", "Connection failed, attempting to reach google play services");
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        bool IsGooglePlayServicesInstalled()
        {
            int queryResult = GoogleApiAvailability.Instance.IsGooglePlayServicesAvailable(this);
            if (queryResult == ConnectionResult.Success)
            {
                Log.Info("MainActivity", "Google Play Services is installed on this device.");
                return true;
            }

            if (GoogleApiAvailability.Instance.IsUserResolvableError(queryResult))
            {
                string errorString = GoogleApiAvailability.Instance.GetErrorString(queryResult);
                Log.Error("ManActivity", "There is a problem with Google Play Services on this device: {0} - {1}", queryResult, errorString);

                // Show error dialog to let user debug google play services
            }
            return false;
        }
    }

    public class CustomTagClientInfo:Java.Lang.Object
    {
        public ClientInfoItem clientinf;

        public ClientInfoItem Clientinf
        {
            get
            {
                return clientinf;
            }
            set
            {
                clientinf = value;
            }
        }
    }

    public class CustomTagClientTicketPosition:Java.Lang.Object
    {
        public ClientInfoItem client;
        public int position;

        public int Position
        {
            get
            {
                return position;
            }
            set
            {
                position = value;
            }
        }

        public ClientInfoItem Client
        {
            get
            {
                return client;
            }
            set
            {
                client = value;
            }
        }
    }
}

