using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using RewindShared;

namespace RewindAndroid
{
    [Activity(Label = "NotificheActivity")]			
	public class NotificheActivity : BaseActivity
    {
        ListView listNotifiche;
        NotificheTimeAdapter timeAdapter;
        List<NotificheItem> itemsMessage;
        TextView txtTitleMessage;
        TextView txtMessage;
        Button butSeen;
        Typeface OswaldRegualr;
        Typeface OswaldLight;
        LinearLayout notificheLayoutBg;
        LinearLayout boxMenuBack;
        TextView txtNotCommingSoon;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.NotificheLayout);
            try
            {
                this.Window.SetFlags(WindowManagerFlags.KeepScreenOn, WindowManagerFlags.KeepScreenOn);

                listNotifiche = FindViewById<ListView>(Resource.Id.listNotifiche);
                txtTitleMessage = FindViewById<TextView>(Resource.Id.txtTitleMessage);
                txtMessage = FindViewById<TextView>(Resource.Id.txtMessage);
                TextView txtTitle = FindViewById<TextView>(Resource.Id.txtTitle);
                boxMenuBack = FindViewById<LinearLayout>(Resource.Id.boxMenuBack);
                butSeen = FindViewById<Button>(Resource.Id.butSeen);
                notificheLayoutBg = FindViewById<LinearLayout>(Resource.Id.notificheLayoutBg);
                txtNotCommingSoon=FindViewById<TextView>(Resource.Id.textView1);
                //set fonds
                OswaldRegualr = TypeFaces.getTypeface(this, "Oswald-Regular.ttf");
                OswaldLight = TypeFaces.getTypeface(this, "Oswald-Light.ttf");
                txtTitle.SetTypeface(OswaldRegualr, TypefaceStyle.Bold);
                txtTitleMessage.SetTypeface(OswaldRegualr, TypefaceStyle.Bold);
                txtMessage.SetTypeface(OswaldRegualr, TypefaceStyle.Bold);
                butSeen.SetTypeface(OswaldRegualr, TypefaceStyle.Bold);
                txtNotCommingSoon.SetTypeface(OswaldLight, TypefaceStyle.Normal);

                itemsMessage = new List<NotificheItem>();

                for (int i = 0; i < 5; i++)
                {
                    NotificheItem item1 = new NotificheItem();
                    item1.Id = i.ToString();
                    item1.Data = DateTime.Now.AddHours((double)1);
                    item1.Message = "message" + i.ToString();
                    item1.Title = "title" + i.ToString();

                    itemsMessage.Add(item1);
                }
                int posFirst = 0;
                timeAdapter = new NotificheTimeAdapter(this, itemsMessage, -1, posFirst);
                listNotifiche.Adapter = timeAdapter;
                timeAdapter.ItemClickIdEvent += OnClickItem;
                boxMenuBack.Click += OnBackPressedClick;
                txtTitleMessage.Text = itemsMessage[posFirst].Title;
                txtMessage.Text = itemsMessage[posFirst].Message;

			

            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void OnBackPressedClick(object sender, EventArgs e)
        {
            try
            {
                OnBackPressed();
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        void OnClickItem(int position)
        {
            try
            {
                timeAdapter = new NotificheTimeAdapter(this, itemsMessage, 0, position);
                listNotifiche.Adapter = timeAdapter;
                timeAdapter.RowClickEvent += OnRowFromListClick;

                txtTitleMessage.Text = itemsMessage[position].Title;
                txtMessage.Text = itemsMessage[position].Message;

            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void OnRowFromListClick(int position)
        {
            try
            {
                timeAdapter = new NotificheTimeAdapter(this, itemsMessage, 0, position);
                listNotifiche.Adapter = timeAdapter;
                timeAdapter.RowClickEvent += OnRowFromListClick;

                txtTitleMessage.Text = itemsMessage[position].Title;
                txtMessage.Text = itemsMessage[position].Message;

            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }

        }

        protected override void OnPause()
        {
            base.OnPause();
        }

        protected override void OnStop()
        {
            base.OnStop();
            try
            {
                GC.Collect();
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            try
            {
                GC.Collect();
                timeAdapter.ItemClickIdEvent -= OnClickItem;
                boxMenuBack.Click -= OnBackPressedClick;
                if (notificheLayoutBg != null)
                {
                    unbindDrawables(notificheLayoutBg);
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        private void unbindDrawables(View rootView)
        {
            try
            {
                if (rootView.Background != null)
                {
                    rootView.Background.SetCallback(null);
                }
                if (rootView is ViewGroup)
                {
                    for (int i = 0; i < ((ViewGroup)rootView).ChildCount; i++)
                    {
                        unbindDrawables(((ViewGroup)rootView).GetChildAt(i));

                        ((IDisposable)((ViewGroup)rootView).GetChildAt(i)).Dispose();
                    }

                    try
                    {
                        ((ViewGroup)rootView).RemoveAllViews();
                    }
                    catch (Java.Lang.UnsupportedOperationException mayHappen)
                    {
                        // AdapterViews, ListViews and potentially other ViewGroups don抰 support the removeAllViews operation
                        //Utils.writeToDeviceLog (mayHappen);
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

    }
}
