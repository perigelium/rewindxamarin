// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace RewindiOS
{
	[Register ("NotificheCell")]
	partial class NotificheCell
	{
		[Outlet]
		UIKit.UIImageView imgRightArrow { get; set; }

		[Outlet]
		UIKit.UILabel lblDayMonth { get; set; }

		[Outlet]
		UIKit.UILabel lblYear { get; set; }

		[Outlet]
		UIKit.UIView viewCellBackground { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (imgRightArrow != null) {
				imgRightArrow.Dispose ();
				imgRightArrow = null;
			}

			if (viewCellBackground != null) {
				viewCellBackground.Dispose ();
				viewCellBackground = null;
			}

			if (lblDayMonth != null) {
				lblDayMonth.Dispose ();
				lblDayMonth = null;
			}

			if (lblYear != null) {
				lblYear.Dispose ();
				lblYear = null;
			}
		}
	}
}
