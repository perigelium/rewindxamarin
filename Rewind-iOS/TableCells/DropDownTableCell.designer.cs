// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace RewindiOS
{
    [Register ("DropDownTableCell")]
    partial class DropDownTableCell
    {
        [Outlet]
        UIKit.UILabel lblName { get; set; }


        [Outlet]
        UIKit.UITableViewCell mainCell { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (lblName != null) {
                lblName.Dispose ();
                lblName = null;
            }

            if (mainCell != null) {
                mainCell.Dispose ();
                mainCell = null;
            }
        }
    }
}