// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace RewindiOS
{
	[Register ("PraticheCell")]
	partial class PraticheCell
	{
		[Outlet]
		UIKit.UIButton btnCellDettagli { get; set; }

		[Outlet]
		UIKit.UILabel lblCellDate { get; set; }

		[Outlet]
		UIKit.UILabel lblCellGntAttiva { get; set; }

		[Outlet]
		UIKit.UILabel lblCellGntAttivaValue { get; set; }

		[Outlet]
		UIKit.UILabel lblCellgntInserita { get; set; }

		[Outlet]
		UIKit.UILabel lblCellgntInseritaValue { get; set; }

		[Outlet]
		UIKit.UILabel lblCellProdotto { get; set; }

		[Outlet]
		UIKit.UILabel lblCellProdottoValue { get; set; }

		[Outlet]
		UIKit.UILabel lblCellTipoProdotto { get; set; }

		[Outlet]
		UIKit.UILabel lblCellTipoProdottoValue { get; set; }

		[Outlet]
		UIKit.UIView viewCellBg { get; set; }

		[Outlet]
		UIKit.UIView viewCellContentInfo { get; set; }

		[Outlet]
		UIKit.UIView viewCellDate { get; set; }

		[Outlet]
		UIKit.UIView viewCellSeparator { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (btnCellDettagli != null) {
				btnCellDettagli.Dispose ();
				btnCellDettagli = null;
			}

			if (lblCellGntAttiva != null) {
				lblCellGntAttiva.Dispose ();
				lblCellGntAttiva = null;
			}

			if (lblCellGntAttivaValue != null) {
				lblCellGntAttivaValue.Dispose ();
				lblCellGntAttivaValue = null;
			}

			if (lblCellgntInserita != null) {
				lblCellgntInserita.Dispose ();
				lblCellgntInserita = null;
			}

			if (lblCellgntInseritaValue != null) {
				lblCellgntInseritaValue.Dispose ();
				lblCellgntInseritaValue = null;
			}

			if (lblCellDate != null) {
				lblCellDate.Dispose ();
				lblCellDate = null;
			}

			if (lblCellProdotto != null) {
				lblCellProdotto.Dispose ();
				lblCellProdotto = null;
			}

			if (lblCellProdottoValue != null) {
				lblCellProdottoValue.Dispose ();
				lblCellProdottoValue = null;
			}

			if (lblCellTipoProdotto != null) {
				lblCellTipoProdotto.Dispose ();
				lblCellTipoProdotto = null;
			}

			if (lblCellTipoProdottoValue != null) {
				lblCellTipoProdottoValue.Dispose ();
				lblCellTipoProdottoValue = null;
			}

			if (viewCellBg != null) {
				viewCellBg.Dispose ();
				viewCellBg = null;
			}

			if (viewCellContentInfo != null) {
				viewCellContentInfo.Dispose ();
				viewCellContentInfo = null;
			}

			if (viewCellDate != null) {
				viewCellDate.Dispose ();
				viewCellDate = null;
			}

			if (viewCellSeparator != null) {
				viewCellSeparator.Dispose ();
				viewCellSeparator = null;
			}
		}
	}
}
