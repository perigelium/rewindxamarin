// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace RewindiOS
{
	[Register ("ClientiMemoCell")]
	partial class ClientiMemoCell
	{
		[Outlet]
		UIKit.UILabel lblMemoTitleCell { get; set; }

		[Outlet]
		UIKit.UIView viewMemoBgCell { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (viewMemoBgCell != null) {
				viewMemoBgCell.Dispose ();
				viewMemoBgCell = null;
			}

			if (lblMemoTitleCell != null) {
				lblMemoTitleCell.Dispose ();
				lblMemoTitleCell = null;
			}
		}
	}
}
