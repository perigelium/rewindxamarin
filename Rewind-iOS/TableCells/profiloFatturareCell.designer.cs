// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace RewindiOS
{
	[Register ("profiloFatturareCell")]
	partial class profiloFatturareCell
	{
		[Outlet]
		UIKit.UIButton btnCheckBox { get; set; }

		[Outlet]
		UIKit.UIButton btnFatturaExpand { get; set; }

		[Outlet]
		UIKit.UIImageView imgFatturaArrow { get; set; }

		[Outlet]
		UIKit.UILabel lblFatturaPrice { get; set; }

		[Outlet]
		UIKit.UILabel lblFatturaStatus { get; set; }

		[Outlet]
		UIKit.UILabel lblFatturaTitle { get; set; }

		[Outlet]
		UIKit.UIView viewFatturaDetails { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (btnCheckBox != null) {
				btnCheckBox.Dispose ();
				btnCheckBox = null;
			}

			if (btnFatturaExpand != null) {
				btnFatturaExpand.Dispose ();
				btnFatturaExpand = null;
			}

			if (imgFatturaArrow != null) {
				imgFatturaArrow.Dispose ();
				imgFatturaArrow = null;
			}

			if (lblFatturaPrice != null) {
				lblFatturaPrice.Dispose ();
				lblFatturaPrice = null;
			}

			if (lblFatturaStatus != null) {
				lblFatturaStatus.Dispose ();
				lblFatturaStatus = null;
			}

			if (lblFatturaTitle != null) {
				lblFatturaTitle.Dispose ();
				lblFatturaTitle = null;
			}

			if (viewFatturaDetails != null) {
				viewFatturaDetails.Dispose ();
				viewFatturaDetails = null;
			}
		}
	}
}
