// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace RewindiOS
{
	[Register ("appointmentsSearchCell")]
	partial class appointmentsSearchCell
	{
		[Outlet]
		UIKit.UIImageView imgSearchArrow { get; set; }

		[Outlet]
		UIKit.UILabel lblSearchDate { get; set; }

		[Outlet]
		UIKit.UILabel lblSearchHour { get; set; }

		[Outlet]
		UIKit.UILabel lblSearchMinutes { get; set; }

		[Outlet]
		UIKit.UILabel lblSearchRegSociale { get; set; }

		[Outlet]
		UIKit.UILabel lblSearchStatus { get; set; }

		[Outlet]
		UIKit.UIView viewSearchCell { get; set; }

		[Outlet]
		UIKit.UIView viewSearchDateHour { get; set; }

		[Outlet]
		UIKit.UIView viewSearchDateHourSeparator { get; set; }

		[Outlet]
		UIKit.UIView viewSearchRegSocStatus { get; set; }

		[Outlet]
		UIKit.UIView viewSearchSeparatorCell { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (imgSearchArrow != null) {
				imgSearchArrow.Dispose ();
				imgSearchArrow = null;
			}

			if (lblSearchDate != null) {
				lblSearchDate.Dispose ();
				lblSearchDate = null;
			}

			if (lblSearchHour != null) {
				lblSearchHour.Dispose ();
				lblSearchHour = null;
			}

			if (lblSearchMinutes != null) {
				lblSearchMinutes.Dispose ();
				lblSearchMinutes = null;
			}

			if (lblSearchRegSociale != null) {
				lblSearchRegSociale.Dispose ();
				lblSearchRegSociale = null;
			}

			if (lblSearchStatus != null) {
				lblSearchStatus.Dispose ();
				lblSearchStatus = null;
			}

			if (viewSearchCell != null) {
				viewSearchCell.Dispose ();
				viewSearchCell = null;
			}

			if (viewSearchDateHour != null) {
				viewSearchDateHour.Dispose ();
				viewSearchDateHour = null;
			}

			if (viewSearchDateHourSeparator != null) {
				viewSearchDateHourSeparator.Dispose ();
				viewSearchDateHourSeparator = null;
			}

			if (viewSearchRegSocStatus != null) {
				viewSearchRegSocStatus.Dispose ();
				viewSearchRegSocStatus = null;
			}

			if (viewSearchSeparatorCell != null) {
				viewSearchSeparatorCell.Dispose ();
				viewSearchSeparatorCell = null;
			}
		}
	}
}
