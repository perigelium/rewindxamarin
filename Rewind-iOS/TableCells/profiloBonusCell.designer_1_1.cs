// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace RewindiOS
{
	[Register ("profiloBonusCell")]
	partial class profiloBonusCell
	{
		[Outlet]
		UIKit.UILabel lblBonusCell { get; set; }

		[Outlet]
		UIKit.UILabel lblProdottoCell { get; set; }

		[Outlet]
		UIKit.UILabel lblRagioneSocialeCell { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (lblRagioneSocialeCell != null) {
				lblRagioneSocialeCell.Dispose ();
				lblRagioneSocialeCell = null;
			}

			if (lblProdottoCell != null) {
				lblProdottoCell.Dispose ();
				lblProdottoCell = null;
			}

			if (lblBonusCell != null) {
				lblBonusCell.Dispose ();
				lblBonusCell = null;
			}
		}
	}
}
