// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace RewindiOS
{
	[Register ("profiloGareCell")]
	partial class profiloGareCell
	{
		[Outlet]
		UIKit.UILabel lblAgenteCell { get; set; }

		[Outlet]
		UIKit.UILabel lblPaCell { get; set; }

		[Outlet]
		UIKit.UILabel lblPosCell { get; set; }

		[Outlet]
		UIKit.UILabel lblPsCell { get; set; }

		[Outlet]
		UIKit.UILabel lblPuntiCell { get; set; }

		[Outlet]
		UIKit.UILabel lblSsCell { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (lblPosCell != null) {
				lblPosCell.Dispose ();
				lblPosCell = null;
			}

			if (lblAgenteCell != null) {
				lblAgenteCell.Dispose ();
				lblAgenteCell = null;
			}

			if (lblPuntiCell != null) {
				lblPuntiCell.Dispose ();
				lblPuntiCell = null;
			}

			if (lblPaCell != null) {
				lblPaCell.Dispose ();
				lblPaCell = null;
			}

			if (lblPsCell != null) {
				lblPsCell.Dispose ();
				lblPsCell = null;
			}

			if (lblSsCell != null) {
				lblSsCell.Dispose ();
				lblSsCell = null;
			}
		}
	}
}
