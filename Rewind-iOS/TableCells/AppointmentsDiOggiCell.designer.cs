// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace RewindiOS
{
	[Register ("AppointmentsDiOggiCell")]
	partial class AppointmentsDiOggiCell
	{
		[Outlet]
		UIKit.UIImageView imgCellArrow { get; set; }

		[Outlet]
		UIKit.UILabel lblCellDate { get; set; }

		[Outlet]
		UIKit.UILabel lblCellHour { get; set; }

		[Outlet]
		UIKit.UILabel lblCellMinutes { get; set; }

		[Outlet]
		UIKit.UILabel lblCellRegSociale { get; set; }

		[Outlet]
		UIKit.UIView viewBgCell { get; set; }

		[Outlet]
		UIKit.UIView viewCellDateAndTime { get; set; }

		[Outlet]
		UIKit.UIView viewCellLine { get; set; }

		[Outlet]
		UIKit.UIView viewCellRegSociale { get; set; }

		[Outlet]
		UIKit.UIView viewCellSeparator { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (viewBgCell != null) {
				viewBgCell.Dispose ();
				viewBgCell = null;
			}

			if (viewCellSeparator != null) {
				viewCellSeparator.Dispose ();
				viewCellSeparator = null;
			}

			if (viewCellDateAndTime != null) {
				viewCellDateAndTime.Dispose ();
				viewCellDateAndTime = null;
			}

			if (viewCellLine != null) {
				viewCellLine.Dispose ();
				viewCellLine = null;
			}

			if (imgCellArrow != null) {
				imgCellArrow.Dispose ();
				imgCellArrow = null;
			}

			if (viewCellRegSociale != null) {
				viewCellRegSociale.Dispose ();
				viewCellRegSociale = null;
			}

			if (lblCellRegSociale != null) {
				lblCellRegSociale.Dispose ();
				lblCellRegSociale = null;
			}

			if (lblCellHour != null) {
				lblCellHour.Dispose ();
				lblCellHour = null;
			}

			if (lblCellDate != null) {
				lblCellDate.Dispose ();
				lblCellDate = null;
			}

			if (lblCellMinutes != null) {
				lblCellMinutes.Dispose ();
				lblCellMinutes = null;
			}
		}
	}
}
