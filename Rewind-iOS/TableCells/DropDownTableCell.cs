﻿
using System;

using Foundation;
using UIKit;

namespace RewindiOS
{
    public partial class DropDownTableCell : UIViewController
    {
        public static readonly NSString Key = new NSString("DropDownTableCell");
        public static readonly UINib Nib;

        static bool UserInterfaceIdiomIsPhone
        {
            get { return UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone; }
        }

        static DropDownTableCell()
        {
           // Nib = UINib.FromName("DropDownTableCell", NSBundle.MainBundle);
            Foundation.NSBundle.MainBundle.LoadNib (UserInterfaceIdiomIsPhone ? "DropDownTableCell~iphone" : "DropDownTableCell~ipad", NSBundle.MainBundle, null);
        }

//        public DropDownTableCell()
//            : base()
//        {
//        }

//        public static DropDownTableCell Create()
//        {
//            return (DropDownTableCell)Nib.Instantiate(null, null)[0];
//        }

        public UILabel LblName
        {
            set{    lblName = value; }
            get{    return lblName; }
        }
        public UITableViewCell MainCell
        {
            set{ mainCell= value; }
            get{    return mainCell;    }
        }


    }
}

