// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace RewindiOS
{
	[Register ("ClientiRegSocialeCell")]
	partial class ClientiRegSocialeCell
	{
		[Outlet]
		UIKit.UIImageView imgClientiArrow { get; set; }

		[Outlet]
		UIKit.UILabel lblCellRegSocaiale { get; set; }

		[Outlet]
		UIKit.UIView viewBgCell { get; set; }

		[Outlet]
		UIKit.UIView viewSeparator { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (lblCellRegSocaiale != null) {
				lblCellRegSocaiale.Dispose ();
				lblCellRegSocaiale = null;
			}

			if (viewBgCell != null) {
				viewBgCell.Dispose ();
				viewBgCell = null;
			}

			if (imgClientiArrow != null) {
				imgClientiArrow.Dispose ();
				imgClientiArrow = null;
			}

			if (viewSeparator != null) {
				viewSeparator.Dispose ();
				viewSeparator = null;
			}
		}
	}
}
