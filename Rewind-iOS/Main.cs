﻿using UIKit;
using Xamarin;

namespace RewindiOS
{
    public class Application
    {
        // This is the main entry point of the application.
        static void Main(string[] args)
        {
            // if you want to use a different Application Delegate class from "AppDelegate"
            // you can specify it here.
            Insights.Initialize("fcacf625d59dd6163ba9fe95180c23ba87469deb");
            UIApplication.Main(args, null, "AppDelegate");
        }
    }
}
