﻿using System;
using System.Collections.Generic;

using UIKit;
using Foundation;

using RewindShared;
using System.Globalization;

namespace RewindiOS
{
    public class NotificheTblSource : UITableViewSource
    {
        public delegate void OnNotificheRowSelectedDelegate(int position);
        public OnNotificheRowSelectedDelegate OnNotificheRowSelectedEvent;

        NSString cellHeaderIdentifier = new NSString("NotificheCell");

        List<NotificheItem> tableItems;
        int selectedCellIndex = 0;
        UITableView tblMessageItems;

        public NotificheTblSource(List<NotificheItem> tableItems, UITableView tblMessageItems)
        {
            this.tableItems = tableItems;
            this.tblMessageItems = tblMessageItems;
        }

        #region implemented abstract members of UITableViewSource

        public override UITableViewCell GetCell(UITableView tableView, Foundation.NSIndexPath indexPath)
        {
            NotificheCell cell = (NotificheCell)tableView.DequeueReusableCell(cellHeaderIdentifier);
            UIView view = new UIView();

            try
            {
                int index = indexPath.Row;
                view = cell.ContentView;

                using (UIFont fnt = UIFont.FromName("Oswald-Regular", 13.0f))
                {
                    cell.LblDayMonth.Font = fnt;
                    cell.LblYear.Font = fnt;
                }

                cell.LblDayMonth.Text = tableItems[index].Data.ToString("dd.MM", CultureInfo.InvariantCulture);
                cell.LblYear.Text = tableItems[index].Data.ToString("yyyy", CultureInfo.InvariantCulture);

                if(index == selectedCellIndex)
                {
                    cell.ViewCellBackground.BackgroundColor = UIColor.FromRGB(68, 68, 68);
                    cell.ImgRightArrow.Hidden = false;
                }
                else 
                {
                    cell.ViewCellBackground.BackgroundColor = UIColor.FromRGB(0, 95, 167);
                    cell.ImgRightArrow.Hidden = true;
                }

                cell.BackgroundColor = UIColor.Clear;
            }
            catch (Exception ex)
            {
                Utils.writeToIOSDeviceLog(ex);
            }

            return cell;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            if (tableItems != null)
            {
                return tableItems.Count;
            }

            return 0;
        }

        public override nint NumberOfSections(UITableView tableView)
        {
            return 1;
        }

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            try
            {
                if(OnNotificheRowSelectedEvent != null)
                {
                    OnNotificheRowSelectedEvent(indexPath.Row);
                }
                
                this.selectedCellIndex = indexPath.Row;
                tblMessageItems.ReloadData();
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        #endregion
    }
}

