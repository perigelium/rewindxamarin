﻿using System;
using UIKit;
using Foundation;
using System.Collections.Generic;
using RewindShared;

namespace RewindiOS
{
    public class ClientiRagSocialeTableSource:UITableViewSource
    {
        NSString cellHeaderIdentifier = new NSString("ClientiRegSocialeCell");

        List<ClientInfoItem> tableItems;

        int endSection;
        int didSection;
        bool ifOpen;
        UITableView mainTable;
        float headerTblProductsHeight;
        float rowTblProductsHeight;
        int selIndex;

        ClientiScreen mainController;


        public ClientiRagSocialeTableSource(List<ClientInfoItem> items, UITableView _mainTable, float _headerTblProductsHeight, float _rowTblProductsHeight, ClientiScreen _mainController, int _selIndex)
        {
            tableItems = items;
            didSection = items.Count + 1;
            mainTable = _mainTable;
            headerTblProductsHeight = _headerTblProductsHeight;
            rowTblProductsHeight = _rowTblProductsHeight;
            mainController = _mainController;
            selIndex = _selIndex;
        }

        void firstOneClicked()
        {
            didSection = 0;
            endSection = 0;
            didSelectCellRowFirstDo(true, false);
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return (int)tableItems.Count;
        }


        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {

            ClientiRegSocialeCell cell = (ClientiRegSocialeCell)tableView.DequeueReusableCell(cellHeaderIdentifier);
            UIView view = new UIView();
            try
            {
                int index = indexPath.Row;
                view = cell.ContentView;

                using (UIFont fnt = UIFont.FromName("Oswald-Regular", 10.0f))
                {
                    cell.LblCellRegSocaiale.Font = fnt;
                }
                cell.ImgClientiArrow.Hidden=true;

                cell.LblCellRegSocaiale.Text=tableItems[indexPath.Row].Regione_sociale;
                cell.ViewBgCell.BackgroundColor=UIColor.FromRGB(0, 95, 167);
                cell.ViewSeparator.BackgroundColor=UIColor.FromRGB(76,76,76);

                if (selIndex != -1)
                {
                    view.Tag = indexPath.Row;

                    if (selIndex == indexPath.Row)
                    {
                        cell.ViewBgCell.BackgroundColor=UIColor.FromRGB(76, 76, 76);
                        cell.ImgClientiArrow.Hidden=false;
                    }
                    else
                    {
                        //first time
                        cell.ViewBgCell.BackgroundColor=UIColor.FromRGB(0, 95, 167);
                        cell.ImgClientiArrow.Hidden=true;
                    }
                }
                else
                {
                    //                    view.Click += OnItemClick;
                    //                    view.Tag = position;
                }

            }
            catch (Exception ex)
            {
                Utils.writeToIOSDeviceLog(ex);
            }
            return cell;
        }

        public override nint NumberOfSections(UITableView tableView)
        {
            return 1;
        }

        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            return headerTblProductsHeight;
        }

        public override nfloat GetHeightForHeader(UITableView tableView, nint section)
        {
            return 0;
        }


        public override UIView GetViewForHeader(UITableView tableView, nint section)
        {
            return null;
        }

        void addCell(object sender)
        {
            try
            {
                UIButton bt = (UIButton)sender;
                endSection = (int)bt.Tag;
                if (didSection == tableItems.Count + 1)
                {
                    ifOpen = false;
                    didSection = endSection;
                    didSelectCellRowFirstDo(true, false);
                }
                else
                {
                    if (didSection == endSection)
                    {
                        didSelectCellRowFirstDo(false, false);
                    }
                    else
                    {
                        didSelectCellRowFirstDo(false, true);
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.writeToIOSDeviceLog(ex);
            }
        }

        void didSelectCellRowFirstDo(bool firstDoInsert, bool nextDoInsert)
        {
            try
            {
                mainTable.BeginUpdates();
                ifOpen = firstDoInsert;
                NSIndexPath[] rowToInsert = new NSIndexPath [1];
                NSIndexPath indexPath = NSIndexPath.FromRowSection(0, didSection);

                rowToInsert[0] = indexPath;
                if (!ifOpen)
                {
                    didSection = tableItems.Count + 1;
                    mainTable.DeleteRows(rowToInsert, UITableViewRowAnimation.Fade);
                }
                else
                {
                    mainTable.InsertRows(rowToInsert, UITableViewRowAnimation.Fade);
                }

                mainTable.EndUpdates();
                if (nextDoInsert)
                {
                    didSection = endSection;
                    didSelectCellRowFirstDo(true, false);
                }
                mainTable.ScrollToNearestSelected(UITableViewScrollPosition.Top, true);
            }
            catch (Exception ex)
            {
                Utils.writeToIOSDeviceLog(ex);
            }
        }

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            try
            {
                tableView.DeselectRow(indexPath, true);
                mainController.OnRowFromListClick(indexPath.Row,tableItems);
            }
            catch (Exception ex)
            {
                Utils.writeToIOSDeviceLog(ex);
            }
        }
    }
}