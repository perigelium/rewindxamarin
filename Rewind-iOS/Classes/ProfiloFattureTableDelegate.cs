﻿using System;
using UIKit;

namespace RewindiOS
{
    public class ProfiloFattureTableDelegate: UITableViewDelegate
    {
        #region Private Variables
        private ProfiloFatturareScreen Controller;
        #endregion

        #region Constructors
        public ProfiloFattureTableDelegate ()
        {
        }

        public ProfiloFattureTableDelegate (ProfiloFatturareScreen controller)
        {
            // Initialize
            this.Controller = controller;
        }
        #endregion

        #region Override Methods
        public override nfloat EstimatedHeight (UITableView tableView, Foundation.NSIndexPath indexPath)
        {
            return 200f;
        }

        public override void RowSelected (UITableView tableView, Foundation.NSIndexPath indexPath)
        {
            // Output selected row
            Console.WriteLine("Row selected: {0}",indexPath.Row);
        }
        #endregion
    }
}


