﻿using System;
using CoreLocation;
using MapKit;

namespace RewindiOS
{
    public class CustomAnnotation: MKAnnotation
    {
        private CLLocationCoordinate2D coord;
        private string title;
        private string subtitle;
        private string imageName;
        private string typeAnnotation;

        public CustomAnnotation(CLLocationCoordinate2D coord, string title, string subtitle, string imageName, string typeAnnotation)
            : base()
        {
            this.coord = coord;
            this.title = title;
            this.subtitle = subtitle;
            this.imageName = imageName;
            this.typeAnnotation = typeAnnotation;
        }

        public override CLLocationCoordinate2D Coordinate
        {
            get
            {
                return coord;
            }

        }

        public override string Title
        {
            get
            {
                return title;
            }
        }

        public override string Subtitle
        {
            get
            {
                return subtitle;
            }
        }

        public string ImageName
        {
            get
            {
                return imageName;
            }
        }

        public string TypeAnnotation
        {
            get
            {
                return typeAnnotation;
            }
        }
    }

    public class AppointmentsAnnotation: MKAnnotation
    {
        private CLLocationCoordinate2D coord;
        private string title;
        private string subtitle;
        private string hourMinutes;

        public AppointmentsAnnotation(CLLocationCoordinate2D coord, string title, string subtitle, string hourMinutes)
            : base()
        {
            this.coord = coord;
            this.title = title;
            this.subtitle = subtitle;
            this.hourMinutes = hourMinutes;
        }

        public override CLLocationCoordinate2D Coordinate
        {
            get
            {
                return coord;
            }

        }

        public override string Title
        {
            get
            {
                return title;
            }
        }

        public override string Subtitle
        {
            get
            {
                return subtitle;
            }
        }

        public string HourMinutes
        {
            get
            {
                return hourMinutes;
            }
        }
    }
}