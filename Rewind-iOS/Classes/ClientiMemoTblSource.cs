﻿using System;
using UIKit;
using Foundation;
using RewindShared;
using System.Collections.Generic;
using System.Linq;

namespace RewindiOS
{
    public class ClientiMemoTblSource:UITableViewSource
    {
        NSString cellHeaderIdentifier = new NSString("ClientiMemoCell");

        List<ClientiMemoItem> tableItems;

        int endSection;
        int didSection;
        bool ifOpen;
        UITableView mainTable;
        float headerTblProductsHeight;
        float rowTblProductsHeight;
        int selIndex;

        ClientiScreen mainController;


        public ClientiMemoTblSource(List<ClientiMemoItem> items, UITableView _mainTable, float _headerTblProductsHeight, float _rowTblProductsHeight, ClientiScreen _mainController, int _selIndex)
        {
            tableItems = items;
            didSection = items.Count + 1;
            mainTable = _mainTable;
            headerTblProductsHeight = _headerTblProductsHeight;
            rowTblProductsHeight = _rowTblProductsHeight;
            mainController = _mainController;
            selIndex = _selIndex;
        }

        void firstOneClicked()
        {
            didSection = 0;
            endSection = 0;
            didSelectCellRowFirstDo(true, false);
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return (int)tableItems.Count;
        }


        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {

            ClientiMemoCell cell = (ClientiMemoCell)tableView.DequeueReusableCell(cellHeaderIdentifier);
            UIView view = new UIView();
            try
            {
                int index = indexPath.Row;
                view = cell.ContentView;

                using (UIFont fnt = UIFont.FromName("Oswald-Regular", 10.0f))
                {
                    cell.LblMemoTitleCell.Font = fnt;
                }


                cell.LblMemoTitleCell.Text=tableItems[indexPath.Row].Title;
                cell.ViewMemoBgCell.BackgroundColor=UIColor.FromRGB(0, 95, 167);

                if (selIndex != -1)
                {
                    view.Tag = indexPath.Row;

                    if (selIndex == indexPath.Row)
                    {
                        cell.ViewMemoBgCell.BackgroundColor=UIColor.FromRGB(76, 76, 76);
                    }
                    else
                    {
                        //first time
                        cell.ViewMemoBgCell.BackgroundColor=UIColor.FromRGB(0, 95, 167);
                    }
                }
                else
                {
                    //                    view.Click += OnItemClick;
                    //                    view.Tag = position;
                }

            }
            catch (Exception ex)
            {
                Utils.writeToIOSDeviceLog(ex);
            }
            return cell;
        }

        public override nint NumberOfSections(UITableView tableView)
        {
            return 1;
        }

        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            return headerTblProductsHeight;
        }

        public override nfloat GetHeightForHeader(UITableView tableView, nint section)
        {
            return 0;
        }


        public override UIView GetViewForHeader(UITableView tableView, nint section)
        {
            return null;
        }

        void addCell(object sender)
        {
            try
            {
                UIButton bt = (UIButton)sender;
                endSection = (int)bt.Tag;
                if (didSection == tableItems.Count + 1)
                {
                    ifOpen = false;
                    didSection = endSection;
                    didSelectCellRowFirstDo(true, false);
                }
                else
                {
                    if (didSection == endSection)
                    {
                        didSelectCellRowFirstDo(false, false);
                    }
                    else
                    {
                        didSelectCellRowFirstDo(false, true);
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.writeToIOSDeviceLog(ex);
            }
        }

        void didSelectCellRowFirstDo(bool firstDoInsert, bool nextDoInsert)
        {
            try
            {
                mainTable.BeginUpdates();
                ifOpen = firstDoInsert;
                NSIndexPath[] rowToInsert = new NSIndexPath [1];
                NSIndexPath indexPath = NSIndexPath.FromRowSection(0, didSection);

                rowToInsert[0] = indexPath;
                if (!ifOpen)
                {
                    didSection = tableItems.Count + 1;
                    mainTable.DeleteRows(rowToInsert, UITableViewRowAnimation.Fade);
                }
                else
                {
                    mainTable.InsertRows(rowToInsert, UITableViewRowAnimation.Fade);
                }

                mainTable.EndUpdates();
                if (nextDoInsert)
                {
                    didSection = endSection;
                    didSelectCellRowFirstDo(true, false);
                }
                mainTable.ScrollToNearestSelected(UITableViewScrollPosition.Top, true);
            }
            catch (Exception ex)
            {
                Utils.writeToIOSDeviceLog(ex);
            }
        }

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            try
            {
                tableView.DeselectRow(indexPath, true);
                mainController.OnMemoRowListClick(indexPath.Row);
            }
            catch (Exception ex)
            {
                Utils.writeToIOSDeviceLog(ex);
            }
        }
    }
}