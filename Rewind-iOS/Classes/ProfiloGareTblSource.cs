﻿using System;
using System.Collections.Generic;

using UIKit;
using Foundation;

using RewindShared;

namespace RewindiOS
{
    public class ProfiloGareTblSource : UITableViewSource
    {
        NSString cellHeaderIdentifier = new NSString("profiloGareCell");
        List<GareItem> tableItems;

        public ProfiloGareTblSource(List<GareItem> tableItems)
        {
            this.tableItems = tableItems;
        }

        #region implemented abstract members of UITableViewSource

        public override UITableViewCell GetCell(UITableView tableView, Foundation.NSIndexPath indexPath)
        {
            profiloGareCell cell = (profiloGareCell)tableView.DequeueReusableCell(cellHeaderIdentifier);
            UIView view = new UIView();

            try
            {
                int index = indexPath.Row;
                view = cell.ContentView;

                using (UIFont fnt = UIFont.FromName("Oswald-Regular", 16.0f))
                {
                    cell.LblAgenteCell.Font = fnt;
                    cell.LblPaCell.Font = fnt;
                    cell.LblPosCell.Font = fnt;
                    cell.LblPsCell.Font = fnt;
                    cell.LblPuntiCell.Font = fnt;
                    cell.LblSsCell.Font = fnt;
                }

                cell.LblPosCell.Text = (index + 1).ToString();
                cell.LblAgenteCell.Text = tableItems[index].Agente;
                cell.LblPuntiCell.Text = tableItems[index].Punti;
                cell.LblPaCell.Text = tableItems[index].PA;
                cell.LblPsCell.Text = tableItems[index].PS;
                cell.LblSsCell.Text = tableItems[index].SS;
            }
            catch (Exception ex)
            {
                Utils.writeToIOSDeviceLog(ex);
            }

            return cell;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            if (tableItems != null)
            {
                return tableItems.Count;
            }

            return 0;
        }

        public override nint NumberOfSections(UITableView tableView)
        {
            return 1;
        }

        #endregion
    }
}

