﻿using System;
using RewindShared;
using Foundation;
using UIKit;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;

namespace RewindiOS
{
    public class ProfiloFattureExtendedTblSource:UITableViewSource
    {
        NSString cellHeaderIdentifier = new NSString("profiloFatturareCell");
        NSString cellIdentifier = new NSString("FattureDetailsSubCell");

        List<FatturarePeriodItem> tableItems;
        List<UIButton> btns = new List<UIButton>();

        int endSection;
        int didSection;
        int newSelection = -1;
        int oldSelection = -1;
        bool openSection = false;
        bool ifOpen;
        UITableView mainTable;
        float headerTblProductsHeight;
        float rowTblProductsHeight;
        UIViewController parentViewController;

       

        public ProfiloFattureExtendedTblSource(List<FatturarePeriodItem> items, UITableView _mainTable, float _headerTblProductsHeight, float _rowTblProductsHeight,UIViewController _screenController)
        {
            tableItems = items;
            didSection = items.Count + 1;
            mainTable = _mainTable;
            headerTblProductsHeight = _headerTblProductsHeight;
            rowTblProductsHeight = _rowTblProductsHeight;
            parentViewController = _screenController;

        }

        void firstOneClicked()
        {
            didSection = 0;
            endSection = 0;
            didSelectCellRowFirstDo(true, false);
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            if (section == didSection)
            {
                return 1;
            }
            return 0;
        }



        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {

            FattureDetailsSubCell cell = (FattureDetailsSubCell)tableView.DequeueReusableCell(cellIdentifier, indexPath);

            try
            {
//                int index=indexPath.Section;
//                RemoveSubviews(cell);
//                float y=0;
//                foreach (var itemFat in tableItems[indexPath.Section].Facturi) 
//                {
//
//                    UILabel lblGroupName=new UILabel();
//                    lblGroupName.Frame=new CoreGraphics.CGRect(0,y,cell.Frame.Width,20);
//                    lblGroupName.AutoresizingMask= UIViewAutoresizing.None;
//                    lblGroupName.TextAlignment= UITextAlignment.Left;
//                    lblGroupName.TextColor=UIColor.Black;
//                    lblGroupName.Text=itemFat.Keys.ToList()[0];
//                    using (UIFont fnt = UIFont.FromName("Oswald-Regular", 14.0f))
//                    {
//                        lblGroupName.Font = fnt;
//                    }
//                    cell.Add(lblGroupName);
//
//                    UIView viewGroupFactureNames=new UIView();
//                    viewGroupFactureNames.Frame=new CoreGraphics.CGRect(20,y+20,cell.Frame.Width-20,itemFat.Values.ToList()[0].Count*20);
//                    viewGroupFactureNames.AutoresizingMask= UIViewAutoresizing.None;
//                    cell.Add(viewGroupFactureNames);
//
//                    float yz=0;
//                    foreach (var itemFatName in itemFat.Values.ToList()[0]) 
//                    {
//                        UILabel lblFattName=new UILabel();
//                        lblFattName.Frame=new CoreGraphics.CGRect(0,yz,viewGroupFactureNames.Frame.Width,20);
//                        lblFattName.AutoresizingMask= UIViewAutoresizing.None;
//                        lblFattName.TextAlignment= UITextAlignment.Left;
//                        lblFattName.TextColor=UIColor.Gray;
//                        lblFattName.Text=itemFatName.Title_invoice;
//                        using (UIFont fnt = UIFont.FromName("Oswald-Regular", 12.0f))
//                        {
//                            lblFattName.Font = fnt;
//                        }
//                        viewGroupFactureNames.Add(lblFattName);
//                        yz+=20;
//                    }
//                    y+=(float)viewGroupFactureNames.Frame.Height+20;
//                    //txtFattPrice.Text=itemFatName.Total+"€";TODO
//                }
//                cell.BackgroundColor=UIColor.White;
//                cell.ClipsToBounds = false;
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
            return cell;

        }

        public override nint NumberOfSections(UITableView tableView)
        {
            return (int)this.tableItems.Count;
        }

        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
//            if (indexPath.Section == didSection)
//            {
//                rowTblProductsHeight=0;
//                foreach (var itemFat in tableItems[indexPath.Section].Facturi) 
//                {
//                    foreach (var itemFatName in itemFat.Values.ToList()[0]) 
//                    {
//                       
//                    }
//                    rowTblProductsHeight+=itemFat.Values.ToList()[0].Count*20+20;
//                }
//
//                return rowTblProductsHeight;
//            }
            return 0;
        }

        public override nfloat GetHeightForHeader(UITableView tableView, nint section)
        {
            if (section == didSection)
            {
                rowTblProductsHeight = 0;
                foreach (var itemFat in tableItems[(int)section].Facturi)
                {
                    foreach (var itemFatName in itemFat.Values.ToList()[0])
                    {

                    }
                    rowTblProductsHeight += itemFat.Values.ToList()[0].Count * 20 + 20;
                }

                return rowTblProductsHeight + headerTblProductsHeight;
            }
            else
            {
                return headerTblProductsHeight;
            }
            return headerTblProductsHeight;
        }

        public override UIView GetViewForHeader(UITableView tableView, nint section)
        {

            NSIndexPath indexPath = NSIndexPath.FromRowSection(section, section);

            profiloFatturareCell cell = (profiloFatturareCell)tableView.DequeueReusableCell(cellHeaderIdentifier);
            UIView view = new UIView();
            try
            {

                view = cell.ContentView;

//                fromGetCell = true;
                int index = indexPath.Row;
                view = cell.ContentView;

                using (UIFont fnt = UIFont.FromName("Oswald-Regular", 16.0f))
                {
                    cell.LblFatturaTitle.Font = fnt;
                    cell.LblFatturaPrice.Font = fnt;
                    cell.LblFatturaStatus.Font = fnt;
                }

                DateTime fattDateName=DateTime.Now;
                if(tableItems[indexPath.Row].Period!=null && tableItems[indexPath.Row].Period!="")
                {
                    fattDateName=new DateTime(Int32.Parse(tableItems[indexPath.Row].Period.Substring(0,4)),Int32.Parse(tableItems[indexPath.Row].Period.Substring(4,2)),2);
                }
                System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("it-IT");
                cell.LblFatturaTitle.Text = "FATTURA "+fattDateName.ToString("MMMMM", CultureInfo.InvariantCulture) +" "+ fattDateName.Year;

                if (bool.Parse(tableItems[index].Check) == true)
                {
                    using (UIImage imgDown = UIImage.FromBundle("selctedBox2.png"))
                    {
                        cell.BtnCheckBox.SetBackgroundImage(imgDown, UIControlState.Normal);
                    }
                }
                else
                {
                    using (UIImage imgDown = UIImage.FromBundle("selctedBoxNe2.png"))
                    {
                        cell.BtnCheckBox.SetBackgroundImage(imgDown, UIControlState.Normal);
                    }
                }

                string totalString = tableItems[index].Total.ToString();
                double totalDouble;
                double.TryParse(totalString, NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingSign, CultureInfo.InvariantCulture, out totalDouble);
//                cell.BtnFatturaExpand.Tag = index;
//                cell.BtnFatturaExpand.TouchUpInside -= Cell_BtnFatturaExpand_TouchUpInside;
                cell.BtnFatturaExpand.TouchUpInside +=  (object sender, EventArgs e) => 
                    {
                            //addCell(sender);
                            RemoveSubviews(cell.ViewFatturaDetails);
                            float y=0;
                            foreach (var itemFat in tableItems[indexPath.Section].Facturi) 
                            {

                                UILabel lblGroupName=new UILabel();
                                lblGroupName.Frame=new CoreGraphics.CGRect(0,y,cell.ViewFatturaDetails.Frame.Width,20);
                                lblGroupName.AutoresizingMask= UIViewAutoresizing.None;
                                lblGroupName.TextAlignment= UITextAlignment.Left;
                                lblGroupName.TextColor=UIColor.Black;
                                lblGroupName.Text=itemFat.Keys.ToList()[0];
                                using (UIFont fnt = UIFont.FromName("Oswald-Regular", 14.0f))
                                {
                                    lblGroupName.Font = fnt;
                                }
                                cell.ViewFatturaDetails.Add(lblGroupName);

                                UIView viewGroupFactureNames=new UIView();
                                viewGroupFactureNames.Frame=new CoreGraphics.CGRect(20,y+20,cell.ViewFatturaDetails.Frame.Width-20,itemFat.Values.ToList()[0].Count*20);
                                viewGroupFactureNames.AutoresizingMask= UIViewAutoresizing.None;
                                cell.ViewFatturaDetails.Add(viewGroupFactureNames);

                                float yz=0;
                                foreach (var itemFatName in itemFat.Values.ToList()[0]) 
                                {
                                    UILabel lblFattName=new UILabel();
                                    lblFattName.Frame=new CoreGraphics.CGRect(0,yz,viewGroupFactureNames.Frame.Width,20);
                                    lblFattName.AutoresizingMask= UIViewAutoresizing.None;
                                    lblFattName.TextAlignment= UITextAlignment.Left;
                                    lblFattName.TextColor=UIColor.Gray;
                                    lblFattName.Text=itemFatName.Title_invoice;
                                    using (UIFont fnt = UIFont.FromName("Oswald-Regular", 12.0f))
                                    {
                                        lblFattName.Font = fnt;
                                    }
                                    viewGroupFactureNames.Add(lblFattName);
                                    yz+=20;
                                }
                                y+=(float)viewGroupFactureNames.Frame.Height+20;
                                //txtFattPrice.Text=itemFatName.Total+"€";TODO
                            }
                        NSIndexSet indexSect = new NSIndexSet((uint)section);
                        tableView.ReloadSections(indexSect, UITableViewRowAnimation.None);
                    };

                cell.BtnCheckBox.Tag = index;
                cell.BtnCheckBox.TouchUpInside -= Cell_BtnCheckBox_TouchUpInside;
                cell.BtnCheckBox.TouchUpInside += Cell_BtnCheckBox_TouchUpInside;

                //                cell.LblFatturaPrice.Text = totalDouble.ToString("0.00") + " €";
                cell.LblFatturaPrice.Text=tableItems[index].Total.ToString() + "€";
                cell.ImgFatturaArrow.Hidden=true;
//
                cell.Frame=new CoreGraphics.CGRect(0,0,tableView.Frame.Width,30);
                cell.BtnFatturaExpand.Frame=new CoreGraphics.CGRect(20,53,120,40);
                cell.LblFatturaPrice.Frame=new CoreGraphics.CGRect(218,8,94,45);

//
                UIButton bt = new UIButton(UIButtonType.Custom);
                bt.Frame = new CoreGraphics.CGRect(cell.BtnFatturaExpand.Frame.X, cell.BtnFatturaExpand.Frame.Y, cell.BtnFatturaExpand.Frame.Width,cell.BtnFatturaExpand.Frame.Height);

                bt.Tag = section;

                bt.TouchUpInside += (object sender, EventArgs e) =>
                    {
                        addCell(sender);

                        if (newSelection == -1)
                        {
                            newSelection = (int)section;
                            oldSelection = (int)section;
                            openSection = true;
                            NSIndexSet indexSect = new NSIndexSet((uint)section);
                            tableView.ReloadSections(indexSect, UITableViewRowAnimation.None);

                        }
                        else
                        {
                            oldSelection = newSelection;
                            newSelection = (int)section;

                            if (oldSelection == newSelection)
                            {
                                newSelection = -1;
                                openSection = false;
                                NSIndexSet indexSect = new NSIndexSet((uint)section);
                                tableView.ReloadSections(indexSect, UITableViewRowAnimation.None);
                            }
                            else
                            {



                                openSection = true;
                                NSIndexSet indexSect = new NSIndexSet((uint)section);
                                tableView.ReloadSections(indexSect, UITableViewRowAnimation.None);

                                openSection = false;
                                indexSect = new NSIndexSet((uint)oldSelection);
                                tableView.ReloadSections(indexSect, UITableViewRowAnimation.None);
                            }

                        }
                    };
                view.AddSubview(bt);
                //btns.Add(bt);

//                cell.ClipsToBounds = false;
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }

            return view;
        }

        void Cell_BtnFatturaExpand_TouchUpInside (object sender, EventArgs e)
        {
            try
            {
               
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        void addCell(object sender)
        {
            try
            {
                UIButton bt = (UIButton)sender;
                endSection = (int)bt.Tag;
                if (didSection == tableItems.Count + 1)
                {
                    ifOpen = false;
                    didSection = endSection;
                    didSelectCellRowFirstDo(true, false);
                }
                else
                {
                    if (didSection == endSection)
                    {
                        didSelectCellRowFirstDo(false, false);
                    }
                    else
                    {
                        didSelectCellRowFirstDo(false, true);
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        void didSelectCellRowFirstDo(bool firstDoInsert, bool nextDoInsert)
        {
            try
            {
                mainTable.BeginUpdates();
                ifOpen = firstDoInsert;
                NSIndexPath[] rowToInsert = new NSIndexPath [1];
                NSIndexPath indexPath = NSIndexPath.FromRowSection(0, didSection);

                rowToInsert[0] = indexPath;
                if (!ifOpen)
                {
                    didSection = tableItems.Count + 1;
                    mainTable.DeleteRows(rowToInsert, UITableViewRowAnimation.Fade);//fade
                }
                else
                {
                    mainTable.InsertRows(rowToInsert, UITableViewRowAnimation.Fade);//fade
                }

                mainTable.EndUpdates();
                if (nextDoInsert)
                {
                    didSection = endSection;
                    didSelectCellRowFirstDo(true, false);
                }
                mainTable.ScrollToNearestSelected(UITableViewScrollPosition.Top, true);
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        void Cell_BtnCheckBox_TouchUpInside(object sender, EventArgs e)
        {
            try
            {
                UIButton checkboxSender = (UIButton)sender;
                int index = (int)checkboxSender.Tag;

                if (bool.Parse(tableItems[index].Check) == false)
                {
                    var okAlertController = UIAlertController.Create("Attenzione", "Sta per accettare la fattura selezionata. E' sicuro di voler procedere?", UIAlertControllerStyle.Alert);
                    okAlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, alert =>
                        {
                            //                                if (boxSender.Checked != bool.Parse(tableItems[index].Check))
                            {   

                                CustomLoadView ldView7 = new CustomLoadView(parentViewController);
                                ldView7.Show();

                                System.Threading.ThreadPool.QueueUserWorkItem(state =>
                                    {   
                                        try
                                        {
                                            if (Utils.CheckForInternetConn())
                                            {
                                                bool success = ApiCalls.updateInvoiceChecked(Utils.UserToken, tableItems[index].Id);

                                                tableItems[index].Check = (!bool.Parse(tableItems[index].Check)).ToString();
                                                parentViewController.BeginInvokeOnMainThread(() =>
                                                    {    
                                                        using (UIImage imgDown = UIImage.FromBundle("selctedBox2.png"))
                                                        {
                                                            checkboxSender.SetBackgroundImage(imgDown, UIControlState.Normal);
                                                        }

                                                        if (success)
                                                        {
                                                            ToastView toast = new ToastView("Fattura è stato aggiornato con successo", 1800);
                                                            toast.Show();

                                                            //db.UpdateinvoicesInfo(tableItems[index]);
                                                        }
                                                        else
                                                        {
                                                            ToastView toast = new ToastView("Impossibile aggiornare Fatture", 1800);
                                                            toast.Show();
                                                        }
                                                    });
                                            }
                                            else
                                            {
                                                parentViewController.BeginInvokeOnMainThread(() =>
                                                    {
                                                        ToastView toast = new ToastView("Nessuna connessione Internet", 1800);
                                                        toast.Show();
                                                    });
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            Utils.writeToDeviceLog(ex);
                                        }
                                        finally
                                        {
                                            parentViewController.BeginInvokeOnMainThread(() =>
                                                {
                                                    if (ldView7 != null)
                                                    {
                                                        ldView7.Hide();
                                                    }
                                                });
                                        }
                                    });
                            }
                        }));

                    okAlertController.AddAction(UIAlertAction.Create("CANCEL", UIAlertActionStyle.Cancel, alert =>
                        {
                            //                                vh.CheckBill.Checked = !vh.CheckBill.Checked;
                            using (UIImage imgDown = UIImage.FromBundle("selctedBoxNe2.png"))
                            {
                                checkboxSender.SetBackgroundImage(imgDown, UIControlState.Normal);
                            }
                        }));

                    // Present Alert
                    parentViewController.PresentViewController(okAlertController, true, null);
                }

            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        void RemoveSubviews(UIView view)
        {
            try
            {
                foreach (UIView item in view.Subviews) {
                    item.RemoveFromSuperview();
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

    }
}