﻿using System;
using UIKit;
using CoreGraphics;
using Foundation;
using System.Globalization;
using RewindShared;


namespace RewindiOS
{
    public delegate void ModalPickerDimissedEventHandler(object sender, EventArgs e);
    public class ModalPickerViewController: UIViewController
    {
        public event ModalPickerDimissedEventHandler OnModalPickerDismissed;
        const float _headerBarHeight = 40;

        public UIColor HeaderBackgroundColor { get; set; }
        public UIColor HeaderTextColor { get; set; }
        public string HeaderText { get; set; }

        public UIDatePicker DatePicker { get; set; }
        public UIPickerView PickerView { get; set; }
        public UIPickerView PickerViewSecond { get; set; }
        private ModalPickerType _pickerType;
        public ModalPickerType PickerType 
        { 
            get { return _pickerType; }
            set
            {
                switch(value)
                {
                    case ModalPickerType.Date:
                        try
                        {
                            DatePicker = new UIDatePicker(new CGRect(0,0,this.View.Frame.Width,160));
						DatePicker.Locale = new NSLocale("no_nb");//NSLocale.CurrentLocale;
                            DatePicker.TimeZone = NSTimeZone.LocalTimeZone;

                            PickerView = null;
                            PickerViewSecond=null;
                        }
                        catch (Exception ex)
                        {
                            Utils.writeToDeviceLog(ex);
                        }

                        break;
                    case ModalPickerType.Custom:
                        DatePicker = null;
                        PickerView = new UIPickerView(new CGRect(0,0,this.View.Frame.Width/2,160));
                        PickerViewSecond = new UIPickerView(new CGRect(this.View.Frame.Width/2,0,this.View.Frame.Width/2,160));
                        break;
                    case ModalPickerType.YearOnly:
                        DatePicker = null;
                        PickerView = null;
                        PickerViewSecond = new UIPickerView(new CGRect(0,0,this.View.Frame.Width,160));
                        break;
                    default:
                        break;
                }

                _pickerType = value;
            }
        }

        UILabel _headerLabel;
        UIButton _doneButton;
        UIViewController _parent;
        UIView _internalView;

        string _currentHour;
        string _pickerHour;

        public ModalPickerViewController(ModalPickerType pickerType, string headerText, UIViewController parent,string currentHour, string pickerHour)
        {
            _currentHour = currentHour;
            HeaderBackgroundColor = UIColor.White;
            HeaderTextColor = UIColor.Black;
            HeaderText = headerText;
            PickerType = pickerType;
            _parent = parent;
            _pickerHour = pickerHour;

        }

        public ModalPickerViewController(ModalPickerType pickerType, string headerText, UIViewController parent,string currentHour)
        {
            _currentHour = currentHour;
            HeaderBackgroundColor = UIColor.White;
            HeaderTextColor = UIColor.Black;
            HeaderText = headerText;
            PickerType = pickerType;
            _parent = parent;
        }

        DateTime dateTimeFromScreen;
        public ModalPickerViewController(ModalPickerType pickerType, string headerText, UIViewController parent,DateTime _dateTimeFromScreen)
        {
            dateTimeFromScreen = _dateTimeFromScreen;
            HeaderBackgroundColor = UIColor.White;
            HeaderTextColor = UIColor.Black;
            HeaderText = headerText;
            PickerType = pickerType;
            _parent = parent;
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            //            InitializeControls();
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewDidAppear(animated);
            InitializeControls();
            Show();
        }

        void InitializeControls()
        {
            try
            {
                View.BackgroundColor = UIColor.FromRGBA(255, 255, 255, 20);
                _internalView = new UIView();

                _headerLabel = new UILabel(new CGRect(0, 0, 320/2, 44));
                _headerLabel.AutoresizingMask = UIViewAutoresizing.FlexibleWidth;
                _headerLabel.BackgroundColor = HeaderBackgroundColor;
                _headerLabel.TextColor = HeaderTextColor;
                _headerLabel.Text = HeaderText;

                _doneButton = UIButton.FromType(UIButtonType.Custom);
                _doneButton.SetTitleColor(HeaderTextColor, UIControlState.Normal);
                _doneButton.BackgroundColor = UIColor.Clear;
                _doneButton.SetTitle("  OK  ", UIControlState.Normal);
                _doneButton.TouchUpInside += DoneButtonTapped;

                DateTime dt = DateTime.Now;


                switch(PickerType)
                {
                    case ModalPickerType.Date:
                        if(dateTimeFromScreen>new DateTime(1900,1,1))
                        {
                            dt=dateTimeFromScreen;
                        }
                        else
                        {
                            if(_currentHour!="")
                            {
                                DateTime.TryParseExact(_currentHour, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dt);
                            }
                            else if(_pickerHour!="")
                            {
                                DateTime.TryParseExact(_pickerHour, "HH:mm", CultureInfo.CurrentCulture, DateTimeStyles.None, out dt);
                            }
                        }
//                        DateTime.TryParseExact(_currentHour, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dt);
                        break;
                    case ModalPickerType.Custom:
                        if(_currentHour!="")
                        {
                            DateTime.TryParseExact(_currentHour, "MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dt);
                        }
                        else if(_pickerHour!="")
                        {
                            DateTime.TryParseExact(_pickerHour, "HH:mm", CultureInfo.CurrentCulture, DateTimeStyles.None, out dt);
                        }
                       
                        break;
                }

//                if(_currentHour!="")
//                {
//                    dt = DateTime.ParseExact (_currentHour,"dd MMMMM yyyy HH:mm",null);
//                }
                //  "MM/dd/yyyy HH:mm", null);// "MM/dd/yyyy HH:mm", null
//                Foundation.NSDate nsDate = DateTimeToNSDate(dt); //DateTime.SpecifyKind(dt, DateTimeKind.Utc);
//                Foundation.NSDate sourceDate = DateTimeToNSDate(dt);
//
//                Foundation.NSTimeZone sourceTimeZone = Foundation.NSTimeZone.LocalTimeZone;//new Foundation.NSTimeZone ("GMT");//UTC
//                Foundation.NSTimeZone destinationTimeZone = Foundation.NSTimeZone.LocalTimeZone;
//
//                int sourceGMTOffset = (int)sourceTimeZone.SecondsFromGMT (sourceDate);
//                int destinationGMTOffset = (int)destinationTimeZone.SecondsFromGMT (sourceDate);
//                int interval = destinationGMTOffset - sourceGMTOffset;

//                var destinationDate = sourceDate.AddSeconds (interval);

                var dateTime = dt;//new DateTime(2001, 1, 1, 0, 0,0).AddSeconds(destinationDate.SecondsSinceReferenceDate);

                switch(PickerType)
                {
                    case ModalPickerType.Date:
                        //                        DatePicker.Frame = CGRect.Empty;
                        DatePicker.BackgroundColor = UIColor.White;
                        DatePicker.AutoresizingMask = UIViewAutoresizing.FlexibleWidth;

                        DatePicker.Locale = NSLocale.CurrentLocale;
//                        DatePicker.TimeZone = NSTimeZone.LocalTimeZone;

                        DatePicker.Date = DateTimeToNSDate(dateTime);
                        _internalView.Frame = new CGRect(_internalView.Frame.X,_internalView.Frame.Y,this.View.Frame.Width, DatePicker.Frame.Height + _headerBarHeight);
                        _internalView.AddSubview(DatePicker);
                        break;
                    case ModalPickerType.Custom:
                        PickerView.Frame = new CGRect(0,0,this.View.Frame.Width/2,160);
                        PickerView.AutoresizingMask= UIViewAutoresizing.None;
                        PickerViewSecond.AutoresizingMask= UIViewAutoresizing.None;
                        PickerView.BackgroundColor = UIColor.White;
                        PickerViewSecond.BackgroundColor = UIColor.White;

                        _internalView.Frame = new CGRect(_internalView.Frame.X,_internalView.Frame.Y, this.View.Frame.Width, PickerView.Frame.Height + _headerBarHeight);
                        _internalView.AddSubview(PickerView);
                        _internalView.AddSubview(PickerViewSecond);
                        PickerViewSecond.Select(dt.Year-1950, 0, true);
                        PickerView.Select(dt.Month-1,0,true);
                        break;
                    case ModalPickerType.YearOnly:
//                        PickerView.Frame = new CGRect(0,0,this.View.Frame.Width,160);
//                        PickerView.AutoresizingMask= UIViewAutoresizing.None;
                        PickerViewSecond.AutoresizingMask= UIViewAutoresizing.None;
//                        PickerView.BackgroundColor = UIColor.White;
                        PickerViewSecond.BackgroundColor = UIColor.White;

                        _internalView.Frame = new CGRect(_internalView.Frame.X,_internalView.Frame.Y, this.View.Frame.Width, PickerViewSecond.Frame.Height + _headerBarHeight);
//                        _internalView.AddSubview(PickerView);
                        _internalView.AddSubview(PickerViewSecond);
                        PickerViewSecond.Select(dt.Year-1950, 0, true);
//                        PickerView.Select(DateTime.Now.Month-1,0,true);
                        break;
                    default:
                        break;
                }
                _internalView.BackgroundColor = HeaderBackgroundColor;

                _internalView.AddSubview(_headerLabel);
                _internalView.AddSubview(_doneButton);

                Add(_internalView);
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }

        }

//        public static DateTime NSDateToDateTime(Foundation.NSDate date)
//        {
//            return (new DateTime(2001, 1, 1, 0, 0, 0)).AddSeconds(date.SecondsSinceReferenceDate);
//        }
//
//        public static Foundation.NSDate DateTimeToNSDate(DateTime date)
//        {
//            return Foundation.NSDate.FromTimeIntervalSinceReferenceDate((date - (new DateTime(2001, 1, 1, 0, 0, 0))).TotalSeconds);
//        }

        void Show(bool onRotate = false)
        {
            var doneButtonSize = new CGSize(71, 30);

            //            var width = UIApplication.SharedApplication.StatusBarOrientation == UIInterfaceOrientation.Portrait ? 
            //                _parent.View.Frame.Width : _parent.View.Frame.Height;
            var width = _parent.View.Frame.Width;

            var internalViewSize = CGSize.Empty;
            switch(_pickerType)
            {
                case ModalPickerType.Date:
                    //                    DatePicker.Frame = CGRect.Empty;
                    internalViewSize = new CGSize(width, DatePicker.Frame.Height + _headerBarHeight);
                    break;
                case ModalPickerType.Custom:
                    PickerView.Frame = new CGRect(0,0,this.View.Frame.Width/2,160);
                    PickerViewSecond.Frame =new CGRect(this.View.Frame.Width/2,0,this.View.Frame.Width/2,160);
                    //                    PickerView.Frame = CGRect.Empty;
                    internalViewSize = new CGSize(width, PickerView.Frame.Height + _headerBarHeight);
                    break;
                case ModalPickerType.YearOnly:
//                    PickerView.Frame = new CGRect(0,0,this.View.Frame.Width/2,160);
                    PickerViewSecond.Frame =new CGRect(0,0,this.View.Frame.Width,160);
                    //                    PickerView.Frame = CGRect.Empty;
                    internalViewSize = new CGSize(width, PickerViewSecond.Frame.Height + _headerBarHeight);
                    break;
                default:
                    break;
            }

            var internalViewFrame = CGRect.Empty;
            if (InterfaceOrientation == UIInterfaceOrientation.Portrait || InterfaceOrientation == UIInterfaceOrientation.PortraitUpsideDown)
            {
                if (onRotate)
                {
                    internalViewFrame = new CGRect(0, View.Frame.Height - internalViewSize.Height,
                        internalViewSize.Width, internalViewSize.Height);
                }
                else
                {
                    internalViewFrame = new CGRect(0, View.Bounds.Height - internalViewSize.Height,
                        internalViewSize.Width, internalViewSize.Height);
                }
            }
            else
            {
                if (onRotate)
                {
                    internalViewFrame = new CGRect(0, View.Frame.Width - internalViewSize.Height,
                        internalViewSize.Width, internalViewSize.Height);
                }
                else
                {
                    internalViewFrame = new CGRect(0, View.Bounds.Width - internalViewSize.Height,
                        internalViewSize.Width, internalViewSize.Height);
                }
            }
            _internalView.Frame = internalViewFrame;

            switch(_pickerType)
            {
                case ModalPickerType.Date:
                    DatePicker.Frame = new CGRect(DatePicker.Frame.X, _headerBarHeight, _internalView.Frame.Width,
                        DatePicker.Frame.Height);
                    break;
                case ModalPickerType.Custom:
                    PickerView.Frame = new CGRect(PickerView.Frame.X, _headerBarHeight, _internalView.Frame.Width/2,
                        PickerView.Frame.Height);
                    PickerViewSecond.Frame = new CGRect(PickerViewSecond.Frame.X, _headerBarHeight, _internalView.Frame.Width/2,
                        PickerViewSecond.Frame.Height);
                    break;
                case ModalPickerType.YearOnly:

                    PickerViewSecond.Frame = new CGRect(0, _headerBarHeight, _internalView.Frame.Width,
                        PickerViewSecond.Frame.Height);
                    break;
                default:
                    break;
            }

            _headerLabel.Frame = new CGRect(10, 4, _parent.View.Frame.Width - 100, 35);
            _doneButton.Frame = new CGRect(internalViewFrame.Width - doneButtonSize.Width - 10, 7, doneButtonSize.Width, doneButtonSize.Height);

        }

        void DoneButtonTapped (object sender, EventArgs e)
        {
            DismissViewController(true, null);
            if(OnModalPickerDismissed != null)
            {
                OnModalPickerDismissed(sender, e);
            }
        }

        public override void DidRotate(UIInterfaceOrientation fromInterfaceOrientation)
        {
            base.DidRotate(fromInterfaceOrientation);

            if (InterfaceOrientation == UIInterfaceOrientation.Portrait ||
                InterfaceOrientation == UIInterfaceOrientation.LandscapeLeft ||
                InterfaceOrientation == UIInterfaceOrientation.LandscapeRight ||
                InterfaceOrientation == UIInterfaceOrientation.PortraitUpsideDown)
            {
                Show(true);
                View.SetNeedsDisplay();
            }
        }

        public static NSDate DateTimeToNSDate(DateTime date)
        {
//            System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("it-IT");
            if (date.Kind == DateTimeKind.Unspecified)
                date = DateTime.SpecifyKind (date, DateTimeKind.Local);
            return (NSDate) date;
        }
    }

    public enum ModalPickerType
    {
        Date = 0,
        Custom = 1,
        YearOnly=2
    }
}