﻿using System;
using UIKit;


using Foundation;
using System.Collections.Generic;
using RewindShared;
using System.Globalization;

namespace RewindiOS
{
    public class AppuntamentiSearchTblSource:UITableViewSource
    {
        NSString cellHeaderIdentifier = new NSString("appointmentsSearchCell");

        List<AppuntamentiItem> tableItems;

        int endSection;
        int didSection;
        bool ifOpen;
        UITableView mainTable;
        float headerTblProductsHeight;
        float rowTblProductsHeight;
        int selIndex;

        AppuntamentiScreen mainController;


        public AppuntamentiSearchTblSource(List<AppuntamentiItem> items, UITableView _mainTable, float _headerTblProductsHeight, float _rowTblProductsHeight, AppuntamentiScreen _mainController, int _selIndex)
        {
            tableItems = items;
            didSection = items.Count + 1;
            mainTable = _mainTable;
            headerTblProductsHeight = _headerTblProductsHeight;
            rowTblProductsHeight = _rowTblProductsHeight;
            mainController = _mainController;
            selIndex = _selIndex;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return (int)tableItems.Count;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {

            appointmentsSearchCell cell = (appointmentsSearchCell)tableView.DequeueReusableCell(cellHeaderIdentifier);
            UIView view = new UIView();
            try
            {
                int index = indexPath.Row;
                //view = cell.ContentView;

                using (UIFont fnt = UIFont.FromName("Oswald-Regular", 18.0f))
                {
                    cell.LblSearchRegSociale.Font = fnt;
                }

                using (UIFont fnt = UIFont.FromName("Oswald-Regular", 15.0f))
                {
                    cell.LblSearchStatus.Font = fnt;
                }

                using (UIFont fnt = UIFont.FromName("LucidaSans", 10.0f))
                {
                    cell.LblSearchDate.Font = fnt;
                }
                
                using (UIFont fnt = UIFont.FromName("LucidaSans", 20.0f))
                {
                    cell.LblSearchHour.Font = fnt;
                }
                
                using (UIFont fnt = UIFont.FromName("LucidaSans", 13.0f))
                {
                    cell.LblSearchMinutes.Font = fnt;
                }

//                cell.LblSearchRegSociale.Text = tableItems[indexPath.Row].RegSociale;
                if(tableItems[indexPath.Row].AppBloccati)
                {
					cell.LblSearchRegSociale.Text = "STOP APPUNTAMENTI";
                }
                else
                {
                    if(tableItems[indexPath.Row].Id_appto_memo!="")
                    {
                        cell.LblSearchRegSociale.Text ="MEMO";
                    }
                    else
                    {
                        if(tableItems[indexPath.Row].TipAppuntamenti=="business")
                        {
                            cell.LblSearchRegSociale.Text = tableItems[indexPath.Row].RegSociale;
                        }
                        else
                        {
                            cell.LblSearchRegSociale.Text = "IMPEGNO PERSONALE";
                        }
                    }

                }
                cell.LblSearchRegSociale.Lines=1;

                cell.LblSearchDate.Text = tableItems[indexPath.Row].AppuntamentiDateHour.Date.ToString("dd-MM-yy", CultureInfo.InvariantCulture);
                cell.LblSearchHour.Text = tableItems[indexPath.Row].AppuntamentiDateHour.ToString("HH", CultureInfo.InvariantCulture);
                cell.LblSearchMinutes.Text = tableItems[indexPath.Row].AppuntamentiDateHour.ToString("mm", CultureInfo.InvariantCulture);
                cell.LblSearchStatus.Text=System.Net.WebUtility.HtmlDecode(tableItems[indexPath.Row].Status);
                cell.ImgSearchArrow.BackgroundColor = UIColor.White;
                cell.ViewSearchDateHourSeparator.BackgroundColor = UIColor.Clear;
                cell.ViewSearchRegSocStatus.BackgroundColor = UIColor.White;
                cell.LblSearchRegSociale.TextAlignment= UITextAlignment.Left;
                cell.LblSearchStatus.TextAlignment= UITextAlignment.Left;

                cell.ImgSearchArrow.Hidden = true;
                cell.ViewSearchDateHourSeparator.Hidden = true;
                cell.ViewSearchRegSocStatus.Hidden=false;
                cell.LblSearchRegSociale.Hidden = false;
                cell.LblSearchStatus.Hidden = false;
                cell.ViewSearchDateHour.BackgroundColor = UIColor.FromRGB(0, 95, 167);
//                cell.ViewSearchSeparatorCell.BackgroundColor=UIColor.Red;

                if (selIndex != -1)
                {
//                    cell.BackgroundColor = UIColor.Yellow;
                    cell.LblSearchRegSociale.Hidden = true;
                    cell.LblSearchStatus.Hidden = true;
                    cell.ViewSearchRegSocStatus.Hidden=true;
                    view.Tag = indexPath.Row;
                    cell.ViewSearchDateHourSeparator.Hidden = false;
                    cell.ViewSearchDateHourSeparator.BackgroundColor = UIColor.FromRGB(76, 76, 76);
                    cell.ViewSearchSeparatorCell.Frame = new CoreGraphics.CGRect(cell.ViewSearchSeparatorCell.Frame.X, cell.ViewSearchSeparatorCell.Frame.Y, 72, cell.ViewSearchSeparatorCell.Frame.Height);

                    if (selIndex == indexPath.Row)
                    {
                        cell.ViewSearchDateHour.BackgroundColor = UIColor.FromRGB(76, 76, 76);
                        cell.ImgSearchArrow.BackgroundColor = UIColor.Clear;
                        cell.ImgSearchArrow.Hidden = false;
                    }
                    else
                    {
                        //first time
                        cell.ViewSearchDateHour.BackgroundColor = UIColor.FromRGB(0, 95, 167);
                        cell.ImgSearchArrow.BackgroundColor = UIColor.Clear;
                        cell.ImgSearchArrow.Hidden = true;
                    }
                }
                else
                {
                    cell.LblSearchRegSociale.Hidden = false;
                }
            }
            catch (Exception ex)
            {
                Utils.writeToIOSDeviceLog(ex);
            }
            return cell;
        }

        public override nint NumberOfSections(UITableView tableView)
        {
            return 1;
        }

        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            return headerTblProductsHeight;
        }

        public override nfloat GetHeightForHeader(UITableView tableView, nint section)
        {
            return 0;
        }

        public override UIView GetViewForHeader(UITableView tableView, nint section)
        {
            return null;
        }

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            try
            {
                tableView.DeselectRow(indexPath, true);
                mainController.OnRowFromListClickSearch(indexPath.Row);
            }
            catch (Exception ex)
            {
                Utils.writeToIOSDeviceLog(ex);
            }
        }
    }
}