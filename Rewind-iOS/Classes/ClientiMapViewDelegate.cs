﻿using System;
using MapKit;
using UIKit;
using RewindShared;
using System.Drawing;

namespace RewindiOS
{
    public class ClientiMapViewDelegate : MKMapViewDelegate
    {
        ClientiScreen mainController;

        public ClientiMapViewDelegate(ClientiScreen _mainController)
        {
            mainController = _mainController;
        }

        string pId = "PinAnnotation";
        UIButton showCourseButton;

        public override MKAnnotationView GetViewForAnnotation(MKMapView mapView, IMKAnnotation annotation)
        {
            MKAnnotationView anView = null;
            try
            {
                if (annotation is MKUserLocation)
                    return null; 

                //                if (mapView.DequeueReusableAnnotation(pId) is MKPinAnnotationView)
                //                {
                //                   // anView = (MKPinAnnotationView)mapView.DequeueReusableAnnotation(pId);
                //                }
                //                else if (mapView.DequeueReusableAnnotation(pId) is MKAnnotationView)
                //                {
                //                    anView = (MKAnnotationView)mapView.DequeueReusableAnnotation (pId);
                //                }
                anView = (MKAnnotationView)mapView.DequeueReusableAnnotation(pId);
                if (anView != null)
                {
                    anView.Dispose();
                    anView = null;
                }

                if (anView == null)
                    anView = new MKPinAnnotationView(annotation, pId);


                if (annotation is CustomAnnotation)
                {
                    var pointAnnotation = new MKAnnotationView(annotation, pId);

                    pointAnnotation.CanShowCallout = true;
                    pointAnnotation.RightCalloutAccessoryView = UIButton.FromType(UIButtonType.DetailDisclosure);  
                    pointAnnotation.Image = GetImage(((CustomAnnotation)annotation).ImageName);
                    pointAnnotation.RightCalloutAccessoryView .Hidden=true;
                    anView = pointAnnotation;
                }
                else if (annotation is AppointmentsAnnotation)
                {
                    var pointAnnotation = new MKAnnotationView(annotation, pId);

                    UIView viewAnnOggi = new UIView();
                    viewAnnOggi.Frame = new CoreGraphics.CGRect(0, 0, 34, 40);
                    UIImageView imgBg = new UIImageView();
                    imgBg.Frame = new CoreGraphics.CGRect(0, 0, viewAnnOggi.Frame.Width, viewAnnOggi.Frame.Height);
                    imgBg.BackgroundColor = UIColor.Clear;
                    imgBg.ContentMode = UIViewContentMode.ScaleAspectFit;
                    using (UIImage img = UIImage.FromBundle("pin_map_ios.png"))
                    {
                        imgBg.Image = img;
                    }
                    viewAnnOggi.Add(imgBg);

                    UILabel lblHour = new UILabel();
                    lblHour.Frame = new CoreGraphics.CGRect(12, 5, 20, 15);
                    lblHour.TextAlignment = UITextAlignment.Center;
                    lblHour.TextColor = UIColor.FromRGB(0, 95, 167);
                    lblHour.Text = ((AppointmentsAnnotation)annotation).HourMinutes;

                    using (UIFont fnt = UIFont.FromName("Oswald-Light", 8.0f))
                    {
                        lblHour.Font = fnt;
                    }
                    viewAnnOggi.Add(lblHour);

                    pointAnnotation.CanShowCallout = true;
                    pointAnnotation.RightCalloutAccessoryView = UIButton.FromType(UIButtonType.DetailDisclosure);  
                    pointAnnotation.Selected = true;

                    UIButton btnPinMap = new UIButton();
                    btnPinMap.Frame = new CoreGraphics.CGRect(0, 0, viewAnnOggi.Frame.Width, viewAnnOggi.Frame.Height);
                    btnPinMap.BackgroundColor = UIColor.Clear;
                    // btnPinMap.ButtonType= UIButtonType.Custom;
                    btnPinMap.SetTitle("", UIControlState.Normal);
                    viewAnnOggi.Add(btnPinMap);

                    pointAnnotation.Image = imageFromView(viewAnnOggi);
                    anView = pointAnnotation;
                }
                else
                {
                    anView = null;
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
            return anView;
        }


        public UIImage GetImage(string imageName)
        {
            UIImage image = null;
            try
            {
                image = UIImage.FromBundle(imageName).Scale(new SizeF() { Height = 40, Width = 30 });
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
            return image;
        }

        public override void CalloutAccessoryControlTapped(MKMapView mapView, MKAnnotationView view, UIControl control)
        {
            try
            {
                //mainController.ShowCourse(view.Annotation.Coordinate,view);
                if (((CustomAnnotation)view.Annotation).TypeAnnotation == "clientiscreen")
                {
                    //go to client selected details
                    mainController.ClientDetailsMapPinClick(((CustomAnnotation)view.Annotation).Coordinate.Latitude, ((CustomAnnotation)view.Annotation).Coordinate.Longitude);
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }


        public override void DidDeselectAnnotationView(MKMapView mapView, MKAnnotationView view)
        {
            // throw new System.NotImplementedException ();
        }

        public UIImage imageFromView(UIView view)
        {
            UIImage image = null;
            try
            {
                UIGraphics.BeginImageContext(new SizeF() { Height = 40, Width = 34 });
                view.Layer.RenderInContext(UIGraphics.GetCurrentContext());
                image = UIGraphics.GetImageFromCurrentImageContext();
                UIGraphics.EndImageContext();
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
            return image;
        }


        public override MKOverlayView GetViewForOverlay(MKMapView mapView, IMKOverlay overlay)
        {           
            if (overlay.GetType() == typeof(MKPolyline))
            {
                MKPolylineView pLineView = new MKPolylineView((MKPolyline)overlay);

                pLineView.LineWidth = 2;
                pLineView.StrokeColor = UIColor.FromRGB(242, 113, 48);

                return pLineView;
            }
            return null;
        }
    }
}