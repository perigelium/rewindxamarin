﻿using System;
using System.Collections.Generic;

using UIKit;
using Foundation;
using RewindShared;

namespace RewindiOS
{
    public class ProfiloBonusTblSource : UITableViewSource
    {
        NSString cellHeaderIdentifier = new NSString("profiloBonusCell");
        List<BonusItem> tableItems;

        public ProfiloBonusTblSource(List<BonusItem> tableItems)
        {
            this.tableItems = tableItems;
        }

        #region implemented abstract members of UITableViewSource

        public override UITableViewCell GetCell(UITableView tableView, Foundation.NSIndexPath indexPath)
        {
            profiloBonusCell cell = (profiloBonusCell)tableView.DequeueReusableCell(cellHeaderIdentifier);
            UIView view = new UIView();

            try
            {
                int index = indexPath.Row;
                view = cell.ContentView;

                using (UIFont fnt = UIFont.FromName("Oswald-Regular", 12.0f))
                {
                    cell.LblBonusCell.Font = fnt;
                    cell.LblProdottoCell.Font = fnt;
                    cell.LblRagioneSocialeCel.Font = fnt;
                }

                cell.LblRagioneSocialeCel.Text = tableItems[index].Regionalesociale;
                cell.LblProdottoCell.Text = tableItems[index].Prodato;
                cell.LblBonusCell.Text = tableItems[index].Bonus.ToString("0.00")+" €";
            }
            catch (Exception ex)
            {
                Utils.writeToIOSDeviceLog(ex);
            }

            return cell;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            if (tableItems != null)
            {
                return tableItems.Count;
            }
            
            return 0;
        }

        public override nint NumberOfSections(UITableView tableView)
        {
            return 1;
        }

        #endregion
    }
}

