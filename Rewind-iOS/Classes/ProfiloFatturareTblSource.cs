﻿using System;
using System.Collections.Generic;

using UIKit;
using Foundation;

using RewindShared;
using System.Globalization;
using System.Linq;

namespace RewindiOS
{
    public class ProfiloFatturareTblSource : UITableViewSource
    {
        NSString cellHeaderIdentifier = new NSString("profiloFatturareCell");
        List<FatturarePeriodItem> tableItems;
        int selectedCellIndex = -1;
        UITableView theTable;
        List<int> expandedRows;
        UIViewController parentViewController;

        bool fromGetCell = false;

        float EXPANDED_CELL_HEIGHT = 130f;
        float NORMAL_CELL_HEIGHT = 100f;

        UserLocalDB db;

        public ProfiloFatturareTblSource(List<FatturarePeriodItem> tableItems, UITableView theTable, UIViewController parentViewController)
        {
            this.tableItems = tableItems;

            this.theTable = theTable;
            expandedRows = new List<int>();
            this.parentViewController = parentViewController;

            db = new UserLocalDB();
        }

        #region implemented abstract members of UITableViewSource

        public override UITableViewCell GetCell(UITableView tableView, Foundation.NSIndexPath indexPath)
        {
            profiloFatturareCell cell = (profiloFatturareCell)tableView.DequeueReusableCell(cellHeaderIdentifier);
            UIView view = new UIView();

            try
            {
                cell.AutoresizingMask= UIViewAutoresizing.All;
                cell.AutosizesSubviews=true;
                fromGetCell = true;
                int index = indexPath.Row;
                view = cell.ContentView;

                using (UIFont fnt = UIFont.FromName("Oswald-Regular", 16.0f))
                {
                    cell.LblFatturaTitle.Font = fnt;
                    cell.LblFatturaPrice.Font = fnt;
                    cell.LblFatturaStatus.Font = fnt;
                }

                DateTime fattDateName=DateTime.Now;
                if(tableItems[indexPath.Row].Period!=null && tableItems[indexPath.Row].Period!="")
                {
                    fattDateName=new DateTime(Int32.Parse(tableItems[indexPath.Row].Period.Substring(0,4)),Int32.Parse(tableItems[indexPath.Row].Period.Substring(4,2)),2);
                }
                System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("it-IT");
                cell.LblFatturaTitle.Text = "FATTURA "+fattDateName.ToString("MMMMM", CultureInfo.CurrentCulture).ToUpper() +" "+ fattDateName.Year;
                //cell.LblFatturaStatus.Text = tableItems[index].Status;

                if (bool.Parse(tableItems[index].Check) == true)
                {
                    using (UIImage imgDown = UIImage.FromBundle("selctedBox2.png"))
                    {
                        cell.BtnCheckBox.SetBackgroundImage(imgDown, UIControlState.Normal);
                    }
                }
                else
                {
                    using (UIImage imgDown = UIImage.FromBundle("selctedBoxNe2.png"))
                    {
                        cell.BtnCheckBox.SetBackgroundImage(imgDown, UIControlState.Normal);
                    }
                }
                EXPANDED_CELL_HEIGHT=100;
//                RemoveSubviews(cell.ViewFatturaDetails);
                if (selectedCellIndex != -1)
                {
                    if (indexPath.Row == selectedCellIndex)
                    {
                            if (expandedRows.Contains(indexPath.Row))
                            {
////                                cell.LblFatturaStatus.Hidden = true;      
                                EXPANDED_CELL_HEIGHT=100;
                                RemoveSubviews(cell.ViewFatturaDetails);
                                cell.ViewFatturaDetails.Frame=new CoreGraphics.CGRect(cell.ViewFatturaDetails.Frame.X,cell.ViewFatturaDetails.Frame.Y,cell.ViewFatturaDetails.Frame.Width,0);

                            }
                            else
                            {
//                                cell.LblFatturaStatus.Hidden = false;
                                RemoveSubviews(cell.ViewFatturaDetails);
                                float y=0;
                                foreach (var itemFat in tableItems[indexPath.Row].Facturi) 
                                {
                                    
                                    UILabel lblGroupName=new UILabel();
                                    lblGroupName.Frame=new CoreGraphics.CGRect(0,y,cell.ViewFatturaDetails.Frame.Width,20);
                                    lblGroupName.AutoresizingMask= UIViewAutoresizing.None;
                                    lblGroupName.TextAlignment= UITextAlignment.Left;
                                    lblGroupName.TextColor=UIColor.Black;
                                    lblGroupName.Text=itemFat.Keys.ToList()[0];
                                    using (UIFont fnt = UIFont.FromName("Oswald-Regular", 13.0f))
                                    {
                                        lblGroupName.Font = fnt;
                                    }
                                    cell.ViewFatturaDetails.Add(lblGroupName);

                                    UIView viewGroupFactureNames=new UIView();
                                    viewGroupFactureNames.Frame=new CoreGraphics.CGRect(10,y+20,cell.ViewFatturaDetails.Frame.Width-20,itemFat.Values.ToList()[0].Count*20);
                                    viewGroupFactureNames.AutoresizingMask= UIViewAutoresizing.None;
                                    cell.ViewFatturaDetails.Add(viewGroupFactureNames);

                                    float yz=0;
                                    foreach (var itemFatName in itemFat.Values.ToList()[0]) 
                                    {
                                        UILabel lblFattName=new UILabel();
                                        lblFattName.Frame=new CoreGraphics.CGRect(0,yz,viewGroupFactureNames.Frame.Width-50,20);
                                        lblFattName.AutoresizingMask= UIViewAutoresizing.None;
                                        lblFattName.TextAlignment= UITextAlignment.Left;
                                        lblFattName.TextColor=UIColor.Gray;
                                        lblFattName.Text=itemFatName.Title_invoice;
                                        using (UIFont fnt = UIFont.FromName("Oswald-Regular", 11.0f))
                                        {
                                            lblFattName.Font = fnt;
                                        }
                                        viewGroupFactureNames.Add(lblFattName);

                                    UILabel lblFattPrice=new UILabel();
                                    lblFattPrice.Frame=new CoreGraphics.CGRect(lblFattName.Frame.X+lblFattName.Frame.Width,yz,50,20);
                                    lblFattPrice.AutoresizingMask= UIViewAutoresizing.None;
                                    lblFattPrice.TextAlignment= UITextAlignment.Right;
                                    lblFattPrice.TextColor=UIColor.Gray;
                                    lblFattPrice.Text=itemFatName.Total.ToString("0.00")+" €";
                                    using (UIFont fnt = UIFont.FromName("Oswald-Regular", 11.0f))
                                    {
                                        lblFattPrice.Font = fnt;
                                    }
                                    viewGroupFactureNames.Add(lblFattPrice);

                                        yz+=20;
                                    }
                                    y+=(float)viewGroupFactureNames.Frame.Height+20;
                                    //txtFattPrice.Text=itemFatName.Total+"€";TODO
                                }
                                cell.ViewFatturaDetails.Frame=new CoreGraphics.CGRect(cell.ViewFatturaDetails.Frame.X,cell.ViewFatturaDetails.Frame.Y,cell.ViewFatturaDetails.Frame.Width,y+20);
                                EXPANDED_CELL_HEIGHT+=(float)cell.ViewFatturaDetails.Frame.Height;

                                using (UIImage imgUp = UIImage.FromBundle("arrowBlueUp.png"))
                                {
                                    cell.ImgFatturaArrow.Image = imgUp;
                                }
//                            cell.Frame=new CoreGraphics.CGRect(cell.Frame.X,cell.Frame.Y,cell.Frame.Width,cell.Frame.Height+EXPANDED_CELL_HEIGHT);
                            }
                    }
                }
                else if (expandedRows.Contains(indexPath.Row))
                {
//                    cell.LblFatturaStatus.Hidden = false;
//                    RemoveSubviews(cell.ViewFatturaDetails);
//                    cell.ViewFatturaDetails.Frame=new CoreGraphics.CGRect(cell.ViewFatturaDetails.Frame.X,cell.ViewFatturaDetails.Frame.Y,cell.ViewFatturaDetails.Frame.Width,cell.ViewFatturaDetails.Frame.Height);
//                    EXPANDED_CELL_HEIGHT+=(float)cell.ViewFatturaDetails.Frame.Height;

                    RemoveSubviews(cell.ViewFatturaDetails);
                    float y=0;
                    foreach (var itemFat in tableItems[indexPath.Row].Facturi) 
                    {

                        UILabel lblGroupName=new UILabel();
                        lblGroupName.Frame=new CoreGraphics.CGRect(0,y,cell.ViewFatturaDetails.Frame.Width,20);
                        lblGroupName.AutoresizingMask= UIViewAutoresizing.None;
                        lblGroupName.TextAlignment= UITextAlignment.Left;
                        lblGroupName.TextColor=UIColor.Black;
                        lblGroupName.Text=itemFat.Keys.ToList()[0];
                        using (UIFont fnt = UIFont.FromName("Oswald-Regular", 13.0f))
                        {
                            lblGroupName.Font = fnt;
                        }
                        cell.ViewFatturaDetails.Add(lblGroupName);

                        UIView viewGroupFactureNames=new UIView();
                        viewGroupFactureNames.Frame=new CoreGraphics.CGRect(10,y+20,cell.ViewFatturaDetails.Frame.Width-20,itemFat.Values.ToList()[0].Count*20);
                        viewGroupFactureNames.AutoresizingMask= UIViewAutoresizing.None;
                        cell.ViewFatturaDetails.Add(viewGroupFactureNames);

                        float yz=0;
                        foreach (var itemFatName in itemFat.Values.ToList()[0]) 
                        {
                            UILabel lblFattName=new UILabel();
                            lblFattName.Frame=new CoreGraphics.CGRect(0,yz,viewGroupFactureNames.Frame.Width-50,20);
                            lblFattName.AutoresizingMask= UIViewAutoresizing.None;
                            lblFattName.TextAlignment= UITextAlignment.Left;
                            lblFattName.TextColor=UIColor.Gray;
                            lblFattName.Text=itemFatName.Title_invoice;
                            using (UIFont fnt = UIFont.FromName("Oswald-Regular", 11.0f))
                            {
                                lblFattName.Font = fnt;
                            }
                            viewGroupFactureNames.Add(lblFattName);

                            UILabel lblFattPrice=new UILabel();
                            lblFattPrice.Frame=new CoreGraphics.CGRect(lblFattName.Frame.X+lblFattName.Frame.Width,yz,50,20);
                            lblFattPrice.AutoresizingMask= UIViewAutoresizing.None;
                            lblFattPrice.TextAlignment= UITextAlignment.Right;
                            lblFattPrice.TextColor=UIColor.Gray;
                            lblFattPrice.Text=itemFatName.Total.ToString("0.00")+ " €";
                            using (UIFont fnt = UIFont.FromName("Oswald-Regular", 11.0f))
                            {
                                lblFattPrice.Font = fnt;
                            }
                            viewGroupFactureNames.Add(lblFattPrice);

                            yz+=20;
                        }
                        y+=(float)viewGroupFactureNames.Frame.Height+20;
                        //txtFattPrice.Text=itemFatName.Total+"€";TODO
                    }
                    cell.ViewFatturaDetails.Frame=new CoreGraphics.CGRect(cell.ViewFatturaDetails.Frame.X,cell.ViewFatturaDetails.Frame.Y,cell.ViewFatturaDetails.Frame.Width,y+20);
                    EXPANDED_CELL_HEIGHT+=(float)cell.ViewFatturaDetails.Frame.Height;

                }
                else
                {
                    EXPANDED_CELL_HEIGHT=100;
                    RemoveSubviews(cell.ViewFatturaDetails);
                    cell.ViewFatturaDetails.Frame=new CoreGraphics.CGRect(cell.ViewFatturaDetails.Frame.X,cell.ViewFatturaDetails.Frame.Y,cell.ViewFatturaDetails.Frame.Width,0);
                    cell.LblFatturaStatus.Hidden = true;     
                }

                string totalString = tableItems[index].Total.ToString();
                double totalDouble;
                double.TryParse(totalString, NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingSign, CultureInfo.InvariantCulture, out totalDouble);

                cell.BtnFatturaExpand.Tag = index;
                cell.BtnFatturaExpand.TouchUpInside -= Cell_BtnFatturaExpand_TouchUpInside;
                cell.BtnFatturaExpand.TouchUpInside += Cell_BtnFatturaExpand_TouchUpInside;

                cell.BtnCheckBox.Tag = index;
                cell.BtnCheckBox.TouchUpInside -= Cell_BtnCheckBox_TouchUpInside;
                cell.BtnCheckBox.TouchUpInside += Cell_BtnCheckBox_TouchUpInside;

//                cell.LblFatturaPrice.Text = totalDouble.ToString("0.00") + " €";
                cell.LblFatturaPrice.Text=tableItems[index].Total.ToString("0.00") + " €";
                cell.ImgFatturaArrow.Hidden=true;
            }
            catch (Exception ex)
            {
                Utils.writeToIOSDeviceLog(ex);
            }

            return cell;
        }

        void Cell_BtnCheckBox_TouchUpInside(object sender, EventArgs e)
        {
            try
            {
                UIButton checkboxSender = (UIButton)sender;
                int index = (int)checkboxSender.Tag;

                if (bool.Parse(tableItems[index].Check) == false)//false
                {
                    var okAlertController = UIAlertController.Create("Attenzione", "Sei sicuro di voler accettare questa pre-fattura?", UIAlertControllerStyle.Alert);
                    okAlertController.AddAction(UIAlertAction.Create("SI", UIAlertActionStyle.Default, alert =>
                            {
//                                if (boxSender.Checked != bool.Parse(tableItems[index].Check))
                                {   

                                    CustomLoadView ldView7 = new CustomLoadView(parentViewController);
                                    ldView7.Show();

                                    System.Threading.ThreadPool.QueueUserWorkItem(state =>
                                        {   
                                            try
                                            {
                                                if (Utils.CheckForInternetConn())
                                                {
                                                    bool success = ApiCalls.updateInvoiceChecked(Utils.UserToken, tableItems[index].Id);

                                                    tableItems[index].Check = (!bool.Parse(tableItems[index].Check)).ToString();
                                                    parentViewController.BeginInvokeOnMainThread(() =>
                                                        {    
                                                            using (UIImage imgDown = UIImage.FromBundle("selctedBox2.png"))
                                                            {
                                                                checkboxSender.SetBackgroundImage(imgDown, UIControlState.Normal);
                                                            }

                                                            if (success)
                                                            {
                                                                ToastView toast = new ToastView("Fattura è stato aggiornato con successo", 1800);
                                                                toast.Show();

                                                                foreach (var itemFat in tableItems[index].Facturi) 
                                                                {
                                                                    foreach (var itemFatName in itemFat.Values.ToList()[0]) 
                                                                    {
                                                                        itemFatName.Check= tableItems[index].Check;
                                                                        db.UpdateinvoicesInfo(itemFatName);
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                ToastView toast = new ToastView("Impossibile aggiornare Fatture", 1800);
                                                                toast.Show();
                                                            }
                                                        });
                                                }
                                                else
                                                {
                                                    parentViewController.BeginInvokeOnMainThread(() =>
                                                        {
                                                            ToastView toast = new ToastView("Nessuna connessione Internet", 1800);
                                                            toast.Show();
                                                        });
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                Utils.writeToDeviceLog(ex);
                                            }
                                            finally
                                            {
                                                parentViewController.BeginInvokeOnMainThread(() =>
                                                    {
                                                        if (ldView7 != null)
                                                        {
                                                            ldView7.Hide();
                                                        }
                                                    });
                                            }
                                        });
                                }
                            }));

                    okAlertController.AddAction(UIAlertAction.Create("CANCELLA", UIAlertActionStyle.Cancel, alert =>
                            {
//                                vh.CheckBill.Checked = !vh.CheckBill.Checked;
                                using (UIImage imgDown = UIImage.FromBundle("selctedBoxNe2.png"))
                                {
                                    checkboxSender.SetBackgroundImage(imgDown, UIControlState.Normal);
                                }
                            }));


                    // Present Alert
                    parentViewController.PresentViewController(okAlertController, true, null);
                }
//                else
//                {
//                    using (UIImage imgDown = UIImage.FromBundle("selctedBox2.png"))
//                    {
//                        checkboxSender.SetBackgroundImage(imgDown, UIControlState.Normal);
//                    }
//                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        void Cell_BtnFatturaExpand_TouchUpInside(object sender, EventArgs e)
        {
            try
            {
                UIButton btn = (UIButton)sender;

                this.selectedCellIndex = (int)btn.Tag;

                NSIndexPath[] rowsToReload = new NSIndexPath[]{ NSIndexPath.FromRowSection(btn.Tag, 0) };

                this.theTable.ReloadRows(rowsToReload, UITableViewRowAnimation.None);

                if (selectedCellIndex == tableItems.Count - 1)
                {
                    theTable.ScrollToRow(NSIndexPath.FromRowSection(selectedCellIndex, 0), UITableViewScrollPosition.Bottom, true);
                }

                this.selectedCellIndex = -1;
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            if (tableItems != null)
            {
                return tableItems.Count;
            }

            return 0;
        }

        public override nint NumberOfSections(UITableView tableView)
        {
            return 1;
        }

        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            try
            {
                if (fromGetCell == true)
                {
                    fromGetCell = false;

                    if (selectedCellIndex != -1)
                    {
                        if (indexPath.Row == selectedCellIndex)
                        {
                            if (expandedRows.Contains(selectedCellIndex))
                            {
                                expandedRows.Remove(selectedCellIndex);
                            }
                            else
                            {
                                expandedRows.Add(selectedCellIndex);
                            }
                        }
                    }
                }

                if (expandedRows.Contains(indexPath.Row))
                {
                    EXPANDED_CELL_HEIGHT=0;
                    foreach (var itemFat in tableItems[indexPath.Row].Facturi)
                    {
//                        foreach (var itemFatName in itemFat.Values.ToList()[0])
//                        {
//
//                        }
                        EXPANDED_CELL_HEIGHT+=itemFat.Values.ToList()[0].Count*20+20;
                    }
                   return EXPANDED_CELL_HEIGHT+=100;
                }
                else
                {
                    return NORMAL_CELL_HEIGHT;
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }

            return NORMAL_CELL_HEIGHT;
        }

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            try
            {
                tableView.DeselectRow(indexPath, true);
            }
            catch (Exception ex)
            {
                Utils.writeToIOSDeviceLog(ex);
            }
        }

        void RemoveSubviews(UIView view)
        {
            try
            {
                foreach (UIView item in view.Subviews) {
                    item.RemoveFromSuperview();
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }
        #endregion
    }
}
