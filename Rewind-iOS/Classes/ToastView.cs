﻿using System;
using CoreGraphics;
using UIKit;
using Foundation;
using RewindShared;

namespace RewindiOS
{
    public enum ToastGravity
    {
        Top = 0,
        Bottom = 1,
        Center = 2
    }

    public class ToastSettings
    {
        public ToastSettings ()
        {
            this.Duration = 500;
            this.Gravity = ToastGravity.Center;
        }

        public int Duration
        {
            get;
            set;
        }

        public double DurationSeconds
        {
            get { return (double) Duration/1000 ;}

        }

        public ToastGravity Gravity
        {
            get;
            set;
        }

        public CGPoint Position
        {
            get;
            set;
        }


    }
    public class ToastView: NSObject
    {

        ToastSettings theSettings = new ToastSettings ();

        private string text = null;
        UIView view;
        public ToastView (string Text, int durationMilliseonds)
        {
            text = Text;
            theSettings.Duration = durationMilliseonds;
        }

        int offsetLeft = 0;
        int offsetTop = 0;
        public ToastView SetGravity (ToastGravity gravity, int OffSetLeft, int OffSetTop)
        {
            theSettings.Gravity = gravity;
            offsetLeft = OffSetLeft;
            offsetTop = OffSetTop;
            return this;
        }

        public ToastView SetPosition (CGPoint Position)
        {
            theSettings.Position = Position;
            return this;
        }

        public void Show ()
        {
            try{

                UIButton v = UIButton.FromType (UIButtonType.Custom);
                //  });
                view = v;

                UIFont font = UIFont.SystemFontOfSize (16);
                //public static CGSize StringSize (this NSString This, UIFont font, nfloat forWidth, UILineBreakMode breakMode);
                //            CGSize textSize = view.StringSize (text, font, new CGSize (280, 60));
                CGSize textSize = new CGSize(280,60);
                UILabel label = new UILabel (new CGRect (0, 0, textSize.Width + 5, textSize.Height + 5));
                label.BackgroundColor = UIColor.Clear;
                label.TextColor = UIColor.White;
                label.Font = font;
                label.Text = text;
                label.Lines = 0;
                label.ShadowColor = UIColor.DarkGray;
                label.ShadowOffset = new CGSize (1, 1);
                label.TextAlignment = UITextAlignment.Center;

                v.Frame = new CGRect (0, 0, textSize.Width + 10, textSize.Height + 10);
                label.Center = new CGPoint (v.Frame.Size.Width / 2, v.Frame.Height / 2);
                v.AddSubview (label);

                v.BackgroundColor = UIColor.FromRGBA (0, 0, 0, 0.7f);
                v.Layer.CornerRadius = 5;

                UIWindow window = UIApplication.SharedApplication.Windows[0];

                CGPoint point = new CGPoint (window.Frame.Size.Width / 2, window.Frame.Size.Height / 2);

                if (theSettings.Gravity == ToastGravity.Top)
                {
                    point = new CGPoint (window.Frame.Size.Width / 2, 45);
                }
                else if (theSettings.Gravity == ToastGravity.Bottom)
                {
                    point = new CGPoint (window.Frame.Size.Width / 2, window.Frame.Size.Height - 45);
                }
                else if (theSettings.Gravity == ToastGravity.Center)
                {
                    point = new CGPoint (window.Frame.Size.Width / 2, window.Frame.Size.Height / 2);
                }
                else
                {
                    point = theSettings.Position;
                }

                point = new CGPoint (point.X + offsetLeft, point.Y + offsetTop);
                v.Center = point;
                window.AddSubview (v);
                v.AllTouchEvents += delegate { HideToast (); };

                Action<NSTimer> test=new Action<NSTimer>(s=>HideToast());

                NSTimer.CreateScheduledTimer (theSettings.DurationSeconds, test);
            }
            catch(Exception ex)
            {
                Utils.writeToDeviceLog (ex);
            }
        }


        void HideToast ()
        {
            UIView.BeginAnimations ("");
            view.Alpha = 0;
            UIView.CommitAnimations ();
        }

        void RemoveToast ()
        {
            view.RemoveFromSuperview ();
        }
    }
}