﻿using System;
using UIKit;
using RewindShared;
using System.Threading;
using System.IO;

namespace RewindiOS
{
	public class BaseViewController:UIViewController
	{
		protected BaseViewController(IntPtr handle)
			: base(handle)
		{

		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			try {
				if (NavigationController != null)
				{
					NavigationController.NavigationBar.Hidden = true;
				}
			} catch (Exception ex) {
				Utils.writeToDeviceLog(ex);
			}

		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);

			try
			{
				Utils.UserRemovedEventApp += ShowLogoutPopup;
			}
			catch (Exception ex)
			{
				Utils.writeToDeviceLog(ex);
			}
		}

		public override void ViewWillDisappear(bool animated)
		{
			base.ViewWillDisappear(animated);
			try
			{
				Utils.UserRemovedEventApp -= ShowLogoutPopup;               
			}
			catch (Exception ex)
			{
				Utils.writeToDeviceLog(ex);
			}
		}

		void ShowLogoutPopup()
		{
			try
			{
				CustomLoadView ldView7 = new CustomLoadView(this);
				BeginInvokeOnMainThread(() =>
					{
						ldView7.Show();
					});


				ThreadPool.QueueUserWorkItem(state =>
					{
						try
						{
							if (Utils.CheckForInternetConn())
							{  
								string success = ApiCalls.Logout(Utils.UserToken);

								if(success.Contains("true"))
								{
									BeginInvokeOnMainThread(() =>
										{
											if (File.Exists(Utils.PersonalFolder + "/" + "userToken.txt"))
											{
												File.Delete(Utils.PersonalFolder + "/" + "userToken.txt");
											}
											Utils.deleteProfileInfoFromDB();

											if(File.Exists(RewindShared.Consts.USER_AVATAR_IMAGE_PATH))
											{
												File.Delete(RewindShared.Consts.USER_AVATAR_IMAGE_PATH);
											}
											try {
												this.NavigationController.PopToRootViewController(true);
											} catch (Exception ex) {
												Utils.writeToDeviceLog(ex);
											}
											BeginInvokeOnMainThread(() =>
												{
													ToastView toast = new ToastView("Utente non attivo", 3500);
													toast.Show();
												});
										});
								}
								else
								{
									BeginInvokeOnMainThread(() =>
										{
											ToastView toast = new ToastView("Riprova, per favore", 1800);
											toast.Show();
										});
								}
							}
							else
							{
								BeginInvokeOnMainThread(() =>
									{
										ToastView toast = new ToastView("Nessuna connessione Internet", 1800);
										toast.Show();
									});
							}
						}
						catch (Exception ex)
						{
							Utils.writeToDeviceLog(ex);
						}
						finally
						{
							BeginInvokeOnMainThread(() =>
								{
									if (ldView7 != null)
									{
										ldView7.Hide();
									}
								});
						}
					});
			}
			catch (Exception ex)
			{
				Utils.writeToDeviceLog(ex);
			}
		}
	}
}

