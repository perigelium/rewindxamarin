﻿using System;
using System.Collections.Generic;
using UIKit;
using Foundation;
using RewindShared;

namespace RewindiOS
{
    public class PhoneEmailListTablesource: UITableViewSource
    {
        public delegate void PhoneSelectedDelegate(string itemText);
        public event PhoneSelectedDelegate PhoneSelectedEvent;

        public delegate void EmailSelectedDelegate(string itemText);
        public event EmailSelectedDelegate EmailSelectedEvent;

        string cellIdentifier = "TableCell";
        List<string> tableItems;
        float textSize = 15;
        int flag;
        UIView viewParent;

        public PhoneEmailListTablesource(List<string> items,UIView _viewParent, int _flag)
        {
            tableItems = items;
            flag = _flag;
            viewParent = _viewParent;
        }
        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return tableItems.Count;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            UITableViewCell cell = tableView.DequeueReusableCell(cellIdentifier);

            try
            {
                DropDownTableCell statCell = null;
                // if there are no cells to reuse, create a new one
                if (cell == null)
                    cell = new UITableViewCell(UITableViewCellStyle.Default, cellIdentifier);


                cell.TextLabel.Text=tableItems[indexPath.Row];
                cell.TextLabel.TextAlignment= UITextAlignment.Center;
                using(UIFont fnt=UIFont.FromName("Oswald-Regular", 15.0f))
                {
                    cell.TextLabel.Font=fnt;
                }

                if(flag==1)
                {
                    cell.BackgroundColor = UIColor.Clear;
                    cell.TextLabel.TextColor=UIColor.FromRGB(242,113,48);
                } 
                else if(flag==2)
                {
                    cell.BackgroundColor = UIColor.Clear;
                    cell.TextLabel.TextColor=UIColor.FromRGB(242,113,48);
                }

                UIView v = new UIView();
//                v.BackgroundColor = UIColor.FromRGB(242,113,48);
                cell.SelectedBackgroundView = v;
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }

            return cell;
        }

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            try
            {
                tableView.DeselectRow(indexPath, true); // normal iOS behaviour is to remove the blue highlight


                string selectedItem = tableItems[indexPath.Row];
                if(flag==1)
                {
                    if(PhoneSelectedEvent != null)
                    {
                        PhoneSelectedEvent(selectedItem);
                    }
                }
                else if(flag==2)
                {
                    if(EmailSelectedEvent != null)
                    {
                        EmailSelectedEvent(selectedItem);
                    }
                }

            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }
    }
}