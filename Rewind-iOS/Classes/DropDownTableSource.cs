﻿using System;
using RewindShared;
using UIKit;
using Foundation;
using System.Collections.Generic;

namespace RewindiOS
{
    public class DropDownTableSource: UITableViewSource
    {
        public delegate void ItemSelectedDelegate(string itemText,int position);
        public event ItemSelectedDelegate ItemSelectedEvent;

        string cellIdentifier = "TableCell";
        List<string> tableItems;
        float textSize = 15;
        int flag;

        public DropDownTableSource(List<string> items, float _textSize, int _flag)
        {
            tableItems = items;
            textSize = _textSize;
            flag = _flag;
        }
        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return tableItems.Count;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            UITableViewCell cell = tableView.DequeueReusableCell(cellIdentifier);

            try
            {
                DropDownTableCell statCell = null;
                // if there are no cells to reuse, create a new one
                if (cell == null)
                    cell = new UITableViewCell(UITableViewCellStyle.Default, cellIdentifier);


                cell.TextLabel.Text=tableItems[indexPath.Row].ToUpper();
                using(UIFont fnt=UIFont.FromName("Oswald-Regular", 13.0f))
                {
                    cell.TextLabel.Font=fnt;
                }

//                statCell = new DropDownTableCell();
//                cell = statCell.MainCell;
//
//                statCell.LblName.Text = tableItems[indexPath.Row];
//
//                try
//                {
//                    statCell.LblName.AdjustsFontSizeToFitWidth = true;
//                }
//                catch
//                {
//                }
//
//                using (UIFont font = UIFont.SystemFontOfSize(textSize))
//                {
//                    statCell.LblName.Font = font;
//                }
//                statCell.LblName.TextColor = UIColor.FromRGB(53, 131, 198);
//                statCell.LblName.TextAlignment = UITextAlignment.Center;
//
                if(flag==1)
                {
                    cell.BackgroundColor = UIColor.FromRGB(0,95,167);
                    cell.TextLabel.TextColor=UIColor.White;
//                    cell.TextLabel.ContentInset = new UIEdgeInsets((nfloat)(-10),(nfloat)0.0,(nfloat)0,(nfloat)0.0);
//                    cell.TextLabel.Frame=new CoreGraphics.CGRect(100,cell.TextLabel.Frame.Y,cell.TextLabel.Frame.Width,cell.TextLabel.Frame.Height);
                    using(UIFont fnt=UIFont.FromName("Oswald-Regular", 17.0f))
                    {
                        cell.TextLabel.Font=fnt;
                    }
//                    cell.TextLabel.Frame=new CoreGraphics.CGRect(cell.TextLabel.Frame.X+50,cell.TextLabel.Frame.Y,cell.TextLabel.Frame.Width,cell.TextLabel.Frame.Height);
                }
                else
                {
                    cell.BackgroundColor = UIColor.Clear;
                    cell.TextLabel.TextColor=UIColor.FromRGB(68,68,68);
                }

                UIView v = new UIView();
                v.BackgroundColor = UIColor.FromRGB(242,113,48);
                cell.SelectedBackgroundView = v;
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }

            return cell;
        }

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            try
            {
                tableView.DeselectRow(indexPath, true); // normal iOS behaviour is to remove the blue highlight


                string selectedItem = tableItems[indexPath.Row];

                if(ItemSelectedEvent != null)
                {
                    ItemSelectedEvent(selectedItem,indexPath.Row);
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }
    }
}