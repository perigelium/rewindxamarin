﻿using System;
using System.Collections.Generic;
using RewindShared;
using Foundation;
using UIKit;
using System.Globalization;
using CoreGraphics;

namespace RewindiOS
{
    public class PracticheListAdapter:UITableViewSource
    {
        NSString cellHeaderIdentifier = new NSString("PraticheCell");

        List<ClientPraticheItemInfo> tableItems;

        int endSection;
        int didSection;
        bool ifOpen;
        UITableView mainTable;
        float headerTblProductsHeight;
        float rowTblProductsHeight;
        int selIndex;
        int clientPosition;

        ClientiScreen mainController;


        public PracticheListAdapter(List<ClientPraticheItemInfo> items, UITableView _mainTable, float _headerTblProductsHeight, float _rowTblProductsHeight, ClientiScreen _mainController, int _selIndex,int _clientPosition)
        {
            tableItems = items;
            didSection = items.Count + 1;
            mainTable = _mainTable;
            headerTblProductsHeight = _headerTblProductsHeight;
            rowTblProductsHeight = _rowTblProductsHeight;
            mainController = _mainController;
            selIndex = _selIndex;
            clientPosition = _clientPosition;
        }

        void firstOneClicked()
        {
            didSection = 0;
            endSection = 0;
            didSelectCellRowFirstDo(true, false);
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return (int)tableItems.Count;
        }


        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {

            PraticheCell cell = (PraticheCell)tableView.DequeueReusableCell(cellHeaderIdentifier);
            UIView view = new UIView();
            try
            {
                int index = indexPath.Row;
                view = cell.ContentView;

                using (UIFont fnt = UIFont.FromName("Oswald-Regular", 15.0f))
                {
                    cell.LblCellGntAttiva.Font = fnt;
                    cell.LblCellGntAttivaValue.Font = fnt;
                    cell.LblCellgntInserita.Font = fnt;
                    cell.LblCellgntInseritaValue.Font = fnt;
                    cell.LblCellTipoProdotto.Font = fnt;
                    cell.LblCellTipoProdottoValue.Font = fnt;
                    cell.LblCellProdotto.Font = fnt;
                    cell.LblCellProdottoValue.Font = fnt;
                    cell.LblCellDate.Font=fnt;
                    cell.BtnCellDettagli.Font=fnt;
                }
                cell.LblCellDate.Lines=2;

                cell.LblCellgntInserita.Text="Q.TA' INSERITA";
                cell.LblCellGntAttiva.Text="Q.TA' ATTIVA";
                cell.LblCellTipoProdotto.Text="TIPO PRODOTTO";
                cell.LblCellProdotto.Text="PRODOTTO";

                cell.LblCellgntInserita.TextColor=UIColor.FromRGB(242,113,48);
                cell.LblCellGntAttiva.TextColor=UIColor.FromRGB(242,113,48);
                cell.LblCellTipoProdotto.TextColor=UIColor.FromRGB(242,113,48);
                cell.LblCellProdotto.TextColor=UIColor.FromRGB(242,113,48);
                
                cell.LblCellgntInseritaValue.Text = tableItems[indexPath.Row].Gnt_inserita;
                cell.LblCellGntAttivaValue.Text = tableItems[indexPath.Row].Gnt_attiva;
                cell.LblCellTipoProdottoValue.Text = tableItems[indexPath.Row].Tipo_prodotto;
                cell.LblCellProdottoValue.Text = tableItems[indexPath.Row].Prodotto;

                UIFont aFont = UIFont.FromName ("Oswald-Regular", 15.0f);
                CGSize aTextSize = Utils.getSizeToFitText (tableItems[indexPath.Row].Prodotto, aFont);
                int aNrOfLines = Utils.nrOfLinesToFitText (aTextSize, (float)tableView.Frame.Width/3);
                cell.LblCellProdottoValue.Lines=aNrOfLines;

                cell.LblCellDate.Text = tableItems[indexPath.Row].Data_inserimento.ToString("dd.MM. yyyy", CultureInfo.InvariantCulture);

                cell.BtnCellDettagli.TouchUpInside -= Cell_BtnCellDettagli_TouchUpInside;
                cell.BtnCellDettagli.TouchUpInside += Cell_BtnCellDettagli_TouchUpInside;
                cell.BtnCellDettagli.Tag = indexPath.Row;
//                cell.BtnCellDettagli.TouchUpInside+= (object sender, EventArgs e) => 
//                    {
//                        try {
//                            UIButton btn=(UIButton)sender;
//                            int pos=(int)btn.Tag;
//                            mainController.ShowPraticaScreen( tableItems[pos],clientPosition);//tableItems[pos]
//                        } catch (Exception ex) {
//                            Utils.writeToDeviceLog(ex);
//                        }
//
//                    };
              

            }
            catch (Exception ex)
            {
                Utils.writeToIOSDeviceLog(ex);
            }
            return cell;
        }

        void Cell_BtnCellDettagli_TouchUpInside (object sender, EventArgs e)
        {
            try
            {
                UIButton btn=(UIButton)sender;
                int pos=(int)btn.Tag;
                mainController.ShowPraticaScreen( mainController.refinedItems[pos],clientPosition);//tableItems[pos]
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public override nint NumberOfSections(UITableView tableView)
        {
            return 1;
        }

        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            UIFont aFont = UIFont.FromName ("Oswald-Regular", 15.0f);
            CGSize aTextSize = Utils.getSizeToFitText (tableItems[indexPath.Row].Prodotto, aFont);
            int aNrOfLines = Utils.nrOfLinesToFitText (aTextSize, (float)tableView.Frame.Width/3);
            if(aNrOfLines==1)
            {
                return headerTblProductsHeight;
            }
            else
            {
                return headerTblProductsHeight+(aTextSize.Height * aNrOfLines);
            }

            return headerTblProductsHeight;
        }

        public override nfloat GetHeightForHeader(UITableView tableView, nint section)
        {
            return 0;
        }


        public override UIView GetViewForHeader(UITableView tableView, nint section)
        {
            return null;
        }

        void addCell(object sender)
        {
            try
            {
                UIButton bt = (UIButton)sender;
                endSection = (int)bt.Tag;
                if (didSection == tableItems.Count + 1)
                {
                    ifOpen = false;
                    didSection = endSection;
                    didSelectCellRowFirstDo(true, false);
                }
                else
                {
                    if (didSection == endSection)
                    {
                        didSelectCellRowFirstDo(false, false);
                    }
                    else
                    {
                        didSelectCellRowFirstDo(false, true);
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.writeToIOSDeviceLog(ex);
            }
        }

        void didSelectCellRowFirstDo(bool firstDoInsert, bool nextDoInsert)
        {
            try
            {
                mainTable.BeginUpdates();
                ifOpen = firstDoInsert;
                NSIndexPath[] rowToInsert = new NSIndexPath [1];
                NSIndexPath indexPath = NSIndexPath.FromRowSection(0, didSection);

                rowToInsert[0] = indexPath;
                if (!ifOpen)
                {
                    didSection = tableItems.Count + 1;
                    mainTable.DeleteRows(rowToInsert, UITableViewRowAnimation.Fade);
                }
                else
                {
                    mainTable.InsertRows(rowToInsert, UITableViewRowAnimation.Fade);
                }

                mainTable.EndUpdates();
                if (nextDoInsert)
                {
                    didSection = endSection;
                    didSelectCellRowFirstDo(true, false);
                }
                mainTable.ScrollToNearestSelected(UITableViewScrollPosition.Top, true);
            }
            catch (Exception ex)
            {
                Utils.writeToIOSDeviceLog(ex);
            }
        }

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            try
            {
                tableView.DeselectRow(indexPath, true);
            }
            catch (Exception ex)
            {
                Utils.writeToIOSDeviceLog(ex);
            }
        }
    }
}