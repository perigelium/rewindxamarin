﻿using System;
using Foundation;
using System.Collections.Generic;
using RewindShared;
using UIKit;
using System.Globalization;

namespace RewindiOS
{
    public class AppuntamentiDiOggiTblSource:UITableViewSource
    {
        NSString cellHeaderIdentifier = new NSString("appointmentsDiOggiCell");

        List<AppuntamentiItem> tableItems;

        int endSection;
        int didSection;
        bool ifOpen;
        UITableView mainTable;
        float headerTblProductsHeight;
        float rowTblProductsHeight;
        int selIndex;

        AppuntamentiScreen mainController;


        public AppuntamentiDiOggiTblSource(List<AppuntamentiItem> items, UITableView _mainTable, float _headerTblProductsHeight, float _rowTblProductsHeight, AppuntamentiScreen _mainController, int _selIndex)
        {
            tableItems = items;
            didSection = items.Count + 1;
            mainTable = _mainTable;
            headerTblProductsHeight = _headerTblProductsHeight;
            rowTblProductsHeight = _rowTblProductsHeight;
            mainController = _mainController;
            selIndex = _selIndex;
        }

        void firstOneClicked()
        {
            didSection = 0;
            endSection = 0;
            didSelectCellRowFirstDo(true, false);
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return (int)tableItems.Count;
        }


        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {

            AppointmentsDiOggiCell cell = (AppointmentsDiOggiCell)tableView.DequeueReusableCell(cellHeaderIdentifier);
            UIView view = new UIView();
            try
            {
                int index = indexPath.Row;
                view = cell.ContentView;

                using (UIFont fnt = UIFont.FromName("Oswald-Regular", 18.0f))
                {
                    cell.LblCellRegSociale.Font = fnt;
                }

                using (UIFont fnt = UIFont.FromName("LucidaSans", 10.0f))
                {
                    cell.LblCellDate.Font = fnt;
                }

                using (UIFont fnt = UIFont.FromName("LucidaSans", 20.0f))
                {
                    cell.LblCellHour.Font = fnt;
                }

                using (UIFont fnt = UIFont.FromName("LucidaSans", 13.0f))
                {
                    cell.LblCellMinutes.Font = fnt;
                }
//                cell.LblCellRegSociale.Text=tableItems[indexPath.Row].RegSociale;
                if(tableItems[indexPath.Row].AppBloccati)
                {
					cell.LblCellRegSociale.Text = "STOP APPUNTAMENTI";
                }
                else
                {
                    if(tableItems[indexPath.Row].Id_appto_memo!="")
                    {
                        cell.LblCellRegSociale.Text ="MEMO";
                    }
                    else
                    {
                        if(tableItems[indexPath.Row].TipAppuntamenti=="business")
                        {
                            cell.LblCellRegSociale.Text = tableItems[indexPath.Row].RegSociale;
                        }
                        else
                        {
                            cell.LblCellRegSociale.Text = "IMPEGNO PERSONALE";
                        }
                    }

                }
                cell.LblCellRegSociale.Lines=1;

                cell.LblCellDate.Text = tableItems[indexPath.Row].AppuntamentiDateHour.Date.ToString("dd-MM-yy", CultureInfo.InvariantCulture);
                cell.LblCellHour.Text = tableItems[indexPath.Row].AppuntamentiDateHour.ToString("HH", CultureInfo.InvariantCulture);
                cell.LblCellMinutes.Text = tableItems[indexPath.Row].AppuntamentiDateHour.ToString("mm", CultureInfo.InvariantCulture);
                cell.ImgCellArrow.BackgroundColor=UIColor.Clear;
                cell.ViewCellLine.BackgroundColor=UIColor.Clear;
                cell.ViewCellDateAndTime.BackgroundColor=UIColor.Clear;
                cell.ViewCellRegSociale.BackgroundColor=UIColor.Clear;
                cell.ViewBgCell.BackgroundColor=UIColor.FromRGB(0, 95, 167);

                cell.ImgCellArrow.Hidden=true;
                cell.ViewCellLine.Hidden=true;

                if (selIndex != -1)
                {
                    cell.ViewBgCell.BackgroundColor=UIColor.Clear;
                    cell.LblCellRegSociale.Hidden = true;
                    view.Tag = indexPath.Row;
                    cell.ViewCellLine.Hidden=false;
                    cell.ViewCellLine.BackgroundColor=UIColor.FromRGB(76, 76, 76);
                    cell.ViewCellSeparator.Frame=new CoreGraphics.CGRect(cell.ViewCellSeparator.Frame.X,cell.ViewCellSeparator.Frame.Y,72,cell.ViewCellSeparator.Frame.Height);
//                    cell.ViewCellRegSociale.Frame=new CoreGraphics.CGRect(cell.ViewCellRegSociale.Frame.X-11,cell.ViewCellRegSociale.Frame.Y,cell.ViewCellRegSociale.Frame.Width,cell.ViewCellRegSociale.Frame.Height);

                    if (selIndex == indexPath.Row)
                    {
                        cell.ViewCellDateAndTime.BackgroundColor=UIColor.FromRGB(76, 76, 76);
                        cell.ImgCellArrow.BackgroundColor=UIColor.Clear;
                        cell.ImgCellArrow.Hidden=false;
                    }
                    else
                    {
                        //first time
                        cell.ViewCellDateAndTime.BackgroundColor=UIColor.FromRGB(0, 95, 167);
                        cell.ImgCellArrow.BackgroundColor=UIColor.Clear;
                        cell.ImgCellArrow.Hidden=true;
                    }
                }
                else
                {
                    cell.LblCellRegSociale.Hidden = false;
//                    view.Click += OnItemClick;
//                    view.Tag = position;
                }
//                cell.ViewBgCell.BackgroundColor=UIColor.Clear;
//                cell.ImgCellArrow.BackgroundColor=UIColor.Clear;
//                cell.ViewCellRegSociale.BackgroundColor=UIColor.Clear;
//                cell.ViewCellDateAndTime.BackgroundColor=UIColor.Clear;
                view.BackgroundColor=UIColor.Clear;

            }
            catch (Exception ex)
            {
                Utils.writeToIOSDeviceLog(ex);
            }
            return cell;
        }

        public override nint NumberOfSections(UITableView tableView)
        {
            return 1;
        }

        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            return headerTblProductsHeight;
        }

        public override nfloat GetHeightForHeader(UITableView tableView, nint section)
        {
            return 0;
        }


        public override UIView GetViewForHeader(UITableView tableView, nint section)
        {
            return null;
        }

        void addCell(object sender)
        {
            try
            {
                UIButton bt = (UIButton)sender;
                endSection = (int)bt.Tag;
                if (didSection == tableItems.Count + 1)
                {
                    ifOpen = false;
                    didSection = endSection;
                    didSelectCellRowFirstDo(true, false);
                }
                else
                {
                    if (didSection == endSection)
                    {
                        didSelectCellRowFirstDo(false, false);
                    }
                    else
                    {
                        didSelectCellRowFirstDo(false, true);
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.writeToIOSDeviceLog(ex);
            }
        }

        void didSelectCellRowFirstDo(bool firstDoInsert, bool nextDoInsert)
        {
            try
            {
                mainTable.BeginUpdates();
                ifOpen = firstDoInsert;
                NSIndexPath[] rowToInsert = new NSIndexPath [1];
                NSIndexPath indexPath = NSIndexPath.FromRowSection(0, didSection);

                rowToInsert[0] = indexPath;
                if (!ifOpen)
                {
                    didSection = tableItems.Count + 1;
                    mainTable.DeleteRows(rowToInsert, UITableViewRowAnimation.Fade);
                }
                else
                {
                    mainTable.InsertRows(rowToInsert, UITableViewRowAnimation.Fade);
                }

                mainTable.EndUpdates();
                if (nextDoInsert)
                {
                    didSection = endSection;
                    didSelectCellRowFirstDo(true, false);
                }
                mainTable.ScrollToNearestSelected(UITableViewScrollPosition.Top, true);
            }
            catch (Exception ex)
            {
                Utils.writeToIOSDeviceLog(ex);
            }
        }

       public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            try
            {
                tableView.DeselectRow(indexPath, true);
                mainController.OnRowFromListClickEvent(indexPath.Row);
            }
            catch (Exception ex)
            {
                Utils.writeToIOSDeviceLog(ex);
            }
        }
    }
}