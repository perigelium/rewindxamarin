﻿using System;
using UIKit;
using RewindShared;
using CoreGraphics;
using System.Collections.Generic;
using Foundation;
using MessageUI;

namespace RewindiOS
{
    public class CustomLoadView:UIView
    {
        UIViewController mainView;
        UIView view;
        UIView viewTest;

        public CustomLoadView(UIViewController control)
        {
            this.mainView = control;
        }

        public void Show()
        {
            try
            {
                view = new UIView();
                view.Frame = new CGRect(0, 0, mainView.View.Bounds.Width, mainView.View.Bounds.Height);
                view.AutoresizingMask = UIViewAutoresizing.FlexibleTopMargin | UIViewAutoresizing.FlexibleBottomMargin | UIViewAutoresizing.FlexibleHeight;// |UIViewAutoresizing.FlexibleLeftMargin | UIViewAutoresizing.FlexibleRightMargin | UIViewAutoresizing.FlexibleWidth;

                view.BackgroundColor = UIColor.FromPatternImage(UIImage.FromFile("LoadBg.png"));
                mainView.Add(view);
                UIView container = new UIView();
                container.Frame = new CGRect((view.Frame.Width / 2) - 93, (view.Frame.Height / 2) - 93, 186, 186);
                container.BackgroundColor = UIColor.Clear;
                container.AutoresizingMask = UIViewAutoresizing.None;
                view.Add(container);

                UIImageView imgBg = new UIImageView();
                imgBg.Frame = new CGRect(0, 0, container.Frame.Width, container.Frame.Height);
                imgBg.Image = UIImage.FromFile("Rectangle.png");
                container.Add(imgBg);

                UILabel lblTitle = new UILabel();
                lblTitle.Frame = new CGRect((container.Frame.Width / 2) - 93, 25, 186, 50);
                lblTitle.TextAlignment = UITextAlignment.Center;

                lblTitle.Text = "Attendere prego";
                container.Add(lblTitle);
                UIActivityIndicatorView activityIndicator = new UIActivityIndicatorView(UIActivityIndicatorViewStyle.WhiteLarge);
                activityIndicator.Frame = new CGRect((container.Frame.Width / 2) - 20, (container.Frame.Height / 2), 40, 40);
                activityIndicator.Color = UIColor.FromRGB(242, 113, 48);
                container.Add(activityIndicator);
                //view.AddSubview(activityIndicator);236,131,19
                activityIndicator.StartAnimating();
            }
            catch (Exception ex)
            {
                Utils.writeToIOSDeviceLog(ex);
            }
        }

        public void Hide()
        {
            try
            {
                view.RemoveFromSuperview();
                view.Dispose();

            }
            catch (Exception ex)
            {
                Utils.writeToIOSDeviceLog(ex);
            }
        }

       
        public void Hide(UIView viewParent)
        {
            try
            {
                //if(viewParent.Frame.X==0)
                {
                    view.RemoveFromSuperview();
                    mainView.Dispose();
                }
            }
            catch (Exception ex)
            {
                Utils.writeToIOSDeviceLog(ex);
            }
        }


        public void ShowPhones(List<string> PhoneList,int flag)
        {
            try
            {
                view = new UIView();
                view.Frame = new CGRect(0, 0, mainView.View.Bounds.Width, mainView.View.Bounds.Height);
                view.AutoresizingMask = UIViewAutoresizing.FlexibleTopMargin | UIViewAutoresizing.FlexibleBottomMargin | UIViewAutoresizing.FlexibleHeight;// |UIViewAutoresizing.FlexibleLeftMargin | UIViewAutoresizing.FlexibleRightMargin | UIViewAutoresizing.FlexibleWidth;

                view.BackgroundColor = UIColor.FromPatternImage(UIImage.FromFile("LoadBg.png"));
                mainView.Add(view);
                UIView container = new UIView();
                container.Frame = new CGRect((view.Frame.Width / 2) - 93, (view.Frame.Height / 2) - 50, 186, 150);
                container.BackgroundColor = UIColor.Clear;
                container.AutoresizingMask = UIViewAutoresizing.None;
                view.Add(container);

                UIImageView imgBg = new UIImageView();
                imgBg.Frame = new CGRect(0, 0, container.Frame.Width, container.Frame.Height);
                imgBg.Image = UIImage.FromFile("Rectangle.png");
                imgBg.ContentMode= UIViewContentMode.ScaleAspectFill;
                container.Add(imgBg);

                UIView viewPhoneList=new UIView();
                viewPhoneList.Frame=new CGRect(0,0,container.Frame.Width,container.Frame.Height-50);
                viewPhoneList.BackgroundColor=UIColor.Clear;
                container.Add(viewPhoneList);

                UITableView tblPhoneList=new UITableView();
                tblPhoneList.BackgroundColor=UIColor.Clear;
                tblPhoneList.Frame=new CGRect(0,0,viewPhoneList.Frame.Width,viewPhoneList.Frame.Height);
                tblPhoneList.SeparatorStyle= UITableViewCellSeparatorStyle.None;
                viewPhoneList.Add(tblPhoneList);

                if(flag==1)
                {
                    PhoneEmailListTablesource tblsource=new PhoneEmailListTablesource(PhoneList,viewPhoneList,flag);
                    tblPhoneList.Source=tblsource;
                    //tblPhoneList.ReloadData();
                    tblsource.PhoneSelectedEvent += OnCallPhoneFromList;
                }
                else if(flag==2)
                {
                    PhoneEmailListTablesource tblsource=new PhoneEmailListTablesource(PhoneList,viewPhoneList,flag);
                    tblPhoneList.Source=tblsource;
                    tblPhoneList.ReloadData();
                    tblsource.EmailSelectedEvent += OnCallEmailFromList;
                }

                UIButton btnCancel=new UIButton();
                btnCancel.Frame=new CGRect((container.Frame.Width-80)/2,viewPhoneList.Frame.Y+viewPhoneList.Frame.Height+10,80,30);
                btnCancel.SetTitle("CANCELLA",UIControlState.Normal);
                btnCancel.TouchUpInside+= BtnCancel_TouchUpInside;
                using (UIFont fnt = UIFont.FromName("Oswald-Regular", 15.0f))
                {
                    btnCancel.Font = fnt;
                }
                container.Add(btnCancel);
                btnCancel.BackgroundColor=UIColor.FromRGB(242,113,48);
                btnCancel.SetBackgroundImage(Utils.GetImageFromColor(UIColor.Clear), UIControlState.Normal);
                btnCancel.SetBackgroundImage(Utils.GetImageFromColor(UIColor.FromRGBA(0, 95, 167, 200)), UIControlState.Highlighted);
            }
            catch (Exception ex)
            {
                Utils.writeToIOSDeviceLog(ex);
            }
        }

        void BtnCancel_TouchUpInside (object sender, EventArgs e)
        {
            try
            {
                view.RemoveFromSuperview();
//                mainView.Dispose();
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void OnCallPhoneFromList(string itemText)
        {
            try
            {
                view.RemoveFromSuperview();

                string urlString = "tel://" + itemText.Trim();
                urlString = urlString.Replace(" ", String.Empty);

                NSUrl callUrl = NSUrl.FromString(urlString);

                if (!UIApplication.SharedApplication.OpenUrl(callUrl))
                {
                    ToastView toast = new ToastView("Applicazione non disponibile", 3000);
                    toast.Show();
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void OnCallEmailFromList(string itemText)
        {
            try
            {
                view.RemoveFromSuperview();
                //                mainView.Dispose();

                MFMailComposeViewController _mailController = new MFMailComposeViewController();
                _mailController.SetToRecipients(new string[]{ itemText.Trim() });
                _mailController.SetSubject("");
                _mailController.SetMessageBody("", false);
                _mailController.Finished += ( object s, MFComposeResultEventArgs args) =>
                    {
                        Console.WriteLine(args.Result.ToString());
                        args.Controller.DismissViewController(true, null);
                    };

                mainView.PresentViewController(_mailController, true, null);

            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void RemoveSubviews(UIView view)
        {
            try
            {
                foreach (UIView item in view.Subviews) 
                {
                    item.RemoveFromSuperview();
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }
    }
}