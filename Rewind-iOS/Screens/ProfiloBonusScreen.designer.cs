// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace RewindiOS
{
	[Register ("ProfiloBonusScreen")]
	partial class ProfiloBonusScreen
	{
		[Outlet]
		UIKit.UIButton btnDataAl { get; set; }

		[Outlet]
		UIKit.UIButton btnDataDal { get; set; }

		[Outlet]
		UIKit.UIButton btnTopMenuBack { get; set; }

		[Outlet]
		UIKit.UIImageView imgTopMenuBack { get; set; }

		[Outlet]
		UIKit.UILabel lblBonus { get; set; }

		[Outlet]
		UIKit.UILabel lblBonusTotale { get; set; }

		[Outlet]
		UIKit.UILabel lblDataAl { get; set; }

		[Outlet]
		UIKit.UILabel lblDataDal { get; set; }

		[Outlet]
		UIKit.UILabel lblProdotto { get; set; }

		[Outlet]
		UIKit.UILabel lblRagioneSociale { get; set; }

		[Outlet]
		UIKit.UILabel lblTopMenuTitle { get; set; }

		[Outlet]
		UIKit.UITableView tblBonusItems { get; set; }

		[Outlet]
		UIKit.UILabel txtBonusTotale { get; set; }

		[Outlet]
		UIKit.UILabel txtDataAl { get; set; }

		[Outlet]
		UIKit.UILabel txtDataDal { get; set; }

		[Outlet]
		UIKit.UIView viewDataAl { get; set; }

		[Outlet]
		UIKit.UIView viewDataDal { get; set; }

		[Outlet]
		UIKit.UIView viewTopMenuBack { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (btnDataAl != null) {
				btnDataAl.Dispose ();
				btnDataAl = null;
			}

			if (btnDataDal != null) {
				btnDataDal.Dispose ();
				btnDataDal = null;
			}

			if (btnTopMenuBack != null) {
				btnTopMenuBack.Dispose ();
				btnTopMenuBack = null;
			}

			if (imgTopMenuBack != null) {
				imgTopMenuBack.Dispose ();
				imgTopMenuBack = null;
			}

			if (lblBonus != null) {
				lblBonus.Dispose ();
				lblBonus = null;
			}

			if (lblBonusTotale != null) {
				lblBonusTotale.Dispose ();
				lblBonusTotale = null;
			}

			if (lblDataAl != null) {
				lblDataAl.Dispose ();
				lblDataAl = null;
			}

			if (lblDataDal != null) {
				lblDataDal.Dispose ();
				lblDataDal = null;
			}

			if (lblProdotto != null) {
				lblProdotto.Dispose ();
				lblProdotto = null;
			}

			if (lblRagioneSociale != null) {
				lblRagioneSociale.Dispose ();
				lblRagioneSociale = null;
			}

			if (lblTopMenuTitle != null) {
				lblTopMenuTitle.Dispose ();
				lblTopMenuTitle = null;
			}

			if (txtBonusTotale != null) {
				txtBonusTotale.Dispose ();
				txtBonusTotale = null;
			}

			if (txtDataAl != null) {
				txtDataAl.Dispose ();
				txtDataAl = null;
			}

			if (txtDataDal != null) {
				txtDataDal.Dispose ();
				txtDataDal = null;
			}

			if (viewDataAl != null) {
				viewDataAl.Dispose ();
				viewDataAl = null;
			}

			if (viewDataDal != null) {
				viewDataDal.Dispose ();
				viewDataDal = null;
			}

			if (viewTopMenuBack != null) {
				viewTopMenuBack.Dispose ();
				viewTopMenuBack = null;
			}

			if (tblBonusItems != null) {
				tblBonusItems.Dispose ();
				tblBonusItems = null;
			}
		}
	}
}
