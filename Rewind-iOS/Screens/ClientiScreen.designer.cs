// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace RewindiOS
{
	[Register ("ClientiScreen")]
	partial class ClientiScreen
	{
		[Outlet]
		UIKit.UIButton btnAltoBottomBack { get; set; }

		[Outlet]
		UIKit.UIButton btnBackFromAltuofianco { get; set; }

		[Outlet]
		UIKit.UIButton btnBackFromCreateTicket { get; set; }

		[Outlet]
		UIKit.UIButton btnBackFromPractica { get; set; }

		[Outlet]
		UIKit.UIButton btnBackFromSeriale { get; set; }

		[Outlet]
		UIKit.UIButton btnClientiAltuofianco { get; set; }

		[Outlet]
		UIKit.UIButton btnClientiBack { get; set; }

		[Outlet]
		UIKit.UIButton btnClientiCall { get; set; }

		[Outlet]
		UIKit.UIButton btnClientiChooseFilter { get; set; }

		[Outlet]
		UIKit.UIButton btnClientiMemo { get; set; }

		[Outlet]
		UIKit.UIButton btnClientiPratiche { get; set; }

		[Outlet]
		UIKit.UIButton btnClientiSearchFilter { get; set; }

		[Outlet]
		UIKit.UIButton btnClientiSendEmail { get; set; }

		[Outlet]
		UIKit.UIButton btnInviaNewTicket { get; set; }

		[Outlet]
		UIKit.UIButton btnMemoChooseData { get; set; }

		[Outlet]
		UIKit.UIButton btnMemoChooseOra { get; set; }

		[Outlet]
		UIKit.UIButton btnMemoSendMemo { get; set; }

		[Outlet]
		UIKit.UIButton btnNewTicketChooseTicket { get; set; }

		[Outlet]
		UIKit.UIButton btnPlusMemo { get; set; }

		[Outlet]
		UIKit.UIButton btnPracticaApriTicket { get; set; }

		[Outlet]
		MapKit.MKMapView clientiMap { get; set; }

		[Outlet]
		UIKit.UIImageView imgArrowNewTicket { get; set; }

		[Outlet]
		UIKit.UIImageView imgClientiSearchFilter { get; set; }

		[Outlet]
		UIKit.UIImageView imgFilterArrow { get; set; }

		[Outlet]
		UIKit.UILabel lblAltoCodiceCliente { get; set; }

		[Outlet]
		UIKit.UILabel lblAltoCodiceClienteValue { get; set; }

		[Outlet]
		UIKit.UILabel lblAltoDataInserimento { get; set; }

		[Outlet]
		UIKit.UILabel lblAltoDataInserimentoValue { get; set; }

		[Outlet]
		UIKit.UILabel lblAltoEmailShop { get; set; }

		[Outlet]
		UIKit.UILabel lblAltoEmailSopValue { get; set; }

		[Outlet]
		UIKit.UILabel lblAltoNumeroRef { get; set; }

		[Outlet]
		UIKit.UILabel lblAltoNumeroRefValue { get; set; }

		[Outlet]
		UIKit.UILabel lblAltoPuntiDisponib { get; set; }

		[Outlet]
		UIKit.UILabel lblAltoPuntiDisponibValue { get; set; }

		[Outlet]
		UIKit.UILabel lblAltoStatoRid { get; set; }

		[Outlet]
		UIKit.UILabel lblAltoStatoRidValue { get; set; }

		[Outlet]
		UIKit.UILabel lblAltoTipoContratto { get; set; }

		[Outlet]
		UIKit.UILabel lblAltoTipoContrattoValue { get; set; }

		[Outlet]
		UIKit.UILabel lblClientiCellulare { get; set; }

		[Outlet]
		UIKit.UILabel lblClientiCellulareValue { get; set; }

		[Outlet]
		UIKit.UILabel lblClientiFax { get; set; }

		[Outlet]
		UIKit.UILabel lblClientiFaxValue { get; set; }

		[Outlet]
		UIKit.UILabel lblClientiFIlterText { get; set; }

		[Outlet]
		UIKit.UILabel lblClientiIndirizzo { get; set; }

		[Outlet]
		UIKit.UILabel lblClientiIndirizzoValue { get; set; }

		[Outlet]
		UIKit.UILabel lblClientiMail { get; set; }

		[Outlet]
		UIKit.UILabel lblClientiMailValue { get; set; }

		[Outlet]
		UIKit.UILabel lblClientiPartitaIva { get; set; }

		[Outlet]
		UIKit.UILabel lblClientiPartitaIvaValue { get; set; }

		[Outlet]
		UIKit.UILabel lblClientiReferente { get; set; }

		[Outlet]
		UIKit.UILabel lblClientiReferenteValue { get; set; }

		[Outlet]
		UIKit.UILabel lblClientiTelefono { get; set; }

		[Outlet]
		UIKit.UILabel lblClientiTelefonoValue { get; set; }

		[Outlet]
		UIKit.UILabel lblClientiTitle { get; set; }

		[Outlet]
		UIKit.UILabel lblCreateTicketTitle { get; set; }

		[Outlet]
		UIKit.UILabel lblInviaNewTicket { get; set; }

		[Outlet]
		UIKit.UILabel lblMemoData { get; set; }

		[Outlet]
		UIKit.UILabel lblMemoDataValue { get; set; }

		[Outlet]
		UIKit.UILabel lblMemoDescPlaceHolder { get; set; }

		[Outlet]
		UIKit.UILabel lblMemoLuogoHolder { get; set; }

		[Outlet]
		UIKit.UILabel lblMemoOra { get; set; }

		[Outlet]
		UIKit.UILabel lblMemoOraValue { get; set; }

		[Outlet]
		UIKit.UILabel lblMemoTitle { get; set; }

		[Outlet]
		UIKit.UILabel lblNewTicketDestinatario { get; set; }

		[Outlet]
		UIKit.UILabel lblNewTicketDestinatarioValue { get; set; }

		[Outlet]
		UIKit.UILabel lblNewTicketOggetto { get; set; }

		[Outlet]
		UIKit.UILabel lblNewTicketOggettoValue { get; set; }

		[Outlet]
		UIKit.UILabel lblNewTicketPartitaIva { get; set; }

		[Outlet]
		UIKit.UILabel lblNewTicketPartitaIvaValue { get; set; }

		[Outlet]
		UIKit.UILabel lblNewTicketRagSociale { get; set; }

		[Outlet]
		UIKit.UILabel lblNewTicketRagSocialeValue { get; set; }

		[Outlet]
		UIKit.UILabel lblNewTicketTIpo { get; set; }

		[Outlet]
		UIKit.UILabel lblNewTicketTipoValue { get; set; }

		[Outlet]
		UIKit.UILabel lblPracticaAgente { get; set; }

		[Outlet]
		UIKit.UILabel lblPracticaApriTicket { get; set; }

		[Outlet]
		UIKit.UILabel lblPracticaDataAttivazione { get; set; }

		[Outlet]
		UIKit.UILabel lblPracticaDataAttivazioneValue { get; set; }

		[Outlet]
		UIKit.UILabel lblPracticaDataCreazione { get; set; }

		[Outlet]
		UIKit.UILabel lblPracticaDataCreazioneValue { get; set; }

		[Outlet]
		UIKit.UILabel lblPracticaDataInserimento { get; set; }

		[Outlet]
		UIKit.UILabel lblPracticaDataInserimentoValue { get; set; }

		[Outlet]
		UIKit.UILabel lblPracticaLinee { get; set; }

		[Outlet]
		UIKit.UILabel lblPracticaLineeValue { get; set; }

		[Outlet]
		UIKit.UILabel lblPracticaProdotto { get; set; }

		[Outlet]
		UIKit.UILabel lblPracticaProdottoValue { get; set; }

		[Outlet]
		UIKit.UILabel lblPracticaQtaAttiva { get; set; }

		[Outlet]
		UIKit.UILabel lblPracticaQtaAttivaValue { get; set; }

		[Outlet]
		UIKit.UILabel lblPracticaQtaInserita { get; set; }

		[Outlet]
		UIKit.UILabel lblPracticaQtaInseritaValue { get; set; }

		[Outlet]
		UIKit.UILabel lblPracticaSeriali { get; set; }

		[Outlet]
		UIKit.UILabel lblPracticaSerialiValue { get; set; }

		[Outlet]
		UIKit.UILabel lblPracticaStatoOrdine { get; set; }

		[Outlet]
		UIKit.UILabel lblPracticaStatoOrdineValue { get; set; }

		[Outlet]
		UIKit.UILabel lblPracticaStatoPratica { get; set; }

		[Outlet]
		UIKit.UILabel lblPracticaStatoPraticaValue { get; set; }

		[Outlet]
		UIKit.UILabel lblPracticaTipoContratto { get; set; }

		[Outlet]
		UIKit.UILabel lblPracticaTipoContrattoValue { get; set; }

		[Outlet]
		UIKit.UILabel lblPracticaTipoProdotto { get; set; }

		[Outlet]
		UIKit.UILabel lblPracticaTipoProdottoValue { get; set; }

		[Outlet]
		UIKit.UILabel lblPracticaTitle { get; set; }

		[Outlet]
		UIKit.UILabel lblPraticaCanone { get; set; }

		[Outlet]
		UIKit.UILabel lblPraticaCanoneValue { get; set; }

		[Outlet]
		UIKit.UILabel lblSerialeInsSeriale { get; set; }

		[Outlet]
		UIKit.UILabel lblSerialeTitle { get; set; }

		[Outlet]
		UIKit.UIScrollView scrlViewPractica { get; set; }

		[Outlet]
		UIKit.UITableView tblClientiPratiche { get; set; }

		[Outlet]
		UIKit.UITableView tblClientiRegSociale { get; set; }

		[Outlet]
		UIKit.UITableView tblMemo { get; set; }

		[Outlet]
		UIKit.UITextField tfClientiSearch { get; set; }

		[Outlet]
		UIKit.UITextField tfMemoClientiPlace { get; set; }

		[Outlet]
		UIKit.UITextField tfMemoDescription { get; set; }

		[Outlet]
		UIKit.UITextField tfMemoTitle { get; set; }

		[Outlet]
		UIKit.UITextField tfNewTicketDescription { get; set; }

		[Outlet]
		UIKit.UITextField tfNewTicketOggetto { get; set; }

		[Outlet]
		UIKit.UITextField tfSerialeInserSeriale { get; set; }

		[Outlet]
		UIKit.UITextView txtViewMemoDescription { get; set; }

		[Outlet]
		UIKit.UIView viewAltoBottom { get; set; }

		[Outlet]
		UIKit.UIView viewAltuofianco { get; set; }

		[Outlet]
		UIKit.UIView viewAltuofiancoContent { get; set; }

		[Outlet]
		UIKit.UIView viewBgUnderTable { get; set; }

		[Outlet]
		UIKit.UIView viewBottomAltuofiancoPratiche { get; set; }

		[Outlet]
		UIKit.UIView viewBottomCallEmail { get; set; }

		[Outlet]
		UIKit.UIView viewClientiCall { get; set; }

		[Outlet]
		UIKit.UIView viewClientiContent { get; set; }

		[Outlet]
		UIKit.UIView viewClientiFilter { get; set; }

		[Outlet]
		UIKit.UIView viewClientiInfo { get; set; }

		[Outlet]
		UIKit.UIView viewClientiMemo { get; set; }

		[Outlet]
		UIKit.UIView viewClientiPratiche { get; set; }

		[Outlet]
		UIKit.UIView viewClientiPraticheDataAttivazione { get; set; }

		[Outlet]
		UIKit.UIView viewClientiPraticheInstradamento { get; set; }

		[Outlet]
		UIKit.UIView viewClientiPraticheLinee { get; set; }

		[Outlet]
		UIKit.UIView viewClientiPraticheNote { get; set; }

		[Outlet]
		UIKit.UIView viewClientiPraticheProdotto { get; set; }

		[Outlet]
		UIKit.UIView viewClientiPraticheSeriale { get; set; }

		[Outlet]
		UIKit.UIView viewClientiSendEmail { get; set; }

		[Outlet]
		UIKit.UIView viewCreateTicketChooseTipo { get; set; }

		[Outlet]
		UIKit.UIView viewCreateTicketContent { get; set; }

		[Outlet]
		UIKit.UIView viewInviaNewTicket { get; set; }

		[Outlet]
		UIKit.UIView viewMemoBottom { get; set; }

		[Outlet]
		UIKit.UIView viewMemoContent { get; set; }

		[Outlet]
		UIKit.UIView viewPlusMemo { get; set; }

		[Outlet]
		UIKit.UIView viewPracticaBottom { get; set; }

		[Outlet]
		UIKit.UIView viewPracticaDetailContent { get; set; }

		[Outlet]
		UIKit.UIView viewPracticTopBar { get; set; }

		[Outlet]
		UIKit.UIView viewSerialeContent { get; set; }

		[Outlet]
		UIKit.UIView viewTopBar { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (btnAltoBottomBack != null) {
				btnAltoBottomBack.Dispose ();
				btnAltoBottomBack = null;
			}

			if (btnBackFromAltuofianco != null) {
				btnBackFromAltuofianco.Dispose ();
				btnBackFromAltuofianco = null;
			}

			if (btnBackFromCreateTicket != null) {
				btnBackFromCreateTicket.Dispose ();
				btnBackFromCreateTicket = null;
			}

			if (btnBackFromPractica != null) {
				btnBackFromPractica.Dispose ();
				btnBackFromPractica = null;
			}

			if (btnBackFromSeriale != null) {
				btnBackFromSeriale.Dispose ();
				btnBackFromSeriale = null;
			}

			if (btnClientiAltuofianco != null) {
				btnClientiAltuofianco.Dispose ();
				btnClientiAltuofianco = null;
			}

			if (btnClientiBack != null) {
				btnClientiBack.Dispose ();
				btnClientiBack = null;
			}

			if (btnClientiCall != null) {
				btnClientiCall.Dispose ();
				btnClientiCall = null;
			}

			if (btnClientiChooseFilter != null) {
				btnClientiChooseFilter.Dispose ();
				btnClientiChooseFilter = null;
			}

			if (btnClientiMemo != null) {
				btnClientiMemo.Dispose ();
				btnClientiMemo = null;
			}

			if (btnClientiPratiche != null) {
				btnClientiPratiche.Dispose ();
				btnClientiPratiche = null;
			}

			if (btnClientiSearchFilter != null) {
				btnClientiSearchFilter.Dispose ();
				btnClientiSearchFilter = null;
			}

			if (btnClientiSendEmail != null) {
				btnClientiSendEmail.Dispose ();
				btnClientiSendEmail = null;
			}

			if (btnInviaNewTicket != null) {
				btnInviaNewTicket.Dispose ();
				btnInviaNewTicket = null;
			}

			if (btnMemoChooseData != null) {
				btnMemoChooseData.Dispose ();
				btnMemoChooseData = null;
			}

			if (btnMemoChooseOra != null) {
				btnMemoChooseOra.Dispose ();
				btnMemoChooseOra = null;
			}

			if (btnMemoSendMemo != null) {
				btnMemoSendMemo.Dispose ();
				btnMemoSendMemo = null;
			}

			if (btnNewTicketChooseTicket != null) {
				btnNewTicketChooseTicket.Dispose ();
				btnNewTicketChooseTicket = null;
			}

			if (btnPracticaApriTicket != null) {
				btnPracticaApriTicket.Dispose ();
				btnPracticaApriTicket = null;
			}

			if (clientiMap != null) {
				clientiMap.Dispose ();
				clientiMap = null;
			}

			if (imgArrowNewTicket != null) {
				imgArrowNewTicket.Dispose ();
				imgArrowNewTicket = null;
			}

			if (imgClientiSearchFilter != null) {
				imgClientiSearchFilter.Dispose ();
				imgClientiSearchFilter = null;
			}

			if (imgFilterArrow != null) {
				imgFilterArrow.Dispose ();
				imgFilterArrow = null;
			}

			if (lblAltoCodiceCliente != null) {
				lblAltoCodiceCliente.Dispose ();
				lblAltoCodiceCliente = null;
			}

			if (lblAltoCodiceClienteValue != null) {
				lblAltoCodiceClienteValue.Dispose ();
				lblAltoCodiceClienteValue = null;
			}

			if (lblAltoDataInserimento != null) {
				lblAltoDataInserimento.Dispose ();
				lblAltoDataInserimento = null;
			}

			if (lblAltoDataInserimentoValue != null) {
				lblAltoDataInserimentoValue.Dispose ();
				lblAltoDataInserimentoValue = null;
			}

			if (lblAltoEmailShop != null) {
				lblAltoEmailShop.Dispose ();
				lblAltoEmailShop = null;
			}

			if (lblAltoEmailSopValue != null) {
				lblAltoEmailSopValue.Dispose ();
				lblAltoEmailSopValue = null;
			}

			if (lblAltoNumeroRef != null) {
				lblAltoNumeroRef.Dispose ();
				lblAltoNumeroRef = null;
			}

			if (lblAltoNumeroRefValue != null) {
				lblAltoNumeroRefValue.Dispose ();
				lblAltoNumeroRefValue = null;
			}

			if (lblAltoPuntiDisponib != null) {
				lblAltoPuntiDisponib.Dispose ();
				lblAltoPuntiDisponib = null;
			}

			if (lblAltoPuntiDisponibValue != null) {
				lblAltoPuntiDisponibValue.Dispose ();
				lblAltoPuntiDisponibValue = null;
			}

			if (lblAltoStatoRid != null) {
				lblAltoStatoRid.Dispose ();
				lblAltoStatoRid = null;
			}

			if (lblAltoStatoRidValue != null) {
				lblAltoStatoRidValue.Dispose ();
				lblAltoStatoRidValue = null;
			}

			if (lblAltoTipoContratto != null) {
				lblAltoTipoContratto.Dispose ();
				lblAltoTipoContratto = null;
			}

			if (lblAltoTipoContrattoValue != null) {
				lblAltoTipoContrattoValue.Dispose ();
				lblAltoTipoContrattoValue = null;
			}

			if (lblClientiCellulare != null) {
				lblClientiCellulare.Dispose ();
				lblClientiCellulare = null;
			}

			if (lblClientiCellulareValue != null) {
				lblClientiCellulareValue.Dispose ();
				lblClientiCellulareValue = null;
			}

			if (lblClientiFax != null) {
				lblClientiFax.Dispose ();
				lblClientiFax = null;
			}

			if (lblClientiFaxValue != null) {
				lblClientiFaxValue.Dispose ();
				lblClientiFaxValue = null;
			}

			if (lblClientiFIlterText != null) {
				lblClientiFIlterText.Dispose ();
				lblClientiFIlterText = null;
			}

			if (lblClientiIndirizzo != null) {
				lblClientiIndirizzo.Dispose ();
				lblClientiIndirizzo = null;
			}

			if (lblClientiIndirizzoValue != null) {
				lblClientiIndirizzoValue.Dispose ();
				lblClientiIndirizzoValue = null;
			}

			if (lblClientiMail != null) {
				lblClientiMail.Dispose ();
				lblClientiMail = null;
			}

			if (lblClientiMailValue != null) {
				lblClientiMailValue.Dispose ();
				lblClientiMailValue = null;
			}

			if (lblClientiPartitaIva != null) {
				lblClientiPartitaIva.Dispose ();
				lblClientiPartitaIva = null;
			}

			if (lblClientiPartitaIvaValue != null) {
				lblClientiPartitaIvaValue.Dispose ();
				lblClientiPartitaIvaValue = null;
			}

			if (lblClientiReferente != null) {
				lblClientiReferente.Dispose ();
				lblClientiReferente = null;
			}

			if (lblClientiReferenteValue != null) {
				lblClientiReferenteValue.Dispose ();
				lblClientiReferenteValue = null;
			}

			if (lblClientiTelefono != null) {
				lblClientiTelefono.Dispose ();
				lblClientiTelefono = null;
			}

			if (lblClientiTelefonoValue != null) {
				lblClientiTelefonoValue.Dispose ();
				lblClientiTelefonoValue = null;
			}

			if (lblClientiTitle != null) {
				lblClientiTitle.Dispose ();
				lblClientiTitle = null;
			}

			if (lblCreateTicketTitle != null) {
				lblCreateTicketTitle.Dispose ();
				lblCreateTicketTitle = null;
			}

			if (lblInviaNewTicket != null) {
				lblInviaNewTicket.Dispose ();
				lblInviaNewTicket = null;
			}

			if (lblMemoData != null) {
				lblMemoData.Dispose ();
				lblMemoData = null;
			}

			if (btnPlusMemo != null) {
				btnPlusMemo.Dispose ();
				btnPlusMemo = null;
			}

			if (viewPlusMemo != null) {
				viewPlusMemo.Dispose ();
				viewPlusMemo = null;
			}

			if (lblMemoDataValue != null) {
				lblMemoDataValue.Dispose ();
				lblMemoDataValue = null;
			}

			if (lblMemoDescPlaceHolder != null) {
				lblMemoDescPlaceHolder.Dispose ();
				lblMemoDescPlaceHolder = null;
			}

			if (lblMemoLuogoHolder != null) {
				lblMemoLuogoHolder.Dispose ();
				lblMemoLuogoHolder = null;
			}

			if (lblMemoOra != null) {
				lblMemoOra.Dispose ();
				lblMemoOra = null;
			}

			if (lblMemoOraValue != null) {
				lblMemoOraValue.Dispose ();
				lblMemoOraValue = null;
			}

			if (lblMemoTitle != null) {
				lblMemoTitle.Dispose ();
				lblMemoTitle = null;
			}

			if (lblNewTicketDestinatario != null) {
				lblNewTicketDestinatario.Dispose ();
				lblNewTicketDestinatario = null;
			}

			if (lblNewTicketDestinatarioValue != null) {
				lblNewTicketDestinatarioValue.Dispose ();
				lblNewTicketDestinatarioValue = null;
			}

			if (lblNewTicketOggetto != null) {
				lblNewTicketOggetto.Dispose ();
				lblNewTicketOggetto = null;
			}

			if (lblNewTicketOggettoValue != null) {
				lblNewTicketOggettoValue.Dispose ();
				lblNewTicketOggettoValue = null;
			}

			if (lblNewTicketPartitaIva != null) {
				lblNewTicketPartitaIva.Dispose ();
				lblNewTicketPartitaIva = null;
			}

			if (lblNewTicketPartitaIvaValue != null) {
				lblNewTicketPartitaIvaValue.Dispose ();
				lblNewTicketPartitaIvaValue = null;
			}

			if (lblNewTicketRagSociale != null) {
				lblNewTicketRagSociale.Dispose ();
				lblNewTicketRagSociale = null;
			}

			if (lblNewTicketRagSocialeValue != null) {
				lblNewTicketRagSocialeValue.Dispose ();
				lblNewTicketRagSocialeValue = null;
			}

			if (lblNewTicketTIpo != null) {
				lblNewTicketTIpo.Dispose ();
				lblNewTicketTIpo = null;
			}

			if (lblNewTicketTipoValue != null) {
				lblNewTicketTipoValue.Dispose ();
				lblNewTicketTipoValue = null;
			}

			if (lblPracticaAgente != null) {
				lblPracticaAgente.Dispose ();
				lblPracticaAgente = null;
			}

			if (lblPracticaApriTicket != null) {
				lblPracticaApriTicket.Dispose ();
				lblPracticaApriTicket = null;
			}

			if (lblPracticaDataAttivazione != null) {
				lblPracticaDataAttivazione.Dispose ();
				lblPracticaDataAttivazione = null;
			}

			if (lblPracticaDataAttivazioneValue != null) {
				lblPracticaDataAttivazioneValue.Dispose ();
				lblPracticaDataAttivazioneValue = null;
			}

			if (lblPracticaDataCreazione != null) {
				lblPracticaDataCreazione.Dispose ();
				lblPracticaDataCreazione = null;
			}

			if (lblPracticaDataCreazioneValue != null) {
				lblPracticaDataCreazioneValue.Dispose ();
				lblPracticaDataCreazioneValue = null;
			}

			if (lblPracticaDataInserimento != null) {
				lblPracticaDataInserimento.Dispose ();
				lblPracticaDataInserimento = null;
			}

			if (lblPracticaDataInserimentoValue != null) {
				lblPracticaDataInserimentoValue.Dispose ();
				lblPracticaDataInserimentoValue = null;
			}

			if (lblPracticaLinee != null) {
				lblPracticaLinee.Dispose ();
				lblPracticaLinee = null;
			}

			if (lblPracticaLineeValue != null) {
				lblPracticaLineeValue.Dispose ();
				lblPracticaLineeValue = null;
			}

			if (lblPracticaProdotto != null) {
				lblPracticaProdotto.Dispose ();
				lblPracticaProdotto = null;
			}

			if (lblPracticaProdottoValue != null) {
				lblPracticaProdottoValue.Dispose ();
				lblPracticaProdottoValue = null;
			}

			if (lblPracticaQtaAttiva != null) {
				lblPracticaQtaAttiva.Dispose ();
				lblPracticaQtaAttiva = null;
			}

			if (lblPracticaQtaAttivaValue != null) {
				lblPracticaQtaAttivaValue.Dispose ();
				lblPracticaQtaAttivaValue = null;
			}

			if (lblPracticaQtaInserita != null) {
				lblPracticaQtaInserita.Dispose ();
				lblPracticaQtaInserita = null;
			}

			if (lblPracticaQtaInseritaValue != null) {
				lblPracticaQtaInseritaValue.Dispose ();
				lblPracticaQtaInseritaValue = null;
			}

			if (lblPracticaSeriali != null) {
				lblPracticaSeriali.Dispose ();
				lblPracticaSeriali = null;
			}

			if (lblPracticaSerialiValue != null) {
				lblPracticaSerialiValue.Dispose ();
				lblPracticaSerialiValue = null;
			}

			if (lblPracticaStatoOrdine != null) {
				lblPracticaStatoOrdine.Dispose ();
				lblPracticaStatoOrdine = null;
			}

			if (lblPracticaStatoOrdineValue != null) {
				lblPracticaStatoOrdineValue.Dispose ();
				lblPracticaStatoOrdineValue = null;
			}

			if (lblPracticaStatoPratica != null) {
				lblPracticaStatoPratica.Dispose ();
				lblPracticaStatoPratica = null;
			}

			if (lblPracticaStatoPraticaValue != null) {
				lblPracticaStatoPraticaValue.Dispose ();
				lblPracticaStatoPraticaValue = null;
			}

			if (lblPracticaTipoContratto != null) {
				lblPracticaTipoContratto.Dispose ();
				lblPracticaTipoContratto = null;
			}

			if (lblPracticaTipoContrattoValue != null) {
				lblPracticaTipoContrattoValue.Dispose ();
				lblPracticaTipoContrattoValue = null;
			}

			if (lblPracticaTipoProdotto != null) {
				lblPracticaTipoProdotto.Dispose ();
				lblPracticaTipoProdotto = null;
			}

			if (lblPracticaTipoProdottoValue != null) {
				lblPracticaTipoProdottoValue.Dispose ();
				lblPracticaTipoProdottoValue = null;
			}

			if (lblPracticaTitle != null) {
				lblPracticaTitle.Dispose ();
				lblPracticaTitle = null;
			}

			if (lblPraticaCanone != null) {
				lblPraticaCanone.Dispose ();
				lblPraticaCanone = null;
			}

			if (lblPraticaCanoneValue != null) {
				lblPraticaCanoneValue.Dispose ();
				lblPraticaCanoneValue = null;
			}

			if (lblSerialeInsSeriale != null) {
				lblSerialeInsSeriale.Dispose ();
				lblSerialeInsSeriale = null;
			}

			if (lblSerialeTitle != null) {
				lblSerialeTitle.Dispose ();
				lblSerialeTitle = null;
			}

			if (scrlViewPractica != null) {
				scrlViewPractica.Dispose ();
				scrlViewPractica = null;
			}

			if (tblClientiPratiche != null) {
				tblClientiPratiche.Dispose ();
				tblClientiPratiche = null;
			}

			if (tblClientiRegSociale != null) {
				tblClientiRegSociale.Dispose ();
				tblClientiRegSociale = null;
			}

			if (tblMemo != null) {
				tblMemo.Dispose ();
				tblMemo = null;
			}

			if (tfClientiSearch != null) {
				tfClientiSearch.Dispose ();
				tfClientiSearch = null;
			}

			if (tfMemoClientiPlace != null) {
				tfMemoClientiPlace.Dispose ();
				tfMemoClientiPlace = null;
			}

			if (tfMemoDescription != null) {
				tfMemoDescription.Dispose ();
				tfMemoDescription = null;
			}

			if (tfMemoTitle != null) {
				tfMemoTitle.Dispose ();
				tfMemoTitle = null;
			}

			if (tfNewTicketDescription != null) {
				tfNewTicketDescription.Dispose ();
				tfNewTicketDescription = null;
			}

			if (tfNewTicketOggetto != null) {
				tfNewTicketOggetto.Dispose ();
				tfNewTicketOggetto = null;
			}

			if (tfSerialeInserSeriale != null) {
				tfSerialeInserSeriale.Dispose ();
				tfSerialeInserSeriale = null;
			}

			if (txtViewMemoDescription != null) {
				txtViewMemoDescription.Dispose ();
				txtViewMemoDescription = null;
			}

			if (viewAltoBottom != null) {
				viewAltoBottom.Dispose ();
				viewAltoBottom = null;
			}

			if (viewAltuofianco != null) {
				viewAltuofianco.Dispose ();
				viewAltuofianco = null;
			}

			if (viewAltuofiancoContent != null) {
				viewAltuofiancoContent.Dispose ();
				viewAltuofiancoContent = null;
			}

			if (viewBgUnderTable != null) {
				viewBgUnderTable.Dispose ();
				viewBgUnderTable = null;
			}

			if (viewBottomAltuofiancoPratiche != null) {
				viewBottomAltuofiancoPratiche.Dispose ();
				viewBottomAltuofiancoPratiche = null;
			}

			if (viewBottomCallEmail != null) {
				viewBottomCallEmail.Dispose ();
				viewBottomCallEmail = null;
			}

			if (viewClientiCall != null) {
				viewClientiCall.Dispose ();
				viewClientiCall = null;
			}

			if (viewClientiContent != null) {
				viewClientiContent.Dispose ();
				viewClientiContent = null;
			}

			if (viewClientiFilter != null) {
				viewClientiFilter.Dispose ();
				viewClientiFilter = null;
			}

			if (viewClientiInfo != null) {
				viewClientiInfo.Dispose ();
				viewClientiInfo = null;
			}

			if (viewClientiMemo != null) {
				viewClientiMemo.Dispose ();
				viewClientiMemo = null;
			}

			if (viewClientiPratiche != null) {
				viewClientiPratiche.Dispose ();
				viewClientiPratiche = null;
			}

			if (viewClientiPraticheDataAttivazione != null) {
				viewClientiPraticheDataAttivazione.Dispose ();
				viewClientiPraticheDataAttivazione = null;
			}

			if (viewClientiPraticheInstradamento != null) {
				viewClientiPraticheInstradamento.Dispose ();
				viewClientiPraticheInstradamento = null;
			}

			if (viewClientiPraticheLinee != null) {
				viewClientiPraticheLinee.Dispose ();
				viewClientiPraticheLinee = null;
			}

			if (viewClientiPraticheNote != null) {
				viewClientiPraticheNote.Dispose ();
				viewClientiPraticheNote = null;
			}

			if (viewClientiPraticheProdotto != null) {
				viewClientiPraticheProdotto.Dispose ();
				viewClientiPraticheProdotto = null;
			}

			if (viewClientiPraticheSeriale != null) {
				viewClientiPraticheSeriale.Dispose ();
				viewClientiPraticheSeriale = null;
			}

			if (viewClientiSendEmail != null) {
				viewClientiSendEmail.Dispose ();
				viewClientiSendEmail = null;
			}

			if (viewCreateTicketChooseTipo != null) {
				viewCreateTicketChooseTipo.Dispose ();
				viewCreateTicketChooseTipo = null;
			}

			if (viewCreateTicketContent != null) {
				viewCreateTicketContent.Dispose ();
				viewCreateTicketContent = null;
			}

			if (viewInviaNewTicket != null) {
				viewInviaNewTicket.Dispose ();
				viewInviaNewTicket = null;
			}

			if (viewMemoBottom != null) {
				viewMemoBottom.Dispose ();
				viewMemoBottom = null;
			}

			if (viewMemoContent != null) {
				viewMemoContent.Dispose ();
				viewMemoContent = null;
			}

			if (viewPracticaBottom != null) {
				viewPracticaBottom.Dispose ();
				viewPracticaBottom = null;
			}

			if (viewPracticaDetailContent != null) {
				viewPracticaDetailContent.Dispose ();
				viewPracticaDetailContent = null;
			}

			if (viewPracticTopBar != null) {
				viewPracticTopBar.Dispose ();
				viewPracticTopBar = null;
			}

			if (viewSerialeContent != null) {
				viewSerialeContent.Dispose ();
				viewSerialeContent = null;
			}

			if (viewTopBar != null) {
				viewTopBar.Dispose ();
				viewTopBar = null;
			}
		}
	}
}
