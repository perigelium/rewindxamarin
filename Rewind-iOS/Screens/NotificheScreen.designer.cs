// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace RewindiOS
{
	[Register ("NotificheScreen")]
	partial class NotificheScreen
	{
		[Outlet]
		UIKit.UIButton btnLetto { get; set; }

		[Outlet]
		UIKit.UIButton btnTopMenuBack { get; set; }

		[Outlet]
		UIKit.UIImageView imgTopMenuBack { get; set; }

		[Outlet]
		UIKit.UILabel lblCoomingSoon { get; set; }

		[Outlet]
		UIKit.UILabel lblLetto { get; set; }

		[Outlet]
		UIKit.UILabel lblMessage { get; set; }

		[Outlet]
		UIKit.UILabel lblTitle { get; set; }

		[Outlet]
		UIKit.UILabel lblTopMenuTitle { get; set; }

		[Outlet]
		UIKit.UITableView tblMessageItems { get; set; }

		[Outlet]
		UIKit.UIView viewLetto { get; set; }

		[Outlet]
		UIKit.UIView viewTopMenuBack { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (btnLetto != null) {
				btnLetto.Dispose ();
				btnLetto = null;
			}

			if (btnTopMenuBack != null) {
				btnTopMenuBack.Dispose ();
				btnTopMenuBack = null;
			}

			if (imgTopMenuBack != null) {
				imgTopMenuBack.Dispose ();
				imgTopMenuBack = null;
			}

			if (lblLetto != null) {
				lblLetto.Dispose ();
				lblLetto = null;
			}

			if (lblMessage != null) {
				lblMessage.Dispose ();
				lblMessage = null;
			}

			if (lblTitle != null) {
				lblTitle.Dispose ();
				lblTitle = null;
			}

			if (lblTopMenuTitle != null) {
				lblTopMenuTitle.Dispose ();
				lblTopMenuTitle = null;
			}

			if (tblMessageItems != null) {
				tblMessageItems.Dispose ();
				tblMessageItems = null;
			}

			if (viewLetto != null) {
				viewLetto.Dispose ();
				viewLetto = null;
			}

			if (viewTopMenuBack != null) {
				viewTopMenuBack.Dispose ();
				viewTopMenuBack = null;
			}

			if (lblCoomingSoon != null) {
				lblCoomingSoon.Dispose ();
				lblCoomingSoon = null;
			}
		}
	}
}
