// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace RewindiOS
{
	[Register ("LoginScreen")]
	partial class LoginScreen
	{
		[Outlet]
		UIKit.UIButton btnLogin { get; set; }

		[Outlet]
		UIKit.UITextField tfPassword { get; set; }

		[Outlet]
		UIKit.UITextField tfUSer { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (btnLogin != null) {
				btnLogin.Dispose ();
				btnLogin = null;
			}

			if (tfPassword != null) {
				tfPassword.Dispose ();
				tfPassword = null;
			}

			if (tfUSer != null) {
				tfUSer.Dispose ();
				tfUSer = null;
			}
		}
	}
}
