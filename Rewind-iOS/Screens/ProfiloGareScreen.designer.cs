// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace RewindiOS
{
	[Register ("ProfiloGareScreen")]
	partial class ProfiloGareScreen
	{
		[Outlet]
		UIKit.UIButton btnDataAl { get; set; }

		[Outlet]
		UIKit.UIButton btnDataDal { get; set; }

		[Outlet]
		UIKit.UIButton btnTopMenuBack { get; set; }

		[Outlet]
		UIKit.UIImageView imgTopMenuBack { get; set; }

		[Outlet]
		UIKit.UILabel lblAgente { get; set; }

		[Outlet]
		UIKit.UILabel lblDataAl { get; set; }

		[Outlet]
		UIKit.UILabel lblDataDal { get; set; }

		[Outlet]
		UIKit.UILabel lblPa { get; set; }

		[Outlet]
		UIKit.UILabel lblPos { get; set; }

		[Outlet]
		UIKit.UILabel lblPs { get; set; }

		[Outlet]
		UIKit.UILabel lblPunti { get; set; }

		[Outlet]
		UIKit.UILabel lblSs { get; set; }

		[Outlet]
		UIKit.UILabel lblTopMenuTitle { get; set; }

		[Outlet]
		UIKit.UITableView tblGareItems { get; set; }

		[Outlet]
		UIKit.UILabel txtDataAl { get; set; }

		[Outlet]
		UIKit.UILabel txtDataDal { get; set; }

		[Outlet]
		UIKit.UIView viewDataAl { get; set; }

		[Outlet]
		UIKit.UIView viewDataDal { get; set; }

		[Outlet]
		UIKit.UIView viewTopMenuBack { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (btnDataAl != null) {
				btnDataAl.Dispose ();
				btnDataAl = null;
			}

			if (btnDataDal != null) {
				btnDataDal.Dispose ();
				btnDataDal = null;
			}

			if (btnTopMenuBack != null) {
				btnTopMenuBack.Dispose ();
				btnTopMenuBack = null;
			}

			if (imgTopMenuBack != null) {
				imgTopMenuBack.Dispose ();
				imgTopMenuBack = null;
			}

			if (lblAgente != null) {
				lblAgente.Dispose ();
				lblAgente = null;
			}

			if (lblDataAl != null) {
				lblDataAl.Dispose ();
				lblDataAl = null;
			}

			if (lblDataDal != null) {
				lblDataDal.Dispose ();
				lblDataDal = null;
			}

			if (lblPa != null) {
				lblPa.Dispose ();
				lblPa = null;
			}

			if (lblPos != null) {
				lblPos.Dispose ();
				lblPos = null;
			}

			if (lblPs != null) {
				lblPs.Dispose ();
				lblPs = null;
			}

			if (lblPunti != null) {
				lblPunti.Dispose ();
				lblPunti = null;
			}

			if (lblSs != null) {
				lblSs.Dispose ();
				lblSs = null;
			}

			if (lblTopMenuTitle != null) {
				lblTopMenuTitle.Dispose ();
				lblTopMenuTitle = null;
			}

			if (txtDataAl != null) {
				txtDataAl.Dispose ();
				txtDataAl = null;
			}

			if (txtDataDal != null) {
				txtDataDal.Dispose ();
				txtDataDal = null;
			}

			if (viewDataAl != null) {
				viewDataAl.Dispose ();
				viewDataAl = null;
			}

			if (viewDataDal != null) {
				viewDataDal.Dispose ();
				viewDataDal = null;
			}

			if (viewTopMenuBack != null) {
				viewTopMenuBack.Dispose ();
				viewTopMenuBack = null;
			}

			if (tblGareItems != null) {
				tblGareItems.Dispose ();
				tblGareItems = null;
			}
		}
	}
}
