// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace RewindiOS
{
	[Register ("HomeScreen")]
	partial class HomeScreen
	{
		[Outlet]
		UIKit.UIButton btnAppuntamenti { get; set; }

		[Outlet]
		UIKit.UIButton btnClienti { get; set; }

		[Outlet]
		UIKit.UIButton btnNotifiche { get; set; }

		[Outlet]
		UIKit.UIButton btnProfilo { get; set; }

		[Outlet]
		UIKit.UILabel lblAppuntamenti { get; set; }

		[Outlet]
		UIKit.UILabel lblClienti { get; set; }

		[Outlet]
		UIKit.UILabel lblNotifiche { get; set; }

		[Outlet]
		UIKit.UILabel lblProfilo { get; set; }

		[Outlet]
		UIKit.UIImageView txtProfilo { get; set; }

		[Outlet]
		UIKit.UIView viewAppuntamenti { get; set; }

		[Outlet]
		UIKit.UIView viewClienti { get; set; }

		[Outlet]
		UIKit.UIView viewNotifiche { get; set; }

		[Outlet]
		UIKit.UIView viewProfilo { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (viewProfilo != null) {
				viewProfilo.Dispose ();
				viewProfilo = null;
			}

			if (lblProfilo != null) {
				lblProfilo.Dispose ();
				lblProfilo = null;
			}

			if (txtProfilo != null) {
				txtProfilo.Dispose ();
				txtProfilo = null;
			}

			if (btnProfilo != null) {
				btnProfilo.Dispose ();
				btnProfilo = null;
			}

			if (viewNotifiche != null) {
				viewNotifiche.Dispose ();
				viewNotifiche = null;
			}

			if (lblNotifiche != null) {
				lblNotifiche.Dispose ();
				lblNotifiche = null;
			}

			if (btnNotifiche != null) {
				btnNotifiche.Dispose ();
				btnNotifiche = null;
			}

			if (viewClienti != null) {
				viewClienti.Dispose ();
				viewClienti = null;
			}

			if (lblClienti != null) {
				lblClienti.Dispose ();
				lblClienti = null;
			}

			if (btnClienti != null) {
				btnClienti.Dispose ();
				btnClienti = null;
			}

			if (viewAppuntamenti != null) {
				viewAppuntamenti.Dispose ();
				viewAppuntamenti = null;
			}

			if (lblAppuntamenti != null) {
				lblAppuntamenti.Dispose ();
				lblAppuntamenti = null;
			}

			if (btnAppuntamenti != null) {
				btnAppuntamenti.Dispose ();
				btnAppuntamenti = null;
			}
		}
	}
}
