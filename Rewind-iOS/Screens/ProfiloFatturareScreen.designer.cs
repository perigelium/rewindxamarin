// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace RewindiOS
{
	[Register ("ProfiloFatturareScreen")]
	partial class ProfiloFatturareScreen
	{
		[Outlet]
		UIKit.UIButton btnDataAl { get; set; }

		[Outlet]
		UIKit.UIButton btnDataDal { get; set; }

		[Outlet]
		UIKit.UIButton btnTopMenuBack { get; set; }

		[Outlet]
		UIKit.UIImageView imgTopMenuBack { get; set; }

		[Outlet]
		UIKit.UILabel lblDataAl { get; set; }

		[Outlet]
		UIKit.UILabel lblDataDal { get; set; }

		[Outlet]
		UIKit.UILabel lblTopMenuTitle { get; set; }

		[Outlet]
		UIKit.UITableView tblFatturareItems { get; set; }

		[Outlet]
		UIKit.UILabel txtDataAl { get; set; }

		[Outlet]
		UIKit.UILabel txtDataDal { get; set; }

		[Outlet]
		UIKit.UIView viewDataAl { get; set; }

		[Outlet]
		UIKit.UIView viewDataDal { get; set; }

		[Outlet]
		UIKit.UIView viewTopMenuBack { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (btnTopMenuBack != null) {
				btnTopMenuBack.Dispose ();
				btnTopMenuBack = null;
			}

			if (imgTopMenuBack != null) {
				imgTopMenuBack.Dispose ();
				imgTopMenuBack = null;
			}

			if (lblTopMenuTitle != null) {
				lblTopMenuTitle.Dispose ();
				lblTopMenuTitle = null;
			}

			if (viewTopMenuBack != null) {
				viewTopMenuBack.Dispose ();
				viewTopMenuBack = null;
			}

			if (viewDataDal != null) {
				viewDataDal.Dispose ();
				viewDataDal = null;
			}

			if (lblDataDal != null) {
				lblDataDal.Dispose ();
				lblDataDal = null;
			}

			if (txtDataDal != null) {
				txtDataDal.Dispose ();
				txtDataDal = null;
			}

			if (btnDataDal != null) {
				btnDataDal.Dispose ();
				btnDataDal = null;
			}

			if (viewDataAl != null) {
				viewDataAl.Dispose ();
				viewDataAl = null;
			}

			if (lblDataAl != null) {
				lblDataAl.Dispose ();
				lblDataAl = null;
			}

			if (txtDataAl != null) {
				txtDataAl.Dispose ();
				txtDataAl = null;
			}

			if (btnDataAl != null) {
				btnDataAl.Dispose ();
				btnDataAl = null;
			}

			if (tblFatturareItems != null) {
				tblFatturareItems.Dispose ();
				tblFatturareItems = null;
			}
		}
	}
}
