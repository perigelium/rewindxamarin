// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace RewindiOS
{
    [Register ("AppuntamentiScreen")]
    partial class AppuntamentiScreen
    {
        [Outlet]
        UIKit.UIButton btnAppOggiCall { get; set; }


        [Outlet]
        UIKit.UIButton btnAppOggiChooseStatus { get; set; }


        [Outlet]
        UIKit.UIButton btnAppOggiMoreDetails { get; set; }


        [Outlet]
        UIKit.UIButton btnAppOggiSalva { get; set; }


        [Outlet]
        UIKit.UIButton btnAppOggiSendEmail { get; set; }


        [Outlet]
        UIKit.UIButton btnAppTopMenuApp { get; set; }


        [Outlet]
        UIKit.UIButton btnAppTopMenuBack { get; set; }


        [Outlet]
        UIKit.UIButton btnAppTopMenuCalendar { get; set; }


        [Outlet]
        UIKit.UIButton btnAppTopMenuLocation { get; set; }


        [Outlet]
        UIKit.UIButton btnAppTopMenuSearch { get; set; }


        [Outlet]
        UIKit.UIButton btnCalendarioAggImpPersonale { get; set; }


        [Outlet]
        UIKit.UIButton btnChangeAppHourDay { get; set; }


        [Outlet]
        UIKit.UIButton btnChBoxBorsellino { get; set; }


        [Outlet]
        UIKit.UIButton btnChBoxDeact { get; set; }


        [Outlet]
        UIKit.UIButton btnChBoxDiOggi { get; set; }


        [Outlet]
        UIKit.UIButton btnChBoxNonConvergenti { get; set; }


        [Outlet]
        UIKit.UIButton btnChBoxNonEsitati { get; set; }


        [Outlet]
        UIKit.UIButton btnChBoxTrattativa { get; set; }


        [Outlet]
        UIKit.UIButton btnCheckBoxConvergenti { get; set; }


        [Outlet]
        UIKit.UIButton btnImpPersChooseDateTo { get; set; }


        [Outlet]
        UIKit.UIButton btnImpPersChooseHour { get; set; }


        [Outlet]
        UIKit.UIButton btnImpPersDataFrom { get; set; }


        [Outlet]
        UIKit.UIButton btnImpPersSalva { get; set; }


        [Outlet]
        UIKit.UIButton btnLocationAppuntamenti { get; set; }


        [Outlet]
        UIKit.UIButton btnLocationCerca { get; set; }


        [Outlet]
        UIKit.UIButton btnLocationClienti { get; set; }


        [Outlet]
        UIKit.UIButton btnLocationNavigator { get; set; }


        [Outlet]
        UIKit.UIButton btnLocationNavMinus { get; set; }


        [Outlet]
        UIKit.UIButton btnLocationNavPlus { get; set; }


        [Outlet]
        UIKit.UIButton btnMemoVaiAlCliente { get; set; }


        [Outlet]
        UIKit.UIButton btnNavigatordiOggi { get; set; }


        [Outlet]
        UIKit.UIButton btnSearchAppChooseStatus { get; set; }


        [Outlet]
        UIKit.UIButton btnSearchAppMoreDetails { get; set; }


        [Outlet]
        UIKit.UIButton btnSearchCall { get; set; }


        [Outlet]
        UIKit.UIButton btnSearchChangeAppHour { get; set; }


        [Outlet]
        UIKit.UIButton btnSearchEmail { get; set; }


        [Outlet]
        UIKit.UIButton btnSearchLocalita { get; set; }


        [Outlet]
        UIKit.UIButton btnSearchRegSociale { get; set; }


        [Outlet]
        UIKit.UIButton btnSearchSalva { get; set; }


        [Outlet]
        UIKit.UIButton btnSearchVaiAlClienteMemo { get; set; }


        [Outlet]
        UIKit.UIButton btnSearchWord { get; set; }


        [Outlet]
        UIKit.UITextView editTextAppOggiDescription { get; set; }


        [Outlet]
        UIKit.UITextView editTextAppOggiMotivoEsito { get; set; }


        [Outlet]
        UIKit.UITextView editTextAppOggiNote { get; set; }


        [Outlet]
        UIKit.UIImageView imgAppOggiStatus { get; set; }


        [Outlet]
        UIKit.UIImageView imgAppTopMenuApp { get; set; }


        [Outlet]
        UIKit.UIImageView imgAppTopMenuBack { get; set; }


        [Outlet]
        UIKit.UIImageView imgAppTopMenuCalendar { get; set; }


        [Outlet]
        UIKit.UIImageView imgAppTopMenuLocation { get; set; }


        [Outlet]
        UIKit.UIImageView imgAppTopMenuSearch { get; set; }


        [Outlet]
        UIKit.UIImageView imgArrowLocationAppuntamenti { get; set; }


        [Outlet]
        UIKit.UIImageView imgArrowLocationClienti { get; set; }


        [Outlet]
        UIKit.UIImageView imgChBoxBorsellino { get; set; }


        [Outlet]
        UIKit.UIImageView imgChBoxConvergenti { get; set; }


        [Outlet]
        UIKit.UIImageView imgChBoxDeact { get; set; }


        [Outlet]
        UIKit.UIImageView imgChBoxDiOggi { get; set; }


        [Outlet]
        UIKit.UIImageView imgChBoxNonConvergenti { get; set; }


        [Outlet]
        UIKit.UIImageView imgChBoxNonEsitati { get; set; }


        [Outlet]
        UIKit.UIImageView imgChBoxTrattativa { get; set; }


        [Outlet]
        UIKit.UIImageView imgSearchApparrow { get; set; }


        [Outlet]
        UIKit.UIImageView imgSearchIcon { get; set; }


        [Outlet]
        UIKit.UILabel lblAppOggiDetCategoria { get; set; }


        [Outlet]
        UIKit.UILabel lblAppOggiDetCategoriaValue { get; set; }


        [Outlet]
        UIKit.UILabel lblAppOggiDetCodChiamata { get; set; }


        [Outlet]
        UIKit.UILabel lblAppOggiDetCodChiamataValue { get; set; }


        [Outlet]
        UIKit.UILabel lblAppOggiDetContatti { get; set; }


        [Outlet]
        UIKit.UILabel lblAppOggiDetContattiValue1 { get; set; }


        [Outlet]
        UIKit.UILabel lblAppOggiDetContattiValue2 { get; set; }


        [Outlet]
        UIKit.UILabel lblAppOggiDetContattiValue3 { get; set; }


        [Outlet]
        UIKit.UILabel lblAppOggiDetContattiValue4 { get; set; }


        [Outlet]
        UIKit.UILabel lblAppOggiDetDataApp { get; set; }


        [Outlet]
        UIKit.UILabel lblAppOggiDetDataAppValue { get; set; }


        [Outlet]
        UIKit.UILabel lblAppOggiDetDataChiamata { get; set; }


        [Outlet]
        UIKit.UILabel lblAppOggiDetDataChiamataValue { get; set; }


        [Outlet]
        UIKit.UILabel lblAppOggiDetIndirizzo { get; set; }


        [Outlet]
        UIKit.UILabel lblAppOggiDetIndirizzoValue1 { get; set; }


        [Outlet]
        UIKit.UILabel lblAppOggiDetIndirizzoValue2 { get; set; }


        [Outlet]
        UIKit.UILabel lblAppOggiDetMotive { get; set; }


        [Outlet]
        UIKit.UILabel lblAppOggiDetMotiveValue { get; set; }


        [Outlet]
        UIKit.UILabel lblAppOggiDetNoteAg { get; set; }


        [Outlet]
        UIKit.UILabel lblAppOggiDetNoteAgValue { get; set; }


        [Outlet]
        UIKit.UILabel lblAppOggiDetNoteChiamata { get; set; }


        [Outlet]
        UIKit.UILabel lblAppOggiDetNoteChiamataValue { get; set; }


        [Outlet]
        UIKit.UILabel lblAppOggiDetNoteSecretaria { get; set; }


        [Outlet]
        UIKit.UILabel lblAppOggiDetNoteSecretariaValue { get; set; }


        [Outlet]
        UIKit.UILabel lblAppOggiDetOraApp { get; set; }


        [Outlet]
        UIKit.UILabel lblAppOggiDetOraAppValue { get; set; }


        [Outlet]
        UIKit.UILabel lblAppOggiDetRagSociale { get; set; }


        [Outlet]
        UIKit.UILabel lblAppOggiDetRagSocialeValue { get; set; }


        [Outlet]
        UIKit.UILabel lblAppOggiDetRiepilogDati { get; set; }


        [Outlet]
        UIKit.UILabel lblAppOggiEmail { get; set; }


        [Outlet]
        UIKit.UILabel lblAppOggiMemoLuogo { get; set; }


        [Outlet]
        UIKit.UILabel lblAppOggiMemoMuogoValue { get; set; }


        [Outlet]
        UIKit.UILabel lblAppOggiMemoTitle { get; set; }


        [Outlet]
        UIKit.UILabel lblAppOggiMemoTitolo { get; set; }


        [Outlet]
        UIKit.UILabel lblAppOggiMemoTitoloValue { get; set; }


        [Outlet]
        UIKit.UILabel lblAppOggiNameReg { get; set; }


        [Outlet]
        UIKit.UILabel lblAppOggiNotePlaceHolder { get; set; }


        [Outlet]
        UIKit.UILabel lblAppOggiPhone { get; set; }


        [Outlet]
        UIKit.UILabel lblAppOggiRegSociale { get; set; }


        [Outlet]
        UIKit.UILabel lblAppOggiStatus { get; set; }


        [Outlet]
        UIKit.UILabel lblAppOggiStatusLabel { get; set; }


        [Outlet]
        UIKit.UILabel lblCalendarioAggImpPersonale { get; set; }


        [Outlet]
        UIKit.UILabel lblCalendarioTitle { get; set; }


        [Outlet]
        UIKit.UILabel lblChBoxBorsellino { get; set; }


        [Outlet]
        UIKit.UILabel lblChBoxConvergenti { get; set; }


        [Outlet]
        UIKit.UILabel lblChBoxDeact { get; set; }


        [Outlet]
        UIKit.UILabel lblChBoxDiOggi { get; set; }


        [Outlet]
        UIKit.UILabel lblChBoxNonConvergenti { get; set; }


        [Outlet]
        UIKit.UILabel lblChBoxNonEsitati { get; set; }


        [Outlet]
        UIKit.UILabel lblChBoxTrattativa { get; set; }


        [Outlet]
        UIKit.UILabel lblImpPersDataAl { get; set; }


        [Outlet]
        UIKit.UILabel lblImpPersDataDal { get; set; }


        [Outlet]
        UIKit.UILabel lblImpPersDataToValue { get; set; }


        [Outlet]
        UIKit.UILabel lblImpPersDescPlaceHolder { get; set; }


        [Outlet]
        UIKit.UILabel lblImpPersDetailsDataAl { get; set; }


        [Outlet]
        UIKit.UILabel lblImpPersDetailsDataAlValue { get; set; }


        [Outlet]
        UIKit.UILabel lblImpPersDetailsDataDal { get; set; }


        [Outlet]
        UIKit.UILabel lblImpPersDetailsDataDalValue { get; set; }


        [Outlet]
        UIKit.UILabel lblImpPersDetailsDescription { get; set; }


        [Outlet]
        UIKit.UILabel lblImpPersDetailsLuogo { get; set; }


        [Outlet]
        UIKit.UILabel lblImpPersDetailsLuogoValue { get; set; }


        [Outlet]
        UIKit.UILabel lblImpPersDetailsOra { get; set; }


        [Outlet]
        UIKit.UILabel lblImpPersDetailsOraValue { get; set; }


        [Outlet]
        UIKit.UILabel lblImpPersDetailsTitle { get; set; }


        [Outlet]
        UIKit.UILabel lblImpPersHour { get; set; }


        [Outlet]
        UIKit.UILabel lblImpPersHourValue { get; set; }


        [Outlet]
        UIKit.UILabel lblImpPersonaleDataFromValue { get; set; }


        [Outlet]
        UIKit.UILabel lblImpPersPlace { get; set; }


        [Outlet]
        UIKit.UILabel lblImpPersSalva { get; set; }


        [Outlet]
        UIKit.UILabel lblImpPersTitle { get; set; }


        [Outlet]
        UIKit.UILabel lblLocationAppuntamenti { get; set; }


        [Outlet]
        UIKit.UILabel lblLocationCerca { get; set; }


        [Outlet]
        UIKit.UILabel lblLocationClienti { get; set; }


        [Outlet]
        UIKit.UILabel lblMotivoSearchPlaceHolder { get; set; }


        [Outlet]
        UIKit.UILabel lblSearchAppEmail { get; set; }


        [Outlet]
        UIKit.UILabel lblSearchAppPhone { get; set; }


        [Outlet]
        UIKit.UILabel lblSearchAppRegSociale { get; set; }


        [Outlet]
        UIKit.UILabel lblSearchAppRepLegale { get; set; }


        [Outlet]
        UIKit.UILabel lblSearchAppStatus { get; set; }


        [Outlet]
        UIKit.UILabel lblSearchAppStatusLabel { get; set; }


        [Outlet]
        UIKit.UILabel lblSearchLocalita { get; set; }


        [Outlet]
        UIKit.UILabel lblSearchMemoLuogo { get; set; }


        [Outlet]
        UIKit.UILabel lblSearchMemoLuogoValue { get; set; }


        [Outlet]
        UIKit.UILabel lblSearchMemoTitle { get; set; }


        [Outlet]
        UIKit.UILabel lblSearchMemoTitleDescription { get; set; }


        [Outlet]
        UIKit.UILabel lblSearchMemoTitleDescriptionValue { get; set; }


        [Outlet]
        UIKit.UILabel lblSearchRagSociale { get; set; }


        [Outlet]
        MapKit.MKMapView mapEvents { get; set; }


        [Outlet]
        MapKit.MKMapView mapViewLocation { get; set; }


        [Outlet]
        UIKit.UIScrollView scrlSearchMain { get; set; }


        [Outlet]
        UIKit.UIScrollView scrlViewAppOggi { get; set; }


        [Outlet]
        UIKit.UITableView tblSearchList { get; set; }


        [Outlet]
        UIKit.UITableView tblViewAppdiOgi { get; set; }


        [Outlet]
        UIKit.UITextView textViewSearchMemoDescription { get; set; }


        [Outlet]
        UIKit.UITextField tfeditTextAppOggiNote { get; set; }


        [Outlet]
        UIKit.UITextField tfImpPersDescription { get; set; }


        [Outlet]
        UIKit.UITextField tfImpPersPlace { get; set; }


        [Outlet]
        UIKit.UITextField tfSearchDescriptionAppNote { get; set; }


        [Outlet]
        UIKit.UITextField tfSearchWord { get; set; }


        [Outlet]
        UIKit.UITextView txtViewAppOggiMemoDescription { get; set; }


        [Outlet]
        UIKit.UITextView txtViewImpPersDescription { get; set; }


        [Outlet]
        UIKit.UITextView txtViewImpPersDetailsDescription { get; set; }


        [Outlet]
        UIKit.UITextView txtViewSearchAppDescr { get; set; }


        [Outlet]
        UIKit.UITextView txtViewSearchAPpNote { get; set; }


        [Outlet]
        UIKit.UITableView txtViewSearchMemoDescription { get; set; }


        [Outlet]
        UIKit.UIView viewAppCalendar { get; set; }


        [Outlet]
        UIKit.UIView viewAppdiOggiEvents { get; set; }


        [Outlet]
        UIKit.UIView viewAppOggiBottomBar { get; set; }


        [Outlet]
        UIKit.UIView viewAppOggiCall { get; set; }


        [Outlet]
        UIKit.UIView viewAppOggiMain { get; set; }


        [Outlet]
        UIKit.UIView viewAppOggiMemoDetails { get; set; }


        [Outlet]
        UIKit.UIView viewAppOggiOver { get; set; }


        [Outlet]
        UIKit.UIView viewAppOggiSalva { get; set; }


        [Outlet]
        UIKit.UIView viewAppOggiSCrollChild { get; set; }


        [Outlet]
        UIKit.UIView viewAppOggiSendEmail { get; set; }


        [Outlet]
        UIKit.UIView viewAppOggiSmallInfoDetail { get; set; }


        [Outlet]
        UIKit.UIView viewAppOggiStatus { get; set; }


        [Outlet]
        UIKit.UIView viewAppTopMenu { get; set; }


        [Outlet]
        UIKit.UIView viewAppTopMenuApp { get; set; }


        [Outlet]
        UIKit.UIView viewAppTopMenuBack { get; set; }


        [Outlet]
        UIKit.UIView viewAppTopMenuCalendar { get; set; }


        [Outlet]
        UIKit.UIView viewAppTopMenuLocation { get; set; }


        [Outlet]
        UIKit.UIView viewAppTopMenuSearch { get; set; }


        [Outlet]
        UIKit.UIView viewAppuntamentiMoreDetails { get; set; }


        [Outlet]
        UIKit.UIView viewCalendarioAggImpPersonale { get; set; }


        [Outlet]
        UIKit.UIView viewCalendarioContent { get; set; }


        [Outlet]
        UIKit.UIView viewContentAggImpPersonale { get; set; }


        [Outlet]
        UIKit.UIView viewContentLocationMap { get; set; }


        [Outlet]
        UIKit.UIView viewContentSearch { get; set; }


        [Outlet]
        UIKit.UIView viewImpPersonaleDetails { get; set; }


        [Outlet]
        UIKit.UIView viewImpPersSalva { get; set; }


        [Outlet]
        UIKit.UIView viewLocationAppCheckboxes { get; set; }


        [Outlet]
        UIKit.UIView viewLocationAppuntamenti { get; set; }


        [Outlet]
        UIKit.UIView viewLocationCerca { get; set; }


        [Outlet]
        UIKit.UIView viewLocationClienti { get; set; }


        [Outlet]
        UIKit.UIView viewLocationClientiCheckBoxes { get; set; }


        [Outlet]
        UIKit.UIView viewMemoVaiAlCliente { get; set; }


        [Outlet]
        UIKit.UIView viewSearchAppMoreDetails { get; set; }


        [Outlet]
        UIKit.UIView viewSearchBottomBar { get; set; }


        [Outlet]
        UIKit.UIView viewSearchCall { get; set; }


        [Outlet]
        UIKit.UIView viewSearchChooseStatus { get; set; }


        [Outlet]
        UIKit.UIView viewSearchEmail { get; set; }


        [Outlet]
        UIKit.UIView viewSearchMainDetails { get; set; }


        [Outlet]
        UIKit.UIView viewSearchMemoDetails { get; set; }


        [Outlet]
        UIKit.UIView viewSearchSalva { get; set; }


        [Outlet]
        UIKit.UIView viewSearchSmallInfoDetails { get; set; }


        [Outlet]
        UIKit.UIView viewSearchVaiAlClienteMemo { get; set; }


        [Outlet]
        UIKit.UIView viewTblOverSearchApp { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextView txtViewImpPersDetailsEventPers { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView viewAppOggiMemoLuogo { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView viewAppOggiMemoTitle { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView viewImpPersDetailsLuogo { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView viewSearchMemoLuogo { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView viewSearchMemoTitleDescription { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (btnAppOggiCall != null) {
                btnAppOggiCall.Dispose ();
                btnAppOggiCall = null;
            }

            if (btnAppOggiChooseStatus != null) {
                btnAppOggiChooseStatus.Dispose ();
                btnAppOggiChooseStatus = null;
            }

            if (btnAppOggiMoreDetails != null) {
                btnAppOggiMoreDetails.Dispose ();
                btnAppOggiMoreDetails = null;
            }

            if (btnAppOggiSalva != null) {
                btnAppOggiSalva.Dispose ();
                btnAppOggiSalva = null;
            }

            if (btnAppOggiSendEmail != null) {
                btnAppOggiSendEmail.Dispose ();
                btnAppOggiSendEmail = null;
            }

            if (btnAppTopMenuApp != null) {
                btnAppTopMenuApp.Dispose ();
                btnAppTopMenuApp = null;
            }

            if (btnAppTopMenuBack != null) {
                btnAppTopMenuBack.Dispose ();
                btnAppTopMenuBack = null;
            }

            if (btnAppTopMenuCalendar != null) {
                btnAppTopMenuCalendar.Dispose ();
                btnAppTopMenuCalendar = null;
            }

            if (btnAppTopMenuLocation != null) {
                btnAppTopMenuLocation.Dispose ();
                btnAppTopMenuLocation = null;
            }

            if (btnAppTopMenuSearch != null) {
                btnAppTopMenuSearch.Dispose ();
                btnAppTopMenuSearch = null;
            }

            if (btnCalendarioAggImpPersonale != null) {
                btnCalendarioAggImpPersonale.Dispose ();
                btnCalendarioAggImpPersonale = null;
            }

            if (btnChangeAppHourDay != null) {
                btnChangeAppHourDay.Dispose ();
                btnChangeAppHourDay = null;
            }

            if (btnChBoxBorsellino != null) {
                btnChBoxBorsellino.Dispose ();
                btnChBoxBorsellino = null;
            }

            if (btnChBoxDeact != null) {
                btnChBoxDeact.Dispose ();
                btnChBoxDeact = null;
            }

            if (btnChBoxDiOggi != null) {
                btnChBoxDiOggi.Dispose ();
                btnChBoxDiOggi = null;
            }

            if (btnChBoxNonConvergenti != null) {
                btnChBoxNonConvergenti.Dispose ();
                btnChBoxNonConvergenti = null;
            }

            if (btnChBoxNonEsitati != null) {
                btnChBoxNonEsitati.Dispose ();
                btnChBoxNonEsitati = null;
            }

            if (btnChBoxTrattativa != null) {
                btnChBoxTrattativa.Dispose ();
                btnChBoxTrattativa = null;
            }

            if (btnCheckBoxConvergenti != null) {
                btnCheckBoxConvergenti.Dispose ();
                btnCheckBoxConvergenti = null;
            }

            if (btnImpPersChooseDateTo != null) {
                btnImpPersChooseDateTo.Dispose ();
                btnImpPersChooseDateTo = null;
            }

            if (btnImpPersChooseHour != null) {
                btnImpPersChooseHour.Dispose ();
                btnImpPersChooseHour = null;
            }

            if (btnImpPersDataFrom != null) {
                btnImpPersDataFrom.Dispose ();
                btnImpPersDataFrom = null;
            }

            if (btnImpPersSalva != null) {
                btnImpPersSalva.Dispose ();
                btnImpPersSalva = null;
            }

            if (btnLocationAppuntamenti != null) {
                btnLocationAppuntamenti.Dispose ();
                btnLocationAppuntamenti = null;
            }

            if (btnLocationCerca != null) {
                btnLocationCerca.Dispose ();
                btnLocationCerca = null;
            }

            if (btnLocationClienti != null) {
                btnLocationClienti.Dispose ();
                btnLocationClienti = null;
            }

            if (btnLocationNavigator != null) {
                btnLocationNavigator.Dispose ();
                btnLocationNavigator = null;
            }

            if (btnLocationNavMinus != null) {
                btnLocationNavMinus.Dispose ();
                btnLocationNavMinus = null;
            }

            if (btnLocationNavPlus != null) {
                btnLocationNavPlus.Dispose ();
                btnLocationNavPlus = null;
            }

            if (btnMemoVaiAlCliente != null) {
                btnMemoVaiAlCliente.Dispose ();
                btnMemoVaiAlCliente = null;
            }

            if (btnNavigatordiOggi != null) {
                btnNavigatordiOggi.Dispose ();
                btnNavigatordiOggi = null;
            }

            if (btnSearchAppChooseStatus != null) {
                btnSearchAppChooseStatus.Dispose ();
                btnSearchAppChooseStatus = null;
            }

            if (btnSearchAppMoreDetails != null) {
                btnSearchAppMoreDetails.Dispose ();
                btnSearchAppMoreDetails = null;
            }

            if (btnSearchCall != null) {
                btnSearchCall.Dispose ();
                btnSearchCall = null;
            }

            if (btnSearchChangeAppHour != null) {
                btnSearchChangeAppHour.Dispose ();
                btnSearchChangeAppHour = null;
            }

            if (btnSearchEmail != null) {
                btnSearchEmail.Dispose ();
                btnSearchEmail = null;
            }

            if (btnSearchLocalita != null) {
                btnSearchLocalita.Dispose ();
                btnSearchLocalita = null;
            }

            if (btnSearchRegSociale != null) {
                btnSearchRegSociale.Dispose ();
                btnSearchRegSociale = null;
            }

            if (btnSearchSalva != null) {
                btnSearchSalva.Dispose ();
                btnSearchSalva = null;
            }

            if (btnSearchVaiAlClienteMemo != null) {
                btnSearchVaiAlClienteMemo.Dispose ();
                btnSearchVaiAlClienteMemo = null;
            }

            if (btnSearchWord != null) {
                btnSearchWord.Dispose ();
                btnSearchWord = null;
            }

            if (editTextAppOggiDescription != null) {
                editTextAppOggiDescription.Dispose ();
                editTextAppOggiDescription = null;
            }

            if (editTextAppOggiMotivoEsito != null) {
                editTextAppOggiMotivoEsito.Dispose ();
                editTextAppOggiMotivoEsito = null;
            }

            if (imgAppOggiStatus != null) {
                imgAppOggiStatus.Dispose ();
                imgAppOggiStatus = null;
            }

            if (imgAppTopMenuApp != null) {
                imgAppTopMenuApp.Dispose ();
                imgAppTopMenuApp = null;
            }

            if (imgAppTopMenuBack != null) {
                imgAppTopMenuBack.Dispose ();
                imgAppTopMenuBack = null;
            }

            if (imgAppTopMenuCalendar != null) {
                imgAppTopMenuCalendar.Dispose ();
                imgAppTopMenuCalendar = null;
            }

            if (imgAppTopMenuLocation != null) {
                imgAppTopMenuLocation.Dispose ();
                imgAppTopMenuLocation = null;
            }

            if (imgAppTopMenuSearch != null) {
                imgAppTopMenuSearch.Dispose ();
                imgAppTopMenuSearch = null;
            }

            if (imgArrowLocationAppuntamenti != null) {
                imgArrowLocationAppuntamenti.Dispose ();
                imgArrowLocationAppuntamenti = null;
            }

            if (imgArrowLocationClienti != null) {
                imgArrowLocationClienti.Dispose ();
                imgArrowLocationClienti = null;
            }

            if (imgChBoxBorsellino != null) {
                imgChBoxBorsellino.Dispose ();
                imgChBoxBorsellino = null;
            }

            if (imgChBoxConvergenti != null) {
                imgChBoxConvergenti.Dispose ();
                imgChBoxConvergenti = null;
            }

            if (imgChBoxDeact != null) {
                imgChBoxDeact.Dispose ();
                imgChBoxDeact = null;
            }

            if (imgChBoxDiOggi != null) {
                imgChBoxDiOggi.Dispose ();
                imgChBoxDiOggi = null;
            }

            if (imgChBoxNonConvergenti != null) {
                imgChBoxNonConvergenti.Dispose ();
                imgChBoxNonConvergenti = null;
            }

            if (imgChBoxNonEsitati != null) {
                imgChBoxNonEsitati.Dispose ();
                imgChBoxNonEsitati = null;
            }

            if (imgChBoxTrattativa != null) {
                imgChBoxTrattativa.Dispose ();
                imgChBoxTrattativa = null;
            }

            if (imgSearchApparrow != null) {
                imgSearchApparrow.Dispose ();
                imgSearchApparrow = null;
            }

            if (imgSearchIcon != null) {
                imgSearchIcon.Dispose ();
                imgSearchIcon = null;
            }

            if (lblAppOggiDetCategoria != null) {
                lblAppOggiDetCategoria.Dispose ();
                lblAppOggiDetCategoria = null;
            }

            if (lblAppOggiDetCategoriaValue != null) {
                lblAppOggiDetCategoriaValue.Dispose ();
                lblAppOggiDetCategoriaValue = null;
            }

            if (lblAppOggiDetCodChiamata != null) {
                lblAppOggiDetCodChiamata.Dispose ();
                lblAppOggiDetCodChiamata = null;
            }

            if (lblAppOggiDetCodChiamataValue != null) {
                lblAppOggiDetCodChiamataValue.Dispose ();
                lblAppOggiDetCodChiamataValue = null;
            }

            if (lblAppOggiDetContatti != null) {
                lblAppOggiDetContatti.Dispose ();
                lblAppOggiDetContatti = null;
            }

            if (lblAppOggiDetContattiValue1 != null) {
                lblAppOggiDetContattiValue1.Dispose ();
                lblAppOggiDetContattiValue1 = null;
            }

            if (lblAppOggiDetContattiValue2 != null) {
                lblAppOggiDetContattiValue2.Dispose ();
                lblAppOggiDetContattiValue2 = null;
            }

            if (lblAppOggiDetContattiValue3 != null) {
                lblAppOggiDetContattiValue3.Dispose ();
                lblAppOggiDetContattiValue3 = null;
            }

            if (lblAppOggiDetContattiValue4 != null) {
                lblAppOggiDetContattiValue4.Dispose ();
                lblAppOggiDetContattiValue4 = null;
            }

            if (lblAppOggiDetDataApp != null) {
                lblAppOggiDetDataApp.Dispose ();
                lblAppOggiDetDataApp = null;
            }

            if (lblAppOggiDetDataAppValue != null) {
                lblAppOggiDetDataAppValue.Dispose ();
                lblAppOggiDetDataAppValue = null;
            }

            if (lblAppOggiDetDataChiamata != null) {
                lblAppOggiDetDataChiamata.Dispose ();
                lblAppOggiDetDataChiamata = null;
            }

            if (lblAppOggiDetDataChiamataValue != null) {
                lblAppOggiDetDataChiamataValue.Dispose ();
                lblAppOggiDetDataChiamataValue = null;
            }

            if (lblAppOggiDetIndirizzo != null) {
                lblAppOggiDetIndirizzo.Dispose ();
                lblAppOggiDetIndirizzo = null;
            }

            if (lblAppOggiDetIndirizzoValue1 != null) {
                lblAppOggiDetIndirizzoValue1.Dispose ();
                lblAppOggiDetIndirizzoValue1 = null;
            }

            if (lblAppOggiDetIndirizzoValue2 != null) {
                lblAppOggiDetIndirizzoValue2.Dispose ();
                lblAppOggiDetIndirizzoValue2 = null;
            }

            if (lblAppOggiDetMotive != null) {
                lblAppOggiDetMotive.Dispose ();
                lblAppOggiDetMotive = null;
            }

            if (lblAppOggiDetMotiveValue != null) {
                lblAppOggiDetMotiveValue.Dispose ();
                lblAppOggiDetMotiveValue = null;
            }

            if (lblAppOggiDetNoteAg != null) {
                lblAppOggiDetNoteAg.Dispose ();
                lblAppOggiDetNoteAg = null;
            }

            if (lblAppOggiDetNoteAgValue != null) {
                lblAppOggiDetNoteAgValue.Dispose ();
                lblAppOggiDetNoteAgValue = null;
            }

            if (lblAppOggiDetNoteChiamata != null) {
                lblAppOggiDetNoteChiamata.Dispose ();
                lblAppOggiDetNoteChiamata = null;
            }

            if (lblAppOggiDetNoteChiamataValue != null) {
                lblAppOggiDetNoteChiamataValue.Dispose ();
                lblAppOggiDetNoteChiamataValue = null;
            }

            if (lblAppOggiDetNoteSecretaria != null) {
                lblAppOggiDetNoteSecretaria.Dispose ();
                lblAppOggiDetNoteSecretaria = null;
            }

            if (lblAppOggiDetNoteSecretariaValue != null) {
                lblAppOggiDetNoteSecretariaValue.Dispose ();
                lblAppOggiDetNoteSecretariaValue = null;
            }

            if (lblAppOggiDetOraApp != null) {
                lblAppOggiDetOraApp.Dispose ();
                lblAppOggiDetOraApp = null;
            }

            if (lblAppOggiDetOraAppValue != null) {
                lblAppOggiDetOraAppValue.Dispose ();
                lblAppOggiDetOraAppValue = null;
            }

            if (lblAppOggiDetRagSociale != null) {
                lblAppOggiDetRagSociale.Dispose ();
                lblAppOggiDetRagSociale = null;
            }

            if (lblAppOggiDetRagSocialeValue != null) {
                lblAppOggiDetRagSocialeValue.Dispose ();
                lblAppOggiDetRagSocialeValue = null;
            }

            if (lblAppOggiDetRiepilogDati != null) {
                lblAppOggiDetRiepilogDati.Dispose ();
                lblAppOggiDetRiepilogDati = null;
            }

            if (lblAppOggiEmail != null) {
                lblAppOggiEmail.Dispose ();
                lblAppOggiEmail = null;
            }

            if (lblAppOggiMemoLuogo != null) {
                lblAppOggiMemoLuogo.Dispose ();
                lblAppOggiMemoLuogo = null;
            }

            if (lblAppOggiMemoMuogoValue != null) {
                lblAppOggiMemoMuogoValue.Dispose ();
                lblAppOggiMemoMuogoValue = null;
            }

            if (lblAppOggiMemoTitle != null) {
                lblAppOggiMemoTitle.Dispose ();
                lblAppOggiMemoTitle = null;
            }

            if (lblAppOggiMemoTitolo != null) {
                lblAppOggiMemoTitolo.Dispose ();
                lblAppOggiMemoTitolo = null;
            }

            if (lblAppOggiMemoTitoloValue != null) {
                lblAppOggiMemoTitoloValue.Dispose ();
                lblAppOggiMemoTitoloValue = null;
            }

            if (lblAppOggiNameReg != null) {
                lblAppOggiNameReg.Dispose ();
                lblAppOggiNameReg = null;
            }

            if (lblAppOggiNotePlaceHolder != null) {
                lblAppOggiNotePlaceHolder.Dispose ();
                lblAppOggiNotePlaceHolder = null;
            }

            if (lblAppOggiPhone != null) {
                lblAppOggiPhone.Dispose ();
                lblAppOggiPhone = null;
            }

            if (lblAppOggiRegSociale != null) {
                lblAppOggiRegSociale.Dispose ();
                lblAppOggiRegSociale = null;
            }

            if (lblAppOggiStatus != null) {
                lblAppOggiStatus.Dispose ();
                lblAppOggiStatus = null;
            }

            if (lblAppOggiStatusLabel != null) {
                lblAppOggiStatusLabel.Dispose ();
                lblAppOggiStatusLabel = null;
            }

            if (lblCalendarioAggImpPersonale != null) {
                lblCalendarioAggImpPersonale.Dispose ();
                lblCalendarioAggImpPersonale = null;
            }

            if (lblCalendarioTitle != null) {
                lblCalendarioTitle.Dispose ();
                lblCalendarioTitle = null;
            }

            if (lblChBoxBorsellino != null) {
                lblChBoxBorsellino.Dispose ();
                lblChBoxBorsellino = null;
            }

            if (lblChBoxConvergenti != null) {
                lblChBoxConvergenti.Dispose ();
                lblChBoxConvergenti = null;
            }

            if (lblChBoxDeact != null) {
                lblChBoxDeact.Dispose ();
                lblChBoxDeact = null;
            }

            if (lblChBoxDiOggi != null) {
                lblChBoxDiOggi.Dispose ();
                lblChBoxDiOggi = null;
            }

            if (lblChBoxNonConvergenti != null) {
                lblChBoxNonConvergenti.Dispose ();
                lblChBoxNonConvergenti = null;
            }

            if (lblChBoxNonEsitati != null) {
                lblChBoxNonEsitati.Dispose ();
                lblChBoxNonEsitati = null;
            }

            if (lblChBoxTrattativa != null) {
                lblChBoxTrattativa.Dispose ();
                lblChBoxTrattativa = null;
            }

            if (lblImpPersDataAl != null) {
                lblImpPersDataAl.Dispose ();
                lblImpPersDataAl = null;
            }

            if (lblImpPersDataDal != null) {
                lblImpPersDataDal.Dispose ();
                lblImpPersDataDal = null;
            }

            if (lblImpPersDataToValue != null) {
                lblImpPersDataToValue.Dispose ();
                lblImpPersDataToValue = null;
            }

            if (lblImpPersDescPlaceHolder != null) {
                lblImpPersDescPlaceHolder.Dispose ();
                lblImpPersDescPlaceHolder = null;
            }

            if (lblImpPersDetailsDataAl != null) {
                lblImpPersDetailsDataAl.Dispose ();
                lblImpPersDetailsDataAl = null;
            }

            if (lblImpPersDetailsDataAlValue != null) {
                lblImpPersDetailsDataAlValue.Dispose ();
                lblImpPersDetailsDataAlValue = null;
            }

            if (lblImpPersDetailsDataDal != null) {
                lblImpPersDetailsDataDal.Dispose ();
                lblImpPersDetailsDataDal = null;
            }

            if (lblImpPersDetailsDataDalValue != null) {
                lblImpPersDetailsDataDalValue.Dispose ();
                lblImpPersDetailsDataDalValue = null;
            }

            if (lblImpPersDetailsDescription != null) {
                lblImpPersDetailsDescription.Dispose ();
                lblImpPersDetailsDescription = null;
            }

            if (lblImpPersDetailsLuogo != null) {
                lblImpPersDetailsLuogo.Dispose ();
                lblImpPersDetailsLuogo = null;
            }

            if (lblImpPersDetailsLuogoValue != null) {
                lblImpPersDetailsLuogoValue.Dispose ();
                lblImpPersDetailsLuogoValue = null;
            }

            if (lblImpPersDetailsOra != null) {
                lblImpPersDetailsOra.Dispose ();
                lblImpPersDetailsOra = null;
            }

            if (lblImpPersDetailsOraValue != null) {
                lblImpPersDetailsOraValue.Dispose ();
                lblImpPersDetailsOraValue = null;
            }

            if (lblImpPersDetailsTitle != null) {
                lblImpPersDetailsTitle.Dispose ();
                lblImpPersDetailsTitle = null;
            }

            if (lblImpPersHour != null) {
                lblImpPersHour.Dispose ();
                lblImpPersHour = null;
            }

            if (lblImpPersHourValue != null) {
                lblImpPersHourValue.Dispose ();
                lblImpPersHourValue = null;
            }

            if (lblImpPersonaleDataFromValue != null) {
                lblImpPersonaleDataFromValue.Dispose ();
                lblImpPersonaleDataFromValue = null;
            }

            if (lblImpPersPlace != null) {
                lblImpPersPlace.Dispose ();
                lblImpPersPlace = null;
            }

            if (lblImpPersSalva != null) {
                lblImpPersSalva.Dispose ();
                lblImpPersSalva = null;
            }

            if (lblImpPersTitle != null) {
                lblImpPersTitle.Dispose ();
                lblImpPersTitle = null;
            }

            if (lblLocationAppuntamenti != null) {
                lblLocationAppuntamenti.Dispose ();
                lblLocationAppuntamenti = null;
            }

            if (lblLocationCerca != null) {
                lblLocationCerca.Dispose ();
                lblLocationCerca = null;
            }

            if (lblLocationClienti != null) {
                lblLocationClienti.Dispose ();
                lblLocationClienti = null;
            }

            if (lblMotivoSearchPlaceHolder != null) {
                lblMotivoSearchPlaceHolder.Dispose ();
                lblMotivoSearchPlaceHolder = null;
            }

            if (lblSearchAppEmail != null) {
                lblSearchAppEmail.Dispose ();
                lblSearchAppEmail = null;
            }

            if (lblSearchAppPhone != null) {
                lblSearchAppPhone.Dispose ();
                lblSearchAppPhone = null;
            }

            if (lblSearchAppRegSociale != null) {
                lblSearchAppRegSociale.Dispose ();
                lblSearchAppRegSociale = null;
            }

            if (lblSearchAppRepLegale != null) {
                lblSearchAppRepLegale.Dispose ();
                lblSearchAppRepLegale = null;
            }

            if (lblSearchAppStatus != null) {
                lblSearchAppStatus.Dispose ();
                lblSearchAppStatus = null;
            }

            if (lblSearchAppStatusLabel != null) {
                lblSearchAppStatusLabel.Dispose ();
                lblSearchAppStatusLabel = null;
            }

            if (lblSearchLocalita != null) {
                lblSearchLocalita.Dispose ();
                lblSearchLocalita = null;
            }

            if (lblSearchMemoLuogo != null) {
                lblSearchMemoLuogo.Dispose ();
                lblSearchMemoLuogo = null;
            }

            if (lblSearchMemoLuogoValue != null) {
                lblSearchMemoLuogoValue.Dispose ();
                lblSearchMemoLuogoValue = null;
            }

            if (lblSearchMemoTitle != null) {
                lblSearchMemoTitle.Dispose ();
                lblSearchMemoTitle = null;
            }

            if (lblSearchMemoTitleDescription != null) {
                lblSearchMemoTitleDescription.Dispose ();
                lblSearchMemoTitleDescription = null;
            }

            if (lblSearchMemoTitleDescriptionValue != null) {
                lblSearchMemoTitleDescriptionValue.Dispose ();
                lblSearchMemoTitleDescriptionValue = null;
            }

            if (lblSearchRagSociale != null) {
                lblSearchRagSociale.Dispose ();
                lblSearchRagSociale = null;
            }

            if (mapEvents != null) {
                mapEvents.Dispose ();
                mapEvents = null;
            }

            if (mapViewLocation != null) {
                mapViewLocation.Dispose ();
                mapViewLocation = null;
            }

            if (scrlSearchMain != null) {
                scrlSearchMain.Dispose ();
                scrlSearchMain = null;
            }

            if (scrlViewAppOggi != null) {
                scrlViewAppOggi.Dispose ();
                scrlViewAppOggi = null;
            }

            if (tblSearchList != null) {
                tblSearchList.Dispose ();
                tblSearchList = null;
            }

            if (tblViewAppdiOgi != null) {
                tblViewAppdiOgi.Dispose ();
                tblViewAppdiOgi = null;
            }

            if (textViewSearchMemoDescription != null) {
                textViewSearchMemoDescription.Dispose ();
                textViewSearchMemoDescription = null;
            }

            if (tfeditTextAppOggiNote != null) {
                tfeditTextAppOggiNote.Dispose ();
                tfeditTextAppOggiNote = null;
            }

            if (tfImpPersDescription != null) {
                tfImpPersDescription.Dispose ();
                tfImpPersDescription = null;
            }

            if (tfImpPersPlace != null) {
                tfImpPersPlace.Dispose ();
                tfImpPersPlace = null;
            }

            if (tfSearchDescriptionAppNote != null) {
                tfSearchDescriptionAppNote.Dispose ();
                tfSearchDescriptionAppNote = null;
            }

            if (tfSearchWord != null) {
                tfSearchWord.Dispose ();
                tfSearchWord = null;
            }

            if (txtViewAppOggiMemoDescription != null) {
                txtViewAppOggiMemoDescription.Dispose ();
                txtViewAppOggiMemoDescription = null;
            }

            if (txtViewImpPersDescription != null) {
                txtViewImpPersDescription.Dispose ();
                txtViewImpPersDescription = null;
            }

            if (txtViewImpPersDetailsDescription != null) {
                txtViewImpPersDetailsDescription.Dispose ();
                txtViewImpPersDetailsDescription = null;
            }

            if (txtViewImpPersDetailsEventPers != null) {
                txtViewImpPersDetailsEventPers.Dispose ();
                txtViewImpPersDetailsEventPers = null;
            }

            if (txtViewSearchAppDescr != null) {
                txtViewSearchAppDescr.Dispose ();
                txtViewSearchAppDescr = null;
            }

            if (txtViewSearchAPpNote != null) {
                txtViewSearchAPpNote.Dispose ();
                txtViewSearchAPpNote = null;
            }

            if (txtViewSearchMemoDescription != null) {
                txtViewSearchMemoDescription.Dispose ();
                txtViewSearchMemoDescription = null;
            }

            if (viewAppCalendar != null) {
                viewAppCalendar.Dispose ();
                viewAppCalendar = null;
            }

            if (viewAppdiOggiEvents != null) {
                viewAppdiOggiEvents.Dispose ();
                viewAppdiOggiEvents = null;
            }

            if (viewAppOggiBottomBar != null) {
                viewAppOggiBottomBar.Dispose ();
                viewAppOggiBottomBar = null;
            }

            if (viewAppOggiCall != null) {
                viewAppOggiCall.Dispose ();
                viewAppOggiCall = null;
            }

            if (viewAppOggiMain != null) {
                viewAppOggiMain.Dispose ();
                viewAppOggiMain = null;
            }

            if (viewAppOggiMemoDetails != null) {
                viewAppOggiMemoDetails.Dispose ();
                viewAppOggiMemoDetails = null;
            }

            if (viewAppOggiMemoLuogo != null) {
                viewAppOggiMemoLuogo.Dispose ();
                viewAppOggiMemoLuogo = null;
            }

            if (viewAppOggiMemoTitle != null) {
                viewAppOggiMemoTitle.Dispose ();
                viewAppOggiMemoTitle = null;
            }

            if (viewAppOggiOver != null) {
                viewAppOggiOver.Dispose ();
                viewAppOggiOver = null;
            }

            if (viewAppOggiSalva != null) {
                viewAppOggiSalva.Dispose ();
                viewAppOggiSalva = null;
            }

            if (viewAppOggiSCrollChild != null) {
                viewAppOggiSCrollChild.Dispose ();
                viewAppOggiSCrollChild = null;
            }

            if (viewAppOggiSendEmail != null) {
                viewAppOggiSendEmail.Dispose ();
                viewAppOggiSendEmail = null;
            }

            if (viewAppOggiSmallInfoDetail != null) {
                viewAppOggiSmallInfoDetail.Dispose ();
                viewAppOggiSmallInfoDetail = null;
            }

            if (viewAppOggiStatus != null) {
                viewAppOggiStatus.Dispose ();
                viewAppOggiStatus = null;
            }

            if (viewAppTopMenu != null) {
                viewAppTopMenu.Dispose ();
                viewAppTopMenu = null;
            }

            if (viewAppTopMenuApp != null) {
                viewAppTopMenuApp.Dispose ();
                viewAppTopMenuApp = null;
            }

            if (viewAppTopMenuBack != null) {
                viewAppTopMenuBack.Dispose ();
                viewAppTopMenuBack = null;
            }

            if (viewAppTopMenuCalendar != null) {
                viewAppTopMenuCalendar.Dispose ();
                viewAppTopMenuCalendar = null;
            }

            if (viewAppTopMenuLocation != null) {
                viewAppTopMenuLocation.Dispose ();
                viewAppTopMenuLocation = null;
            }

            if (viewAppTopMenuSearch != null) {
                viewAppTopMenuSearch.Dispose ();
                viewAppTopMenuSearch = null;
            }

            if (viewAppuntamentiMoreDetails != null) {
                viewAppuntamentiMoreDetails.Dispose ();
                viewAppuntamentiMoreDetails = null;
            }

            if (viewCalendarioAggImpPersonale != null) {
                viewCalendarioAggImpPersonale.Dispose ();
                viewCalendarioAggImpPersonale = null;
            }

            if (viewCalendarioContent != null) {
                viewCalendarioContent.Dispose ();
                viewCalendarioContent = null;
            }

            if (viewContentAggImpPersonale != null) {
                viewContentAggImpPersonale.Dispose ();
                viewContentAggImpPersonale = null;
            }

            if (viewContentLocationMap != null) {
                viewContentLocationMap.Dispose ();
                viewContentLocationMap = null;
            }

            if (viewContentSearch != null) {
                viewContentSearch.Dispose ();
                viewContentSearch = null;
            }

            if (viewImpPersDetailsLuogo != null) {
                viewImpPersDetailsLuogo.Dispose ();
                viewImpPersDetailsLuogo = null;
            }

            if (viewImpPersonaleDetails != null) {
                viewImpPersonaleDetails.Dispose ();
                viewImpPersonaleDetails = null;
            }

            if (viewImpPersSalva != null) {
                viewImpPersSalva.Dispose ();
                viewImpPersSalva = null;
            }

            if (viewLocationAppCheckboxes != null) {
                viewLocationAppCheckboxes.Dispose ();
                viewLocationAppCheckboxes = null;
            }

            if (viewLocationAppuntamenti != null) {
                viewLocationAppuntamenti.Dispose ();
                viewLocationAppuntamenti = null;
            }

            if (viewLocationCerca != null) {
                viewLocationCerca.Dispose ();
                viewLocationCerca = null;
            }

            if (viewLocationClienti != null) {
                viewLocationClienti.Dispose ();
                viewLocationClienti = null;
            }

            if (viewLocationClientiCheckBoxes != null) {
                viewLocationClientiCheckBoxes.Dispose ();
                viewLocationClientiCheckBoxes = null;
            }

            if (viewMemoVaiAlCliente != null) {
                viewMemoVaiAlCliente.Dispose ();
                viewMemoVaiAlCliente = null;
            }

            if (viewSearchAppMoreDetails != null) {
                viewSearchAppMoreDetails.Dispose ();
                viewSearchAppMoreDetails = null;
            }

            if (viewSearchBottomBar != null) {
                viewSearchBottomBar.Dispose ();
                viewSearchBottomBar = null;
            }

            if (viewSearchCall != null) {
                viewSearchCall.Dispose ();
                viewSearchCall = null;
            }

            if (viewSearchChooseStatus != null) {
                viewSearchChooseStatus.Dispose ();
                viewSearchChooseStatus = null;
            }

            if (viewSearchEmail != null) {
                viewSearchEmail.Dispose ();
                viewSearchEmail = null;
            }

            if (viewSearchMainDetails != null) {
                viewSearchMainDetails.Dispose ();
                viewSearchMainDetails = null;
            }

            if (viewSearchMemoDetails != null) {
                viewSearchMemoDetails.Dispose ();
                viewSearchMemoDetails = null;
            }

            if (viewSearchMemoLuogo != null) {
                viewSearchMemoLuogo.Dispose ();
                viewSearchMemoLuogo = null;
            }

            if (viewSearchMemoTitleDescription != null) {
                viewSearchMemoTitleDescription.Dispose ();
                viewSearchMemoTitleDescription = null;
            }

            if (viewSearchSalva != null) {
                viewSearchSalva.Dispose ();
                viewSearchSalva = null;
            }

            if (viewSearchSmallInfoDetails != null) {
                viewSearchSmallInfoDetails.Dispose ();
                viewSearchSmallInfoDetails = null;
            }

            if (viewSearchVaiAlClienteMemo != null) {
                viewSearchVaiAlClienteMemo.Dispose ();
                viewSearchVaiAlClienteMemo = null;
            }

            if (viewTblOverSearchApp != null) {
                viewTblOverSearchApp.Dispose ();
                viewTblOverSearchApp = null;
            }
        }
    }
}