// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace RewindiOS
{
	[Register ("ProfiloScreen")]
	partial class ProfiloScreen
	{
		[Outlet]
		UIKit.UIButton btnBonus { get; set; }

		[Outlet]
		UIKit.UIButton btnGare { get; set; }

		[Outlet]
		UIKit.UIButton btnInvito { get; set; }

		[Outlet]
		UIKit.UIButton btnTopMenuBack { get; set; }

		[Outlet]
		UIKit.UIButton btnTopMenuLogout { get; set; }

		[Outlet]
		UIKit.UIImageView imgProfilImage { get; set; }

		[Outlet]
		UIKit.UIImageView imgTopMenuBack { get; set; }

		[Outlet]
		UIKit.UIImageView imgTopMenuLogout { get; set; }

		[Outlet]
		UIKit.UIImageView imgTopStar { get; set; }

		[Outlet]
		UIKit.UILabel lblBonus { get; set; }

		[Outlet]
		UIKit.UILabel lblCellulare { get; set; }

		[Outlet]
		UIKit.UILabel lblGare { get; set; }

		[Outlet]
		UIKit.UILabel lblIndirizzo { get; set; }

		[Outlet]
		UIKit.UILabel lblInvito { get; set; }

		[Outlet]
		UIKit.UILabel lblMail { get; set; }

		[Outlet]
		UIKit.UILabel lblPartitaIva { get; set; }

		[Outlet]
		UIKit.UILabel lblTopMenuTitle { get; set; }

		[Outlet]
		UIKit.UILabel lblUserName { get; set; }

		[Outlet]
		UIKit.UILabel txtCellulare { get; set; }

		[Outlet]
		UIKit.UILabel txtIndirizzo { get; set; }

		[Outlet]
		UIKit.UILabel txtMail { get; set; }

		[Outlet]
		UIKit.UILabel txtPartitaIva { get; set; }

		[Outlet]
		UIKit.UIView viewBonus { get; set; }

		[Outlet]
		UIKit.UIView viewGare { get; set; }

		[Outlet]
		UIKit.UIView viewInvito { get; set; }

		[Outlet]
		UIKit.UIView viewTopMenuBack { get; set; }

		[Outlet]
		UIKit.UIView viewTopMenuLogout { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (btnBonus != null) {
				btnBonus.Dispose ();
				btnBonus = null;
			}

			if (imgProfilImage != null) {
				imgProfilImage.Dispose ();
				imgProfilImage = null;
			}

			if (btnGare != null) {
				btnGare.Dispose ();
				btnGare = null;
			}

			if (btnInvito != null) {
				btnInvito.Dispose ();
				btnInvito = null;
			}

			if (btnTopMenuBack != null) {
				btnTopMenuBack.Dispose ();
				btnTopMenuBack = null;
			}

			if (btnTopMenuLogout != null) {
				btnTopMenuLogout.Dispose ();
				btnTopMenuLogout = null;
			}

			if (imgTopMenuBack != null) {
				imgTopMenuBack.Dispose ();
				imgTopMenuBack = null;
			}

			if (imgTopMenuLogout != null) {
				imgTopMenuLogout.Dispose ();
				imgTopMenuLogout = null;
			}

			if (imgTopStar != null) {
				imgTopStar.Dispose ();
				imgTopStar = null;
			}

			if (lblBonus != null) {
				lblBonus.Dispose ();
				lblBonus = null;
			}

			if (lblCellulare != null) {
				lblCellulare.Dispose ();
				lblCellulare = null;
			}

			if (lblGare != null) {
				lblGare.Dispose ();
				lblGare = null;
			}

			if (lblIndirizzo != null) {
				lblIndirizzo.Dispose ();
				lblIndirizzo = null;
			}

			if (lblInvito != null) {
				lblInvito.Dispose ();
				lblInvito = null;
			}

			if (lblMail != null) {
				lblMail.Dispose ();
				lblMail = null;
			}

			if (lblPartitaIva != null) {
				lblPartitaIva.Dispose ();
				lblPartitaIva = null;
			}

			if (lblTopMenuTitle != null) {
				lblTopMenuTitle.Dispose ();
				lblTopMenuTitle = null;
			}

			if (lblUserName != null) {
				lblUserName.Dispose ();
				lblUserName = null;
			}

			if (txtCellulare != null) {
				txtCellulare.Dispose ();
				txtCellulare = null;
			}

			if (txtIndirizzo != null) {
				txtIndirizzo.Dispose ();
				txtIndirizzo = null;
			}

			if (txtMail != null) {
				txtMail.Dispose ();
				txtMail = null;
			}

			if (txtPartitaIva != null) {
				txtPartitaIva.Dispose ();
				txtPartitaIva = null;
			}

			if (viewBonus != null) {
				viewBonus.Dispose ();
				viewBonus = null;
			}

			if (viewGare != null) {
				viewGare.Dispose ();
				viewGare = null;
			}

			if (viewInvito != null) {
				viewInvito.Dispose ();
				viewInvito = null;
			}

			if (viewTopMenuBack != null) {
				viewTopMenuBack.Dispose ();
				viewTopMenuBack = null;
			}

			if (viewTopMenuLogout != null) {
				viewTopMenuLogout.Dispose ();
				viewTopMenuLogout = null;
			}
		}
	}
}
