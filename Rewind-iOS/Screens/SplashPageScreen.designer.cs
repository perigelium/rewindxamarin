// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace RewindiOS
{
	[Register ("SplashPageScreen")]
	partial class SplashPageScreen
	{
		[Outlet]
		UIKit.UIImageView imgBigWheel { get; set; }

		[Outlet]
		UIKit.UIImageView imgSmallWheel { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (imgBigWheel != null) {
				imgBigWheel.Dispose ();
				imgBigWheel = null;
			}

			if (imgSmallWheel != null) {
				imgSmallWheel.Dispose ();
				imgSmallWheel = null;
			}
		}
	}
}
