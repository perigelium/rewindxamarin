﻿using System;
using UIKit;

namespace Factorymind.ComponentsUnified
{
    public class GestureDelegate:UISwipeGestureRecognizer
    {
        public override bool ShouldBeRequiredToFailByGestureRecognizer(UIGestureRecognizer otherGestureRecognizer)
        {
            return true;
        }
    }
}