﻿#if __IOS__

using System;
using System.Runtime.InteropServices;
using System.IO;
using System.Threading;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;
using SystemConfiguration;
using UIKit;
using CoreGraphics;
using Foundation;
using MapKit;
using System.Xml;
using CoreLocation;
using System.Globalization;
using System.Drawing;
using Xamarin;

namespace RewindShared
{
    public partial class Utils
    {
        public static void writeToIOSDeviceLog(Exception exception)
        {
            try
            {
                using (var nss = new NSString(string.Format(exception.Message + " - " + exception.StackTrace)))
                {
                    // NSLog(nss.Handle);
                    Console.WriteLine(exception.Message + "  " + exception.StackTrace);
                    Insights.Report(exception, Insights.Severity.Error);
                }
                // AppDelegate.BugsnagClient.Notify(exception);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + "  " + ex.StackTrace);
            }
        }

        public static bool hasInternetConnectionIOS()
        {
            try
            {
                if (IsHostReachable("google.com"))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                writeToIOSDeviceLog(ex);
            }

            return false;
        }

        private static bool IsHostReachable(string host)
        {
            try
            {
                if (host == null || host.Length == 0)
                    return false;

                using (var r = new NetworkReachability(host))
                {
                    NetworkReachabilityFlags flags;

                    if (r.TryGetFlags(out flags))
                    {
                        return IsReachableWithoutRequiringConnection(flags);
                    }
                }

                return false;
            }
            catch (Exception ex)
            {
                writeToIOSDeviceLog(ex);
            }         

            return false;
        }

        private static bool IsReachableWithoutRequiringConnection(NetworkReachabilityFlags flags)
        {
            try
            {
// Is it reachable with the current network configuration?
                bool isReachable = (flags & NetworkReachabilityFlags.Reachable) != 0;

// Do we need a connection to reach it?
                bool noConnectionRequired = (flags & NetworkReachabilityFlags.ConnectionRequired) == 0;

// Since the network stack will automatically try to get the WAN up,
// probe that
                if ((flags & NetworkReachabilityFlags.IsWWAN) != 0)
                    noConnectionRequired = true;

                return isReachable && noConnectionRequired;
            }
            catch (Exception ex)
            {
                writeToIOSDeviceLog(ex);
            }

            return false;
        }

        public static void ShowRoute(MKMapView mapLocation)
        {
            try
            {
                MKPolyline polyLine; // route 

                // load the xml file created earlier
                XmlDocument doc = new XmlDocument();

                doc.Load(Utils.PersonalFolder + "/routeContent.xml");

                XmlNode statusCode = doc.GetElementsByTagName("status")[0];

                if (!statusCode.InnerText.ToLower().Equals("ok"))
                {
                    using (var alert = new UIAlertView("Error", "Route could not be found", null, "Ok", null))
                    {
                        alert.Show();
                    }

                    return;
                }

                XmlNode routeNode = doc.GetElementsByTagName("route")[0];

                //the node wich contains the entire polyline
                XmlNode overViewPolyline = routeNode.SelectSingleNode("./overview_polyline");

                //the node wich contains the entire route encoded points
                XmlNode points = overViewPolyline.FirstChild;

                // create coordinate array
                CLLocationCoordinate2D[] coordArray = Utils.DecodePolylinePoints(points.InnerText).ToArray();

                //create the route and display it
                polyLine = MKPolyline.FromCoordinates(coordArray);

                mapLocation.AddOverlay(polyLine);

                // get the bounds from google api
                XmlNode boundsNode = doc.GetElementsByTagName("bounds")[0];

                UpdateMapRegion(boundsNode, mapLocation);

                if (File.Exists(Utils.PersonalFolder + "/routeContent.xml"))
                {
                    File.Delete(Utils.PersonalFolder + "/routeContent.xml");
                }

            }
            catch (Exception ex)
            {
                using (var alert = new UIAlertView("Error", ex.Message, null, "Ok", null))
                {
                    alert.Show();
                }
            }
        }

        // compute the region to show the entire route
        private static void UpdateMapRegion(XmlNode boundsNode, MKMapView mapLocation)
        {
            try
            {
                // south west
                XmlNode southWestNode = boundsNode.FirstChild;
                XmlNode southWestLat = southWestNode.FirstChild; // south west -> latitude
                XmlNode southWestLon = southWestNode.LastChild;  // south west -> longitude

                // north east
                XmlNode northEastNode = boundsNode.LastChild;
                XmlNode northEastLat = northEastNode.FirstChild; // north east -> latitude
                XmlNode northEastLon = northEastNode.LastChild;  // north east -> longitude


                double swLat = 0;
                double swLon = 0;

                double neLat = 0;
                double neLon = 0;


                double.TryParse(southWestLat.InnerText, NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingSign, CultureInfo.InvariantCulture, out swLat);
                double.TryParse(southWestLon.InnerText, NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingSign, CultureInfo.InvariantCulture, out swLon);

                double.TryParse(northEastLat.InnerText, NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingSign, CultureInfo.InvariantCulture, out neLat);
                double.TryParse(northEastLon.InnerText, NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingSign, CultureInfo.InvariantCulture, out neLon);


                // create coordinates based on xml nodes values
                CLLocationCoordinate2D southWestCoord = new CLLocationCoordinate2D(swLat, swLon);
                CLLocationCoordinate2D northEastCoors = new CLLocationCoordinate2D(neLat, neLon);

                // Create the center coordinate for the new region
                CLLocationCoordinate2D centerCoord = new CLLocationCoordinate2D();
                centerCoord.Latitude = (southWestCoord.Latitude + northEastCoors.Latitude) / 2;
                centerCoord.Longitude = (southWestCoord.Longitude + northEastCoors.Longitude) / 2;

                //double boundsPadding = 0.05;

                double latDifference = Math.Abs(southWestCoord.Latitude - northEastCoors.Latitude);
                double lonDifference = Math.Abs(southWestCoord.Longitude - northEastCoors.Longitude);


                // create the new span
                MKCoordinateSpan newSpan = new MKCoordinateSpan(latDifference, lonDifference);
				MKCoordinateRegion newRegion;
				//my position
				if (mapLocation.UserLocation.Location != null)
				{
					CLLocationCoordinate2D myPos = new CLLocationCoordinate2D(mapLocation.UserLocation.Location.Coordinate.Latitude, mapLocation.UserLocation.Location.Coordinate.Longitude);
					newRegion = new MKCoordinateRegion(myPos, newSpan);
				}
				else
				{
					newRegion = new MKCoordinateRegion(centerCoord, newSpan);
				}
                // create the new region

                // set region to mapview
                mapLocation.SetRegion(newRegion, true);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public static List<CLLocationCoordinate2D> DecodePolylinePoints(string encodedPoints)
        {
            if (encodedPoints == "" || encodedPoints == null)
            {
                return null;
            }

            List<CLLocationCoordinate2D> coordList = new List<CLLocationCoordinate2D>();

            char[] polylineChars = encodedPoints.ToCharArray();
            int index = 0;

            int currentLat = 0;
            int currentLon = 0;
            int next5bits;
            int sum;
            int shifter;

            try
            {
                while (index < polylineChars.Length)
                {
                    // calculate next latitude
                    sum = 0;
                    shifter = 0;
                    do
                    {
                        next5bits = (int)polylineChars[index++] - 63;
                        sum |= (next5bits & 31) << shifter;
                        shifter += 5;
                    } while (next5bits >= 32 && index < polylineChars.Length);

                    if (index >= polylineChars.Length)
                        break;

                    currentLat += (sum & 1) == 1 ? ~(sum >> 1) : (sum >> 1);

                    //calculate next longitude
                    sum = 0;
                    shifter = 0;
                    do
                    {
                        next5bits = (int)polylineChars[index++] - 63;
                        sum |= (next5bits & 31) << shifter;
                        shifter += 5;
                    } while (next5bits >= 32 && index < polylineChars.Length);

                    if (index >= polylineChars.Length && next5bits >= 32)
                        break;

                    currentLon += (sum & 1) == 1 ? ~(sum >> 1) : (sum >> 1);

                    double dLat = Convert.ToDouble(currentLat) / 100000.0;
                    double dLon = Convert.ToDouble(currentLon) / 100000.0;

                    // create new coordinate and add it to the list
                    CLLocationCoordinate2D coord = new CLLocationCoordinate2D(dLat, dLon);
                    coordList.Add(coord);
                } 
            }
            catch
            {
            }           

            return coordList;
        }

        public static UIImage GetImageFromColor(UIColor color)
        {
            try
            {
                var imageSize = new SizeF(30, 30);
                var imageSizeRectF = new RectangleF(0, 0, 30, 30);

                UIGraphics.BeginImageContextWithOptions(imageSize, false, 0);
                var context = UIGraphics.GetCurrentContext();
               
                context.SetFillColor(color.CGColor);
                context.FillRect(imageSizeRectF);

                var image = UIGraphics.GetImageFromCurrentImageContext();
                UIGraphics.EndImageContext();

                return image;
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }

            return null;
        }

        public static DateTime NSDateToDateTime(NSDate date)
        {
            DateTime reference = TimeZone.CurrentTimeZone.ToLocalTime(
                                     new DateTime(2001, 1, 1, 0, 0, 0));
            return reference.AddSeconds(date.SecondsSinceReferenceDate);
        }

        public static NSDate DateTimeToNSDate(DateTime date)
        {
//            DateTime reference = TimeZone.CurrentTimeZone.ToLocalTime(
//                                     new DateTime(2001, 1, 1, 0, 0, 0));
//            return NSDate.FromTimeIntervalSinceReferenceDate(
//                (date - reference).TotalSeconds);
            if (date.Kind == DateTimeKind.Unspecified)
                date = DateTime.SpecifyKind (date, DateTimeKind.Local);
            return (NSDate) date;
        }

        private static void saveUserAvatariOS(string avatarUrl)
        {
            try
            {
                NSUrl nsUrl = new NSUrl(avatarUrl);
                NSData data = NSData.FromUrl(nsUrl);

                NSError er;
                if (data != null)
                {
                    data.Save(Consts.USER_AVATAR_IMAGE_PATH, false, out er);   
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public static CGSize getSizeToFitText(string text, UIFont textFont)
        {
            return Utils.getSizeToFitText(new List<string>{ text }, textFont);
        }

        public static CGSize getSizeToFitText(List<string> textsList, UIFont textFont)
        {
            CGSize stringSize = new CGSize(0, 0);
            try
            {
                int max = textsList[0].Length;
                int position = 0;

                for (int i = 1; i < textsList.Count; i++)
                {
                    if (max < textsList[i].Length)
                    {
                        max = textsList[i].Length;
                        position = i;
                    }
                }

                NSString myString = new NSString(textsList[position]);
                stringSize = myString.StringSize(textFont);
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
            return stringSize;
        }

        public static int nrOfLinesToFitText(CGSize textSize, float controlWidth)
        {
            if (textSize.Width < controlWidth)
            {
                return 1;
            }
            else
            {
                return (int)Math.Ceiling(textSize.Width / controlWidth) + 1;
            }
        }

        public static CLLocation GotoAddressLocation(string address)
        {
            CLLocation location = new CLLocation(0, 0);
            try
            {
                var addressGeocode = System.Net.WebUtility.HtmlEncode(address);
                string url = "https://maps.googleapis.com/maps/api/geocode/json?address=" + addressGeocode;
                    var json = ApiCalls.GetJsonFromGeocode(url);
                    JObject jsonObject = JObject.Parse(json);
                    JToken jgeometry = jsonObject["results"].First;
                    string lat = jgeometry["geometry"]["location"]["lat"].ToString();
                    string lng = jgeometry["geometry"]["location"]["lng"].ToString();

                    double latDouble;
                    double.TryParse(lat, NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingSign, CultureInfo.CurrentCulture, out latDouble);

                    double longDouble;
                    double.TryParse(lng, NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingSign, CultureInfo.CurrentCulture, out longDouble);

                    location = new CLLocation(latDouble, longDouble);
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
            return location;
        }

    }
}

#endif
