﻿#if __ANDROID__

using System;
using Android.Telephony;
using RewindAndroid;
using Android.Content;
using Android.Util;
using System.Globalization;
using System.Net;
using Android.Views;
using Java.Interop;
using Android.App;
using System.Collections.Generic;
using Android.Gms.Maps.Model;
using System.IO;
using Xamarin;
using Android.Locations;
using System.Linq;

namespace RewindShared
{
    public partial class Utils
    {
        public static string getDeviceIMEIAndroid()
        {
            try
            {
                TelephonyManager telephonyManager = (TelephonyManager)ApplicationContextProvider.getContext().GetSystemService(Context.TelephonyService);

                if (telephonyManager != null && !string.IsNullOrEmpty(telephonyManager.DeviceId))
                {
                    return telephonyManager.DeviceId;
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }

            return "";
        }

        private static void writeToDeviceLogAndroid(Exception exception)
        {
            try
            {
                //              Java.Lang.Exception bugsNagException = new Java.Lang.Exception(exception.ToString());
//                ApplicationContextProvider.BugsnagClient.Notify(exception);

                string appPackageName = ApplicationContextProvider.getContext().PackageName;
                string timeString = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);

                Insights.Report(exception,Insights.Severity.Error);
                Log.Error(appPackageName, string.Format("Time:  --  {0} Message: {1}  --  StackTrace: {2}", timeString, exception.Message, exception.StackTrace));
            }
            catch (Exception ex)
            {
                //              Utils.writeToDeviceLog(ex);
                Console.WriteLine(ex.Message + "  " + ex.StackTrace);
            }
        }

        private static bool CheckForInternetConnection()
        {
            try
            {
                int count = 0;
                int currentServer = 0;
                string[] servers = { "http://google.com", "http://www.facebook.com", "http://twitter.com" };
                while (true)
                {
                    try
                    {
                        var req = WebRequest.Create(servers[currentServer]);
                        req.Timeout = 2000;
                        req.Method = "HEAD";
                        using (var resp = req.GetResponse())
                        {
                            return true;
                        }
                    }
                    catch
                    {
                        Console.WriteLine("Ping Failed:{0}", count);
                        count++;
                        System.Threading.Thread.Sleep(100);
                        if (count == 9)
                            return false;
                        if (count % 3 == 0)
                            currentServer++;
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
                return false;
            }
        }

//        private static bool CheckForInternetConnection()
//        {
//            int count = 0;
//            int currentServer = 0;
//            string[] servers = { "http://google.com", "http://www.facebook.com", "http://twitter.com" };
//            while (true)
//            {
//                try
//                {
//                    var req = WebRequest.Create(servers[currentServer]);
//                    req.Timeout = 2000;
//                    req.Method = "HEAD";
//                    using (var resp = req.GetResponse())
//                    {
//                        return true;
//                    }
//                }
//                catch
//                {
//                    Console.WriteLine("Ping Failed:{0}", count);
//                    count++;
//                    System.Threading.Thread.Sleep(100);
//                    if (count == 9)
//                        return false;
//                    if (count % 3 == 0)
//                        currentServer++;
//                }
//            }
//        }

        public static float getScreenWidthAndroid()
        {
            try
            { 
                IWindowManager windowManager = ApplicationContextProvider.getContext().GetSystemService(Context.WindowService).JavaCast<IWindowManager>();
                Display display = windowManager.DefaultDisplay;
                return display.Width;
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }

            return 0;
        }

		public static int dpToPx(int dp)
		{
			DisplayMetrics displayMetrics = ApplicationContextProvider.getContext ().Resources.DisplayMetrics;
			int px = (int)Math.Round(dp * (displayMetrics.Xdpi / 160));       
			return px;
		}

        public static void displayErrorAndroid(string errMsg, Context context)
        {
            try
            {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);

                builder.SetTitle("Errore");

                builder.SetMessage(errMsg);

                builder.SetPositiveButton("OK", (sender, e) =>
                    {
                    });

                builder.Show();
            }
            catch (Exception ex)
            {

            }
        }


        public static void displayMessageAndroid(string title, string message, Context context)
        {
            try
            {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);

                builder.SetTitle(title);

                builder.SetMessage(message);

                builder.SetPositiveButton("OK", (sender, e) =>
                    {
                    });

                builder.Show();
            }
            catch (Exception ex)
            {
            }
        }

        public static List<LatLng> DecodePolylinePointsAndroid(string encodedPoints)
        {
            if (encodedPoints == "" || encodedPoints == null)
            {
                return null;
            }

            List<LatLng> coordList = new List<LatLng>();

            char[] polylineChars = encodedPoints.ToCharArray();
            int index = 0;

            int currentLat = 0;
            int currentLon = 0;
            int next5bits;
            int sum;
            int shifter;

            try
            {
                while (index < polylineChars.Length)
                {
                    // calculate next latitude
                    sum = 0;
                    shifter = 0;
                    do
                    {
                        next5bits = (int)polylineChars[index++] - 63;
                        sum |= (next5bits & 31) << shifter;
                        shifter += 5;
                    } while (next5bits >= 32 && index < polylineChars.Length);

                    if (index >= polylineChars.Length)
                        break;

                    currentLat += (sum & 1) == 1 ? ~(sum >> 1) : (sum >> 1);

                    //calculate next longitude
                    sum = 0;
                    shifter = 0;
                    do
                    {
                        next5bits = (int)polylineChars[index++] - 63;
                        sum |= (next5bits & 31) << shifter;
                        shifter += 5;
                    } while (next5bits >= 32 && index < polylineChars.Length);

                    if (index >= polylineChars.Length && next5bits >= 32)
                        break;

                    currentLon += (sum & 1) == 1 ? ~(sum >> 1) : (sum >> 1);

                    double dLat = Convert.ToDouble(currentLat) / 100000.0;
                    double dLon = Convert.ToDouble(currentLon) / 100000.0;

                    // create new coordinate and add it to the list
                    LatLng coord = new LatLng(dLat, dLon);
                    coordList.Add(coord);
                } 
            }
            catch
            {
            }

            return coordList;
        }

        public static void displayError (string errMsg, Android.Content.Context context)
        {
            try {
                displayErrorAndroid(errMsg,context);
            } catch (Exception ex) {
                Console.WriteLine (ex.Message + "  " + ex.StackTrace);
            }
        }


        public static void displayMessage (string title, string message,  Android.Content.Context context)
        {
            try {
                displayMessageAndroid(title,message,context);
            } catch (Exception ex) {
                Console.WriteLine (ex.Message + "  " + ex.StackTrace);
            }
        }

        public static List<Android.Gms.Maps.Model.LatLng> DecodePolylinePoints(string encodedPoints)
        {
            try {
                return DecodePolylinePointsAndroid(encodedPoints);
            } catch (Exception ex) {
                Console.WriteLine (ex.Message + "  " + ex.StackTrace);
                return null;
            }
        }

        private static void saveUserAvatarAndroid(string avatarUrl)
        {
//            System.Threading.ThreadPool.QueueUserWorkItem(state =>
//                {                                   
            try
            {
                if(File.Exists(Consts.USER_AVATAR_IMAGE_PATH))
                {
                    File.Delete(Consts.USER_AVATAR_IMAGE_PATH);
                }

                Stream imgStream = new Java.Net.URL(avatarUrl).OpenStream();

                using (var fileStream = File.Create(Consts.USER_AVATAR_IMAGE_PATH))
                {
                    imgStream.CopyTo(fileStream);
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog (ex);
//                throw ex;
            }
//                });
        }

        public static List<LatLng> AddressToCoordinates (Android.Content.Context context, string address)
        {
            try {
                List<LatLng> locResultsList = new List<LatLng> ();
                Geocoder geocoder = new Geocoder (context);  
                IList<Address> addresses;
                addresses = geocoder.GetFromLocationName (address, 1);
                if (addresses.Count () > 0) {
                    LatLng latLng = new LatLng (addresses [0].Latitude, addresses [0].Longitude);
                    locResultsList.Add (latLng);
                }
                return locResultsList;

            } catch (Exception ex) {
                Utils.writeToDeviceLog (ex);
            }
            return null;
        }

		public static int calculateZoomLevel(int screenWidth) 
		{
			int zoomLevel = 1;
			try {
				double equatorLength = 40075004; // in meters
				double widthInPixels = screenWidth;
				double metersPerPixel = equatorLength / 256;

				while ((metersPerPixel * widthInPixels) > 165000) {
					metersPerPixel /= 2;
					++zoomLevel;
				}
			} 
			catch (Exception ex)
			{
				Utils.writeToDeviceLog (ex);
			}

			return zoomLevel;
		}

//        public static LocationInfoAndroid requestNewLocation(Context context)
//        {
//            LocationInfo resultLocation = null;
//            try
//            {
//                Console.WriteLine("-----------------------context is"+context.ToString());
//                LocationGetter lGetter = new LocationGetter(context);
//
//                //for(int i = 0; i < 10; i++)
//                {
//                    LocationInfo currentLocation1 = lGetter.getLocation(1000 * 10 * 1, 1000 * 5 * 1);
//
//                    //Console.WriteLine("latitudine"+currentLocation1.Location.Latitude.ToString());
//
//                    if (currentLocation1 != null && currentLocation1.Location != null && currentLocation1.Location.Speed * 3.6 <= Utils.DB_SPEED)
//                    {
//                        resultLocation = currentLocation1;
//                        Console.WriteLine("Set Location");
//                        //break;
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                Utils.writeToDeviceLog(ex);
//            }
//
//            return resultLocation;
//        }
    }
}


#endif
