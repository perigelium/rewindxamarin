﻿using System;
//using Android.Telephony;
using System.Globalization;
//using Android.Util;
//using Android.Content;
//using Newtonsoft.Json.Linq;
using System.Net;
//using Android.Content.PM;
//using Android.Views;
//using Android.Runtime;
//using Android.App;
using System.Collections.Generic;
//using Android.Gms.Maps.Model;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Security.AccessControl;
using System.Security.Principal;



namespace RewindShared
{
    public partial class Utils
    {


        const int minGooglePlayServicesRequired = 2012100;
        public static int DB_SPEED = 60;

        public static string PersonalFolder {
            get {
                #if SILVERLIGHT
                // Windows Phone expects a local path, not absolute
                string docPath = "";

                #else

                #if __ANDROID__
                // Just use whatever directory SpecialFolder.Personal returns
                string docPath = Environment.GetFolderPath (Environment.SpecialFolder.Personal);                    
                #else
                // we need to put in /Library/ on iOS5.1 to meet Apple's iCloud terms
                // (they don't want non-user-generated data in Documents)
                string documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal); // Documents folder
                string docPath = Path.Combine(documentsPath, "..", "Library"); // Library folder
                #endif
                #endif     

                return docPath;
            }
        }


        public static string getDeviceIMEI ()
        {
            try {
                #if __ANDROID__
                return getDeviceIMEIAndroid();
                #else
                return "";
                #endif
            } catch (Exception ex) {
                Console.WriteLine (ex.Message + "  " + ex.StackTrace);
                return "";
            }
        }

        public static void writeToDeviceLog (Exception exception)
        {
            try {
                #if __ANDROID__
                writeToDeviceLogAndroid (exception);
                #else
                writeToIOSDeviceLog(exception);
                #endif
            } catch (Exception ex) {
                Console.WriteLine (ex.Message + "  " + ex.StackTrace);
            }
        }

        public static bool CheckForInternetConn ()
        {
            try {
                #if __ANDROID__
                return CheckForInternetConnection ();
                #else
                return hasInternetConnectionIOS();
                #endif
            } catch (Exception ex) {
                Console.WriteLine (ex.Message + "  " + ex.StackTrace);
                return false;
            }
        }




//        public static GooglePlayServicesEnum getGooglePlayServicesStatus()
//        {
//            try
//            {
//                ApplicationInfo info = ApplicationContextProvider.getContext().PackageManager.GetApplicationInfo("com.google.android.gms", 0);
//
//                PackageInfo packageInfo = ApplicationContextProvider.getContext().PackageManager.GetPackageInfo("com.google.android.gms", 0);
//
//                int versionCode = packageInfo.VersionCode;
//
//                if (versionCode >= minGooglePlayServicesRequired)
//                {
//                    return GooglePlayServicesEnum.good;
//                }
//                else
//                {
//                    return GooglePlayServicesEnum.outOfDate;
//                }
//            }
//            catch (ActivityNotFoundException e)
//            {
//                return GooglePlayServicesEnum.missing;
//            }
//            catch (PackageManager.NameNotFoundException e)
//            {               
//                return GooglePlayServicesEnum.missing;
//            }
//            catch (Exception ex)
//            {
//                Utils.writeToDeviceLog(ex);
//            }
//
//            return GooglePlayServicesEnum.missing;
//        }

        public enum GooglePlayServicesEnum
        {
            outOfDate,
            missing,
            good
        }

        public static bool UserValid=true;
        public static float getScreenWidth ()
        {
            try {
                #if __ANDROID__
                return getScreenWidthAndroid();
                #else
                return 0;
                #endif
            } catch (Exception ex) {
                Console.WriteLine (ex.Message + "  " + ex.StackTrace);
                return 0;
            }
        }

//        public static LocationInfo requestNewLocation (Exception exception)
//        {
//            try {
//                #if __ANDROID__
//                return LocationInfoAndroid();
//                #else
//                #endif
//            } catch (Exception ex) {
//                Console.WriteLine (ex.Message + "  " + ex.StackTrace);
//            }
//        }

       

//        public static LocationInfoAndroid requestNewLocation(Context context)
//        {
//            LocationInfo resultLocation = null;
//            try
//            {
//                Console.WriteLine("-----------------------context is"+context.ToString());
//                LocationGetter lGetter = new LocationGetter(context);
//
//                //for(int i = 0; i < 10; i++)
//                {
//                    LocationInfo currentLocation1 = lGetter.getLocation(1000 * 10 * 1, 1000 * 5 * 1);
//
//                    //Console.WriteLine("latitudine"+currentLocation1.Location.Latitude.ToString());
//
//                    if (currentLocation1 != null && currentLocation1.Location != null && currentLocation1.Location.Speed * 3.6 <= DB_SPEED)
//                    {
//                        resultLocation = currentLocation1;
//                        Console.WriteLine("Set Location");
//                        //break;
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                writeToDeviceLog(ex);
//            }
//
//            return resultLocation;
//        }


/*        public static void displayError (string errMsg, Android.Content.Context context)
        {
            try {
                #if __ANDROID__
                displayErrorAndroid(errMsg,context);
                #else
                #endif
            } catch (Exception ex) {
                Console.WriteLine (ex.Message + "  " + ex.StackTrace);
            }
        }


        public static void displayMessage (string title, string message,  Android.Content.Context context)
        {
            try {
                #if __ANDROID__
                displayMessageAndroid(title,message,context);
                #else
                #endif
            } catch (Exception ex) {
                Console.WriteLine (ex.Message + "  " + ex.StackTrace);
            }
        }

        public static List<Android.Gms.Maps.Model.LatLng> DecodePolylinePoints(string encodedPoints)
        {
            try {
                #if __ANDROID__
                return DecodePolylinePointsAndroid(encodedPoints);
                #else
                #endif
            } catch (Exception ex) {
                Console.WriteLine (ex.Message + "  " + ex.StackTrace);
                return null;
            }
        }

*/

        public static string ApplicationDocumentsPath
        {
            get
            {
                #if SILVERLIGHT
                // Windows Phone expects a local path, not absolute
                string docPath = "";

                #else

                #if __ANDROID__
                // Just use whatever directory SpecialFolder.Personal returns
                string docPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);                    
                #else
                // we need to put in /Library/ on iOS5.1 to meet Apple's iCloud terms
                // (they don't want non-user-generated data in Documents)
                string docPath = Environment.GetFolderPath (Environment.SpecialFolder.Personal); // Documents folder
                //                string docPath = Path.Combine (documentsPath, "..", "Library"); // Library folder
                #endif
                #endif     

                return docPath;
            }
        }

        public static void checkApplicationDocuments()
        {
            try
            {
                // user DB Folder
                if (!Directory.Exists(Consts.USER_DB_FOLDER_PATH))
                {
                    Directory.CreateDirectory(Consts.USER_DB_FOLDER_PATH);

//                    DirectoryInfo dInfo = new DirectoryInfo(Consts.USER_DB_FOLDER_PATH);
//                    DirectorySecurity dSecurity = dInfo.GetAccessControl();
//                    dSecurity.AddAccessRule(new FileSystemAccessRule(new SecurityIdentifier(WellKnownSidType.WorldSid, null), FileSystemRights.FullControl, InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit, PropagationFlags.NoPropagateInherit, AccessControlType.Allow));
//                    dInfo.SetAccessControl(dSecurity);
                }

            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public static void deleteDB()
        {
            try
            {
                UserLocalDB db = new UserLocalDB();
                db.deleteTableName("AllClientsTable");
                db.deleteTableName("AllPraticheTable");
                db.deleteTableName("ProfileInfoTable");
                db.deleteTableName("AppuntamentiTable");
                db.deleteTableName("ClassificationTable");
                db.deleteTableName("BonusTable");
                db.deleteTableName("InvoicesTable");

                // user DB Folder
//                if (Directory.Exists(Consts.USER_DB_FOLDER_PATH))
//                {
//                    Directory.Delete(Consts.USER_DB_FOLDER_PATH,true);
//                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public static void deleteProfileInfoFromDB()
        {
            try
            {
                UserLocalDB db = new UserLocalDB();
                db.deleteTableName("ProfileInfoTable");
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        private static string userToken;

        public static string UserToken
        {
            get
            {
                try
                {
                    if (string.IsNullOrEmpty(userToken))
                    {
                        if (File.Exists(Utils.PersonalFolder + "/" + "userToken.txt"))
                        {
                            userToken = File.ReadAllText(Utils.PersonalFolder + "/" + "userToken.txt");
                        }
                    }
                }
                catch (Exception ex)
                {
                    Utils.writeToDeviceLog(ex);
                }

                return userToken;
            }
            set
            {
                try
                {     
                    File.WriteAllText(Utils.PersonalFolder + "/" + "userToken.txt", value.ToString());

                    userToken = value;
                }
                catch (Exception ex)
                {
                    Utils.writeToDeviceLog(ex);
                }
            }
        }


        private static string idAgent;

        public static string IdAgent
        {
            get
            {
                try
                {
                    if (string.IsNullOrEmpty(idAgent))
                    {
                        if (File.Exists(Utils.PersonalFolder + "/" + "idAgent.txt"))
                        {
                            IdAgent = File.ReadAllText(Utils.PersonalFolder + "/" + "idAgent.txt");
                        }
                    }
                }
                catch (Exception ex)
                {
                    Utils.writeToDeviceLog(ex);
                }

                return idAgent;
            }
            set
            {
                try
                {     
                    File.WriteAllText(Utils.PersonalFolder + "/" + "idAgent.txt", value.ToString());

                    idAgent = value;
                }
                catch (Exception ex)
                {
                    Utils.writeToDeviceLog(ex);
                }
            }
        }

//        public static JObject allStatuses;
//        public static JToken allTicketTipes;


        private static JObject allStatuses;

        public static JObject AllStatuses
        {
            get
            {
                try
                {
                    if (File.Exists(Utils.PersonalFolder+ "/" + "JsonAppuntamentiStatus"+ "/" + "StatusesAppuntamentiJson.txt"))
                    {
                        string savedJson = File.ReadAllText(Utils.PersonalFolder+ "/" + "JsonAppuntamentiStatus"+ "/" + "StatusesAppuntamentiJson.txt");
                        allStatuses = JObject.Parse(savedJson);
                    }
                }
                catch (Exception ex)
                {
                    Utils.writeToDeviceLog(ex);
                }

                return allStatuses;
            }
            set
            {
                try
                {  
					if(value!=null)
					{
						File.WriteAllText(Utils.PersonalFolder+ "/" + "JsonAppuntamentiStatus"+ "/" + "StatusesAppuntamentiJson.txt", value.ToString());

						allStatuses = value;
					}
                }
                catch (Exception ex)
                {
                    Utils.writeToDeviceLog(ex);
                }
            }
        }

//        public static void saveAppuntamentiStatus(string jsonString)
//        {
//            try
//            {
//                if (!Directory.Exists(Utils.PersonalFolder+ "/" + "JsonAppuntamentiStatus"))
//                {
//                    Directory.CreateDirectory(Utils.PersonalFolder+ "/" + "JsonAppuntamentiStatus");
//                }  
//
//                {
//                    if (!string.IsNullOrEmpty(jsonString))
//                    {
//                        // save json to disk
//                        File.WriteAllText(Utils.PersonalFolder+ "/" + "JsonAppuntamentiStatus"+ "/" + "StatusesAppuntamentiJson.txt", jsonString);
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                writeToDeviceLog(ex);
//            }
//        }
//
//        public static JObject LoadAppuntamentiStatus()
//        {
//            JObject obj = null;
//            try
//            {
//                // load json from disk
//                if (File.Exists(Utils.PersonalFolder+ "/" + "JsonAppuntamentiStatus"+ "/" + "StatusesAppuntamentiJson.txt"))
//                {
//                    string savedJson = File.ReadAllText(Utils.PersonalFolder+ "/" + "JsonAppuntamentiStatus"+ "/" + "StatusesAppuntamentiJson.txt");
//                    obj = JObject.Parse(savedJson);
//                }
//            }
//            catch (Exception ex)
//            {
//                writeToDeviceLog(ex);
//                obj = null;
//            }
//            return obj;
//        }

        private static JToken allTicketTipes;

        public static JToken AllTicketTipes
        {
            get
            {
                try
                {
                    if (File.Exists(Utils.PersonalFolder+ "/" + "JsonTicketTypes"+ "/" + "TicketTypesJson.txt"))
                    {
                        string savedJson = File.ReadAllText(Utils.PersonalFolder+ "/" + "JsonTicketTypes"+ "/" + "TicketTypesJson.txt");
                        allTicketTipes = JToken.Parse(savedJson);
                    }
                }
                catch (Exception ex)
                {
                    Utils.writeToDeviceLog(ex);
                }

                return allTicketTipes;
            }
            set
            {
                try
				{    if(value!=null)
					{
						File.WriteAllText(Utils.PersonalFolder+ "/" + "JsonTicketTypes"+ "/" + "TicketTypesJson.txt", value.ToString());

						allTicketTipes = value;
					}
                }
                catch (Exception ex)
                {
                    Utils.writeToDeviceLog(ex);
                }
            }
        }

//        public static void saveTicketTypes(string jsonString)
//        {
//            try
//            {
//                if (!Directory.Exists(Utils.PersonalFolder+ "/" + "JsonTicketTypes"))
//                {
//                    Directory.CreateDirectory(Utils.PersonalFolder+ "/" + "JsonTicketTypes");
//                }  
//
//                {
//                    if (!string.IsNullOrEmpty(jsonString))
//                    {
//                        // save json to disk
//                        File.WriteAllText(Utils.PersonalFolder+ "/" + "JsonTicketTypes"+ "/" + "TicketTypesJson.txt", jsonString);
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                writeToDeviceLog(ex);
//            }
//        }
//
//        public static JToken LoadTicketTypes()
//        {
//            JToken obj = null;
//            try
//            {
//                // load json from disk
//                if (File.Exists(Utils.PersonalFolder+ "/" + "JsonTicketTypes"+ "/" + "TicketTypesJson.txt"))
//                {
//                    string savedJson = File.ReadAllText(Utils.PersonalFolder+ "/" + "JsonTicketTypes"+ "/" + "TicketTypesJson.txt");
//                    obj = JToken.Parse(savedJson);
//                }
//            }
//            catch (Exception ex)
//            {
//                writeToDeviceLog(ex);
//                obj = null;
//            }
//            return obj;
//        }

        public static void createDirectory()
        {
            try
            {
                                if (!Directory.Exists(Utils.PersonalFolder+ "/" + "JsonTicketTypes"))
                                {
                                    Directory.CreateDirectory(Utils.PersonalFolder+ "/" + "JsonTicketTypes");
                                } 

                                if (!Directory.Exists(Utils.PersonalFolder+ "/" + "JsonAppuntamentiStatus"))
                                {
                                    Directory.CreateDirectory(Utils.PersonalFolder+ "/" + "JsonAppuntamentiStatus");
                                }  

            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        private static string deviceId;
        public static string DeviceId {
            get {
                try {
                    if (string.IsNullOrEmpty (deviceId)) {
                        if (File.Exists (Utils.PersonalFolder + "/" + "deviceId.txt")) {
                            deviceId = File.ReadAllText (Utils.PersonalFolder + "/" + "deviceId.txt");
                        }
                    }
                } catch (Exception ex) {
                    writeToDeviceLog (ex);
                }

                return deviceId;
            }
            set {
                try {     
                    File.WriteAllText (Utils.PersonalFolder + "/" + "deviceId.txt", value.ToString ());

                    deviceId = value;
                } catch (Exception ex) {
                    writeToDeviceLog (ex);
                }
            }
        }
//        public async  static Task<bool> CheckInternetAsync()
//        {
//            int count = 0;
//            int currentServer = 0;
//            string[] servers = { "http://google.com", "http://www.facebook.com", "http://twitter.com" };
//            while (true)
//            {
//                try
//                {
//                    var req = WebRequest.Create(servers[currentServer]);
//                    req.Timeout = 2000;
//                    req.Method = "HEAD";
//                    using (var resp = await req.GetResponseAsync())
//                    {
//                        return true;
//                    }
//                }
//                catch
//                {
//                    Console.WriteLine("Ping Failed:{0}", count);
//                    count++;
//                    Thread.Sleep(100);
//                    if (count == 9)
//                        return false;
//                    if (count % 3 == 0)
//                        currentServer++;
//                }
//            }
//        }
    

        public static void saveUserAvatar(string avatarUrl)
        {
            try
            {
                #if __ANDROID__
                saveUserAvatarAndroid(avatarUrl);
                #else
                saveUserAvatariOS(avatarUrl);
                #endif
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

		public delegate void UserRemovedDelegateApp ();

		public static event UserRemovedDelegateApp UserRemovedEventApp;

		public static void LogoutApp ()
		{
			try {
				if (Utils.UserRemovedEventApp != null) {
					Utils.UserRemovedEventApp ();
				}
			} catch (Exception ex) {
				Utils.writeToDeviceLog (ex);

			}
		}
    }
}

