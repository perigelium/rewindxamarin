﻿using System;
using System.Net;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Collections.Specialized;
using System.Text;
using RewindShared;

namespace RewindShared
{
    public class ApiCalls
    {
        public static void login(string username, string password)
        {

            try
            {
                string encodedUsername = System.Net.WebUtility.UrlEncode(username);
                string encodedPassword = System.Net.WebUtility.UrlEncode(password);

                string requestString = Consts.API_LOGIN_URL + "?username=" + encodedUsername + "&password=" + encodedPassword;

                // http request for the google api web service
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(requestString);
                request.ContentType = "application/x-www-form-urlencoded";
                request.Method = "POST";

                // get the response from server
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    { 
                        using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                        {
                            string content = reader.ReadToEnd();
                        }
                    }            
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public static JObject HttpLogin(string username, string password, string android_id)
        {
            JObject depts = null;
            string errorEx = string.Empty;
            try
            {

//                string URL = "http://areariservatagi.giovanimprenditori.org/WS/login_gi.ashx";
                WebClient webClient = new WebClient();

                NameValueCollection formData = new NameValueCollection();
                formData["username"] = username;
                formData["password"] = password;
                formData["device_id"] = android_id;

                byte[] responseBytes = webClient.UploadValues(Consts.API_LOGIN_URL, "POST", formData);
                string responsefromserver = Encoding.UTF8.GetString(responseBytes);
                Console.WriteLine(responsefromserver);

				if (responsefromserver.Contains("invalid user id or agent id") || responsefromserver.Contains("token invalid"))
				{
                    Utils.UserValid=false;
					Utils.LogoutApp();
				}
				else
				{
                    Utils.UserValid=true;
					depts = JObject.Parse(responsefromserver);
				}
                return depts;
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
                return null;
            }
        }

        public static JToken getAllClients(string userToken, string lat, string lang, bool city, bool searchFlag, string param, string valueSearch)
        {
            JToken depts = null;
            string errorEx = string.Empty;
            try
            {
                WebClient webClient = new WebClient();

                NameValueCollection formData = new NameValueCollection();
                if (searchFlag)
                {
                    formData["token"] = userToken;
                    formData["lat"] = lat;
                    formData["lng"] = lang;
                    formData["radius"] = "0.3";
                    formData["search"] = "true";
                    formData[param] = valueSearch;
                }
                else
                {
                    if (!city)
                    {
                        formData["token"] = userToken;
                        formData["lat"] = lat;
                        formData["lng"] = lang;
                        formData["radius"] = "0.3";
                    }
                    else
                    {
                        formData["token"] = userToken;
                        formData["lat"] = lat;
                        formData["lng"] = lang;
                        formData["radius"] = "0.2";
                        formData["city"] = "true";
                    }
                }

                byte[] responseBytes = webClient.UploadValues(Consts.API_GET_CLIENTS_URL, "POST", formData);
                string responsefromserver = Encoding.UTF8.GetString(responseBytes);
                Console.WriteLine(responsefromserver);
				if(responsefromserver.Contains("invalid user id or agent id")  ||  responsefromserver.Contains("token invalid"))
				{
                    Utils.UserValid=false;
					Utils.LogoutApp();
				}
				else
				{
                    Utils.UserValid=true;
					depts = JToken.Parse(responsefromserver);
				}
                return depts;
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
                return null;
            }
        }

        public static JObject getAllStatuses(string userToken)
        {
            JObject depts = null;
            string errorEx = string.Empty;
            try
            {
                WebClient webClient = new WebClient();

                NameValueCollection formData = new NameValueCollection();
                formData["token"] = userToken;

                byte[] responseBytes = webClient.UploadValues(Consts.API_GET_STATUSES_URL, "POST", formData);
                string responsefromserver = Encoding.UTF8.GetString(responseBytes);
                Console.WriteLine(responsefromserver);
                
				if(responsefromserver.Contains("invalid user id or agent id")  ||  responsefromserver.Contains("token invalid"))
				{
                    Utils.UserValid=false;
					Utils.LogoutApp();
				}
				else
				{
                    Utils.UserValid=true;
					depts = JObject.Parse(responsefromserver);
				}
                return depts;
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
                return null;
            }
        }

        public static bool postEventToCalendar(string userToken, string data_dal, string data_al, string ora, string place, string description)
        {
            bool saved = false;
            string errorEx = string.Empty;
            try
            {
                WebClient webClient = new WebClient();

                NameValueCollection formData = new NameValueCollection();
                formData["token"] = userToken;
                formData["data"] = data_dal;
                formData["hour"] = ora;
                formData["place"] = place;
                formData["description"] = description;

                byte[] responseBytes = webClient.UploadValues(Consts.API_POST_EVENT_TO_CALENDAR, "POST", formData);
                string responsefromserver = Encoding.UTF8.GetString(responseBytes);
                try
                {
                    responsefromserver = responsefromserver.Replace("\"", "");
                }
                catch (Exception ex)
                {
                    Utils.writeToDeviceLog(ex);
                }
                Console.WriteLine(responsefromserver);

				if(responsefromserver.Contains("invalid user id or agent id")  ||  responsefromserver.Contains("token invalid"))
				{
                    Utils.UserValid=false;
					Utils.LogoutApp();
				}
				else
				{
                    Utils.UserValid=true;
					saved = Convert.ToBoolean(responsefromserver);
				}
                return saved;
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
                return false;
            }
        }

        public static JToken getProfilClasificationandBonus(string userToken, string period, string URL)
        {
            JToken depts = null;
            string errorEx = string.Empty;
            try
            {
                WebClient webClient = new WebClient();

                NameValueCollection formData = new NameValueCollection();
                formData["token"] = userToken;
                formData["period"] = period;

                byte[] responseBytes = webClient.UploadValues(URL, "POST", formData);
                string responsefromserver = Encoding.UTF8.GetString(responseBytes);
                Console.WriteLine(responsefromserver);
				if(responsefromserver.Contains("invalid user id or agent id")  ||  responsefromserver.Contains("token invalid"))
				{
                    Utils.UserValid=false;
					Utils.LogoutApp();
				}
				else
				{
                    Utils.UserValid=true;
					depts = JToken.Parse(responsefromserver);
				}
				return depts;
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
                return null;
            }
        }

		public static JToken getAppuntamentiInPeriod(string userToken, string date_from, string date_to, string reqSociale, string localita, string search_type, string status_array_appuntamenti, string status_array_clienti, double lat, double lang, string idAppForMemo)
        {
            JToken depts = null;
            string errorEx = string.Empty;
            NameValueCollection formData = new NameValueCollection();
            try
            {
                WebClient webClient = new WebClient();
                if (search_type == "1")
                {
                    formData["token"] = userToken;
                    formData["search_type"] = search_type;
                    formData["date_from"] = date_from;//2015-11-19"
                    formData["date_to"] = date_to;//"2015-11-19";
//                    formData["regione_sociale"] = reqSociale;
//                    formData["place"] = localita;
                }
                else if (search_type == "2")
                {
                    formData["token"] = userToken;
                    formData["search_type"] = search_type;
                    formData["status_appointments"] = status_array_appuntamenti;
                    formData["status_clients"] = status_array_clienti;
                    formData["lat"] = lat.ToString();
                    formData["lng"] = lang.ToString();
                }
                else if (search_type == "3")
                {
                    formData["token"] = userToken;
                    formData["search_type"] = search_type;
					if(idAppForMemo!=string.Empty)
					{
						formData["id_appuntamento"] = idAppForMemo;
					}
					else
					{
						formData["regione_sociale"] = reqSociale;
						formData["place"] = localita;
					}
                }

                byte[] responseBytes = webClient.UploadValues(Consts.API_GET_APPUNTAMETI_URL, "POST", formData);
                string responsefromserver = Encoding.UTF8.GetString(responseBytes);
                Console.WriteLine(responsefromserver);

				if(responsefromserver.Contains("invalid user id or agent id")  ||  responsefromserver.Contains("token invalid"))
				{
                    Utils.UserValid=false;
					Utils.LogoutApp();
				}
				else
				{
                    Utils.UserValid=true;
					depts = JToken.Parse(responsefromserver);
				}
                
                return depts;
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
                return null;
            }
        }

        public static bool updateAppuntamenti(string userToken, string id, string status, string description, string source, string data, string hour, string place, string titleMemo, bool isMemo)
        {
            bool saved = false;
            string errorEx = string.Empty;
            try
            {
                WebClient webClient = new WebClient();

                NameValueCollection formData = new NameValueCollection();
                formData["token"] = userToken;
                formData["id"] = id;
                if(status!=string.Empty)
                {
                    formData["status"] = status;
                }

                if(isMemo)
                {
                    formData["note"] = description;
                }
                else
                {
                    formData["description"] = description;
                }
                formData["source"] = source;
                if(place!=string.Empty)
                {
                    formData["place"] = place;
                }
                if(titleMemo!=string.Empty)
                {
                    formData["title_memo"] = titleMemo;
                }
                if(data!=string.Empty && hour!=string.Empty)
                {
                    formData["data"] = data;
                    formData["ora"] = hour;
                }

                byte[] responseBytes = webClient.UploadValues(Consts.API_UPDATE_APPUNTAMETI_URL, "POST", formData);
                string responsefromserver = Encoding.UTF8.GetString(responseBytes);
                Console.WriteLine(responsefromserver);

				if(responsefromserver.Contains("invalid user id or agent id")  ||  responsefromserver.Contains("token invalid"))
				{
                    Utils.UserValid=false;
					Utils.LogoutApp();
				}
				else
				{
                    Utils.UserValid=true;
					saved = bool.Parse(responsefromserver);
				}
                return saved;
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
                return false;
            }
        }

        public static JToken getAllTicketTypes(string userToken)
        {
            JToken depts = null;
            string errorEx = string.Empty;
            try
            {
                WebClient webClient = new WebClient();

                NameValueCollection formData = new NameValueCollection();
                formData["token"] = userToken;

                byte[] responseBytes = webClient.UploadValues(Consts.API_GET_TICKET_TYPES_URL, "POST", formData);
                string responsefromserver = Encoding.UTF8.GetString(responseBytes);
                Console.WriteLine(responsefromserver);

				if(responsefromserver.Contains("invalid user id or agent id")  ||  responsefromserver.Contains("token invalid"))
				{
                    Utils.UserValid=false;
					Utils.LogoutApp();
				}
				else
				{
                    Utils.UserValid=true;
					depts = JToken.Parse(responsefromserver);
				}
                return depts;
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
                return null;
            }
        }

        public static bool postTicket(string userToken, string destinatario, string ticket_type_id, string customer_id, string id_pratiche, string subject, string description)
        {
            bool depts = false;
            string errorEx = string.Empty;
            try
            {
                WebClient webClient = new WebClient();

                NameValueCollection formData = new NameValueCollection();
                formData["token"] = userToken;
                formData["email"] = destinatario;
                formData["ticket_type_id"] = ticket_type_id;
                formData["customer_id"] = customer_id;
                formData["id_pratiche"] = id_pratiche;
                formData["subject"] = subject;
                formData["description"] = description;

                byte[] responseBytes = webClient.UploadValues(Consts.API_POST_TICKET_URL, "POST", formData);
                string responsefromserver = Encoding.UTF8.GetString(responseBytes);
                Console.WriteLine(responsefromserver);

				if(responsefromserver.Contains("invalid user id or agent id")  ||  responsefromserver.Contains("token invalid"))
				{
                    Utils.UserValid=false;
					Utils.LogoutApp();
				}
				else
				{
                    Utils.UserValid=true;
					depts = bool.Parse(responsefromserver);
				}
                return depts;
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
                return false;
            }
        }


        public static bool sendMemo(string userToken, string title, string date, string hour, string Description, string customer_id, string place)
        {
            bool depts = false;
            string errorEx = string.Empty;
            try
            {
                WebClient webClient = new WebClient();

                NameValueCollection formData = new NameValueCollection();
                formData["token"] = userToken;
                formData["title"] = title;
                formData["date"] = date;
                formData["hour"] = hour;
                formData["description"] = Description;
                formData["customer_id"] = customer_id;
                formData["place"] = place;

                byte[] responseBytes = webClient.UploadValues(Consts.API_POST_MEMO_URL, "POST", formData);
                string responsefromserver = Encoding.UTF8.GetString(responseBytes);
                Console.WriteLine(responsefromserver);
				if(responsefromserver.Contains("invalid user id or agent id")  ||  responsefromserver.Contains("token invalid"))
				{
                    Utils.UserValid=false;
					Utils.LogoutApp();
				}
				else
				{
                    Utils.UserValid=true;
					depts = bool.Parse(responsefromserver);
				}
				return depts;
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
                return false;
            }
        }

        public static JToken getAllMemos(string userToken, string client_id)
        {
            JToken depts = null;
            string errorEx = string.Empty;
            try
            {
                WebClient webClient = new WebClient();

                NameValueCollection formData = new NameValueCollection();
                formData["token"] = userToken;
                formData["client_id"] = client_id;

                byte[] responseBytes = webClient.UploadValues(Consts.API_GET_MEMO_URL, "POST", formData);
                string responsefromserver = Encoding.UTF8.GetString(responseBytes);
                Console.WriteLine(responsefromserver);

				if(responsefromserver.Contains("invalid user id or agent id")  ||  responsefromserver.Contains("token invalid"))
				{
                    Utils.UserValid=false;
					Utils.LogoutApp();
				}
				else
				{
                    Utils.UserValid=true;
					depts = JToken.Parse(responsefromserver);
				}
				return depts;
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
                return null;
            }
        }

        public static bool updateInvoiceChecked(string userToken, string invoice_id)
        {
            bool saved = false;
            string errorEx = string.Empty;
            try
            {
                WebClient webClient = new WebClient();

                NameValueCollection formData = new NameValueCollection();
                formData["token"] = userToken;
                formData["invoice_id"] = invoice_id;

                byte[] responseBytes = webClient.UploadValues(Consts.API_POST_INVOICE_CHECKED_URL, "POST", formData);
                string responsefromserver = Encoding.UTF8.GetString(responseBytes);
                Console.WriteLine(responsefromserver);

				if(responsefromserver.Contains("invalid user id or agent id")  ||  responsefromserver.Contains("token invalid"))
				{
                    Utils.UserValid=false;
					Utils.LogoutApp();
				}
				else
				{
                    Utils.UserValid=true;
					if (responsefromserver != null)
					{
						saved = true;
					}
					else
					{
						saved = false;
					}
				}
                return saved;
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
                return false;
            }
        }

        public static string Logout(string userToken)
        {
            string depts = "";
            string errorEx = string.Empty;
            try
            {
                WebClient webClient = new WebClient();

                NameValueCollection formData = new NameValueCollection();
                formData["token"] = userToken;

                byte[] responseBytes = webClient.UploadValues(Consts.API_LOGOUT_URL, "POST", formData);
                string responsefromserver = Encoding.UTF8.GetString(responseBytes);
                Console.WriteLine(responsefromserver);
//                depts=bool.Parse(responsefromserver);
                return responsefromserver;
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
                return "";
            }
        }

        public static string GetJsonFromGeocode(string url)
        {
            try
            {
                if (Utils.CheckForInternetConn())
                {
                    JObject json;
                    System.Net.WebRequest req = System.Net.WebRequest.Create(url);
                    HttpWebResponse resp = req.GetResponse() as HttpWebResponse;

                    if (resp.StatusCode == HttpStatusCode.OK)
                    {
                        using (Stream respStream = resp.GetResponseStream())
                        {
                            StreamReader reader = new StreamReader(respStream, Encoding.UTF8);
                            string readJson = reader.ReadToEnd();
                            return readJson;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
            return null;
        }
    }
}

