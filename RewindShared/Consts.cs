﻿using System;
//using Android.Graphics;

namespace RewindShared
{
	public class Consts
	{
//        http://wind.monitorcrm.it/upload/agenti/206/1438069232.jpg
//        /upload/agenti/{id_agent}/{campo DB age_users.file_name}

        public static string USER_AVATAR_URL = "http://wind.monitorcrm.it/upload/agenti/";

        public static string USER_AVATAR_IMAGE_PATH =  Utils.PersonalFolder + "/" + "avatar.jpg";       

		public static string pathJsonClientMemo=Utils.PersonalFolder + "/" + "jsonMemo.txt";

        public static string API_HOST_URL = "http://wind.monitorcrm.it/webservices/index.php/";

        public static string API_LOGIN_URL = API_HOST_URL + "api/login?ios=true";

        public static string API_GET_CLIENTS_URL = API_HOST_URL + "api/getClients?ios=true";

        public static string API_GET_STATUSES_URL = API_HOST_URL + "api/getStatuses?ios=true";

        public static string API_POST_EVENT_TO_CALENDAR = API_HOST_URL + "api/sendEvent?ios=true";

        public static string API_PROFILE_CLASIFICATION_URL = API_HOST_URL + "api/getClassification?ios=true";

        public static string API_PROFILE_BONUS_URL = API_HOST_URL + "api/getBonuses?ios=true";

        public static string API_PROFILE_INVOICES_URL = API_HOST_URL + "api/getInvoices?ios=true";

        public static string API_GET_APPUNTAMETI_URL = API_HOST_URL + "api/getAppointments?ios=true";

        public static string API_UPDATE_APPUNTAMETI_URL = API_HOST_URL + "api/updateAppuntament?ios=true";

        public static string API_GET_TICKET_TYPES_URL = API_HOST_URL +  "api/getTicketTypes?ios=true";

        public static string API_POST_TICKET_URL = API_HOST_URL +  "api/sendTicket?ios=true";

        public static string API_POST_INVOICE_CHECKED_URL = API_HOST_URL +  "api/sendInvoiceCheck?ios=true";

        public static string API_POST_MEMO_URL = API_HOST_URL +  "api/sendClientMemo?ios=true";

        public static string API_GET_MEMO_URL = API_HOST_URL +  "api/getClientMemos?ios=true";

        public static string API_LOGOUT_URL = API_HOST_URL +  "api/logout?ios=true";
            
        //database
        //database
        public const string USER_DB_FILE_EXTENSION = "db3";
        public const string APP_DB_FILE_EXTENSION = "sqlite";

        public const string USER_DB_NAME = "UserEntryDB." + USER_DB_FILE_EXTENSION;
        public static string USER_DB_FOLDER_PATH = Utils.ApplicationDocumentsPath + "/" + USER_DB_FOLDER_NAME;
        private const string USER_DB_FOLDER_NAME = "UserDbFolder";
        private const string CURRENT_DB_FOLDER_NAME = "CurrentDbFolder";

        public const string ALL_CLIENTS_TABLE_NAME = "AllClientsTable";
        public const string ALL_PRATICHE_TABLE_NAME = "AllPraticheTable";
        public const string PROFILE_INFO_TABLE_NAME = "ProfileInfoTable";
        public const string APPUNTAMENTI_TABLE_NAME = "AppuntamentiTable";

        public const string PROFILE_CLASSIFICATIOB_TABLE_NAME="ClassificationTable";
        public const string PROFILE_BONUS_TABLE_NAME="BonusTable";
        public const string PROFILE_INVOICES_TABLE_NAME="InvoicesTable";
	}
}

