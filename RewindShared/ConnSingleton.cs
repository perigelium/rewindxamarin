﻿using System;
using Mono.Data.Sqlite;
using System.Data;

namespace RewindShared
{
    public class ConnSingleton
    {

        private static ConnSingleton dbInstance;
        static string databasePath = Consts.USER_DB_FOLDER_PATH + "/" + Consts.USER_DB_NAME;
        public static Mono.Data.Sqlite.SqliteConnection conn = (Mono.Data.Sqlite.SqliteConnection)new Mono.Data.Sqlite.SqliteConnection("URI=file:" + databasePath + ",version=3");       

        private ConnSingleton()
        {
        }

        public static ConnSingleton getDbInstance()
        {
            if (dbInstance == null)
            {
                dbInstance = new ConnSingleton();
            }
            return dbInstance;
        }

        public Mono.Data.Sqlite.SqliteConnection GetDBConnection()
        {
            try
            {
                if(!(conn.State== System.Data.ConnectionState.Open))
                {
                    conn.Open();
                }
                Console.WriteLine("Connected");
            }
            catch (SqliteException e)
            {
                Console.WriteLine("Not connected : "+e.ToString());
                Console.ReadLine();
            }
            finally
            {
                Console.WriteLine("End..");
                // Console.WriteLine("Not connected : " + e.ToString());
                Console.ReadLine();

            }
            Console.ReadLine();
            return conn;
        }
    }
}

