﻿using System;

namespace RewindShared
{
    public class BonusItem
    {
        public string id;
        public DateTime data;
        public string regionalesociale;
        public string prodato;
        public float bonus;


        public string Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }

        public DateTime Data
        {
            get
            {
                return data;
            }
            set
            {
                data = value;
            }
        }

        public string Regionalesociale
        {
            get
            {
                return regionalesociale;
            }
            set
            {
                regionalesociale = value;
            }
        }

        public string Prodato
        {
            get
            {
                return prodato;
            }
            set
            {
                prodato = value;
            }
        }

        public float Bonus
        {
            get
            {
                return bonus;
            }
            set
            {
                bonus = value;
            }
        }

    }
}

