﻿using System;

namespace RewindShared
{
    public class ClientPraticheItemInfo
    {
        public string seriale;
        public string gnt_inserita;
        public string gnt_attiva;
        public string tipo_prodotto;
        public string prodotto;
        public DateTime pratiche_date;
        public DateTime data_inserimento;
        public DateTime data_creazione;
        public int qta_inserita;
        public int qta_attiva;
        public string linee;
        public string tipo_contratto;
        public double canone;
        public DateTime data_attivazione;
        public string stato_ordine;
        public string stato_pratica;
        public string codice_cliente;
        public string id_product;
        public string routing;
        public string notes;

        public string Seriale
        {
            get
            {
                return seriale;
            }
            set
            {
                seriale = value;
            }
        }

        public string Gnt_inserita
        {
            get
            {
                return gnt_inserita;
            }
            set
            {
                gnt_inserita = value;
            }
        }

        public string Gnt_attiva
        {
            get
            {
                return gnt_attiva;
            }
            set
            {
                gnt_attiva = value;
            }
        }

        public string Tipo_prodotto
        {
            get
            {
                return tipo_prodotto;
            }
            set
            {
                tipo_prodotto = value;
            }
        }

        public string Prodotto
        {
            get
            {
                return prodotto;
            }
            set
            {
                prodotto = value;
            }
        }

        public DateTime Pratiche_date
        {
            get
            {
                return pratiche_date;
            }
            set
            {
                pratiche_date = value;
            }
        }

        public DateTime Data_inserimento
        {
            get
            {
                return data_inserimento;
            }
            set
            {
                data_inserimento = value;
            }
        }

        public DateTime Data_creazione
        {
            get
            {
                return data_creazione;
            }
            set
            {
                data_creazione = value;
            }
        }

        public int Qta_inserita
        {
            get
            {
                return qta_inserita;
            }
            set
            {
                qta_inserita = value;
            }
        }

        public int Qta_attiva
        {
            get
            {
                return qta_attiva;
            }
            set
            {
                qta_attiva = value;
            }
        }

        public string Linee
        {
            get
            {
                return linee;
            }
            set
            {
                linee = value;
            }
        }

        public string Tipo_contratto
        {
            get
            {
                return tipo_contratto;
            }
            set
            {
                tipo_contratto = value;
            }
        }

        public double Canone
        {
            get
            {
                return canone;
            }
            set
            {
                canone = value;
            }
        }

        public DateTime Data_attivazione
        {
            get
            {
                return data_attivazione;
            }
            set
            {
                data_attivazione = value;
            }
        }

        public string Stato_ordine
        {
            get
            {
                return stato_ordine;
            }
            set
            {
                stato_ordine = value;
            }
        }

        public string Stato_pratica
        {
            get
            {
                return stato_pratica;
            }
            set
            {
                stato_pratica = value;
            }
        }

        public string Codice_cliente
        {
            get
            {
                return codice_cliente;
            }
            set
            {
                codice_cliente = value;
            }
        }

        public string Id_product
        {
            get
            {
                return id_product;
            }
            set
            {
                id_product = value;
            }
        }

        public string Routing
        {
            get
            {
                return routing;
            }
            set
            {
                routing = value;
            }
        }

        public string Notes
        {
            get
            {
                return notes;
            }
            set
            {
                notes = value;
            }
        }
    }
}

