﻿using System;

namespace RewindShared
{
    public class ClientiMemoItem
    {

        public string idclient;
        public string id;
        public DateTime date;
        public string title;
        public string details;
        public string place;


        public string IdClient
        {
            get
            {
                return idclient;
            }
            set
            {
                idclient = value;
            }
        }

        public string Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }

        public DateTime Date
        {
            get
            {
                return date;
            }
            set
            {
                date = value;
            }
        }


        public string Title
        {
            get
            {
                return title;
            }
            set
            {
                title = value;
            }
        }

        public string Details
        {
            get
            {
                return details;
            }
            set
            {
                details = value;
            }
        }


        public string Place
        {
            get
            {
                return place;
            }
            set
            {
                place = value;
            }
        }
    }
}

