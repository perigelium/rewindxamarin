﻿using System;
using System.Collections.Generic;

namespace RewindShared
{
    public class FatturareItem
    {

        public int id;
        public DateTime data;
        public float total;
        public string check;
        public string storni;
        public string storniconsumare;
        public string bonus;
        public string gara;
        public string anticipo;
        public string status;
        public string title_invoice;
        public string period;

        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }

        public DateTime Data
        {
            get
            {
                return data;
            }
            set
            {
                data = value;
            }
        }

        public float Total
        {
            get
            {
                return total;
            }
            set
            {
                total = value;
            }
        }

        public string Check
        {
            get
            {
                return check;
            }
            set
            {
                check = value;
            }
        }

        public string Storni
        {
            get
            {
                return storni;
            }
            set
            {
                storni = value;
            }
        }

        public string Storniconsumare
        {
            get
            {
                return storniconsumare;
            }
            set
            {
                storniconsumare = value;
            }
        }

        public string Bonus
        {
            get
            {
                return bonus;
            }
            set
            {
                bonus = value;
            }
        }

        public string Gara
        {
            get
            {
                return gara;
            }
            set
            {
                gara = value;
            }
        }

        public string Anticipo
        {
            get
            {
                return anticipo;
            }
            set
            {
                anticipo = value;
            }
        }



        public string Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
            }
        }

        public string Title_invoice
        {
            get
            {
                return title_invoice;
            }
            set
            {
                title_invoice = value;
            }
        }

        public string Period
        {
            get
            {
                return period;
            }
            set
            {
                period = value;
            }
        }
    }

    public class FatturarePeriodItem
    {
        /*
"name": "Integrazioni",
    "group": "Altre Competenze",
    "id": "124556",
    "date": "2015-03-19",
    "total": "150",
    "checked": "2015-05-14",
    "period": "201501"
        */
        public string period;
        public float total;
        public DateTime data;
        public string check;
        List<Dictionary<string,List<FatturareItem>>>facturi;
        public string id;

        public string Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }

        public string Period
        {
            get
            {
                return period;
            }
            set
            {
                period = value;
            }
        }

        public float Total
        {
            get
            {
                return total;
            }
            set
            {
                total = value;
            }
        }

        public List<Dictionary<string, List<FatturareItem>>> Facturi
        {
            get
            {
                return facturi;
            }
            set
            {
                facturi = value;
            }
        }

        public DateTime Data
        {
            get
            {
                return data;
            }
            set
            {
                data = value;
            }
        }

        public string Check
        {
            get
            {
                return check;
            }
            set
            {
                check = value;
            }
        }
    }
}

