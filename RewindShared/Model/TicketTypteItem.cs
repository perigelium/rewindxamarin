﻿using System;

namespace RewindAndroid
{
    public class TicketTypteItem
    {
        public string id_type_ticket;
        public string type;
        public string description;

        public string Id_type_ticket
        {
            get
            {
                return id_type_ticket;
            }
            set
            {
                id_type_ticket = value;
            }
        }

        public string Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value;
            }
        }

        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }
    }
}

