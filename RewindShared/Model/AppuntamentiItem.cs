﻿using System;
using System.Collections.Generic;

namespace RewindShared
{
    public class AppuntamentiItem
    {
        public string id;
        public DateTime appuntamentiDateHour;
        public DateTime original_appuntamentiDateHour;
        public DateTime appuntamentiChangedTime;
        public string regSociale;
        public string representatnteLegale;
        public string mail;
        public string phone;
        public string status;
        public string tipclient;
        public string note;
        public double latitude;
        public double longitude;
        public string city;

        public string id_Chiamata;
        public DateTime date_chiamata;
        public string note_agent;
        public string note_chiamata;
        public string note_segretaria;
        public string motivo;
        public string category;
        public string indirizzo;
        public string civico;
        public string cap;
        public string comune;
        public string provincia;
        public string sigla;
        public string contact_name;
        public string contact_phone;
        public string contact_mail;
        public string id_customer;
        public string source;
        public string acronim;

        public string tipAppuntamenti;
        public bool appBloccati;
        public string contattiInfo;
        public List<string>emailList;
        public List<string>phoneList;
        public string id_appto_memo;
        public string evento_pers;
		public bool dateChanged;

        public string Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }

        public DateTime AppuntamentiDateHour
        {
            get
            {
                return appuntamentiDateHour;
            }
            set
            {
                appuntamentiDateHour = value;
            }
        }

        public string RegSociale
        {
            get
            {
                return regSociale;
            }
            set
            {
                regSociale = value;
            }
        }

        public string RepresentatnteLegale
        {
            get
            {
                return representatnteLegale;
            }
            set
            {
                representatnteLegale = value;
            }
        }

        public string Mail
        {
            get
            {
                return mail;
            }
            set
            {
                mail = value;
            }
        }

        public string Phone
        {
            get
            {
                return phone;
            }
            set
            {
                phone = value;
            }
        }

        public string Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
            }
        }

        public string TipClient
        {
            get
            {
                return tipclient;
            }
            set
            {
                tipclient = value;
            }
        }

        public string Note
        {
            get
            {
                return note;
            }
            set
            {
                note = value;
            }
        }

        public double Latitude
        {
            get
            {
                return latitude;
            }
            set
            {
                latitude = value;
            }
        }

        public double Longitude
        {
            get
            {
                return longitude;
            }
            set
            {
                longitude = value;
            }
        }

        public string City
        {
            get
            {
                return city;
            }
            set
            {
                city = value;
            }
        }

        public string Id_Chiamata
        {
            get
            {
                return id_Chiamata;
            }
            set
            {
                id_Chiamata = value;
            }
        }

        public DateTime Date_chiamata
        {
            get
            {
                return date_chiamata;
            }
            set
            {
                date_chiamata = value;
            }
        }

        public string Note_agent
        {
            get
            {
                return note_agent;
            }
            set
            {
                note_agent = value;
            }
        }

        public string Note_chiamata
        {
            get
            {
                return note_chiamata;
            }
            set
            {
                note_chiamata = value;
            }
        }

        public string Note_segretaria
        {
            get
            {
                return note_segretaria;
            }
            set
            {
                note_segretaria = value;
            }
        }

        public string Motivo
        {
            get
            {
                return motivo;
            }
            set
            {
                motivo = value;
            }
        }

        public string Category
        {
            get
            {
                return category;
            }
            set
            {
                category = value;
            }
        }

        public string Indirizzo
        {
            get
            {
                return indirizzo;
            }
            set
            {
                indirizzo = value;
            }
        }

        public string Civico
        {
            get
            {
                return civico;
            }
            set
            {
                civico = value;
            }
        }

        public string Cap
        {
            get
            {
                return cap;
            }
            set
            {
                cap = value;
            }
        }

        public string Comune
        {
            get
            {
                return comune;
            }
            set
            {
                comune = value;
            }
        }

        public string Provincia
        {
            get
            {
                return provincia;
            }
            set
            {
                provincia = value;
            }
        }

        public string Sigla
        {
            get
            {
                return sigla;
            }
            set
            {
                sigla = value;
            }
        }

        public string Contact_name
        {
            get
            {
                return contact_name;
            }
            set
            {
                contact_name = value;
            }
        }

        public string Contact_phone
        {
            get
            {
                return contact_phone;
            }
            set
            {
                contact_phone = value;
            }
        }

        public string Contact_mail
        {
            get
            {
                return contact_mail;
            }
            set
            {
                contact_mail = value;
            }
        }

        public string Id_customer
        {
            get
            {
                return id_customer;
            }
            set
            {
                id_customer = value;
            }
        }

        public string Source
        {
            get
            {
                return source;
            }
            set
            {
                source = value;
            }
        }

        public string Acronim
        {
            get
            {
                return acronim;
            }
            set
            {
                acronim = value;
            }
        }

        public string TipAppuntamenti
        {
            get
            {
                return tipAppuntamenti;
            }
            set
            {
                tipAppuntamenti = value;
            }
        }

        public bool AppBloccati
        {
            get
            {
                return appBloccati;
            }
            set
            {
                appBloccati = value;
            }
        }

        public string ContattiInfo
        {
            get
            {
                return contattiInfo;
            }
            set
            {
                contattiInfo = value;
            }
        }

        public List<string> EmailList
        {
            get
            {
                return emailList;
            }
            set
            {
                emailList = value;
            }
        }

        public List<string> PhoneList
        {
            get
            {
                return phoneList;
            }
            set
            {
                phoneList = value;
            }
        }

        public string Id_appto_memo
        {
            get
            {
                return id_appto_memo;
            }
            set
            {
                id_appto_memo = value;
            }
        }

        public string Evento_pers
        {
            get
            {
                return evento_pers;
            }
            set
            {
                evento_pers = value;
            }
        }

		public bool DateChanged {
			get {
				return dateChanged;
			}
			set {
				dateChanged = value;
			}
		}
    }
}

