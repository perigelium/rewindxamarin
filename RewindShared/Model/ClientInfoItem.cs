﻿using System;
using System.Collections.Generic;

namespace RewindShared
{
    public class ClientInfoItem
    {
        public string regione_sociale;
        public string partita_iva;
        public string phone;
        public string location;
        public string mobile_phone;
        public string province;
        public string mail;
        public string address;
        public string fax;
        public string referente;
        public string status;
        public DateTime date_clienti;
        public double latitude;
        public double longitude;
        public string customer_id;
        public string flag_borsellino;
        public string flag_preso;
        public DateTime date_convergence;

        //altuofianco info
        public DateTime data_inserimento;
        public string tipo_contratto;
        public string codice_cliente;
        public string email_shop;
        public int punti_disponibili;
        public string stato_rid;
        public string numero_referenze;

        public string atf_id_customer;

        List<ClientPraticheItemInfo> practiche_list;

        public string Regione_sociale
        {
            get
            {
                return regione_sociale;
            }
            set
            {
                regione_sociale = value;
            }
        }

        public string Partita_iva
        {
            get
            {
                return partita_iva;
            }
            set
            {
                partita_iva = value;
            }
        }

        public string Phone
        {
            get
            {
                return phone;
            }
            set
            {
                phone = value;
            }
        }

        public string Location
        {
            get
            {
                return location;
            }
            set
            {
                location = value;
            }
        }

        public string Mobile_phone
        {
            get
            {
                return mobile_phone;
            }
            set
            {
                mobile_phone = value;
            }
        }

        public string Province
        {
            get
            {
                return province;
            }
            set
            {
                province = value;
            }
        }

        public string Mail
        {
            get
            {
                return mail;
            }
            set
            {
                mail = value;
            }
        }

        public string Address
        {
            get
            {
                return address;
            }
            set
            {
                address = value;
            }
        }

        public string Fax
        {
            get
            {
                return fax;
            }
            set
            {
                fax = value;
            }
        }

        public string Referente
        {
            get
            {
                return referente;
            }
            set
            {
                referente = value;
            }
        }

        public string Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
            }
        }

        public DateTime Date_clienti
        {
            get
            {
                return date_clienti;
            }
            set
            {
                date_clienti = value;
            }
        }

        public double Latitude
        {
            get
            {
                return latitude;
            }
            set
            {
                latitude = value;
            }
        }

        public double Longitude
        {
            get
            {
                return longitude;
            }
            set
            {
                longitude = value;
            }
        }

        public DateTime Data_inserimento
        {
            get
            {
                return data_inserimento;
            }
            set
            {
                data_inserimento = value;
            }
        }

        public string Tipo_contratto
        {
            get
            {
                return tipo_contratto;
            }
            set
            {
                tipo_contratto = value;
            }
        }

        public string Codice_cliente
        {
            get
            {
                return codice_cliente;
            }
            set
            {
                codice_cliente = value;
            }
        }

        public string Email_shop
        {
            get
            {
                return email_shop;
            }
            set
            {
                email_shop = value;
            }
        }

        public int Punti_disponibili
        {
            get
            {
                return punti_disponibili;
            }
            set
            {
                punti_disponibili = value;
            }
        }

        public string Stato_rid
        {
            get
            {
                return stato_rid;
            }
            set
            {
                stato_rid = value;
            }
        }

        public string Numero_referenze
        {
            get
            {
                return numero_referenze;
            }
            set
            {
                numero_referenze = value;
            }
        }

        public List<ClientPraticheItemInfo> Practiche_list
        {
            get
            {
                return practiche_list;
            }
            set
            {
                practiche_list = value;
            }
        }

        public string Customer_id
        {
            get
            {
                return customer_id;
            }
            set
            {
                customer_id = value;
            }
        }

        public string Flag_borsellino
        {
            get
            {
                return flag_borsellino;
            }
            set
            {
                flag_borsellino = value;
            }
        }

        public string Flag_preso
        {
            get
            {
                return flag_preso;
            }
            set
            {
                flag_preso = value;
            }
        }

        public DateTime Date_convergence
        {
            get
            {
                return date_convergence;
            }
            set
            {
                date_convergence = value;
            }
        }

        public string Atf_id_customer
        {
            get
            {
                return atf_id_customer;
            }
            set
            {
                atf_id_customer = value;
            }
        }
    }
}

