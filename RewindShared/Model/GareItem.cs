﻿using System;

namespace RewindShared
{
    public class GareItem
    {
        public int id;
        public DateTime data;
        public string agente;
        public string punti;
        public string pa;
        public string ps;
        public string ss;

        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }

        public DateTime Data
        {
            get
            {
                return data;
            }
            set
            {
                data = value;
            }
        }

        public string Agente
        {
            get
            {
                return agente;
            }
            set
            {
                agente = value;
            }
        }

        public string Punti
        {
            get
            {
                return punti;
            }
            set
            {
                punti = value;
            }
        }

        public string PA
        {
            get
            {
                return pa;
            }
            set
            {
                pa = value;
            }
        }

        public string PS
        {
            get
            {
                return ps;
            }
            set
            {
                ps = value;
            }
        }

        public string SS
        {
            get
            {
                return ss;
            }
            set
            {
                ss = value;
            }
        }
    }
}

