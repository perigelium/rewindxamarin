﻿using System;

namespace RewindShared
{
    public class ProfileInfoItem
    {
        public string id_user;
        public string id_agency;
        public string id_agent;
        public string surname;
        public string name;
        public string address;
        public string number;
        public string mobile;
        public string mail;
        public string is_in_top;
        public string partita_iva;
        private string avatar;

        public string Id_user
        {
            get
            {
                return id_user;
            }
            set
            {
                id_user = value;
            }
        }

        public string Id_agency
        {
            get
            {
                return id_agency;
            }
            set
            {
                id_agency = value;
            }
        }

        public string Id_agent
        {
            get
            {
                return id_agent;
            }
            set
            {
                id_agent = value;
            }
        }

        public string Surname
        {
            get
            {
                return surname;
            }
            set
            {
                surname = value;
            }
        }

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public string Address
        {
            get
            {
                return address;
            }
            set
            {
                address = value;
            }
        }

        public string Number
        {
            get
            {
                return number;
            }
            set
            {
                number = value;
            }
        }

        public string Mobile
        {
            get
            {
                return mobile;
            }
            set
            {
                mobile = value;
            }
        }

        public string Mail
        {
            get
            {
                return mail;
            }
            set
            {
                mail = value;
            }
        }

        public string Is_in_top
        {
            get
            {
                return is_in_top;
            }
            set
            {
                is_in_top = value;
            }
        }

        public string Partita_iva
        {
            get
            {
                return partita_iva;
            }
            set
            {
                partita_iva = value;
            }
        }

        public string Avatar
        {
            get
            {
                return avatar;
            }
            set
            {
                avatar = value;
            }
        }
    }
}

