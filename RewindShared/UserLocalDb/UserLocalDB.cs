﻿using System;
using Mono.Data.Sqlite;
using System.IO;
using System.Data;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;

namespace RewindShared
{
    public class UserLocalDB
    {
        private const string DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

        string databasePath;

        public UserLocalDB()
        {
            try
            {
                this.databasePath = Consts.USER_DB_FOLDER_PATH + "/" + Consts.USER_DB_NAME;
                Utils.checkApplicationDocuments();
                createDatabase();
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        void createDatabase()
        {
            Mono.Data.Sqlite.SqliteConnection dbcon = null;

            try
            {
                if (!File.Exists(databasePath))
                {
                    SqliteConnection.CreateFile(databasePath);

                    string connectionString = "URI=file:" + databasePath + ",version=3";

                    dbcon = (Mono.Data.Sqlite.SqliteConnection)new Mono.Data.Sqlite.SqliteConnection(connectionString);
                    dbcon.Open();

                    string sSQLQuery;

                    sSQLQuery = "";
                    sSQLQuery = @"CREATE TABLE IF NOT EXISTS `AllClientsTable` (
    `Id`    NVARCHAR(100) NOT NULL,
    `Regione_sociale`   VARCHAR(100),
    `Partita_iva`   VARCHAR(100),
    `Phone`   VARCHAR(100),
    `Location`   VARCHAR(100),
    `Mobile_phone`   VARCHAR(100),
    `Province`   VARCHAR(100),
    `Mail`   VARCHAR(100),
    `Address`   VARCHAR(100),
    `Fax`   VARCHAR(100),
    `Referente`   VARCHAR(100),
    `Status`   VARCHAR(100),
    `Date_clienti`  DATETIME,
    `Latitude`   DOUBLE,
    `Longitude`   DOUBLE,
    `Data_inserimento`  DATETIME,
    `Tipo_contratto`   VARCHAR(100),
    `Codice_cliente`   VARCHAR(100),
    `Email_shop`   VARCHAR(100),
    `Punti_disponibili`   VARCHAR(100),
    `Stato_rid`   VARCHAR(100),
    `Numero_referenze`   VARCHAR(100),
    `Customer_id`   VARCHAR(100),
    `Flag_borsellino`   VARCHAR(100),
    `Flag_preso`   VARCHAR(100),
    `Date_convergence`   DATETIME,
    `Atf_id_customer`   VARCHAR(100),
    PRIMARY KEY(Id)
);";
                    using (var cmd = dbcon.CreateCommand())
                    {
                        cmd.CommandText = sSQLQuery;
                        cmd.CommandType = CommandType.Text;
                        cmd.ExecuteNonQuery();
                    }


                    sSQLQuery = "";
                    sSQLQuery = @"CREATE TABLE IF NOT EXISTS `AllPraticheTable` (
    `Seriale`    NVARCHAR(100) NOT NULL,
    `Qnt_inserita`   VARCHAR(100),
    `Qnt_attiva`   VARCHAR(100),
    `Tipo_prodotto`   VARCHAR(100),
    `Prodotto`   VARCHAR(100),
    `Pratiche_date`   DATETIME,
    `Data_inserimento`   DATETIME,
    `Data_creazione`   DATETIME,
    `Linee`   VARCHAR(100),
    `Tipo_contratto`   VARCHAR(100),
    `Canone`   VARCHAR(100),
    `Data_attivazione`   DATETIME,
    `Stato_ordine`   VARCHAR(100),
    `Stato_pratica`   VARCHAR(100),
    `Codice_cliente`   VARCHAR(100),
    `Id_product`   VARCHAR(100),
    `Routing`   VARCHAR(100),
    `Notes`   VARCHAR(100),
    PRIMARY KEY(Id_product)
);";

                    using (var cmd = dbcon.CreateCommand())
                    {
                        cmd.CommandText = sSQLQuery;
                        cmd.CommandType = CommandType.Text;
                        cmd.ExecuteNonQuery();
                    }


                    sSQLQuery = "";
                    sSQLQuery = @"CREATE TABLE IF NOT EXISTS `ProfileInfoTable` (
    `Id_user`    NVARCHAR(100) NOT NULL,
    `Id_agency`   VARCHAR(100),
    `Id_agent`   VARCHAR(100),
    `Surname`   VARCHAR(100),
    `Name`   VARCHAR(100),
    `Address`   VARCHAR(100),
    `Number`   VARCHAR(100),
    `Mobile`   VARCHAR(100),
    `Mail`   VARCHAR(100),
    `Is_in_top`   VARCHAR(100),
    `Vat`   VARCHAR(100),
    `Avatar`   VARCHAR(100),
    PRIMARY KEY(Id_user)
);";

                    using (var cmd = dbcon.CreateCommand())
                    {
                        cmd.CommandText = sSQLQuery;
                        cmd.CommandType = CommandType.Text;
                        int nrOfAffectedRows = cmd.ExecuteNonQuery();
                        Console.WriteLine(nrOfAffectedRows);
                    }

                    sSQLQuery = "";
                    sSQLQuery = @"CREATE TABLE IF NOT EXISTS `AppuntamentiTable` (
    `Id`    NVARCHAR(100) NOT NULL,
    `AppuntamentiDateHour`   DATETIME,
    `Original_AppuntamentiDateHour`   DATETIME,
    `RegSociale`   VARCHAR(100),
    `RepresentatnteLegale`   VARCHAR(100),
    `Mail`   VARCHAR(100),
    `Phone`   VARCHAR(100),
    `Status`   VARCHAR(100),
    `Tipclient`   VARCHAR(100),
    `Note`   VARCHAR(100),
    `Latitude`   DOUBLE,
    `Longitude`   DOUBLE,
    `City`   VARCHAR(100),
    `Id_Chiamata`   VARCHAR(100),
    `Date_chiamata`   DATETIME,
    `Note_agent`   VARCHAR(100),
    `Note_chiamata`   VARCHAR(100),
    `Note_segretaria`   VARCHAR(100),
    `Motivo`   VARCHAR(100),
    `Category`   VARCHAR(100),
    `Indirizzo`   VARCHAR(100),
    `Civico`   VARCHAR(100),
    `Cap`   VARCHAR(100),
    `Comune`   VARCHAR(100),
    `Provincia`   VARCHAR(100),
    `Sigla`   VARCHAR(100),
    `Contact_name`   VARCHAR(100),
    `Contact_phone`   VARCHAR(100),
    `Contact_mail`   VARCHAR(100),
    `Id_customer`   VARCHAR(100),
    `Source`   VARCHAR(100),
    `TipAppuntamento`   VARCHAR(100),
    `ContattiInfo`   VARCHAR(100),
    `PhoneList`   VARCHAR(200),
    `EmailList`   VARCHAR(200),
    `Id_Appto_Memo`   VARCHAR(100),
    `Evento_Pers`   VARCHAR(100),
    PRIMARY KEY(Id)
);";

                    using (var cmd = dbcon.CreateCommand())
                    {
                        cmd.CommandText = sSQLQuery;
                        cmd.CommandType = CommandType.Text;
                        cmd.ExecuteNonQuery();
                    }

                    sSQLQuery = "";
                    sSQLQuery = @"CREATE TABLE IF NOT EXISTS `ClassificationTable` (
    `Id`    NVARCHAR(100) NOT NULL,
    `Data`   DATETIME,
    `Agente`   VARCHAR(100),
    `Punti`   VARCHAR(100),
    `Pa`   VARCHAR(100),
    `Ps`   VARCHAR(100),
    `Ss`   VARCHAR(100),
    PRIMARY KEY(Id)
);";
                    using (var cmd = dbcon.CreateCommand())
                    {
                        cmd.CommandText = sSQLQuery;
                        cmd.CommandType = CommandType.Text;
                        cmd.ExecuteNonQuery();
                    }
                    /*
                            public string id;
        public string data;
        public string regionalesociale;
        public string prodato;
        public float bonus;
*/
                    sSQLQuery = "";
                    sSQLQuery = @"CREATE TABLE IF NOT EXISTS `BonusTable` (
    `Id`    NVARCHAR(100) NOT NULL,
    `Data`   DATETIME,
    `Regionalesociale`   VARCHAR(100),
    `Prodato`   VARCHAR(100),
    `Bonus`   FLOAT(5),
    PRIMARY KEY(Id)
);";
                    using (var cmd = dbcon.CreateCommand())
                    {
                        cmd.CommandText = sSQLQuery;
                        cmd.CommandType = CommandType.Text;
                        cmd.ExecuteNonQuery();
                    }

                    sSQLQuery = "";
                    sSQLQuery = @"CREATE TABLE IF NOT EXISTS `InvoicesTable` (
    `Id`    NVARCHAR(100) NOT NULL,
    `Data`   DATETIME,
    `Total`   FLOAT(5),
    `CheckInvoice`   VARCHAR(100),
    `Status`   VARCHAR(100),
    `Title_invoice`   VARCHAR(100),
    `Period`   VARCHAR(100),
    PRIMARY KEY(Id)
);";
                    using (var cmd = dbcon.CreateCommand())
                    {
                        cmd.CommandText = sSQLQuery;
                        cmd.CommandType = CommandType.Text;
                        cmd.ExecuteNonQuery();
                    }

                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
            finally
            {
                if (dbcon != null)
                {
                    dbcon.Close();
                    dbcon = null;
                }
            }
        }


        public void InsertClientsToTable(ClientInfoItem clientItem)
        {
            Object obj = new Object();
            try
            {

                lock (obj)
                {
                    string connectionString = "URI=file:" + databasePath + ",version=3";

                    if(ConnSingleton.conn.State!= ConnectionState.Open)
                    {
                        ConnSingleton.conn.Open();
                    }

                    string sSQLQuery = "INSERT OR IGNORE INTO " +
                        Consts.ALL_CLIENTS_TABLE_NAME + " " +
                        "(Id, Regione_sociale, Partita_iva, Phone,Location, Mobile_phone,Province, Mail, Address, Fax, Referente, Status, Date_clienti, Latitude,Longitude, Data_inserimento, Tipo_contratto, Codice_cliente, Email_shop, Punti_disponibili, Stato_rid, Numero_referenze,Customer_id,Flag_borsellino,Flag_preso,Date_convergence,Atf_id_customer) " +
                        "VALUES (@Id, @Regione_sociale, @Partita_iva, @Phone,@Location, @Mobile_phone,@Province, @Mail, @Address, @Fax, @Referente, @Status, @Date_clienti, @Latitude, @Longitude, @Data_inserimento, @Tipo_contratto, @Codice_cliente, @Email_shop, @Punti_disponibili, @Stato_rid, @Numero_referenze,@Customer_id,@Flag_borsellino,@Flag_preso,@Date_convergence,@Atf_id_customer);";

                    using (var cmd = ConnSingleton.conn.CreateCommand())
                    {
                        cmd.CommandTimeout = 0;
                        cmd.CommandText = sSQLQuery;
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.Add(new SqliteParameter("@Id", clientItem.Partita_iva));
                        cmd.Parameters.Add(new SqliteParameter("@Regione_sociale", clientItem.Regione_sociale));
                        cmd.Parameters.Add(new SqliteParameter("@Partita_iva", clientItem.Partita_iva));
                        cmd.Parameters.Add(new SqliteParameter("@Phone", clientItem.Phone));
                        cmd.Parameters.Add(new SqliteParameter("@Location", clientItem.Location));
                        cmd.Parameters.Add(new SqliteParameter("@Mobile_phone", clientItem.Mobile_phone));
                        cmd.Parameters.Add(new SqliteParameter("@Province", clientItem.Province));
                        cmd.Parameters.Add(new SqliteParameter("@Mail", clientItem.Mail));
                        cmd.Parameters.Add(new SqliteParameter("@Address", clientItem.Address));
                        cmd.Parameters.Add(new SqliteParameter("@Fax", clientItem.Fax));
                        cmd.Parameters.Add(new SqliteParameter("@Referente", clientItem.Referente));
                        cmd.Parameters.Add(new SqliteParameter("@Status", clientItem.Status));
                       
                        cmd.Parameters.Add(new SqliteParameter("@Date_clienti", clientItem.Date_clienti));
                        cmd.Parameters.Add(new SqliteParameter("@Latitude", clientItem.Latitude));
                        cmd.Parameters.Add(new SqliteParameter("@Longitude", clientItem.Longitude));

                        cmd.Parameters.Add(new SqliteParameter("@Data_inserimento", clientItem.Data_inserimento));
                        cmd.Parameters.Add(new SqliteParameter("@Tipo_contratto", clientItem.Tipo_contratto));
                        cmd.Parameters.Add(new SqliteParameter("@Codice_cliente", clientItem.Codice_cliente));
                        cmd.Parameters.Add(new SqliteParameter("@Email_shop", clientItem.Email_shop));
                        cmd.Parameters.Add(new SqliteParameter("@Punti_disponibili", clientItem.Punti_disponibili.ToString()));
                        cmd.Parameters.Add(new SqliteParameter("@Stato_rid", clientItem.Stato_rid));
                        cmd.Parameters.Add(new SqliteParameter("@Numero_referenze", clientItem.Numero_referenze));
                        cmd.Parameters.Add(new SqliteParameter("@Customer_id", clientItem.Customer_id));
                        cmd.Parameters.Add(new SqliteParameter("@Flag_borsellino", clientItem.Flag_borsellino));
                        cmd.Parameters.Add(new SqliteParameter("@Flag_preso", clientItem.Flag_preso));
                        cmd.Parameters.Add(new SqliteParameter("@Date_convergence", clientItem.Date_convergence));
                        cmd.Parameters.Add(new SqliteParameter("@Atf_id_customer", clientItem.Atf_id_customer));

                        var result = cmd.ExecuteNonQuery();
                    }
                }

            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void UpdateClientItem(ClientInfoItem clientItem)
        {
            Object obj = new Object();
            try
            {

                lock (obj)
                {
                    string connectionString = "URI=file:" + databasePath + ",version=3";
                    if(ConnSingleton.conn.State!= ConnectionState.Open)
                    {
                        ConnSingleton.conn.Open();
                    }

                    string sSQLQuery = "UPDATE " +
                        Consts.ALL_CLIENTS_TABLE_NAME + " " +
                        "SET Regione_sociale=@Regione_sociale, Partita_iva=@Partita_iva, Phone=@Phone,Location=@Location, Mobile_phone=@Mobile_phone,Province=@Province, Mail=@Mail, Address=@Address, Fax=@Fax, Referente=@Referente, Status=@Status, Date_clienti=@Date_clienti, Latitude=@Latitude, Longitude=@Longitude, Data_inserimento=@Data_inserimento, Tipo_contratto=@Tipo_contratto, Codice_cliente=@Codice_cliente, Email_shop=@Email_shop, Punti_disponibili=@Punti_disponibili, Stato_rid=@Stato_rid, Numero_referenze=@Numero_referenze, Customer_id=@Customer_id, Flag_borsellino=@Flag_borsellino, Flag_preso=@Flag_preso, Date_convergence=@Date_convergence, Atf_id_customer=@Atf_id_customer " +
                        "where Id = @Id";

                    using (var cmd = ConnSingleton.conn.CreateCommand())
                    {
                        cmd.CommandTimeout = 0;
                        cmd.CommandText = sSQLQuery;
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.Add(new SqliteParameter("@Id", clientItem.Partita_iva));
                        cmd.Parameters.Add(new SqliteParameter("@Regione_sociale", clientItem.Regione_sociale));
                        cmd.Parameters.Add(new SqliteParameter("@Partita_iva", clientItem.Partita_iva));
                        cmd.Parameters.Add(new SqliteParameter("@Phone", clientItem.Phone));
                        cmd.Parameters.Add(new SqliteParameter("@Location", clientItem.Location));
                        cmd.Parameters.Add(new SqliteParameter("@Mobile_phone", clientItem.Mobile_phone));
                        cmd.Parameters.Add(new SqliteParameter("@Province", clientItem.Province));
                        cmd.Parameters.Add(new SqliteParameter("@Mail", clientItem.Mail));
                        cmd.Parameters.Add(new SqliteParameter("@Address", clientItem.Address));
                        cmd.Parameters.Add(new SqliteParameter("@Fax", clientItem.Fax));
                        cmd.Parameters.Add(new SqliteParameter("@Referente", clientItem.Referente));
                        cmd.Parameters.Add(new SqliteParameter("@Status", clientItem.Status));
                        cmd.Parameters.Add(new SqliteParameter("@Date_clienti", clientItem.Date_clienti));
                        cmd.Parameters.Add(new SqliteParameter("@Latitude", clientItem.Latitude));
                        cmd.Parameters.Add(new SqliteParameter("@Longitude", clientItem.Longitude));
                        cmd.Parameters.Add(new SqliteParameter("@Data_inserimento", clientItem.Data_inserimento));
                        cmd.Parameters.Add(new SqliteParameter("@Tipo_contratto", clientItem.Tipo_contratto));
                        cmd.Parameters.Add(new SqliteParameter("@Codice_cliente", clientItem.Codice_cliente));
                        cmd.Parameters.Add(new SqliteParameter("@Email_shop", clientItem.Email_shop));
                        cmd.Parameters.Add(new SqliteParameter("@Punti_disponibili", clientItem.Punti_disponibili));
                        cmd.Parameters.Add(new SqliteParameter("@Stato_rid", clientItem.Stato_rid));
                        cmd.Parameters.Add(new SqliteParameter("@Numero_referenze", clientItem.Numero_referenze));
                        cmd.Parameters.Add(new SqliteParameter("@Customer_id", clientItem.Customer_id));
                        cmd.Parameters.Add(new SqliteParameter("@Flag_borsellino", clientItem.Flag_borsellino));
                        cmd.Parameters.Add(new SqliteParameter("@Flag_preso", clientItem.Flag_preso));
                        cmd.Parameters.Add(new SqliteParameter("@Date_convergence", clientItem.Date_convergence));
                        cmd.Parameters.Add(new SqliteParameter("@Atf_id_customer", clientItem.Atf_id_customer));

                        var result = cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public List<ClientInfoItem> getAllClients()
        {
            List<ClientInfoItem> clientsList = new List<ClientInfoItem>();

            Mono.Data.Sqlite.SqliteDataReader reader = null;
            try
            {
                if (File.Exists(databasePath))
                {

                    string connectionString = "URI=file:" + databasePath + ",version=3";
                    if(ConnSingleton.conn.State!= ConnectionState.Open)
                    {
                        ConnSingleton.conn.Open();
                    }
                    string sSQLQuery = "SELECT * FROM " +
                        Consts.ALL_CLIENTS_TABLE_NAME;

                    using (var cmd = ConnSingleton.conn.CreateCommand())
                    {
                        cmd.CommandTimeout = 0;
                        cmd.CommandText = sSQLQuery;
                        cmd.CommandType = CommandType.Text;

                        reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            string id = "";
                            string regione_sociale = "";
                            string partita_iva = "";
                            string phone = "";
                            string location = "";
                            string mobile_phone = "";
                            string province = "";
                            string mail = "";
                            string address = "";
                            string fax = "";
                            string referente = "";
                            string status="";
                            DateTime date_clienti=DateTime.Now.Date;
                            double latitude=0;
                            double longitude=0;
                            DateTime data_inserimento=DateTime.Now.Date;
                            string tipo_contratto="";
                            string codice_cliente="";
                            string email_shop="";
                            string punti_disponibili="";
                            string stato_rid="";
                            string numero_referenze="";
                            string customer_id="";
                            string flag_borsellino="";
                            string flag_preso="";
                            DateTime date_convergence=DateTime.Now.Date;
                            string atf_id_customer="";

                            try
                            {
                                id = reader.GetString(reader.GetOrdinal("Id"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                regione_sociale = reader.GetString(reader.GetOrdinal("Regione_sociale"));
                            }
                            catch
                            {
                            }

                            try
                            {
                                partita_iva = reader.GetString(reader.GetOrdinal("Partita_iva"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                phone = reader.GetString(reader.GetOrdinal("Phone"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                location = reader.GetString(reader.GetOrdinal("Location"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                mobile_phone = reader.GetString(reader.GetOrdinal("Mobile_phone"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                province = reader.GetString(reader.GetOrdinal("Province"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                mail = reader.GetString(reader.GetOrdinal("Mail"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                address = reader.GetString(reader.GetOrdinal("Address"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                fax = reader.GetString(reader.GetOrdinal("Fax"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                referente = reader.GetString(reader.GetOrdinal("Referente"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                status = reader.GetString(reader.GetOrdinal("Status"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                date_clienti = reader.GetDateTime(reader.GetOrdinal("Date_clienti"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                latitude = reader.GetDouble(reader.GetOrdinal("Latitude"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                longitude = reader.GetDouble(reader.GetOrdinal("Longitude"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                punti_disponibili = reader.GetString(reader.GetOrdinal("Punti_disponibili"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                data_inserimento = reader.GetDateTime(reader.GetOrdinal("Data_inserimento"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                tipo_contratto = reader.GetString(reader.GetOrdinal("Tipo_contratto"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                codice_cliente = reader.GetString(reader.GetOrdinal("Codice_cliente"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                email_shop = reader.GetString(reader.GetOrdinal("Email_shop"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                stato_rid = reader.GetString(reader.GetOrdinal("Stato_rid"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                numero_referenze = reader.GetString(reader.GetOrdinal("Numero_referenze"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                customer_id = reader.GetString(reader.GetOrdinal("Customer_id"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                flag_borsellino = reader.GetString(reader.GetOrdinal("Flag_borsellino"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                flag_preso = reader.GetString(reader.GetOrdinal("Flag_preso"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                date_convergence = reader.GetDateTime(reader.GetOrdinal("Date_convergence"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                atf_id_customer = reader.GetString(reader.GetOrdinal("Atf_id_customer"));
                            }
                            catch
                            {
                            }


                            ClientInfoItem singleItem = new ClientInfoItem();
                            singleItem.Partita_iva = id;
                            singleItem.Regione_sociale = regione_sociale;
                            singleItem.Partita_iva = partita_iva;
                            singleItem.Phone = phone;
                            singleItem.Location = location;
                            singleItem.Mobile_phone = mobile_phone;
                            singleItem.Province = province;
                            singleItem.Mail = mail;
                            singleItem.Address = address;
                            singleItem.Fax = fax;
                            singleItem.Referente = referente;
                            singleItem.Status = status;

//                            DateTime dateClienti;
//                            DateTime.TryParseExact(date_clienti, DATE_TIME_FORMAT, CultureInfo.InvariantCulture, DateTimeStyles.None, out dateClienti);
//                            singleItem.Date_clienti =  dateClienti;
                            singleItem.Date_clienti = date_clienti;

                            // TODO - test
//                            double latDouble;
//                            double.TryParse(latitude, NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingSign, CultureInfo.InvariantCulture, out latDouble);
//                            double longDouble;
//                            double.TryParse(longitude, NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingSign, CultureInfo.InvariantCulture, out longDouble);

                            singleItem.Latitude = latitude;
                            singleItem.Longitude = longitude;

                            // TODO - test
//                            DateTime dataInserimento;
//                            DateTime.TryParseExact(data_inserimento, DATE_TIME_FORMAT, CultureInfo.InvariantCulture, DateTimeStyles.None, out dataInserimento);
//                            singleItem.Data_inserimento = dataInserimento;
                            singleItem.Data_inserimento = data_inserimento;

                            singleItem.Tipo_contratto = tipo_contratto;
                            singleItem.Codice_cliente = codice_cliente;
                            singleItem.Email_shop = email_shop;
                            singleItem.Punti_disponibili = Int32.Parse(punti_disponibili);
                            singleItem.Stato_rid = stato_rid;
                            singleItem.Numero_referenze = (numero_referenze);
                            singleItem.Customer_id=customer_id;
                            singleItem.Flag_borsellino=flag_borsellino;
                            singleItem.Flag_preso=flag_preso;
                            singleItem.Date_convergence=date_convergence;
                            singleItem.Atf_id_customer=atf_id_customer;

                            clientsList.Add(singleItem);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader = null;
                }
            }

            return clientsList;
        }

        public bool CheckIfExiststClientInDB(string id)
        {
            int count = 0;
            Object obj = new Object();
            try
            {
                lock (obj)
                {
                    if (File.Exists(databasePath))
                    {
                        if(ConnSingleton.conn.State!= ConnectionState.Open)
                        {
                            ConnSingleton.conn.Open();
                        }
                        string connectionString = "URI=file:" + databasePath + ",version=3";
                        using (Mono.Data.Sqlite.SqliteCommand dbcmd = ConnSingleton.conn.CreateCommand())
                        {
                            string sql =
                                "SELECT count(*) " +
                                "FROM AllClientsTable " +
                                "where Id = " + "\"" + id + "\"";

                            dbcmd.CommandText = sql;
                            count = Convert.ToInt32(dbcmd.ExecuteScalar());

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }

            if (count > 0)
                return true;
            else
                return false;
        }

        public void InsertProfileInfoToTable(ProfileInfoItem profileItem)
        {
            Object obj = new Object();
            try
            {

                lock (obj)
                {
                    string connectionString = "URI=file:" + databasePath + ",version=3";

                    if(ConnSingleton.conn.State!= ConnectionState.Open)
                    {
                        ConnSingleton.conn.Open();
                    }
                    string sSQLQuery = "INSERT OR IGNORE INTO " +
                        Consts.PROFILE_INFO_TABLE_NAME + " " +
                        "(Id_user, Id_agency, Id_agent, Surname,Name, Address,Number, Mobile, Mail, Is_in_top,Vat, Avatar) " +
                        "VALUES (@Id_user, @Id_agency, @Id_agent, @Surname,@Name, @Address,@Number, @Mobile, @Mail, @Is_in_top,@Vat, @Avatar);";

                    using (var cmd = ConnSingleton.conn.CreateCommand())
                    {
                        cmd.CommandTimeout = 0;
                        cmd.CommandText = sSQLQuery;
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.Add(new SqliteParameter("@Id_user", profileItem.Id_user));
                        cmd.Parameters.Add(new SqliteParameter("@Id_agency", profileItem.Id_agency));
                        cmd.Parameters.Add(new SqliteParameter("@Id_agent", profileItem.Id_agent));
                        cmd.Parameters.Add(new SqliteParameter("@Surname", profileItem.Surname));
                        cmd.Parameters.Add(new SqliteParameter("@Name", profileItem.Name));
                        cmd.Parameters.Add(new SqliteParameter("@Address", profileItem.Address));
                        cmd.Parameters.Add(new SqliteParameter("@Number", profileItem.Number));
                        cmd.Parameters.Add(new SqliteParameter("@Mobile", profileItem.Mobile));
                        cmd.Parameters.Add(new SqliteParameter("@Mail", profileItem.Mail));
                        cmd.Parameters.Add(new SqliteParameter("@Is_in_top", profileItem.Is_in_top.ToString()));
                        cmd.Parameters.Add(new SqliteParameter("@Vat", profileItem.Partita_iva));
                        cmd.Parameters.Add(new SqliteParameter("@Avatar", profileItem.Avatar));

                        var result = cmd.ExecuteNonQuery();
                    }
                }

            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void UpdateProfileInfo(ProfileInfoItem profileItem)
        {
            Object obj = new Object();
            try
            {

                lock (obj)
                {
                    string connectionString = "URI=file:" + databasePath + ",version=3";

                    if(ConnSingleton.conn.State!= ConnectionState.Open)
                    {
                        ConnSingleton.conn.Open();
                    }
                    string sSQLQuery = "UPDATE " +
                        Consts.PROFILE_INFO_TABLE_NAME + " " +
                        "SET Id_user=@Id_user, Id_agency=@Id_agency, Id_agent=@Id_agent,Surname=@Surname, Name=@Name,Address=@Address, Number=@Number, Mobile=@Mobile, Mail=@Mail, Is_in_top=@Is_in_top, Vat=@Vat, Avatar=@Avatar " +
                        "where Id_user = @Id_user";

                    using (var cmd = ConnSingleton.conn.CreateCommand())
                    {
                        cmd.CommandTimeout = 0;
                        cmd.CommandText = sSQLQuery;
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.Add(new SqliteParameter("@Id_user", profileItem.Id_user));
                        cmd.Parameters.Add(new SqliteParameter("@Id_agency", profileItem.Id_agency));
                        cmd.Parameters.Add(new SqliteParameter("@Id_agent", profileItem.Id_agent));
                        cmd.Parameters.Add(new SqliteParameter("@Surname", profileItem.Surname));
                        cmd.Parameters.Add(new SqliteParameter("@Name", profileItem.Name));
                        cmd.Parameters.Add(new SqliteParameter("@Address", profileItem.Address));
                        cmd.Parameters.Add(new SqliteParameter("@Number", profileItem.Number));
                        cmd.Parameters.Add(new SqliteParameter("@Mobile", profileItem.Mobile));
                        cmd.Parameters.Add(new SqliteParameter("@Mail", profileItem.Mail));
                        cmd.Parameters.Add(new SqliteParameter("@Is_in_top", profileItem.Is_in_top.ToString()));
                        cmd.Parameters.Add(new SqliteParameter("@Vat", profileItem.Partita_iva));
                        cmd.Parameters.Add(new SqliteParameter("@Avatar", profileItem.Avatar));

                        var result = cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public ProfileInfoItem getProfileInfo()
        {
            Mono.Data.Sqlite.SqliteDataReader reader = null;
            ProfileInfoItem profinleInfo = new ProfileInfoItem();
            try
            {
                if (File.Exists(databasePath))
                {

                    string connectionString = "URI=file:" + databasePath + ",version=3";

                    if(ConnSingleton.conn.State!= ConnectionState.Open)
                    {
                        ConnSingleton.conn.Open();
                    }

                    string sSQLQuery = "SELECT * FROM " +
                        Consts.PROFILE_INFO_TABLE_NAME;

                    using (var cmd = ConnSingleton.conn.CreateCommand())
                    {
                        cmd.CommandTimeout = 0;
                        cmd.CommandText = sSQLQuery;
                        cmd.CommandType = CommandType.Text;

                        reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            string id_user = "";
                            string id_agency = "";
                            string id_agent = "";
                            string surname = "";
                            string name = "";
                            string address = "";
                            string number = "";
                            string mobile = "";
                            string mail = "";
                            string is_in_top = "";
                            string vat="";
                            string avatar="";

                            try
                            {
                                id_user = reader.GetString(reader.GetOrdinal("Id_user"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                id_agency = reader.GetString(reader.GetOrdinal("Id_agency"));
                            }
                            catch
                            {
                            }

                            try
                            {
                                id_agent = reader.GetString(reader.GetOrdinal("Id_agent"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                surname = reader.GetString(reader.GetOrdinal("Surname"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                name = reader.GetString(reader.GetOrdinal("Name"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                address = reader.GetString(reader.GetOrdinal("Address"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                number = reader.GetString(reader.GetOrdinal("Number"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                mobile = reader.GetString(reader.GetOrdinal("Mobile"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                mail = reader.GetString(reader.GetOrdinal("Mail"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                is_in_top = reader.GetString(reader.GetOrdinal("Is_in_top"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                vat = reader.GetString(reader.GetOrdinal("Vat"));
                            }
                            catch
                            {
                            }

                            try
                            {
                                avatar = reader.GetString(reader.GetOrdinal("Avatar"));
                            }
                            catch
                            {
                            }

                            profinleInfo.Id_user = id_user;
                            profinleInfo.Id_agency = id_agency;
                            profinleInfo.Id_agent = id_agent;
                            profinleInfo.Surname = surname;
                            profinleInfo.Name = name;
                            profinleInfo.Number = number;
                            profinleInfo.Mobile = mobile;
                            profinleInfo.Mail = mail;
                            profinleInfo.Is_in_top = is_in_top;
                            profinleInfo.Partita_iva=vat;
                            profinleInfo.Avatar=avatar;
                            profinleInfo.Address=address;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader = null;
                }
            }

            return profinleInfo;
        }

        public bool CheckIfExistsProfile()
        {
            int count = 0;
            Object obj = new Object();
            try
            {
                lock (obj)
                {
                    if (File.Exists(databasePath))
                    {

                        if(ConnSingleton.conn.State!= ConnectionState.Open)
                        {
                            ConnSingleton.conn.Open();
                        }

                        string connectionString = "URI=file:" + databasePath + ",version=3";
                        using (Mono.Data.Sqlite.SqliteCommand dbcmd = ConnSingleton.conn.CreateCommand())
                        {
                            string sql =
                                "SELECT count(*) " +
                                "FROM ProfileInfoTable";
                            //                                "where Id = " + "\"" + callID + "\"";

                            dbcmd.CommandText = sql;
                            count = Convert.ToInt32(dbcmd.ExecuteScalar());

                        }
                        //   dbcon.Close();
                        // }

                    }
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }

            if (count > 0)
                return true;
            else
                return false;
        }


        public void InsertAppuntamentiToTable(AppuntamentiItem appuntamentiItem)
        {
            Object obj = new Object();
            try
            {

                lock (obj)
                {
                    string connectionString = "URI=file:" + databasePath + ",version=3";

                    if(ConnSingleton.conn.State!= ConnectionState.Open)
                    {
                        ConnSingleton.conn.Open();
                    }
                    string emails="";
                    string phones="";

                    if(appuntamentiItem.EmailList.Count>1)
                    {
                        for (int i = 0; i < appuntamentiItem.EmailList.Count; i++) 
                        {
                            if(i<appuntamentiItem.EmailList.Count-1)
                            {
                                emails+=appuntamentiItem.EmailList[i]+"<<>>";
                            }
                            else
                            {
                                emails+=appuntamentiItem.EmailList[i];
                            }
                        }
                            
                    }
                    else if(appuntamentiItem.EmailList.Count>0)
                    {
                        emails=appuntamentiItem.EmailList[0];
                    }

                    if(appuntamentiItem.PhoneList.Count>1)
                    {
                        for (int i = 0; i < appuntamentiItem.PhoneList.Count; i++) 
                        {
                            if(i<appuntamentiItem.PhoneList.Count-1)
                            {
                                phones+=appuntamentiItem.PhoneList[i]+"<<>>";
                            }
                            else
                            {
                                phones+=appuntamentiItem.PhoneList[i];
                            }
                        }
                    }
                    else if(appuntamentiItem.PhoneList.Count>0)
                    {
                        phones=appuntamentiItem.PhoneList[0];
                    }

                    /*
    `PhoneList`   VARCHAR(100),
    `EmailList`   VARCHAR(100),*/
                    string sSQLQuery = "INSERT OR IGNORE INTO " +
                        Consts.APPUNTAMENTI_TABLE_NAME + " " +
                        "(Id, AppuntamentiDateHour,Original_AppuntamentiDateHour, RegSociale, RepresentatnteLegale,Mail, Phone,Status, Tipclient, Note, Latitude, Longitude, City,Id_Chiamata,Date_chiamata,Note_agent,Note_chiamata,Note_segretaria,Motivo,Category,Indirizzo,Civico,Cap,Comune,Provincia,Sigla,Contact_name,Contact_phone,Contact_mail,Id_customer,Source,TipAppuntamento,ContattiInfo,PhoneList,EmailList,Id_Appto_Memo,Evento_Pers) " +
                        "VALUES (@Id, @AppuntamentiDateHour,@Original_AppuntamentiDateHour, @RegSociale, @RepresentatnteLegale,@Mail, @Phone,@Status, @Tipclient, @Note, @Latitude, @Longitude, @City,@Id_Chiamata,@Date_chiamata,@Note_agent,@Note_chiamata,@Note_segretaria,@Motivo,@Category,@Indirizzo,@Civico,@Cap,@Comune,@Provincia,@Sigla,@Contact_name,@Contact_phone,@Contact_mail,@Id_customer,@Source, @TipAppuntamento,@ContattiInfo, @PhoneList, @EmailList,@Id_Appto_Memo,@Evento_Pers);";

                    using (var cmd = ConnSingleton.conn.CreateCommand())
                    {
                        cmd.CommandTimeout = 0;
                        cmd.CommandText = sSQLQuery;
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.Add(new SqliteParameter("@Id", appuntamentiItem.Id));
                        cmd.Parameters.Add(new SqliteParameter("@AppuntamentiDateHour", appuntamentiItem.AppuntamentiDateHour));
                        cmd.Parameters.Add(new SqliteParameter("@Original_AppuntamentiDateHour", appuntamentiItem.original_appuntamentiDateHour));
                        cmd.Parameters.Add(new SqliteParameter("@RegSociale", appuntamentiItem.RegSociale));
                        cmd.Parameters.Add(new SqliteParameter("@RepresentatnteLegale", appuntamentiItem.RepresentatnteLegale));
                        cmd.Parameters.Add(new SqliteParameter("@Mail", appuntamentiItem.Mail));
                        cmd.Parameters.Add(new SqliteParameter("@Phone", appuntamentiItem.Phone));
                        cmd.Parameters.Add(new SqliteParameter("@Status", appuntamentiItem.Status));
                        cmd.Parameters.Add(new SqliteParameter("@Tipclient", appuntamentiItem.TipClient));
                        cmd.Parameters.Add(new SqliteParameter("@Note", appuntamentiItem.Note));
                        cmd.Parameters.Add(new SqliteParameter("@Latitude", appuntamentiItem.Latitude));
                        cmd.Parameters.Add(new SqliteParameter("@Longitude", appuntamentiItem.Longitude));
                        cmd.Parameters.Add(new SqliteParameter("@City", appuntamentiItem.City));

                        //Contact_name,Contact_phone,Contact_mail,Id_customer,Source
                        cmd.Parameters.Add(new SqliteParameter("@Id_Chiamata", appuntamentiItem.Id_Chiamata));
                        cmd.Parameters.Add(new SqliteParameter("@Date_chiamata", appuntamentiItem.Date_chiamata));
                        cmd.Parameters.Add(new SqliteParameter("@Note_agent", appuntamentiItem.Note_agent));
                        cmd.Parameters.Add(new SqliteParameter("@Note_chiamata", appuntamentiItem.Note_chiamata));
                        cmd.Parameters.Add(new SqliteParameter("@Note_segretaria", appuntamentiItem.Note_segretaria));
                        cmd.Parameters.Add(new SqliteParameter("@Motivo", appuntamentiItem.Motivo));
                        cmd.Parameters.Add(new SqliteParameter("@Category", appuntamentiItem.Category));
                        cmd.Parameters.Add(new SqliteParameter("@Indirizzo", appuntamentiItem.Indirizzo));
                        cmd.Parameters.Add(new SqliteParameter("@Civico", appuntamentiItem.Civico));
                        cmd.Parameters.Add(new SqliteParameter("@Cap", appuntamentiItem.Cap));
                        cmd.Parameters.Add(new SqliteParameter("@Comune", appuntamentiItem.Comune));
                        cmd.Parameters.Add(new SqliteParameter("@Provincia", appuntamentiItem.Provincia));
                        cmd.Parameters.Add(new SqliteParameter("@Sigla", appuntamentiItem.Sigla));
                        cmd.Parameters.Add(new SqliteParameter("@Contact_name", appuntamentiItem.Contact_name));
                        cmd.Parameters.Add(new SqliteParameter("@Contact_phone", appuntamentiItem.Contact_phone));
                        cmd.Parameters.Add(new SqliteParameter("@Contact_mail", appuntamentiItem.Contact_mail));
                        cmd.Parameters.Add(new SqliteParameter("@Id_customer", appuntamentiItem.Id_customer));
                        cmd.Parameters.Add(new SqliteParameter("@Source", appuntamentiItem.Source));
                        cmd.Parameters.Add(new SqliteParameter("@TipAppuntamento", appuntamentiItem.TipAppuntamenti));
                        cmd.Parameters.Add(new SqliteParameter("@ContattiInfo", appuntamentiItem.ContattiInfo));
                        cmd.Parameters.Add(new SqliteParameter("@PhoneList", phones));
                        cmd.Parameters.Add(new SqliteParameter("@EmailList", emails));
                        cmd.Parameters.Add(new SqliteParameter("@Id_Appto_Memo", appuntamentiItem.Id_appto_memo));
                        cmd.Parameters.Add(new SqliteParameter("@Evento_Pers", appuntamentiItem.Evento_pers));

                        var result = cmd.ExecuteNonQuery();
                    }
                }

            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void UpdateAppuntamentiInfo(AppuntamentiItem apputnItem)
        {
            Object obj = new Object();
            try
            {

                lock (obj)
                {
                    string connectionString = "URI=file:" + databasePath + ",version=3";

                    if(ConnSingleton.conn.State!= ConnectionState.Open)
                    {
                        ConnSingleton.conn.Open();
                    }

                    string emails="";
                    string phones="";

                    if(apputnItem.EmailList.Count>1)
                    {
                        for (int i = 0; i < apputnItem.EmailList.Count; i++) 
                        {
                            if(i<apputnItem.EmailList.Count-1)
                            {
                                emails+=apputnItem.EmailList[i]+"<<>>";
                            }
                            else
                            {
                                emails+=apputnItem.EmailList[i];
                            }
                        }

                    }
                    else if(apputnItem.EmailList.Count>0)
                    {
                        emails=apputnItem.EmailList[0];
                    }

                    if(apputnItem.PhoneList.Count>1)
                    {
                        for (int i = 0; i < apputnItem.PhoneList.Count; i++) 
                        {
                            if(i<apputnItem.PhoneList.Count-1)
                            {
                                phones+=apputnItem.PhoneList[i]+"<<>>";
                            }
                            else
                            {
                                phones+=apputnItem.PhoneList[i];
                            }
                        }
                    }
                    else if(apputnItem.PhoneList.Count>0)
                    {
                        phones=apputnItem.PhoneList[0];
                    }

                    string sSQLQuery = "UPDATE " +
                        Consts.APPUNTAMENTI_TABLE_NAME + " " +
                        "SET AppuntamentiDateHour=@AppuntamentiDateHour,Original_AppuntamentiDateHour=@Original_AppuntamentiDateHour, RegSociale=@RegSociale,RepresentatnteLegale=@RepresentatnteLegale, Mail=@Mail,Phone=@Phone, Status=@Status, Tipclient=@Tipclient, Note=@Note, Latitude=@Latitude, Longitude=@Longitude, City=@City ,Id_Chiamata=@Id_Chiamata,Date_chiamata=@Date_chiamata,Note_agent=@Note_agent,Note_chiamata=@Note_chiamata,Note_segretaria=@Note_segretaria,Motivo=@Motivo,Category=@Category,Indirizzo=@Indirizzo,Civico=@Civico,Cap=@Cap,Comune=@Comune,Provincia=@Provincia,Sigla=@Sigla,Contact_name=@Contact_name,Contact_phone=@Contact_phone,Contact_mail=@Contact_mail,Id_customer=@Id_customer,Source=@Source,TipAppuntamento=@TipAppuntamento,ContattiInfo=@ContattiInfo, PhoneList=@PhoneList, EmailList=@EmailList, Id_Appto_Memo=@Id_Appto_Memo, Evento_Pers=@Evento_Pers " +
                        "where Id = @Id";

                    using (var cmd = ConnSingleton.conn.CreateCommand())
                    {
                        cmd.CommandTimeout = 0;
                        cmd.CommandText = sSQLQuery;
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.Add(new SqliteParameter("@Id", apputnItem.Id));
                        cmd.Parameters.Add(new SqliteParameter("@AppuntamentiDateHour", apputnItem.AppuntamentiDateHour));
                        cmd.Parameters.Add(new SqliteParameter("@Original_AppuntamentiDateHour", apputnItem.original_appuntamentiDateHour));
                        cmd.Parameters.Add(new SqliteParameter("@RegSociale", apputnItem.RegSociale));
                        cmd.Parameters.Add(new SqliteParameter("@RepresentatnteLegale", apputnItem.RepresentatnteLegale));
                        cmd.Parameters.Add(new SqliteParameter("@Mail", apputnItem.Mail));
                        cmd.Parameters.Add(new SqliteParameter("@Phone", apputnItem.Phone));
                        cmd.Parameters.Add(new SqliteParameter("@Status", apputnItem.Status));
                        cmd.Parameters.Add(new SqliteParameter("@Tipclient", apputnItem.TipClient));
                        cmd.Parameters.Add(new SqliteParameter("@Note", apputnItem.Note));
                        cmd.Parameters.Add(new SqliteParameter("@Latitude", apputnItem.Latitude));
                        cmd.Parameters.Add(new SqliteParameter("@Longitude", apputnItem.Longitude));
                        cmd.Parameters.Add(new SqliteParameter("@City", apputnItem.City));

                        cmd.Parameters.Add(new SqliteParameter("@Id_Chiamata", apputnItem.Id_Chiamata));
                        cmd.Parameters.Add(new SqliteParameter("@Date_chiamata", apputnItem.Date_chiamata));
                        cmd.Parameters.Add(new SqliteParameter("@Note_agent", apputnItem.Note_agent));
                        cmd.Parameters.Add(new SqliteParameter("@Note_chiamata", apputnItem.Note_chiamata));
                        cmd.Parameters.Add(new SqliteParameter("@Note_segretaria", apputnItem.Note_segretaria));
                        cmd.Parameters.Add(new SqliteParameter("@Motivo", apputnItem.Motivo));
                        cmd.Parameters.Add(new SqliteParameter("@Category", apputnItem.Category));
                        cmd.Parameters.Add(new SqliteParameter("@Indirizzo", apputnItem.Indirizzo));
                        cmd.Parameters.Add(new SqliteParameter("@Civico", apputnItem.Civico));
                        cmd.Parameters.Add(new SqliteParameter("@Cap", apputnItem.Cap));
                        cmd.Parameters.Add(new SqliteParameter("@Comune", apputnItem.Comune));
                        cmd.Parameters.Add(new SqliteParameter("@Provincia", apputnItem.Provincia));
                        cmd.Parameters.Add(new SqliteParameter("@Sigla", apputnItem.Sigla));
                        cmd.Parameters.Add(new SqliteParameter("@Contact_name", apputnItem.Contact_name));
                        cmd.Parameters.Add(new SqliteParameter("@Contact_phone", apputnItem.Contact_phone));
                        cmd.Parameters.Add(new SqliteParameter("@Contact_mail", apputnItem.Contact_mail));
                        cmd.Parameters.Add(new SqliteParameter("@Id_customer", apputnItem.Id_customer));
                        cmd.Parameters.Add(new SqliteParameter("@Source", apputnItem.Source));
                        cmd.Parameters.Add(new SqliteParameter("@TipAppuntamento", apputnItem.TipAppuntamenti));
                        cmd.Parameters.Add(new SqliteParameter("@ContattiInfo", apputnItem.ContattiInfo));
                        cmd.Parameters.Add(new SqliteParameter("@PhoneList", phones));
                        cmd.Parameters.Add(new SqliteParameter("@EmailList", emails));
                        cmd.Parameters.Add(new SqliteParameter("@Id_Appto_Memo", apputnItem.Id_appto_memo));
                        cmd.Parameters.Add(new SqliteParameter("@Evento_Pers", apputnItem.Evento_pers));

                        var result = cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public List<AppuntamentiItem> getAllAppuntamenti(string date_from,string date_to,string regioneSociale, string localita)
        {
            Mono.Data.Sqlite.SqliteDataReader reader = null;
            string sSQLQuery = string.Empty;
            List<AppuntamentiItem> appList = new List<AppuntamentiItem>();
            try
            {
                if (File.Exists(databasePath))
                {

                    string connectionString = "URI=file:" + databasePath + ",version=3";

                    if(ConnSingleton.conn.State!= ConnectionState.Open)
                    {
                        ConnSingleton.conn.Open();
                    }

                    if(date_from==date_to)
                    {
                        sSQLQuery = "SELECT * FROM " +
                            Consts.APPUNTAMENTI_TABLE_NAME +
                            " WHERE AppuntamentiDateHour LIKE '" + date_from + "%'";

                    }
                    else
                    {
                        sSQLQuery = "SELECT * FROM " +
                            Consts.APPUNTAMENTI_TABLE_NAME +
                            " WHERE RegSociale LIKE '%" + regioneSociale + "%'"+
                            " AND City LIKE '%" + localita + "%'";
                    }

                    //                    sSQLQuery = "SELECT * FROM " +
                    //                        Consts.APPUNTAMENTI_TABLE_NAME ;

                    using (var cmd = ConnSingleton.conn.CreateCommand())
                    {
                        cmd.CommandTimeout = 0;
                        cmd.CommandText = sSQLQuery;
                        cmd.CommandType = CommandType.Text;

                        reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            string id = "";
                            DateTime appuntamentiDateHour = DateTime.Now.Date;
                            DateTime original_AppuntamentiDateHour = DateTime.Now.Date;
                            string regSociale = "";
                            string repLegale = "";
                            string mail = "";
                            string phone = "";
                            string status = "";
                            string tipClient = "";
                            string note = "";
                            double latitude = 0;
                            double longitude = 0;
                            string city="";
                            string id_chiamata="";
                            DateTime date_chiamata=DateTime.Now.Date;
                            string note_agent="";
                            string note_chiamata="";
                            string note_segretaria="";
                            string motivo="";
                            string category="";
                            string indirizzo="";
                            string civico="";
                            string cap="";
                            string comune="";
                            string provincia="";
                            string sigla="";
                            string contact_name="";
                            string contact_phone="";
                            string contact_mail="";
                            string id_customer="";
                            string source="";
                            string tipAppuntamento="";
                            string contattiInfo="";
                            string emails="";
                            string phones="";
                            string id_appto_memo="";
                            string evento_pers="";

                            try
                            {
                                id = reader.GetString(reader.GetOrdinal("Id"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                appuntamentiDateHour = reader.GetDateTime(reader.GetOrdinal("AppuntamentiDateHour"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                original_AppuntamentiDateHour = reader.GetDateTime(reader.GetOrdinal("Original_AppuntamentiDateHour"));
                            }
                            catch
                            {
                            }

                            try
                            {
                                regSociale = reader.GetString(reader.GetOrdinal("RegSociale"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                repLegale = reader.GetString(reader.GetOrdinal("RepresentatnteLegale"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                mail = reader.GetString(reader.GetOrdinal("Mail"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                phone = reader.GetString(reader.GetOrdinal("Phone"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                status = reader.GetString(reader.GetOrdinal("Status"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                tipClient = reader.GetString(reader.GetOrdinal("Tipclient"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                note = reader.GetString(reader.GetOrdinal("Note"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                latitude = reader.GetDouble(reader.GetOrdinal("Latitude"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                longitude = reader.GetDouble(reader.GetOrdinal("Longitude"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                city = reader.GetString(reader.GetOrdinal("City"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                id_chiamata = reader.GetString(reader.GetOrdinal("Id_Chiamata"));
                            }
                            catch
                            {
                            }

                            try
                            {
                                date_chiamata = reader.GetDateTime(reader.GetOrdinal("Date_chiamata"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                note_agent = reader.GetString(reader.GetOrdinal("Note_agent"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                note_chiamata = reader.GetString(reader.GetOrdinal("Note_chiamata"));
                            }
                            catch
                            {
                            }

                            try
                            {
                                note_segretaria = reader.GetString(reader.GetOrdinal("Note_segretaria"));
                            }
                            catch
                            {
                            }

                            try
                            {
                                motivo = reader.GetString(reader.GetOrdinal("Motivo"));
                            }
                            catch
                            {
                            }

                            try
                            {
                                category = reader.GetString(reader.GetOrdinal("Category"));
                            }
                            catch
                            {
                            }

                            try
                            {
                                indirizzo = reader.GetString(reader.GetOrdinal("Indirizzo"));
                            }
                            catch
                            {
                            }

                            try
                            {
                                civico = reader.GetString(reader.GetOrdinal("Civico"));
                            }
                            catch
                            {
                            }

                            try
                            {
                                cap = reader.GetString(reader.GetOrdinal("Cap"));
                            }
                            catch
                            {
                            }

                            try
                            {
                                comune = reader.GetString(reader.GetOrdinal("Comune"));
                            }
                            catch
                            {
                            }

                            try
                            {
                                provincia = reader.GetString(reader.GetOrdinal("Provincia"));
                            }
                            catch
                            {
                            }

                            try
                            {
                                sigla = reader.GetString(reader.GetOrdinal("Sigla"));
                            }
                            catch
                            {
                            }

                            try
                            {
                                contact_name = reader.GetString(reader.GetOrdinal("Contact_name"));
                            }
                            catch
                            {
                            }

                            try
                            {
                                contact_phone = reader.GetString(reader.GetOrdinal("Contact_phone"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                contact_mail = reader.GetString(reader.GetOrdinal("Contact_mail"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                id_customer = reader.GetString(reader.GetOrdinal("Id_customer"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                source = reader.GetString(reader.GetOrdinal("Source"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                tipAppuntamento = reader.GetString(reader.GetOrdinal("TipAppuntamento"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                contattiInfo = reader.GetString(reader.GetOrdinal("ContattiInfo"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                phones = reader.GetString(reader.GetOrdinal("PhoneList"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                emails = reader.GetString(reader.GetOrdinal("EmailList"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                id_appto_memo = reader.GetString(reader.GetOrdinal("Id_Appto_Memo"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                evento_pers = reader.GetString(reader.GetOrdinal("Evento_Pers"));
                            }
                            catch
                            {
                            }

                            List<string>emailListApp=new List<string>();
                            List<string>phoneListApp=new List<string>();
                            AppuntamentiItem appitem = new AppuntamentiItem();
                            appitem.Id = id;

                            // TODO - test
//                            DateTime appDateHour;
//                            DateTime.TryParseExact(appuntamentiDateHour, DATE_TIME_FORMAT, CultureInfo.InvariantCulture, DateTimeStyles.None, out appDateHour);
//                            appitem.AppuntamentiDateHour = appDateHour;

                            appitem.AppuntamentiDateHour = appuntamentiDateHour;
                            appitem.original_appuntamentiDateHour = original_AppuntamentiDateHour;
                            appitem.RegSociale = regSociale;
                            appitem.RepresentatnteLegale = repLegale;
                            appitem.Mail = mail;
                            appitem.Phone = phone;
                            appitem.Status = status;
                            appitem.TipClient = tipClient;
                            appitem.Note = note;

                            // TODO - test
//                            double latDouble;
//                            double.TryParse(latitude, NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingSign, CultureInfo.InvariantCulture, out latDouble);
//                            double longDouble;
//                            double.TryParse(longitude, NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingSign, CultureInfo.InvariantCulture, out longDouble);

                            appitem.Latitude = latitude;
                            appitem.Longitude = longitude;

                            appitem.City = city;

                            appitem.Id_Chiamata= id_chiamata;
                            appitem.Date_chiamata=date_chiamata;
                            appitem.Note_agent= note_agent;
                            appitem.Note_chiamata= note_chiamata;
                            appitem.Note_segretaria = note_segretaria;
                            appitem.Motivo=motivo;
                            appitem.Category= category;
                            appitem.Indirizzo= indirizzo;
                            appitem.Civico= civico;
                            appitem.Cap= cap;
                            appitem.Comune= comune;
                            appitem.Provincia= provincia;
                            appitem.Sigla= sigla;
                            appitem.Contact_name= contact_name;
                            appitem.Contact_phone= contact_phone;
                            appitem.Contact_mail= contact_mail;
                            appitem.Id_customer = id_customer;
                            appitem.Source = source;
                            appitem.TipAppuntamenti=tipAppuntamento;
                            appitem.ContattiInfo=contattiInfo;

                            string[] lines = Regex.Split(phones, "<<>>");
                            foreach (string line in lines)
                            {
                                phoneListApp.Add(line);
                            }

                            string[] linesEmails = Regex.Split(emails, "<<>>");
                            foreach (string line in linesEmails)
                            {
                                emailListApp.Add(line);
                            }

                            appitem.EmailList=emailListApp;
                            appitem.PhoneList=phoneListApp;
                            appitem.Id_appto_memo = id_appto_memo;
                            appitem.Evento_pers = evento_pers;

                            appList.Add(appitem);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader = null;
                }
            }

            return appList;
        }

        public bool CheckIfExiststAppuntamentiDB(string id)
        {
            int count = 0;
            Object obj = new Object();
            try
            {
                lock (obj)
                {
                    if (File.Exists(databasePath))
                    {
                        if(ConnSingleton.conn.State!= ConnectionState.Open)
                        {
                            ConnSingleton.conn.Open();
                        }

                        string connectionString = "URI=file:" + databasePath + ",version=3";
                        using (Mono.Data.Sqlite.SqliteCommand dbcmd = ConnSingleton.conn.CreateCommand())
                        {
                            string sql =
                                "SELECT count(*) " +
                                "FROM "+ Consts.APPUNTAMENTI_TABLE_NAME +
                                " where Id = " + "\"" + id + "\"";

                            dbcmd.CommandText = sql;
                            count = Convert.ToInt32(dbcmd.ExecuteScalar());

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }

            if (count > 0)
                return true;
            else
                return false;
        }

        public void InsertClasificationToTable(GareItem gareItem)
        {
            Object obj = new Object();
            try
            {

                lock (obj)
                {
                    string connectionString = "URI=file:" + databasePath + ",version=3";
                    if(ConnSingleton.conn.State!= ConnectionState.Open)
                    {
                        ConnSingleton.conn.Open();
                    }
                    string sSQLQuery = "INSERT OR IGNORE INTO " +
                        Consts.PROFILE_CLASSIFICATIOB_TABLE_NAME + " " +
                        "(Id, Agente, Punti,Pa, Ps,Ss) " +
                        "VALUES (@Id, @Agente, @Punti,@Pa,@Ps, @Ss);";

                    using (var cmd = ConnSingleton.conn.CreateCommand())
                    {
                        cmd.CommandTimeout = 0;
                        cmd.CommandText = sSQLQuery;
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.Add(new SqliteParameter("@Id", gareItem.Id));
//                        cmd.Parameters.Add(new SqliteParameter("@Data", gareItem.Data));
                        cmd.Parameters.Add(new SqliteParameter("@Agente", gareItem.Agente));
                        cmd.Parameters.Add(new SqliteParameter("@Punti", gareItem.Punti));
                        cmd.Parameters.Add(new SqliteParameter("@Pa", gareItem.PA));
                        cmd.Parameters.Add(new SqliteParameter("@Ps", gareItem.PS));
                        cmd.Parameters.Add(new SqliteParameter("@Ss", gareItem.SS));

                        var result = cmd.ExecuteNonQuery();
                    }
                }

            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void UpdateClasificationInfo(GareItem gareItem)
        {
            Object obj = new Object();
            try
            {

                lock (obj)
                {
                    string connectionString = "URI=file:" + databasePath + ",version=3";

                    if(ConnSingleton.conn.State!= ConnectionState.Open)
                    {
                        ConnSingleton.conn.Open();
                    }
                    string sSQLQuery = "UPDATE " +
                        Consts.PROFILE_CLASSIFICATIOB_TABLE_NAME + " " +
                        "SET Agente=@Agente, Punti=@Punti,Pa=@Pa, Ps=@Ps,Ss=@Ss" +
                        "where Id = @Id";

                    using (var cmd = ConnSingleton.conn.CreateCommand())
                    {
                        cmd.CommandTimeout = 0;
                        cmd.CommandText = sSQLQuery;
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.Add(new SqliteParameter("@Id", gareItem.Id));
//                        cmd.Parameters.Add(new SqliteParameter("@Data", gareItem.Data));
                        cmd.Parameters.Add(new SqliteParameter("@Agente", gareItem.Agente));
                        cmd.Parameters.Add(new SqliteParameter("@Punti", gareItem.Punti));
                        cmd.Parameters.Add(new SqliteParameter("@Pa", gareItem.PA));
                        cmd.Parameters.Add(new SqliteParameter("@Ps", gareItem.PS));
                        cmd.Parameters.Add(new SqliteParameter("@Ss", gareItem.SS));

                        var result = cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public List<GareItem> getLastInsertedClassifications()
        {
            Mono.Data.Sqlite.SqliteDataReader reader = null;
            List<GareItem> clasificList = new List<GareItem>();
            try
            {
                if (File.Exists(databasePath))
                {

                    string connectionString = "URI=file:" + databasePath + ",version=3";
                    if(ConnSingleton.conn.State!= ConnectionState.Open)
                    {
                        ConnSingleton.conn.Open();
                    }
                    string sSQLQuery = "SELECT * FROM " +
                        Consts.PROFILE_CLASSIFICATIOB_TABLE_NAME;

                    using (var cmd = ConnSingleton.conn.CreateCommand())
                    {
                        cmd.CommandTimeout = 0;
                        cmd.CommandText = sSQLQuery;
                        cmd.CommandType = CommandType.Text;

                        reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            string id = "";
//                            DateTime data = DateTime.Now.Date;
                            string agente = "";
                            string punti = "";
                            string pa = "";
                            string ps = "";
                            string ss = "";


                            try
                            {
                                id = reader.GetString(reader.GetOrdinal("Id"));
                            }
                            catch
                            {
                            }
//                            try
//                            {
//                                data = reader.GetDateTime(reader.GetOrdinal("Data"));
//                            }
//                            catch
//                            {
//                            }

                            try
                            {
                                agente = reader.GetString(reader.GetOrdinal("Agente"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                punti = reader.GetString(reader.GetOrdinal("Punti"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                pa = reader.GetString(reader.GetOrdinal("Pa"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                ps = reader.GetString(reader.GetOrdinal("Ps"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                ss = reader.GetString(reader.GetOrdinal("Ss"));
                            }
                            catch
                            {
                            }

                            GareItem gareitem = new GareItem();
                            gareitem.Id = Int32.Parse(id);
//                            gareitem.Data = data.ToString();
                            gareitem.Agente=agente;
                            gareitem.Punti=punti;
                            gareitem.PA = pa;
                            gareitem.PS = ps;
                            gareitem.SS = ss;

                            clasificList.Add(gareitem);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader = null;
                }
            }

            return clasificList;
        }

        public void deleteTableName(string table_name)
        {
            try
            {
                if (File.Exists(databasePath))
                {
                    if(ConnSingleton.conn.State!= ConnectionState.Open)
                    {
                        ConnSingleton.conn.Open();
                    }
                    using (Mono.Data.Sqlite.SqliteCommand dbcmd = ConnSingleton.conn.CreateCommand())
                    {
                        string sSQLQuery = "Delete From " +
                            table_name+";";


                        dbcmd.CommandTimeout = 0;
                        dbcmd.CommandText = sSQLQuery;
                        dbcmd.CommandType = CommandType.Text;


                        var result = dbcmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public bool CheckIfTableIsEmpty(string table_name)
        {
            int count = 0;
            Object obj = new Object();
            try
            {
                lock (obj)
                {
                    if (File.Exists(databasePath))
                    {
                        if(ConnSingleton.conn.State!= ConnectionState.Open)
                        {
                            ConnSingleton.conn.Open();
                        }
                        using (Mono.Data.Sqlite.SqliteCommand dbcmd = ConnSingleton.conn.CreateCommand())
                        {
                            string sql =
                                "SELECT count(*) " +
                                "FROM "+table_name;

                            dbcmd.CommandText = sql;
                            count = Convert.ToInt32(dbcmd.ExecuteScalar());
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }

            if (count > 0)
                return true;
            else
                return false;
        }


        public void InsertBonusesToTable(BonusItem bonusItem)
        {
            Object obj = new Object();
            try
            {

                lock (obj)
                {
                    string connectionString = "URI=file:" + databasePath + ",version=3";
                    if(ConnSingleton.conn.State!= ConnectionState.Open)
                    {
                        ConnSingleton.conn.Open();
                    }

                    string sSQLQuery = "INSERT OR IGNORE INTO " +
                        Consts.PROFILE_BONUS_TABLE_NAME + " " +
                        "(Id, Regionalesociale, Prodato,Bonus) " +
                        "VALUES (@Id , @Regionalesociale, @Prodato,@Bonus);";

                    using (var cmd = ConnSingleton.conn.CreateCommand())
                    {
                        cmd.CommandTimeout = 0;
                        cmd.CommandText = sSQLQuery;
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.Add(new SqliteParameter("@Id", bonusItem.Id));
//                        cmd.Parameters.Add(new SqliteParameter("@Data", bonusItem.Data));
                        cmd.Parameters.Add(new SqliteParameter("@Regionalesociale", bonusItem.Regionalesociale));
                        cmd.Parameters.Add(new SqliteParameter("@Prodato", bonusItem.Prodato));
                        cmd.Parameters.Add(new SqliteParameter("@Bonus", bonusItem.Bonus));

                        var result = cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void UpdateBonusesInfo(BonusItem bonusItem)
        {
            Object obj = new Object();
            try
            {
                lock (obj)
                {
                    string connectionString = "URI=file:" + databasePath + ",version=3";

                    if(ConnSingleton.conn.State!= ConnectionState.Open)
                    {
                        ConnSingleton.conn.Open();
                    }

                    string sSQLQuery = "UPDATE " +
                        Consts.PROFILE_BONUS_TABLE_NAME + " " +
                        "SET Regionalesociale=@Regionalesociale, Prodato=@Prodato,Bonus=@Bonus" +
                        "where Id = @Id";

                    using (var cmd = ConnSingleton.conn.CreateCommand())
                    {
                        cmd.CommandTimeout = 0;
                        cmd.CommandText = sSQLQuery;
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.Add(new SqliteParameter("@Id", bonusItem.Id));
//                        cmd.Parameters.Add(new SqliteParameter("@Data", bonusItem.Data));
                        cmd.Parameters.Add(new SqliteParameter("@Regionalesociale", bonusItem.Regionalesociale));
                        cmd.Parameters.Add(new SqliteParameter("@Prodato", bonusItem.Prodato));
                        cmd.Parameters.Add(new SqliteParameter("@Bonus", bonusItem.Bonus));


                        var result = cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public List<BonusItem> getLastInsertedBonuses()
        {
            Mono.Data.Sqlite.SqliteDataReader reader = null;
            List<BonusItem> bonusList = new List<BonusItem>();
            try
            {
                if (File.Exists(databasePath))
                {

                    string connectionString = "URI=file:" + databasePath + ",version=3";
                    if(ConnSingleton.conn.State!= ConnectionState.Open)
                    {
                        ConnSingleton.conn.Open();
                    }

                    string sSQLQuery = "SELECT * FROM " +
                        Consts.PROFILE_BONUS_TABLE_NAME;

                    using (var cmd = ConnSingleton.conn.CreateCommand())
                    {
                        cmd.CommandTimeout = 0;
                        cmd.CommandText = sSQLQuery;
                        cmd.CommandType = CommandType.Text;

                        reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            string id = "";
//                            DateTime data = DateTime.Now.Date;
                            string reqSociale = "";
                            string prodotto = "";
                            float bonus = 0.0f;

                            try
                            {
                                id = reader.GetString(reader.GetOrdinal("Id"));
                            }
                            catch
                            {
                            }
//                            try
//                            {
//                                data = reader.GetDateTime(reader.GetOrdinal("Data"));
//                            }
//                            catch
//                            {
//                            }

                            try
                            {
                                reqSociale = reader.GetString(reader.GetOrdinal("Regionalesociale"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                prodotto = reader.GetString(reader.GetOrdinal("Prodato"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                bonus = reader.GetFloat(reader.GetOrdinal("Bonus"));
                            }
                            catch
                            {
                            }


                            BonusItem bonusitem = new BonusItem();
                            bonusitem.Id = id;
//                            bonusitem.Data = data.ToString();
                            bonusitem.Regionalesociale=reqSociale;
                            bonusitem.Prodato=prodotto;
                            bonusitem.Bonus = bonus;

                            bonusList.Add(bonusitem);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader = null;
                }
            }

            return bonusList;
        }


        public void InsertInvoicesToTable(FatturareItem invoiceItem)
        {
            Object obj = new Object();
            try
            {

                lock (obj)
                {

                    string connectionString = "URI=file:" + databasePath + ",version=3";

                    if(ConnSingleton.conn.State!= ConnectionState.Open)
                    {
                        ConnSingleton.conn.Open();
                    }

                    string sSQLQuery = "INSERT OR IGNORE INTO " +
                        Consts.PROFILE_INVOICES_TABLE_NAME + " " +
                        "(Id, Total,CheckInvoice,Status,Title_invoice,Period) " +
                        "VALUES (@Id, @Total,@CheckInvoice,@Status,@Title_invoice,@Period);";//@Check,

                    using (var cmd = ConnSingleton.conn.CreateCommand())
                    {
                        cmd.CommandTimeout = 0;
                        cmd.CommandText = sSQLQuery;
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.Add(new SqliteParameter("@Id", invoiceItem.Id.ToString()));
//                        cmd.Parameters.Add(new SqliteParameter("@Data", invoiceItem.Data));
                        cmd.Parameters.Add(new SqliteParameter("@Total", invoiceItem.Total));
                        cmd.Parameters.Add(new SqliteParameter("@CheckInvoice", invoiceItem.Check));
                        cmd.Parameters.Add(new SqliteParameter("@Status", invoiceItem.Status));
                        cmd.Parameters.Add(new SqliteParameter("@Title_invoice", invoiceItem.Title_invoice));
                        cmd.Parameters.Add(new SqliteParameter("@Period", invoiceItem.Period));

                        var result = cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void UpdateinvoicesInfo(FatturareItem invoiceItem)
        {
            Object obj = new Object();
            try
            {
                lock (obj)
                {
                    string connectionString = "URI=file:" + databasePath + ",version=3";

                    if(ConnSingleton.conn.State!= ConnectionState.Open)
                    {
                        ConnSingleton.conn.Open();
                    }

                    string sSQLQuery = "UPDATE " +
                        Consts.PROFILE_INVOICES_TABLE_NAME + " " +
                        "SET Total=@Total, CheckInvoice=@CheckInvoice,Period=@Period " +
                        "where Id = @Id";

                    using (var cmd = ConnSingleton.conn.CreateCommand())
                    {
                        cmd.CommandTimeout = 0;
                        cmd.CommandText = sSQLQuery;
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.Add(new SqliteParameter("@Id", invoiceItem.Id.ToString()));
//                        cmd.Parameters.Add(new SqliteParameter("@Data", invoiceItem.Data));
                        cmd.Parameters.Add(new SqliteParameter("@Total", invoiceItem.Total));
                        cmd.Parameters.Add(new SqliteParameter("@CheckInvoice", invoiceItem.Check));
                        cmd.Parameters.Add(new SqliteParameter("@Period", invoiceItem.Period));
//                        cmd.Parameters.Add(new SqliteParameter("@Status", invoiceItem.Status));
//                        cmd.Parameters.Add(new SqliteParameter("@Title_invoice", invoiceItem.Title_invoice));

                        var result = cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public List<FatturareItem> getLastInsertedInvoices()
        {
            Mono.Data.Sqlite.SqliteDataReader reader = null;
            List<FatturareItem> invoicesList = new List<FatturareItem>();
            try
            {
                if (File.Exists(databasePath))
                {

                    string connectionString = "URI=file:" + databasePath + ",version=3";

                    if(ConnSingleton.conn.State!= ConnectionState.Open)
                    {
                        ConnSingleton.conn.Open();
                    }

                    string sSQLQuery = "SELECT * FROM " +
                        Consts.PROFILE_INVOICES_TABLE_NAME;

                    using (var cmd = ConnSingleton.conn.CreateCommand())
                    {
                        cmd.CommandTimeout = 0;
                        cmd.CommandText = sSQLQuery;
                        cmd.CommandType = CommandType.Text;

                        reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            string id = "";
//                            string data ="";
                            float total = 0;
                            string check = "";
                            string status = "";
                            string title_inv="";
                            string period="";

                            try
                            {
                                id = reader.GetString(reader.GetOrdinal("Id"));
                            }
                            catch
                            {
                            }
//                            try
//                            {
//                                data = reader.GetString(reader.GetOrdinal("Data"));
//                            }
//                            catch
//                            {
//                            }

                            try
                            {
                                total = reader.GetFloat(reader.GetOrdinal("Total"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                check = reader.GetString(reader.GetOrdinal("CheckInvoice"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                status = reader.GetString(reader.GetOrdinal("Status"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                title_inv = reader.GetString(reader.GetOrdinal("Title_invoice"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                period = reader.GetString(reader.GetOrdinal("Period"));
                            }
                            catch
                            {
                            }

                            FatturareItem invoicetem = new FatturareItem();
                            int resultOfParse;
                            Int32.TryParse(id,out resultOfParse);
                            invoicetem.Id = resultOfParse;
//                            invoicetem.Data = data;
                            invoicetem.Total=total;
                            if(check!="")
                            {
                                invoicetem.Check=check;
                            }
                            invoicetem.Status = status;
                            invoicetem.Title_invoice = title_inv;
                            invoicetem.Period = period;

                            invoicesList.Add(invoicetem);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader = null;
                }
            }

            return invoicesList;
        }

        public void InsertPraticheToTable(ClientPraticheItemInfo praticheItem)
        {
            Object obj = new Object();
            try
            {

                lock (obj)
                {

                    string connectionString = "URI=file:" + databasePath + ",version=3";

                    if(ConnSingleton.conn.State!= ConnectionState.Open)
                    {
                        ConnSingleton.conn.Open();
                    }
                    //    `Routing`   VARCHAR(100),
                   // `Notes`   VARCHAR(100),
                    string sSQLQuery = "INSERT OR IGNORE INTO " +
                        Consts.ALL_PRATICHE_TABLE_NAME + " " +
                        "(Seriale, Qnt_inserita, Qnt_attiva, Tipo_prodotto,Prodotto, Pratiche_date, Data_inserimento, Data_creazione, Linee, Tipo_contratto, Canone, Data_attivazione, Stato_ordine, Stato_pratica, Codice_cliente,Id_product,Routing,Notes) " +
                        "VALUES (@Seriale, @Qnt_inserita, @Qnt_attiva, @Tipo_prodotto,@Prodotto, @Pratiche_date, @Data_inserimento, @Data_creazione, @Linee, @Tipo_contratto, @Canone, @Data_attivazione, @Stato_ordine, @Stato_pratica, @Codice_cliente,@Id_product,@Routing,@Notes);";

                    using (var cmd = ConnSingleton.conn.CreateCommand())
                    {
                        cmd.CommandTimeout = 0;
                        cmd.CommandText = sSQLQuery;
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.Add(new SqliteParameter("@Seriale", praticheItem.Seriale));
                        cmd.Parameters.Add(new SqliteParameter("@Qnt_inserita", praticheItem.Qta_inserita.ToString()));
                        cmd.Parameters.Add(new SqliteParameter("@Qnt_attiva", praticheItem.Qta_attiva.ToString()));
                        cmd.Parameters.Add(new SqliteParameter("@Tipo_prodotto", praticheItem.Tipo_prodotto));
                        cmd.Parameters.Add(new SqliteParameter("@Prodotto", praticheItem.Prodotto));

                        cmd.Parameters.Add(new SqliteParameter("@Pratiche_date", praticheItem.Pratiche_date));

                        cmd.Parameters.Add(new SqliteParameter("@Data_inserimento", praticheItem.Data_inserimento));
                     
                        cmd.Parameters.Add(new SqliteParameter("@Data_creazione", praticheItem.Data_creazione));
                        cmd.Parameters.Add(new SqliteParameter("@Linee", praticheItem.Linee));
                        cmd.Parameters.Add(new SqliteParameter("@Tipo_contratto", praticheItem.Tipo_contratto));
                        cmd.Parameters.Add(new SqliteParameter("@Canone", praticheItem.Canone.ToString()));

                        cmd.Parameters.Add(new SqliteParameter("@Data_attivazione", praticheItem.Data_attivazione));
                        cmd.Parameters.Add(new SqliteParameter("@Stato_ordine", praticheItem.Stato_ordine));
                        cmd.Parameters.Add(new SqliteParameter("@Stato_pratica", praticheItem.Stato_pratica));
                        cmd.Parameters.Add(new SqliteParameter("@Codice_cliente", praticheItem.Codice_cliente));
                        cmd.Parameters.Add(new SqliteParameter("@Id_product", praticheItem.Id_product));

                        cmd.Parameters.Add(new SqliteParameter("@Routing", praticheItem.Routing));
                        cmd.Parameters.Add(new SqliteParameter("@Notes", praticheItem.Notes));

                        var result = cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public void UpdatePraticheInfo(ClientPraticheItemInfo praticheItem)
        {
            Object obj = new Object();
            try
            {
                lock (obj)
                {
                    string connectionString = "URI=file:" + databasePath + ",version=3";

                    if(ConnSingleton.conn.State!= ConnectionState.Open)
                    {
                        ConnSingleton.conn.Open();
                    }

                    string sSQLQuery = "UPDATE " +
                        Consts.ALL_PRATICHE_TABLE_NAME + " " +
                        "SET Qnt_inserita=@Qnt_inserita, Qnt_attiva=@Qnt_attiva, Tipo_prodotto=@Tipo_prodotto,Prodotto=@Prodotto, Pratiche_date=@Pratiche_date, Data_inserimento=@Data_inserimento, Data_creazione=@Data_creazione, Linee=@Linee, Tipo_contratto=@Tipo_contratto, Canone=@Canone, Data_attivazione=@Data_attivazione, Stato_ordine=@Stato_ordine, Stato_pratica=@Stato_pratica, Codice_cliente=@Codice_cliente, Seriale=@Seriale, Routing=@Routing, Notes=@Notes" +
                        " where Id_product = @Id_product";

                    using (var cmd = ConnSingleton.conn.CreateCommand())
                    {
                        cmd.CommandTimeout = 0;
                        cmd.CommandText = sSQLQuery;
                        cmd.CommandType = CommandType.Text;

                        cmd.Parameters.Add(new SqliteParameter("@Id_product", praticheItem.Id_product));
                        cmd.Parameters.Add(new SqliteParameter("@Qnt_inserita", praticheItem.Qta_inserita.ToString()));
                        cmd.Parameters.Add(new SqliteParameter("@Qnt_attiva", praticheItem.Qta_attiva.ToString()));
                        cmd.Parameters.Add(new SqliteParameter("@Tipo_prodotto", praticheItem.Tipo_prodotto));
                        cmd.Parameters.Add(new SqliteParameter("@Prodotto", praticheItem.Prodotto));
                        cmd.Parameters.Add(new SqliteParameter("@Pratiche_date", praticheItem.Pratiche_date));
                        cmd.Parameters.Add(new SqliteParameter("@Data_inserimento", praticheItem.Data_inserimento));
                        cmd.Parameters.Add(new SqliteParameter("@Data_creazione", praticheItem.Data_creazione));
                        cmd.Parameters.Add(new SqliteParameter("@Linee", praticheItem.Linee));
                        cmd.Parameters.Add(new SqliteParameter("@Tipo_contratto", praticheItem.Tipo_contratto));
                        cmd.Parameters.Add(new SqliteParameter("@Canone", praticheItem.Canone.ToString()));
                        cmd.Parameters.Add(new SqliteParameter("@Data_attivazione", praticheItem.Data_attivazione));
                        cmd.Parameters.Add(new SqliteParameter("@Stato_ordine", praticheItem.Stato_ordine));
                        cmd.Parameters.Add(new SqliteParameter("@Stato_pratica", praticheItem.Stato_pratica));
                        cmd.Parameters.Add(new SqliteParameter("@Codice_cliente", praticheItem.Codice_cliente));
                        cmd.Parameters.Add(new SqliteParameter("@Seriale", praticheItem.Seriale));

                        cmd.Parameters.Add(new SqliteParameter("@Routing", praticheItem.Routing));
                        cmd.Parameters.Add(new SqliteParameter("@Notes", praticheItem.Notes));

                        var result = cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
        }

        public List<ClientPraticheItemInfo> getPraticheListForCustomer(string customer_id)
        {
            Mono.Data.Sqlite.SqliteDataReader reader = null;
            List<ClientPraticheItemInfo> praticheList = new List<ClientPraticheItemInfo>();
            try
            {
                if (File.Exists(databasePath))
                {

                    string connectionString = "URI=file:" + databasePath + ",version=3";

                    if(ConnSingleton.conn.State!= ConnectionState.Open)
                    {
                        ConnSingleton.conn.Open();
                    }

                    string sSQLQuery = "SELECT * FROM " +
                        Consts.ALL_PRATICHE_TABLE_NAME+
                        " where Codice_cliente = " + "\"" + customer_id + "\"";

                    using (var cmd = ConnSingleton.conn.CreateCommand())
                    {
                        cmd.CommandTimeout = 0;
                        cmd.CommandText = sSQLQuery;
                        cmd.CommandType = CommandType.Text;

                        reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            string seriale = "";
                            string qnt_inserita = "";
                            string qnt_attiva = "";
                            string tipo_prodotto = "";
                            string prodotto = "";
                            DateTime pratiche_date = DateTime.Now.Date;
                            DateTime data_inserimento = DateTime.Now.Date;
                            DateTime data_creazione = DateTime.Now.Date;
                            string linee = "";
                            string tipo_contratto = "";
                            string canone = "";
                            DateTime data_attivatione = DateTime.Now.Date;
                            string stato_ordine = "";
                            string stato_pratica="";

                            string routing="";
                            string notes="";
                            try
                            {
                                seriale = reader.GetString(reader.GetOrdinal("Seriale"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                qnt_inserita = reader.GetString(reader.GetOrdinal("Qnt_inserita"));
                            }
                            catch
                            {
                            }

                            try
                            {
                                qnt_attiva = reader.GetString(reader.GetOrdinal("Qnt_attiva"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                tipo_prodotto = reader.GetString(reader.GetOrdinal("Tipo_prodotto"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                prodotto = reader.GetString(reader.GetOrdinal("Prodotto"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                pratiche_date = reader.GetDateTime(reader.GetOrdinal("Pratiche_date"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                data_inserimento = reader.GetDateTime(reader.GetOrdinal("Data_inserimento"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                data_creazione = reader.GetDateTime(reader.GetOrdinal("Data_creazione"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                linee = reader.GetString(reader.GetOrdinal("Linee"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                tipo_contratto = reader.GetString(reader.GetOrdinal("Tipo_contratto"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                canone = reader.GetString(reader.GetOrdinal("Canone"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                data_attivatione = reader.GetDateTime(reader.GetOrdinal("Data_attivazione"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                stato_ordine = reader.GetString(reader.GetOrdinal("Stato_ordine"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                stato_pratica = reader.GetString(reader.GetOrdinal("Stato_pratica"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                routing = reader.GetString(reader.GetOrdinal("Routing"));
                            }
                            catch
                            {
                            }
                            try
                            {
                                notes = reader.GetString(reader.GetOrdinal("Notes"));
                            }
                            catch
                            {
                            }

                            ClientPraticheItemInfo praticheitem = new ClientPraticheItemInfo();
                            praticheitem.Seriale = seriale;
                            praticheitem.Qta_inserita=Int32.Parse(qnt_inserita);
                            praticheitem.Qta_attiva=Int32.Parse(qnt_attiva);
                            praticheitem.Tipo_prodotto=tipo_prodotto;
                            praticheitem.Prodotto=prodotto;
                            praticheitem.Pratiche_date=pratiche_date;
                            praticheitem.Data_inserimento=data_inserimento;
                            praticheitem.Data_creazione=data_creazione;
                            praticheitem.Linee=linee;
                            praticheitem.Tipo_contratto=tipo_contratto;

//                            praticheitem.Canone=Int32.Parse(canone);
                            if (canone != null && canone != "")
                            {
                                double canon;
                                double.TryParse(canone, NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingSign, CultureInfo.InvariantCulture, out canon);

                                praticheitem.Canone = canon;
                            }

                            praticheitem.Data_attivazione = data_attivatione;
                            praticheitem.Stato_ordine=stato_ordine;
                            praticheitem.Stato_pratica=stato_pratica;
                            praticheitem.Codice_cliente=customer_id;
                            praticheitem.Routing=routing;
                            praticheitem.Notes=notes;

                            praticheList.Add(praticheitem);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader = null;
                }
            }

            return praticheList;
        }

        public bool CheckIfClientPraticheExistinDB(string pratiche_id)
        {
            int count = 0;
            Object obj = new Object();
            try
            {
                lock (obj)
                {
                    if (File.Exists(databasePath))
                    {
                        if(ConnSingleton.conn.State!= ConnectionState.Open)
                        {
                            ConnSingleton.conn.Open();
                        }
                        string connectionString = "URI=file:" + databasePath + ",version=3";
                        using (Mono.Data.Sqlite.SqliteCommand dbcmd = ConnSingleton.conn.CreateCommand())
                        {
                            string sql =
                                "SELECT count(*) " +
                                "FROM AllPraticheTable " +
                                "where Id_product = " + "\"" + pratiche_id + "\"";

                            dbcmd.CommandText = sql;
                            count = Convert.ToInt32(dbcmd.ExecuteScalar());

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Utils.writeToDeviceLog(ex);
            }

            if (count > 0)
                return true;
            else
                return false;
        }
    }
}

