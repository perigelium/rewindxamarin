using System;
using UIKit;

namespace Factorymind.Components
{
    public class GestureDelegate:UISwipeGestureRecognizer
    {
        public override bool ShouldBeRequiredToFailByGestureRecognizer(UIGestureRecognizer otherGestureRecognizer)
        {
            return true;
        }
    }
}