//	New implementations, refactoring and restyling - FactoryMind || http://factorymind.com 
//  Converted to MonoTouch on 1/22/09 - Eduardo Scoz || http://escoz.com
//  Originally reated by Devin Ross on 7/28/09  - tapku.com || http://github.com/devinross/tapkulibrary
//
/*
 
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:
 
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.
 
 */

using System;
using System.Collections.Generic;
using CoreGraphics;
using System.Globalization;

#if __UNIFIED__
using Foundation;
using UIKit;

// Mappings Unified CoreGraphic classes to MonoTouch classes
using CGRect = global::System.Drawing.RectangleF;
using CGSize = global::System.Drawing.SizeF;
using CGPoint = global::System.Drawing.PointF;

// Mappings Unified types to MonoTouch types
using nfloat = global::System.Single;
using nint = global::System.Int32;
using nuint = global::System.UInt32;

#else
	using Foundation;
	using UIKit;
#endif

namespace Factorymind.Components
{

    public delegate void DateSelected(DateTime date);
    public delegate void MonthChanged(DateTime monthSelected);

    public class FMCalendar : UIView
    {
        /// <summary>
        /// Fired when new date selected.
        /// </summary>
        public Action<DateTime> DateSelected;

        /// <summary>
        /// Fired when date selection finished
        /// </summary>
        public Action<DateTime> DateSelectionFinished;

        /// <summary>
        /// Fired when Selected month changed
        /// </summary>
        public Action<DateTime> MonthChanged;

        /// <summary>
        /// Mark with a dot dates that fulfill the predicate
        /// </summary>
        public Func<DateTime, bool> IsDayMarkedDelegate;

        /// <summary>
        /// Turn gray dates that fulfill the predicate
        /// </summary>
        public Func<DateTime, bool> IsDateAvailable;

        /// <summary>
        /// Gets the current selected Date.
        /// </summary>
        /// <value>The current selected date.</value>
        public DateTime CurrentSelectedDate { get { return SelectedDate; } }

        public DateTime CurrentMonthYear;

        protected DateTime CurrentDate { get; set; }

        internal DateTime SelectedDate { get; set; }

        private UIScrollView _scrollView;
        private bool calendarIsLoaded;

        private MonthGridView _monthGridView;
        private UIButton _leftButton, _rightButton;

        private UIButton _leftYearButton, _rightYearButton;

        public Factorymind.ComponentsUnified.GestureDelegate swipeleft;
        public Factorymind.ComponentsUnified.GestureDelegate swiperight;

        // User Customizations

        /// <summary>
        /// If true, Sunday will be showed as the first day of the week, otherwise the first one will be Monday
        /// </summary>
        /// <value><c>true</c> if sunday first; otherwise, <c>false</c>.</value>
        public Boolean SundayFirst { get; set; }

        /// <summary>
        /// Format string used to display the month's name
        /// </summary>
        /// <value>The month format string.</value>
        public String MonthFormatString { get; set; }

        public String YearFormatString{ get; set; }

        /// <summary>
        /// Specify the color for the selected date
        /// </summary>
        /// <value>The color of the selection.</value>
        public UIColor SelectionColor { get; set; }

        public UIFont yearMonthFont{ get; set; }

        public UIFont dayFont{ get; set; }

        /// <summary>
        /// Specify the color for the today circle
        /// </summary>
        /// <value>The color of the selection.</value>
        public UIColor TodayCircleColor { get; set; }

        /// <summary>
        /// Specify the background color for the calendar
        /// </summary>
        /// <value>The color of the selection.</value>
        public UIColor MonthBackgroundColor { get; set; }

        /// <summary>
        /// Gets or sets the left arrow image.
        /// </summary>
        /// <value>The left arrow.</value>
        public UIImage LeftArrow { get; set; }

        /// <summary>
        /// Gets or sets the right arrow image.
        /// </summary>
        /// <value>The right arrow.</value>
        public UIImage RightArrow { get; set; }

        /// <summary>
        /// Gets or sets the top bar View.
        /// </summary>
        /// <value>The top bar.</value>
        public UIImage TopBar { get; set; }

        #if __UNIFIED__

        private CoreGraphics.CGRect HeaderViewSize { get; set; }

        private CoreGraphics.CGRect MainViewSize { get; set; }

        #else
        
		private CGRect HeaderViewSize { get; set; }
		private CGRect MainViewSize { get; set; }

		#endif

        private float HeaderMiddleY { get { return (float)(HeaderViewSize.Height / 2) - (HeaderElementSize / 2); } }

        private float HeaderElementSize { get { return (float)(HeaderViewSize.Height - (HeaderViewSize.Height * 0.10f)); } }

        private float HeaderBorderOffset { get { return 2f; } }

        private float DayNameHeight { get { return 20f; } }

        internal int DayCellWidth { get { return (int)MainViewSize.Width / 7; } }

        internal int DayCellHeight { get { return (int)MainViewSize.Height / 8; } }

        #if __UNIFIED__

        public FMCalendar()
            : this(new CoreGraphics.CGRect(0, 0, 320, 400))
        {
        }

        public FMCalendar(CoreGraphics.CGRect mainViewSize)
            : this(mainViewSize, new CoreGraphics.CGRect(0, 0, mainViewSize.Width, 60))
        {
        }

        public FMCalendar(CoreGraphics.CGRect mainViewSize, CoreGraphics.CGRect headerViewSize)
            : base(mainViewSize)
        {
            this.MainViewSize = mainViewSize;
            this.HeaderViewSize = headerViewSize;

            Initialize();
        }

        #else
        
		public FMCalendar() : this(new CGRect(0, 0, 320, 400))
		{
		}

		public FMCalendar(CGRect mainViewSize) : this(mainViewSize, new CGRect(0, 0, mainViewSize.Width, 60))
		{
		}

		public FMCalendar(CGRect mainViewSize, CGRect headerViewSize) : base (mainViewSize)
		{
			this.MainViewSize = mainViewSize;
			this.HeaderViewSize = headerViewSize;

			Initialize ();
		}

		#endif

        private void Initialize()
        {
            CurrentDate = DateTime.Now.Date;
            SelectedDate = CurrentDate;
            CurrentMonthYear = new DateTime(CurrentDate.Year, CurrentDate.Month, 1);

            // Defaults

            SundayFirst = false;

            MonthFormatString = "MMMM";
            YearFormatString = "yyyy";

            MonthBackgroundColor = UIColor.White;
            SelectionColor = UIColor.Red;
            TodayCircleColor = UIColor.Red;

            LeftArrow = UIImage.FromFile("leftArrow.png");
            RightArrow = UIImage.FromFile("rightArrow.png");
            BackgroundColor = MonthBackgroundColor;
        }

        public override void SetNeedsDisplay()
        {
            base.SetNeedsDisplay();

            AdjustBackgroundColor();

            if (_monthGridView != null)
                _monthGridView.Update();
        }

        public override void LayoutSubviews()
        {
            AdjustBackgroundColor();

            if (calendarIsLoaded)
                return;

            #if __UNIFIED__

            _scrollView = new UIScrollView(new CoreGraphics.CGRect(0, HeaderViewSize.Height, MainViewSize.Width, MainViewSize.Height - HeaderViewSize.Height))
            {
                ContentSize = new CoreGraphics.CGSize(MainViewSize.Width, MainViewSize.Height),
                ScrollEnabled = true,
                Frame = new CoreGraphics.CGRect(0, HeaderViewSize.Height + 10, MainViewSize.Width, MainViewSize.Height),
                BackgroundColor = MonthBackgroundColor,
            };

            #else

			_scrollView = new UIScrollView(new CGRect(0, HeaderViewSize.Height, MainViewSize.Width, MainViewSize.Height - HeaderViewSize.Height))
			{
				ContentSize = new CGSize(MainViewSize.Width, MainViewSize.Height),
                    ScrollEnabled = true,
				Frame = new CGRect(0, HeaderViewSize.Height+10, MainViewSize.Width, MainViewSize.Height),
				BackgroundColor = MonthBackgroundColor,
			};

            #endif

            LoadButtons();

            LoadButtonsForYear();

            LoadInitialGrids();

            AddSubview(_scrollView);
            _scrollView.AddSubview(_monthGridView);

            calendarIsLoaded = true;

            swipeleft = new Factorymind.ComponentsUnified.GestureDelegate();
            swipeleft.Direction = UISwipeGestureRecognizerDirection.Left;
            swipeleft.AddTarget(() => HandleSwipe(swipeleft));
            _scrollView.AddGestureRecognizer(swipeleft);

            swiperight = new Factorymind.ComponentsUnified.GestureDelegate();
            swiperight.Direction = UISwipeGestureRecognizerDirection.Right;
            swiperight.AddTarget(() => HandleSwipeRight(swiperight));
            _scrollView.AddGestureRecognizer(swiperight);

            //TODO ????? 
            MoveCalendarMonths(false, false, 0);
        }

        public void DeselectDate()
        {
            if (_monthGridView != null)
                _monthGridView.DeselectDayView();
        }

        void HandleSwipe(UISwipeGestureRecognizer sender)
        {
            try
            {                
                if (sender.Direction == UISwipeGestureRecognizerDirection.Left)
                {
                    HandleNextMonthTouch(null, null);
                }
                else if (sender.Direction == UISwipeGestureRecognizerDirection.Right)
                {
                    HandlePreviousMonthTouch(null, null);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        void HandleSwipeRight(UISwipeGestureRecognizer sender)
        {
            try
            {                
                if (sender.Direction == UISwipeGestureRecognizerDirection.Left)
                {
                    HandleNextMonthTouch(null, null);
                }
                else if (sender.Direction == UISwipeGestureRecognizerDirection.Right)
                {
                    HandlePreviousMonthTouch(null, null);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void LoadButtons()
        {
            _leftButton = UIButton.FromType(UIButtonType.Custom);
            _leftButton.TouchUpInside += HandlePreviousMonthTouch;
            _leftButton.ContentMode = UIViewContentMode.ScaleAspectFit;
            _leftButton.ContentEdgeInsets = new UIEdgeInsets(5, 10, 5, 10);
            _leftButton.SetImage(LeftArrow, UIControlState.Normal);
            AddSubview(_leftButton);

            #if __UNIFIED__

            _leftButton.Frame = new CoreGraphics.CGRect(HeaderBorderOffset + 10, HeaderMiddleY + HeaderElementSize / 4 + 3 - 5, HeaderElementSize / 3 + 12, HeaderElementSize / 3 + 5);

            #else

            _leftButton.Frame = new CGRect(HeaderBorderOffset+10, HeaderMiddleY+HeaderElementSize/4+3-5, HeaderElementSize/3+12, HeaderElementSize/3+5);

            #endif

            _rightButton = UIButton.FromType(UIButtonType.Custom);
            _rightButton.TouchUpInside += HandleNextMonthTouch;
            _rightButton.ContentMode = UIViewContentMode.ScaleAspectFit;
            _rightButton.ContentEdgeInsets = new UIEdgeInsets(5, 10, 5, 10);
            _rightButton.SetImage(RightArrow, UIControlState.Normal);
            AddSubview(_rightButton);

            #if __UNIFIED__

            _rightButton.Frame = new CoreGraphics.CGRect(HeaderViewSize.Width / 2 - HeaderElementSize / 2 - 10, HeaderMiddleY + HeaderElementSize / 4 + 3 - 5, HeaderElementSize / 3 + 12, HeaderElementSize / 3 + 5);

            #else

            _rightButton.Frame = new CGRect(HeaderViewSize.Width/2 - HeaderElementSize/2 - 10, HeaderMiddleY+HeaderElementSize/4+3-5, HeaderElementSize/3+12, HeaderElementSize/3+5);

            #endif
        }

        private void LoadButtonsForYear()
        {

            _leftYearButton = UIButton.FromType(UIButtonType.Custom);
            _leftYearButton.TouchUpInside += HandlePreviousYearTouch;
            _leftYearButton.ContentMode = UIViewContentMode.ScaleAspectFit;
            _leftYearButton.ContentEdgeInsets = new UIEdgeInsets(5, 10, 5, 10);
            _leftYearButton.SetImage(LeftArrow, UIControlState.Normal);
            AddSubview(_leftYearButton);

            #if __UNIFIED__

            _leftYearButton.Frame = new CoreGraphics.CGRect(HeaderBorderOffset + HeaderViewSize.Width / 2 + 30, HeaderMiddleY + HeaderElementSize / 4 + 3 - 5, HeaderElementSize / 3 + 12, HeaderElementSize / 3 + 5);

            #else

            _leftYearButton.Frame = new CGRect(HeaderBorderOffset+HeaderViewSize.Width/2+30, HeaderMiddleY+HeaderElementSize/4+3-5, HeaderElementSize/3+12, HeaderElementSize/3+5);

            #endif

            _rightYearButton = UIButton.FromType(UIButtonType.Custom);
            _rightYearButton.TouchUpInside += HandleNextYearTouch;
            _rightYearButton.ContentMode = UIViewContentMode.ScaleAspectFit;
            _rightYearButton.ContentEdgeInsets = new UIEdgeInsets(5, 10, 5, 10);
            _rightYearButton.SetImage(RightArrow, UIControlState.Normal);
            AddSubview(_rightYearButton);

            #if __UNIFIED__

            _rightYearButton.Frame = new CoreGraphics.CGRect(HeaderViewSize.Width - HeaderElementSize / 2 - HeaderBorderOffset - 30, HeaderMiddleY + HeaderElementSize / 4 + 3 - 5, HeaderElementSize / 3 + 12, HeaderElementSize / 3 + 5);

            #else

            _rightYearButton.Frame = new CGRect(HeaderViewSize.Width - HeaderElementSize/2 - HeaderBorderOffset-30, HeaderMiddleY+HeaderElementSize/4+3-5, HeaderElementSize/3+12, HeaderElementSize/3+5);

            #endif
        }


        private void HandlePreviousMonthTouch(object sender, EventArgs e)
        {
            MoveCalendarMonths(false, true, 1);
        }

        private void HandleNextMonthTouch(object sender, EventArgs e)
        {
            MoveCalendarMonths(true, true, 1);
        }

        private void HandlePreviousYearTouch(object sender, EventArgs e)
        {
            MoveCalendarMonths(false, true, 12);
        }

        private void HandleNextYearTouch(object sender, EventArgs e)
        {
            MoveCalendarMonths(true, true, 12);
        }

        /// <summary>
        /// Moves the calendar months.
        /// </summary>
        /// <param name="upwards">If set to <c>true</c> moves the month upwards.</param>
        /// <param name="animated">If set to <c>true</c> the transition will be animated.</param>
        public void MoveCalendarMonths(bool upwards, bool animated, int nrMonths)
        {
            if (_monthGridView == null)
                return;

            CurrentMonthYear = CurrentMonthYear.AddMonths(upwards ? nrMonths : -nrMonths);
            UserInteractionEnabled = false;

            // Dispatch event
            if (MonthChanged != null)
                MonthChanged(CurrentMonthYear);

            var gridToMove = CreateNewGrid(CurrentMonthYear);
            var pointsToMove = (upwards ? 0 + _monthGridView.Lines : 0 - _monthGridView.Lines) * DayCellHeight;

            if (upwards && gridToMove.weekdayOfFirst == 0)
                pointsToMove += DayCellHeight;
            if (!upwards && _monthGridView.weekdayOfFirst == 0)
                pointsToMove -= DayCellHeight;

            #if __UNIFIED__

            gridToMove.Frame = new CoreGraphics.CGRect(new CoreGraphics.CGPoint(pointsToMove, 0), gridToMove.Frame.Size);

            #else

            gridToMove.Frame = new CGRect(new CGPoint(pointsToMove, 0), (CGRect)gridToMove.Frame.Size);

            #endif

            _scrollView.AddSubview(gridToMove);

            if (animated)
            {
                UIView.BeginAnimations("changeMonth");
                UIView.SetAnimationDuration(0.4);
                UIView.SetAnimationDelay(0.1);
                UIView.SetAnimationCurve(UIViewAnimationCurve.EaseInOut);
            }

            _monthGridView.Alpha = 0;

            #if __UNIFIED__

            _monthGridView.Center = new CoreGraphics.CGPoint(_monthGridView.Center.X - pointsToMove, _monthGridView.Center.Y);
            gridToMove.Center = new CoreGraphics.CGPoint(gridToMove.Center.X - pointsToMove, gridToMove.Center.Y);

            _scrollView.Frame = new CoreGraphics.CGRect(
                _scrollView.Frame.Location,
                new CoreGraphics.CGSize(_scrollView.Frame.Width, (gridToMove.Lines) * DayCellHeight));

            #else

            _monthGridView.Center = new CGPoint((CGPoint)_monthGridView.Center.X- pointsToMove, (CGPoint)_monthGridView.Center.Y );
            gridToMove.Center = new CGPoint((CGPoint)gridToMove.Center.X - pointsToMove, (CGPoint)gridToMove.Center.Y );

			_scrollView.Frame = new CGRect(
(CGRect)				_scrollView.Frame.Location,
				new CGSize((CGRect)_scrollView.Frame.Width, (gridToMove.Lines) * DayCellHeight));

            #endif

            _scrollView.ContentSize = _scrollView.Frame.Size;
            SetNeedsDisplay();

            if (animated)
                UIView.CommitAnimations();

            _monthGridView = gridToMove;

            UserInteractionEnabled = true;

            AdjustBackgroundColor();
        }

        /// <summary>
        /// Gos to the specified date date.
        /// </summary>
        /// <param name="targetDate">Target date.</param>
        public void GoToDate(DateTime targetDate)
        {
            if (_monthGridView == null)
                return;

            bool upwards = targetDate >= CurrentMonthYear;

            SelectedDate = targetDate.Date;
            CurrentMonthYear = new DateTime(targetDate.Year, targetDate.Month, 1);

            UserInteractionEnabled = false;

            // Dispatch event
            if (MonthChanged != null)
                MonthChanged(CurrentMonthYear);

            var gridToMove = CreateNewGrid(CurrentMonthYear);
            var pointsToMove = (upwards ? 0 + _monthGridView.Lines : 0 - _monthGridView.Lines) * DayCellHeight;

            if (upwards && gridToMove.weekdayOfFirst == 0)
                pointsToMove += DayCellHeight;
            if (!upwards && _monthGridView.weekdayOfFirst == 0)
                pointsToMove -= DayCellHeight;

            #if __UNIFIED__

            gridToMove.Frame = new CoreGraphics.CGRect(new CoreGraphics.CGPoint(0, pointsToMove), gridToMove.Frame.Size);

            #else

			gridToMove.Frame = new CGRect(new CGPoint(0, pointsToMove), (CGRect)gridToMove.Frame.Size);

            #endif

            _scrollView.AddSubview(gridToMove);

            #if __UNIFIED__

            _monthGridView.Center = new CoreGraphics.CGPoint(_monthGridView.Center.X, _monthGridView.Center.Y - pointsToMove);
            gridToMove.Center = new CoreGraphics.CGPoint(gridToMove.Center.X, gridToMove.Center.Y - pointsToMove);

            _scrollView.Frame = new CoreGraphics.CGRect(
                _scrollView.Frame.Location,
                new CoreGraphics.CGSize(_scrollView.Frame.Width, (gridToMove.Lines) * DayCellHeight));

            #else

			_monthGridView.Center = new CGPoint((CGPoint)_monthGridView.Center.X, (CGPoint)_monthGridView.Center.Y - pointsToMove);
			gridToMove.Center = new CGPoint((CGPoint)gridToMove.Center.X, (CGPoint)gridToMove.Center.Y - pointsToMove);

			_scrollView.Frame = new CGRect(
(CGRect)			_scrollView.Frame.Location,
            new CGSize((CGRect)_scrollView.Frame.Width, (gridToMove.Lines) * DayCellHeight));

            #endif

            _monthGridView.Alpha = 0;

            _scrollView.ContentSize = _scrollView.Frame.Size;
            SetNeedsDisplay();

            _monthGridView = gridToMove;

            UserInteractionEnabled = true;

            AdjustBackgroundColor();
        }

        private void AdjustBackgroundColor()
        {
            if (_scrollView != null)
                _scrollView.BackgroundColor = MonthBackgroundColor;
            BackgroundColor = MonthBackgroundColor;
        }

        private MonthGridView CreateNewGrid(DateTime date)
        {
            var grid = new MonthGridView(this, date);
            grid.CurrentDate = CurrentDate;
            grid.BuildGrid();
            grid.Frame = MainViewSize;
            return grid;
        }

        private void LoadInitialGrids()
        {
            _monthGridView = CreateNewGrid(CurrentMonthYear);

            var rect = (CGRect)_scrollView.Frame;

            //#if __UNIFIED__

            //rect.Size = new CoreGraphics.CGSize { Height = MainViewSize.Height - HeaderViewSize.Height, Width = MainViewSize.Width };

            //#else

			rect.Size =  new CGSize { Height = (float)(MainViewSize.Height - HeaderViewSize.Height), Width = (float)MainViewSize.Width };

            //#endif

            _scrollView.Frame = rect;
        }

        #if __UNIFIED__

        public override void Draw(CoreGraphics.CGRect rect)
        {
            if (TopBar != null)
                TopBar.Draw(new CoreGraphics.CGPoint(0, 10));

            DrawDayLabels(rect);
            DrawMonthLabel(rect);
            DrawYearLabel(rect);

        }

        private void DrawMonthLabel(CoreGraphics.CGRect rect)
        {
            var r = new CoreGraphics.CGRect(new CoreGraphics.CGPoint(0, (HeaderViewSize.Height / 2) - 13 - 5), new CoreGraphics.CGSize { Width = HeaderViewSize.Width / 2, Height = HeaderElementSize });
            UIColor.White.SetColor();
            CurrentMonthYear.ToString(MonthFormatString, new CultureInfo(NSLocale.CurrentLocale.LanguageCode))
                .DrawString(r, yearMonthFont, UILineBreakMode.WordWrap, UITextAlignment.Center);
        }

        private void DrawYearLabel(CoreGraphics.CGRect rect)
        {
            var r = new CoreGraphics.CGRect(new CoreGraphics.CGPoint(HeaderViewSize.Width / 2, (HeaderViewSize.Height / 2) - 13 - 5), new CoreGraphics.CGSize { Width = HeaderViewSize.Width / 2, Height = HeaderElementSize });
            UIColor.White.SetColor();
            CurrentMonthYear.ToString(YearFormatString, new CultureInfo(NSLocale.CurrentLocale.LanguageCode))
                .DrawString(r, yearMonthFont,
                UILineBreakMode.WordWrap, UITextAlignment.Center);
        }

        private void DrawDayLabels(CoreGraphics.CGRect rect)
        {
            var font = dayFont;
            UIColor.FromRGB(150, 150, 150).SetColor();
            var context = UIGraphics.GetCurrentContext();
            context.SaveState();
            var i = 0;

            var cultureInfo = new CultureInfo(NSLocale.CurrentLocale.LanguageCode);

            cultureInfo.DateTimeFormat.FirstDayOfWeek = DayOfWeek.Monday;
            var dayNames = cultureInfo.DateTimeFormat.DayNames;

            if (!SundayFirst)
            {
                // Shift Sunday to the end of the week
                var firstDay = dayNames[0];
                for (int count = 0; count < dayNames.Length - 1; count++)
                    dayNames[count] = dayNames[count + 1];
                dayNames[dayNames.Length - 1] = firstDay;
            }

            foreach (var d in dayNames)
            {
                d.Substring(0, 3).DrawString(new CoreGraphics.CGRect(i * DayCellWidth, HeaderViewSize.Height - DayNameHeight / 2 + 4 - 4, DayCellWidth, DayNameHeight), font, UILineBreakMode.WordWrap, UITextAlignment.Center);
                i++;
            }
            context.RestoreState();
        }

        #else
        
		public override void Draw(CGRect rect)
		{
			if(TopBar != null)
			TopBar.Draw((CGPoint)new CGPoint(0,10));


			DrawDayLabels((CGRect)rect);
			DrawMonthLabel((CGRect)rect);
            DrawYearLabel((CGRect)rect);
		}

		private void DrawMonthLabel(CGRect rect)
		{
        var r = new CGRect(new CGPoint(0, (HeaderViewSize.Height / 2) - 13-5), new CGSize { Width = HeaderViewSize.Width/2, Height = HeaderElementSize });
			UIColor.White.SetColor ();
			DrawString(CurrentMonthYear.ToString(MonthFormatString, new CultureInfo(NSLocale.CurrentLocale.LanguageCode)), 
        r, yearMonthFont,
			UILineBreakMode.WordWrap, UITextAlignment.Center);
		}

        private void DrawYearLabel(CGRect rect)
        {
            var r = new CGRect(new CGPoint(HeaderViewSize.Width/2, (HeaderViewSize.Height / 2) - 13-5), new CGSize { Width = HeaderViewSize.Width/2, Height = HeaderElementSize });
            UIColor.White.SetColor ();
            DrawString(CurrentMonthYear.ToString(YearFormatString, new CultureInfo(NSLocale.CurrentLocale.LanguageCode)), 
        r, yearMonthFont,
                UILineBreakMode.WordWrap, UITextAlignment.Center);
        }

		private void DrawDayLabels(CGRect rect)
		{
        var font = dayFont;
            UIColor.Black.SetColor ();
			var context = UIGraphics.GetCurrentContext();
			context.SaveState();
			var i = 0;

			var cultureInfo = new CultureInfo(NSLocale.CurrentLocale.LanguageCode);

			cultureInfo.DateTimeFormat.FirstDayOfWeek = DayOfWeek.Monday;
			var dayNames = cultureInfo.DateTimeFormat.DayNames;

			if (!SundayFirst) 
			{
			// Shift Sunday to the end of the week
			var firstDay = dayNames [0];
			for (int count = 0; count < dayNames.Length - 1; count++)
			dayNames [count] = dayNames [count + 1];
			dayNames [dayNames.Length - 1] = firstDay;
			}

			foreach (var d in dayNames)
			{
			DrawString(d.Substring(0, 3), new CGRect(i*DayCellWidth, HeaderViewSize.Height - DayNameHeight/2 +4-4, DayCellWidth, DayNameHeight), font,
			UILineBreakMode.WordWrap, UITextAlignment.Center);
			i++;
			}
			context.RestoreState();
		}

		#endif

    }
}